<?php
return [
    'dashboard' => 'Dashboard',
    'subscriptions' => 'Subscriptions',
    'users_management' => 'Users Management',
    'users' => 'Users',
    'roles' => 'Roles',
    'services' => 'Services',
    'platforms' => 'Platforms',
    'EServices' => 'E-Services'
];
