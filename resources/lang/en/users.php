<?php
return [
    'users' => 'Users',
    'all_users' => 'All Users',
    'user' => 'User',
    'index' => 'User Index',
    'create' => 'Create User',
    'update' => 'Update User',
    'delete' => 'Delete User',
    'show' => 'Show User',
    'name' => 'Name',
    'username' => 'Username',
    'phone' => 'Phone',
    'address' => 'Address',
    'active' => 'Active',
    'inactive' => 'Inactive',
    'email' => 'Email',
    'password' => 'Password',
    'password_confirmation' => 'Password Confirmation',
    'type' => 'User Type',
    'role' => 'Role',
    'writer' => 'Writer',
    'producer' => 'Producer',

];
