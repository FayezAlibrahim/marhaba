<?php

return [
    'create' => 'انشاء منتج',
    'info' => 'معلومات اساسية',
    'related_info' => 'معلومات ارتباط',
    'tags' => 'كلمات مفتاحية',
    'attributes' => 'معاملات المنتج',
    'images' => 'صور المنتج',
    'name' => 'اسم المنتج',
    'en_name' => 'اسم المنتج الاجنبي',
    // 'sku' => 'رقم sku',
    // 'made_in' => 'بلد المنشأ',
    'currency' => 'العملة',
    'description' => 'الوصف',
    'en_description' => 'الوصف الاجنبي',
    'unit_price' => 'السعر',
    'unit_discount_price' => 'سعر التخفيض',
    'wholese_price' => 'سعر الجملة',
    'wholelse_discount_price' => 'خصم سعر الجملة',
    // 'stock_status' => 'حالة المخزن',
    // 'stock_quantity' => 'عدد المتاح في المخزن',
    'status' => 'حالة المنتج',
    'categories' => 'الأصناف',
    'colors' => 'الوان المنتج',
    'sizes' => 'قياسات المنتج',
    'active' => 'مفعل',
    'inactive' => 'غير مفعل',
    'in_stock' => 'متاح في المخزن',
    'out_of_stock' => 'غير متاح في المخزن',
    'featured' => 'مميز',
    'products' => 'الوجبات',
    'variations' => 'افرع المنتج',
    'add_variation' => 'اضافة فرع جديد',
    'color' => 'اللون',
    'size' => 'القياس',
    'any_colors' => 'كل الالوان',
    'any_sizes' => 'كل القياسات',
    'variation_images' => 'صور الفرع',
    'edit' => 'تعديل المنتج',
    'types' => 'الانواع',
    'type' => 'النوع',
    'any_types' => 'اي نوع',
    'any' => 'اياً كان',
    'id' => 'الرقم التسلسلي',
    'in' => 'متاح في المخزن',
    'out' => 'غير متاح في المخزن',
    'IQD' => 'ليرة سورية',
    'SYP' => 'ليرة سورية',
    'no' => 'لا',
    'yes' => 'نعم',
    'edit_variation' => 'تعديل الفرع',
    'create_log' => 'انشاء منتج رقم',
    'update_log' => 'تعديل منتج رقم',
    'delete_log' => 'حذف منتج رقم',
    'stock_management' => 'ادارة المخزن',
    'clear_files' => 'الغاء الصور',
    'food_types'=>'أصناف المأكولات',
    'extras'=>'الإضافات'


];
