<?php
return [
    'salesReport' => 'تقرير الطلبات',
    'totalOrders' => 'عدد الطلبات الكلي',
    'totalOrdersCompleted' => 'عدد الطلبات المكتمل',
    'totalOrdersCanceled' => 'عدد الطلبات الملغى',
    'totalEarning' => 'اجمالي الايرادات',
    'salesReportDetails' => 'تفاصيل الطلبات',
    'itemsReportDetails' => 'تقرير المبيعات'
];
