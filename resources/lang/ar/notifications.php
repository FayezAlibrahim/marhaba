<?php
return [
    'we_receive_your_request_number' => ' تم تسجيل طلبك ',
    'delivered1'=>' الطلب ',
    'delivered2'=>' قيد التوصيل ',

    'received' => 'تم توصيل الطلب',
    'status_changed_to' => 'تم تغيير حالة الطلب الى',
    'new_order' => 'تم قبول الطلب',
    'update_order' => 'تغيير حالة الطلب',
    'canceled'=>'تم الغاء الطلب',
    'canceled1'=>' تم الغاء الطلب ',
    'canceled2'=>' بنجاح ',
    'on_hold'=>'تم تعليق الطلب',


];
