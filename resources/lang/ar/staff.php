<?php
return [
    'name'=> 'الاسم',
    'gender'=> 'الجنس',
    'nationality'=>'الجنسية' ,
    'male'=>'ذكر',
    'female'=>'أنثى',
    'age'=>'العمر',
    'passportnum'=>'رقم جواز السفر',
    'experiance'=>'الخبرة',
    'videos'=>'الفيديو',
    'images'=>'الصور',
    'attachment'=>'المرفقات',
    'yearsold'=>'سنة ',


];
