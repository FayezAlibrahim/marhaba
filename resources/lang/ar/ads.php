<?php
return [
    'id' => 'الرقم التسلسلي',
    'image' => 'صورة الاعلان',
    'title' => 'عنوان الاعلان',
    'type' => 'نوع الاعلان',
    'product' => 'اعلان عن منتج',
    'category' => 'اعلان عن صنف',
    'ads' => 'اعلان عام',
    'create' => 'انشاء اعلان',
    'item' => 'المعلن عنه',
    'offers' => 'عروض',
    'create_log' => 'انشاء اعلان رقم',
    'update_log' => 'تعديل اعلان رقم',
    'delete_log' => 'حذف اعلان رقم',
    'edit' => 'تعديل اعلان'
];
