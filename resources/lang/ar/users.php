<?php
return [
    'users' => 'المستخدمين',
    'edit' => 'تعديل المستخدمين',
    'id' => 'الرقم التسلسلي',
    'all_users' => 'كل المستخدمين',
    'user' => 'المستخدم',
    'index' => 'فهرس المستخدمين',
    'create' => 'اضافة مستخدم',
    'update' => 'تعديل مستخدم',
    'delete' => 'حذف مستخدم',
    'show' => 'اظهار مستخدم',
    'name' => 'الاسم الكامل',
    'username' => 'اسم المستخدم',
    'phone' => 'رقم الهاتف',
    'address' => 'العنوان',
    'active' => 'مفعل',
    'inactive' => 'غير مفعل',
    'email' => 'البريد الالكتروني',
    'created_at' => 'تاريخ الإنشاء',
    'settings' => 'الإعدادات',
    'password' => 'كلمة السر',
    'password_confirmation' => 'تأكيد كلمة السر',
    'type' => 'نوع المستخدم',
    'delivery_type' => 'نوع التوصيل',
    'role' => 'الدور',
    'writer' => 'كاتب',
    'city' => 'المدينة',
    'region' => 'المنطقة',
    'producer' => 'شركة',
    'admin' => 'مدير النظام',
    'client' => 'زبون',
    'vendor' => 'صاحب مطعم',
    'delivery' => 'عامل توصيل',
    'general' => 'توصيل من كل المطاعم',
    'specific' => 'توصيل من مطعم محدد ',
    'first_name' => 'الاسم الأول',
    'last_name' => 'اسم العائلة',
    'status' => 'الحالة',
    'users_log' => 'سجل العمليات',
    'activity_name' => 'اسم العملية',
    'create_log' => 'انشاء مستخدم',
    'update_log' => 'تعديل مستخدم', 
    'delete_log' => 'حذف مستخدم',
    'wallet' => 'المحفظة',

];
