<?php
return [
    'home'=>'الرئيسية',
    'allstaff'=>'جميع الموظفين ',
    'alljob'=>'جميع فرص العمل',
'about'=>'من نحن',
'services'=>'خدماتنا',
'staff'=>'الموظفين',
'jobvecancies'=>'فرص العمل',
'team'=>'مدراء الشركة',
'seeall'=>'مشاهدة الجميع',
'applynow'=>'تقدم الآن',
'contactus'=>'تواصل معنا',
'usefullinks'=>'روابط مفيدة',
'uploadyourcv'=>'أرفق سيرتك الذاتية هنا',
'phone'=>'الهاتف',
'email'=>'البريد الكتروني',
'address'=>'العنوان',
'callus'=>'اتصل بنا',
'seeless'=>'مشاهدة أقل',
'location'=>'موقعنا',
'company'=>'الشركة',
'followus'=>'تابعنا'


];
