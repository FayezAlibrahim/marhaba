<?php
return [
    'normal_writer' => 'صلاحيات كاتب عادي',
    'active_writer' => 'صلاحيات كاتب نشط',
    'committee_writer' => 'صلاحيات كاتب لجنة',
    'producer' => 'صلاحية شركة',
    'roles' => 'الادوار',
    'id' => 'الرقم التسلسلي',
    'name' => 'اسم الدور',
    'description' => 'وصف الدور',
    'edit' => 'تعديل الدور'
];
