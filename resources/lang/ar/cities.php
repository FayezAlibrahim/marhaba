<?php


return [
    'id' => 'الرقم التسلسلي',
    'name' => 'اسم المدينة',
    'en_name' => 'اسم المدينة الاجنبي',
    'create' => 'انشاء مدينة',
    'cities' => 'المدن',
    'edit' => 'تعديل المدينة',
    'create_log' => 'انشاء مدينة',
    'update_log' => 'تعديل مدينة',
    'delete_log' => 'حذف مدينة'
];
