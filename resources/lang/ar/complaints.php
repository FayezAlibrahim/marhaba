<?php
return [
    'title' => 'عنوان الشكوى',
    'content' => 'محتوى الشكوى',
    'mark_as_read' => 'تعيين كمقروء',
    'user' => 'صاحب الشكوى',
    'phone' => 'رقم صاحب الشكوى',
    'complaints' => 'الشكاوى',
    'id' => 'الرقم التسلسلي',
    'new_complaint' => 'شكوى جديدة'
];
