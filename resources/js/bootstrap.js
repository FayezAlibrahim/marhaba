window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

} catch (e) {}



require('../assets/vendor/bootstrap/js/bootstrap.bundle.min.js');
require('../assets/vendor/jquery-easing/jquery.easing.min.js');
require('../assets/client/js/sb-admin-2.min.js');
require('../assets/vendor/chart.js/Chart.min.js');
require('../assets/vendor/datatables/jquery.dataTables.min.js');
require('../assets/vendor/datatables/dataTables.bootstrap4.min.js');
require('../assets/vendor/select2/select2.min.js');
require('../assets/vendor/taginput/tagsinput.js');
require('../assets/vendor/sweetalert/sweetalert.min.js');
require('../assets/vendor/tilt/tilt.jquery.min.js');
require('../assets/vendor/datepicker/js/bootstrap-datepicker.min.js');
//require('../assets/vendor/moment/moment.min.js');
require('../assets/vendor/datepicker/bootstrap-datepicker.min.js');
require('../assets/client/js/custom.js');
//  require('../assets/writable/js/demo/chart-area-demo.js');
//require('../assets/writable/js/demo/chart-pie-demo.js');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });
