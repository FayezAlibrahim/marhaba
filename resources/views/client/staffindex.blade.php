@extends('client.index')

@section('content')
  <main id="main" data-aos="fade-up">
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container">
            <ol>
                <li><a href="/">@lang('navbar.home')</a></li>
                <li>@lang('navbar.allstaff')</li>
              </ol>
              <h2>@lang('navbar.staff')</h2>
        </div>
      </section><!-- End Breadcrumbs -->

      <section id="team" class="team section-bg">
        <div class="container" data-aos="fade-up">
            <div class="section-header">
                <h3>@lang('navbar.staff')</h3>
            </div>

            <div class="row">
                @foreach ($staff as $st)
                    <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="100">
                        <a href="/staff/profile/{{ $st->id }}">
                        <div class="member">
                            <img src="{{ $st->image ? $st->image : '/uploads/media/users/default.jpg' }}" class="img-fluid" alt="" style="height: 300px">
                            <div class="member-info">
                                <div class="member-info-content">
                                <h4>{{ $st->name }}</h4>
                                <span>@lang('staff.nationality'): {{ $st->nationality }} </span>
                                <span>@lang('staff.age'): {{ $st->age }} @lang('staff.yearsold')</span>
                                <span>@lang('staff.gender'): @if($st->gender=="female") @lang('staff.female')@else @lang('staff.male') @endif </span>

                                </div>
                            </div>
                        </div>
                    </a>
                    </div>

                @endforeach
            </div>
            {{ $staff->links() }}

        </div>
    </section><!-- End Team Section -->

    {{--  <section id="blog" class="blog">
        <div class="row">
        <div class="col-md-8">

            <header class="section-header" style="margin-top: 50px">
                <p>Our Staff</p>
            </header>
        </div>
     <div class="col-md-4">
          <div class="sidebar">
            <div class="sidebar-item search-form">
              <form action="">
                <input type="text">
                <button type="submit"><i class="bi bi-search"></i></button>
              </form>
            </div><!-- End sidebar search formn-->
          </div>
        </div>
    </div>
        </section>  --}}
    {{--  <section id="testimonials" class="testimonials" style="padding: 0px">

        <div class="container" data-aos="fade-up">
            <div class="testimonials-slider swiper-container" data-aos="fade-up" data-aos-delay="200">
                <div class="row">
                    @foreach ($staff as $st)
                             <div class="col-lg-4 col-md-6 aos-init aos-animate" data-aos="zoom-in" data-aos-delay="100">
                                <a href="/staff/profile/{{ $st->id }}">
                                <div class="testimonial-item">
                                <div class="stars">
                                    @for ($i = 0; $i <  5;  $i++)
                                    <i class="bi bi-star-fill"></i>
                                    @endfor

                                </div>
                                <div class="profile">
                                    <img src="{{ $st->image ? $st->image : '/uploads/media/users/default.jpg' }}"
                                        class="testimonial-img" alt="" style="width: 130px">
                                    <h3>{{ $st->name }}</h3>
                                    <h4>@lang('staff.passportnum'): {{ $st->passport_number }} </h4>
                                    <h4>@lang('staff.nationality'): {{ $st->nationality }} </h4>
                                    <h4>@lang('staff.age'): {{ $st->age }} @lang('staff.yearsold')</h4>
                                    <h4>@lang('staff.gender'): @if($st->gender=="female") @lang('staff.female')@else @lang('staff.male') @endif </h4>
                                </div>

                            </div>
                    </a>

                        </div><!-- End testimonial item -->


                        @endforeach
                </div>

                </div>
                {{ $staff->links() }}

            </div>


        </div>

    </section>  --}}
  </main><!-- End #main -->
  @endsection
