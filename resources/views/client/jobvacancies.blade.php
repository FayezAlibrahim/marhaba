
@extends('client.index')

@section('content')
  <main id="main" data-aos="fade-up">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs" style="margin-top: 120px">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <h2>@lang('navbar.jobvecancies')</h2>
          <ol>
            <li><a href="/">@lang('navbar.home')</a></li>
            <li>@lang('navbar.jobvecancies')</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

    {{--  <section id="blog" class="blog">
        <div class="row">
        <div class="col-md-4">
        </div>
     <div class="col-md-4">
               <div class="sidebar">

            <h3 class="sidebar-title">Search</h3>
            <div class="sidebar-item search-form">
              <form action="">
                <input type="text">
                <button type="submit"><i class="bi bi-search"></i></button>
              </form>
            </div><!-- End sidebar search formn-->
          </div>
        </div>
    </div>
        </section>  --}}
    <section id="testimonials" class="testimonials">

        <div class="container" data-aos="fade-up">


            <div class="testimonials-slider swiper-container" data-aos="fade-up" data-aos-delay="200">
                <div class="row">

                    @foreach ($jobvacanies as $job)
                    <div class="col-lg-4 col-md-6 aos-init aos-animate" data-aos="zoom-in" data-aos-delay="100">
                        <div class="box" data-aos="fade-up" data-aos-delay="200">
                            <img src="{{ $job->image }}" class="img-fluid" style="width: 200px;  height: 200px;"
                                alt="">
                            <h3>{{ $job->title }}</h3>
                            <p>{{ $job->description }}</p>

                            <a style="float: @if(app()->getlocale()=='ar') left @else right @endif;;" href="#" onclick="$('#jobid').val({{$job->id}})" data-toggle="modal" data-target="#exampleModalLong"
                                class="readmore mt-auto"><span>@lang('navbar.applynow')</span><i class="bi @if(app()->getlocale()=='ar') bi-arrow-left @else bi-arrow-right @endif;"></i></a>
                        </div>
                    </div>
                </div><!-- End testimonial item -->
                @endforeach
                </div>

                </div>
                {{ $jobvacanies ->links() }}

            </div>


        </div>

    </section>

  </main><!-- End #main -->
  <div class="modal fade" id="exampleModalLong" data-backdrop="static" tabindex="-1" role="dialog"aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="/cv/upload" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">@lang('navbar.jobvecancies')</h5>
                    <button type="button" class="btn" data-dismiss="modal" aria-label="Close">
                        X
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-xl-4 col-lg-6 text-right col-form-label" data-required="yes">@lang('navbar.uploadyourcv')</label>
                        <input type="hidden" id="jobid" name="id">
                        <input type="file" name="cv" required class="form-control" accept=".pdf">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold"
                        data-dismiss="modal">@lang('admin.close')</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">@lang('admin.save')</button>
                </div>
            </div>
        </form>

    </div>
</div>
  @endsection
  <script src="/global/plugins.bundle.js"></script>

