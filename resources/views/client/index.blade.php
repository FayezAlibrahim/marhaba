<!DOCTYPE html>
<html lang="{{app()->getLocale()}}" @if(\App::getlocale()=='ar') direction="rtl" style="direction: rtl;" @endif>
    <head>

        <!--====== Required meta tags ======-->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--====== Title ======-->
        <title>Marhaba</title>

        <!--====== Favicon Icon ======-->
        <link rel="shortcut icon" href="/assets/images/favicon.png" type="image/png">

        <!--====== Bootstrap css ======-->
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css">

        <!--====== Line Icons css ======-->
        <link rel="stylesheet" href="/assets/css/LineIcons.css">

        <!--====== Magnific Popup css ======-->
        <link rel="stylesheet" href="/assets/css/magnific-popup.css">

        <!--====== Default css ======-->
        <link rel="stylesheet" href="/assets/css/default.css">
        <link href="/assets/swiper/swiper-bundle.min.css" rel="stylesheet">

        <!--====== Style css ======-->
        <link rel="stylesheet" href="/assets/css/style.css">
        <style>
    #map {
  height: 200px;
  /* The height is 400 pixels */
  width: 100%;
  /* The width is the width of the web page */
}
    </style>
    @yield('css-links')

    @yield('styles')
</head>

<body>
    <header class="header-area">
        <div class="navgition navgition-transparent">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <nav class="navbar navbar-expand-lg">
                            <a class="navbar-brand" href="/" style="max-width: 109px;height: 21px;margin: 0px;padding: 0px;">
                                <img src="/uploads/media/logos/marhaba.png" alt="Logo">
                            </a>

                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarOne" aria-controls="navbarOne" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="toggler-icon"></span>
                                <span class="toggler-icon"></span>
                                <span class="toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse sub-menu-bar" id="navbarOne">
                                <ul class="navbar-nav m-auto">
                                    <li class="nav-item active">
                                        <a class="page-scroll" href="/#home">@lang('navbar.about')</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="page-scroll" href="/#testimonials">@lang('navbar.staff')</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="page-scroll" href="/#service">@lang('navbar.services')</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="page-scroll" href="/#pricing">@lang('navbar.jobvecancies')</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="page-scroll"  href="/#team">@lang('navbar.team')</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="page-scroll" href="/#contact">@lang('navbar.contactus')</a>
                                    </li>

                                </ul>
                            </div>

                            <div class="navbar-social d-none d-sm-flex align-items-center">
                                <span>@lang('navbar.followus')</span>
                                <ul>
                                    <li><a href="{{ $appinfo->facebook ?? '' }}" ><i class="lni-facebook-filled"></i></a></li>
                                    <a target="_blank" href="https://api.whatsapp.com/send/?phone={{$appinfo->twitter}}&text=Hi&app_absent=0" ><i style="font-size: 32px;color: #3f9d3f;" class="lni-whatsapp"></i></a>
                                    <li><a href="{{ $appinfo->instagram ?? '' }}"><i class="lni-instagram-original"></i></a></li>

                                    <li>
                                        {{--  <a href="#"><i class="lni-linkedin-original"></i></a>  --}}
                                    </li>
                                </ul>
                                @if (\App::getlocale() == 'ar')
                                <span><a href="/setlocale/en"><span  style="font-weight: bold">العربية</span> <i class="bi bi-chevron-down"></i></a>
                            </span>
                            @else
                                <span ><a href="/setlocale/ar"><span style="font-weight: bold">English</span> <i class="bi bi-chevron-down"></i></a>
                        </span>
                            @endif
                            </div>
                        </nav> <!-- navbar -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- navgition -->


    </header>

    @yield('content')
    <footer id="footer" class="footer-area" style="background-color: #f4f6f7 ">
        <div class="footer-widget"  @if (\App::getlocale() == 'ar') style="text-align: right;" @endif>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer-logo-support d-md-flex align-items-end justify-content-between">
                            <div class="footer-logo d-flex align-items-end">
                                <a class="mt-30" href="/" style="height: 35px;width: 143px;x;"><img src="/uploads/media/logos/marhaba.png" alt="Logo"></a>

                                <ul class="social mt-30">
                                    <li><a href="{{ $appinfo->facebook ?? '' }}" ><i class="lni-facebook-filled"></i></a></li>
                                    <a target="_blank" href="https://api.whatsapp.com/send/?phone={{$appinfo->twitter}}&text=Hi&app_absent=0" ><i style="font-size: 25px;color: #3f9d3f;" class="lni-whatsapp"></i></a>
                                    <li><a href="{{ $appinfo->instagram ?? '' }}" ><i class="lni-instagram-original"></i></a></li>
                                  
                                </ul>
                            </div> <!-- footer logo -->

                        </div> <!-- footer logo support -->
                    </div>
                </div> <!-- row -->
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="footer-link">
                            <h6 class="footer-title">@lang('navbar.company')</h6>
                            <ul>
                                <li><a href="/#home">@lang('navbar.about')</a></li>
                                <li><a href="/#contactus">@lang('navbar.contactus')</a></li>
                                <li><a href="/#team">@lang('navbar.team')</a></li>
                                <li><a href="/#pricing">@lang('navbar.jobvecancies')</a></li>
                                <li><a href="/#service">@lang('navbar.services')</a></li>
                                <li><a href="/#testimonials">@lang('navbar.staff')</a></li>
                            </ul>
                        </div> <!-- footer link -->
                    </div>
          
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="footer-link">
                            <h6 class="footer-title">Call us</h6>
                            <ul>
                                <li><a >{{App\models\Appinfo::first()->call_center_number}}</a></li>
                                <li><a >{{App\models\Appinfo::first()->phone}}</a></li>
                                <li><a >{{App\models\Appinfo::first()->email_info}}</a></li>
                                <li><a>{{App\models\Appinfo::first()->email_contact}}</a></li>

                            </ul>
                        </div> <!-- footer link -->
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-7">
                        <div class="footer-newsletter">
<h6 class="footer-title">@lang('navbar.location')</h6>
                                    <div id="map"></div>

                        </div> <!-- footer newsletter -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- footer widget -->
    </footer>

    <a class="back-to-top" href="#"><i class="lni-chevron-up"></i></a>


            <!--====== jquery js ======-->
            <script src="/assets/js/vendor/modernizr-3.6.0.min.js"></script>
            <script src="/assets/js/vendor/jquery-1.12.4.min.js"></script>

            <!--====== Bootstrap js ======-->
            <script src="/assets/js/bootstrap.min.js"></script>
            <script src="/assets/js/popper.min.js"></script>

            <!--====== Scrolling Nav js ======-->
            <script src="/assets/js/jquery.easing.min.js"></script>
            <script src="/assets/js/scrolling-nav.js"></script>
            <script src="/assets/swiper/swiper-bundle.min.js"></script>

            <!--====== Magnific Popup js ======-->
            <script src="/assets/js/jquery.magnific-popup.min.js"></script>

            <!--====== Main js ======-->
            <script src="/assets/js/main.js"></script>

    <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoNx0lZSVmTUatcvCJsQB8_DOwmhjRup4&callback=initMap&libraries=&v=weekly"
      async
    ></script>
    <script>
// Initialize and add the map
function initMap() {
  // The location of Uluru  25.410893916668318, 55.443948303918894
  const uluru = { lat: 25.410893916668318, lng: 55.443948303918894 };
  // The map, centered at Uluru
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 9,
    center: uluru,
  });
  // The marker, positioned at Uluru
  const marker = new google.maps.Marker({
    position: uluru,
    map: map,
  });
}
    </script>
</body>

</html>
