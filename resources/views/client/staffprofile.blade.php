@extends('client.index')
@section('styles')
    <style type="text/css">
        #over {
            position: relative;
            width: 50%;
          }

          .image {
            opacity: 1;
            display: block;
            width: 100%;
            height: auto;
            transition: .5s ease;
            backface-visibility: hidden;
          }

          .middle {
            transition: .5s ease;
            opacity: 0;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            text-align: center;
          }

          #over:hover .video {
            opacity: 0.3;
          }

          #over:hover .middle {
            opacity: 1;
          }

          .text {
            background-color: #4CAF50;
            color: white;
            font-size: 16px;
            padding: 16px 32px;
          }
        .timeline {
            border-left: 3px solid #727cf5;
            border-bottom-right-radius: 4px;
            border-top-right-radius: 4px;
            letter-spacing: 0.2px;
            position: relative;
            line-height: 1.4em;
            font-size: 1.03em;
            padding: 50px;
            list-style: none;
            text-align: left;
            max-width: 60%;
        }

        @media (max-width: 767px) {
            .timeline {
                max-width: 98%;
                padding: 25px;
            }
        }

        .timeline h1 {
            font-weight: 300;
            font-size: 1.4em;
        }

        .timeline h2,
        .timeline h3 {
            font-weight: 600;
            font-size: 1rem;
            margin-bottom: 10px;
        }

        .timeline .event {
            border-bottom: 1px dashed #e8ebf1;
            padding-bottom: 25px;
            margin-bottom: 25px;
            position: relative;
        }

        @media (max-width: 767px) {
            .timeline .event {
                padding-top: 30px;
            }
        }

        .timeline .event:last-of-type {
            padding-bottom: 0;
            margin-bottom: 0;
            border: none;
        }

        .timeline .event:before,
        .timeline .event:after {
            position: absolute;
            display: block;
            top: 0;
        }

        .timeline .event:before {
            left: -207px;
            content: attr(data-date);
            text-align: right;
            font-weight: 100;
            font-size: 0.9em;
            min-width: 120px;
        }

        @media (max-width: 767px) {
            .timeline .event:before {
                left: 0px;
                text-align: left;
            }
        }

        .timeline .event:after {
            -webkit-box-shadow: 0 0 0 3px #727cf5;
            box-shadow: 0 0 0 3px #727cf5;
            left: -55.8px;
            background: #fff;
            border-radius: 50%;
            height: 9px;
            width: 9px;
            content: "";
            top: 5px;
        }

        @media (max-width: 767px) {
            .timeline .event:after {
                left: -31.8px;
            }
        }

        .rtl .timeline {
            border-left: 0;
            text-align: right;
            border-bottom-right-radius: 0;
            border-top-right-radius: 0;
            border-bottom-left-radius: 4px;
            border-top-left-radius: 4px;
            border-right: 3px solid #727cf5;
        }

        .rtl .timeline .event::before {
            left: 0;
            right: -170px;
        }

        .rtl .timeline .event::after {
            left: 0;
            right: -55.8px;
        }
        .pulsate {
            -webkit-animation: pulsate 3s ease-out;
            -webkit-animation-iteration-count: infinite;
            opacity: 0.5;
        }

        @-webkit-keyframes pulsate {
            0% {
                opacity: 0.5;
            }

            50% {
                opacity: 1.0;
            }

            100% {
                opacity: 0.5;
            }
        }

        /* ===================== */
        /* ==   FontAwesome   == */
        /* ===================== */
        @font-face {
            font-family: 'FontAwesome';
            src: url(https://netdna.bootstrapcdn.com/font-awesome/2.0/font//fontawesome-webfont.eot?#iefix) format('eot'),
                url(https://netdna.bootstrapcdn.com/font-awesome/2.0/font//fontawesome-webfont.woff) format('woff'),
                url(https://netdna.bootstrapcdn.com/font-awesome/2.0/font//fontawesome-webfont.ttf) format('truetype'),
                url(https://netdna.bootstrapcdn.com/font-awesome/2.0/font//fontawesome-webfont.svg#FontAwesome) format('svg');
            font-weight: normal;
            font-style: normal;
        }

        div.rfWrapper {
            display: inline-block;
            vertical-align: top;
            height: 36px;
        }

        /* ======================= */
        /* == CSS RATINGS FIELD == */
        /* ======================= */
        div.rfWrapper div.rfArea {
            display: block;
            direction: rtl;
            height: 100%;
            font-size: 0px;
            text-align: center;
            border-style: none;
            cursor: default;
            padding: 0px;

            /* BOX SIZING */
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
        }

        div.rfWrapper div.rfArea>input[type="radio"] {
            display: inline-block;
            width: 36px;
            height: 36px;
            position: absolute;
            top: -32px;
            clip: rect(0, 0, 0, 0);
        }

        div.rfWrapper div.rfArea>label {
            display: inline-block;
            width: 36px;
            height: 36px;
            font-size: 24px;
            line-height: 40px;
            text-align: center;
            cursor: pointer;
            position: relative;
            overflow: hidden;
            text-indent: 100%;
        }

        div.rfWrapper div.rfArea>label:before {
            font-family: 'FontAwesome';
            content: '\f006';

            position: absolute;
            top: 0px;
            left: 0px;

            width: 100%;
            height: 100%;

            line-height: inherit;
            text-indent: 0;
            color: #555555;

            cursor: inherit;

            padding-top: 0px;

            /* CSS3 TEXT-SHADOW */
            text-shadow: 0px 3px 0px rgba(0, 0, 0, 0.2);

            /* BOX-SIZING */
            box-sizing;
            border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
        }

        div.rfWrapper div.rfArea>input[type="radio"]:checked~label:before {
            content: '\f005';
        }

        div.rfWrapper div.rfArea>label:hover:before,
        div.rfWrapper div.rfArea>label:hover~label:before {
            color: #FFFFFF;
        }

        div.rfWrapper div.rfArea>input[type="radio"]:checked~label:before {
            color: #F9A825;
        }

        div.rfWrapper div.rfArea>label:active {
            position: relative;
            top: 2px;
        }

        div.rfWrapper div.rfArea>label:active:before {
            content: '\f005';

            /* CSS3 TEXT-SHADOW */
            text-shadow: 0px 1px 0px rgba(0, 0, 0, 0.2);
        }

        .crop {
            width: 100%;
            height: 400px;
            overflow: hidden;
        }

        .crop img {
            width: 100%;
            / / height: 500 px;
            margin: -150px 0 0 0;
        }

        .emp-profile {
            padding: 3%;
            margin-top: 3%;
            margin-bottom: 3%;
            border-radius: 0.5rem;
            background: #fff;
        }

        .profile-img {
            text-align: center;
        }

        .profile-img img {
            width: 70%;
            height: 100%;
        }

        .profile-img .file {
            position: relative;
            overflow: hidden;
            margin-top: -20%;
            width: 70%;
            border: none;
            border-radius: 0;
            font-size: 15px;
            background: #212529b8;
        }

        .profile-img .file input {
            position: absolute;
            opacity: 0;
            right: 0;
            top: 0;
        }

        .profile-head h5 {
            color: #333;
        }

        .profile-head h6 {
            color: #4F534C;
        }

        .profile-edit-btn {
            border: none;
            border-radius: 1.5rem;
            width: 70%;
            padding: 2%;
            font-weight: 600;
            color: #6c757d;
            cursor: pointer;
        }

        .proile-rating {
            font-size: 12px;
            color: #818182;
            margin-top: 5%;
        }

        .proile-rating span {
            color: #495057;
            font-size: 15px;
            font-weight: 600;
        }

        .profile-head .nav-pills {
            margin-bottom: 5%;
        }

        .profile-head .nav-pills .nav-link {
            font-weight: 600;
            border: none;
        }

        .profile-head .nav-pills .nav-link.active {
            border: none;
            border-bottom: 2px solid blue;
        }

        .profile-work {
            padding: 0 14% 14% 14%;
            margin-top: -30px;
        }

        .profile-work p {
            font-size: 12px;
            color: #818182;
            font-weight: 600;
            margin-top: 10%;
        }

        .profile-work a {
            text-decoration: none;
            color: #495057;
            font-weight: 600;
            font-size: 14px;
        }

        .profile-work ul {
            list-style: none;
        }

        .profile-tab label {
            font-weight: 600;
        }

        .profile-tab p {
            font-weight: 600;
            color: #4F534C;
        }


        .img-wrapper {
            position: relative;
            margin-top: 15px;
        }

        .img-wrapper img {
            width: 100%;
        }

        .img-overlay {
            background: rgba(0, 0, 0, 0.7);
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            opacity: 0;
        }

        .img-overlay i {
            color: #fff;
            font-size: 3em;
        }


        #overlay {
            background: rgba(0, 0, 0, 0.7);
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            z-index: 999;
            Removes blue highlight -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        #overlay img {
            margin: 0;
            width: 80%;
            height: auto;
            object-fit: contain;
            padding: 5%;
        }

        #nextButton {
            color: #fff;
            font-size: 2em;
            transition: opacity 0.8s;
        }

        #nextButton:hover {
            opacity: 0.7;
            font-size: 3em;
        }

        #prevButton {
            color: #fff;
            font-size: 2em;
            transition: opacity 0.8s;
        }

        #prevButton:hover {
            opacity: 0.7;
            font-size: 3em;
        }

        #exitButton {
            color: #fff;
            font-size: 2em;
            transition: opacity 0.8s;
            position: absolute;
            top: 15px;
            right: 15px;
        }

        #exitButton:hover {
            opacity: 0.7;
            font-size: 3em;
        }

        .nav-pills .nav-link.active,
        .nav-pills .show>.nav-link {
            background-color: white;
            color: blue;

        }
        .playpause {
            background-image:url(/assets/images/playbutton.png);
            background-repeat:no-repeat;
            width:20%;
            height:20%;
            position:absolute;
            left:0%;
            right:0%;
            top:0%;
            bottom:0%;
            margin:auto;
            background-size:contain;
            background-position: center;
        }
    </style>
@endsection
@section('content')
    <main id="main" data-aos="fade-up">

        <section id="breadcrumbs" class="breadcrumbs">
            <div class="container">
                <ol>
                    <li><a href="/">@lang('navbar.home')</a></li>
                    <li><a href="/staff/all">@lang('navbar.allstaff')</a></li>
                    <li>{{ $staff->name }}</li>
                  </ol>
                  <h2>{{ $staff->name }}</h2>
            </div>
          </section><!-- End Breadcrumbs -->
          <!-- ======= Portfolio Details Section ======= -->
          <section id="portfolio-details" class="portfolio-details">
            <div class="container">

              <div class="row gy-4">

                <div class="col-lg-8">
                  <div class="portfolio-details-slider swiper-container">
                    <div class="swiper-wrapper align-items-center">

                      <div class="swiper-slide">
                        <img src=" {{ $staff->image ?? '/uploads/media/users/blank.png' }} " alt="">
                      </div>
                      @foreach ($staff->images as $img)
                      <div class="swiper-slide">
                        <img src="{{ $img->path }}" alt="{{ $img->name }}">
                      </div>
@endforeach
@foreach ($staff->videos as $media)
<div class="swiper-slide">
    <div class="embed-responsive embed-responsive-1by1">
        <div class="wrapp">
        <a class=" video-popup" href="{{ $media->path }}">
            <video width="320" height="240" >
                <source src="{{ $media->path }}"
                    type="video/mp4" />
            </video>
        <div class="playpause"></div>

        </a>

</div>


    </div>
  </div>
@endforeach


                    </div>
                    <div class="swiper-pagination"></div>
                  </div>
                </div>

                <div class="col-lg-4">
                  <div class="portfolio-info">
                    <h3 style="
                    color: #0067f4;
                ">Personal information</h3>
                    <ul>
                      <li><strong>Name</strong>:  {{ $staff->name }}</li>
                      <li><strong>Nationality</strong>:   {{ $staff->nationality }}</li>
                      <li><strong>Passport Number</strong>:    {{ $staff->passport_number }}</li>
                      <li><strong>Gender</strong>: @if($staff->gender=="female") @lang('staff.female')@else @lang('staff.male') @endif</li>
                    </ul>
                  </div>
                  <div class="portfolio-description">
                      @if(count($staff->experience))
                    <h2 style="
                    color: #0067f4;
                ">@lang('staff.experiance')</h2>
                    @endif
<p>
    <ul>
        @foreach ($staff->experience as $exp)

        <li class="event" data-date="">
            <h6><i class="lni-check-mark-circle"></i> {{ $exp->title }}</h6>
            <span
class="font-weight-normal font-size-sm  text-muted pl-3">Total Experiances:
                {{ explode('.', $exp->total/12)[0] }} Years &  {{ $exp->total-explode('.', $exp->total/12)[0]*12 }} Months  <br></span>
                <span
                class="font-weight-normal font-size-lg  text-muted pl-3">{{ $exp->from_date ?? '-' }}
                    / {{ $exp->to_date ?? '-' }}</span>
        </li>
    @endforeach


    </ul>
</p>
@if(count($staff->pdfs))
<h2 style="margin-top:2px ; color: #0067f4">
    @lang('staff.attachment')</h2>
    @endif
<div class="row">
@foreach ($staff->pdfs as $pdf)
<div class="col-12" id="p{{ $pdf->id }}">
        <a href="{{ $pdf->path }}" target="_blanck"
            style="max-width: 60px; max-height: 60px; margin-top:2px">
            <img style="max-width: 30px;"
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMsAAAD5CAMAAAC+lzGnAAAAzFBMVEX19fX/IRb///8sLCz1+/v/CwD6m5n6oJ317u38dHD/ZF3/Ggz/AAD1+Pj6+vooKCgeHh7a2toZGRkzMzOurq70//9DQ0PQ0NAkJCRPT0/AwMD/GAkTExNra2v24+L23dyfn5/3zcv5r6z+PTX+LyVjY2NYWFj8Z2H6j4v/7ez24N/21NL9WFL5qKX4u7j9U0z4wb/p6emEhIT+Myr9SkP7eHP4v737g375rKn8bGf7h4T/zcv6k4/+Qjr9S0QHBweRkZGGhoanp6d3d3fUHSfIAAAOzElEQVR4nO2da1visNaG09JAIQ2KWmWotgJVDi0FNwJy3LPH//+f3rRNegBUENLDe/F8mEunWHJ3rWStHAvEHfWMj1llPRZSkLmZd51+b7dMBwls/S47KxspEKVBQoQ0qJiV7ugMLL3SWIBaShyBNCi8L6snsjhjJS2DbAkptnM0TYSlN9MyQuJKQ+Vj603IsoQw7fLHBWHzlywfQoaM4guZpV+xrOJNlwbTUdzLEZz9gqUV9S+NNIuLYgpalDvteDyAkyNaAJ9laYY3gIq9NHQgpSAgy/1mtx2tuHB+JEsvfBaaObNUjEFawlhSjZkZxjhlLh/FMgkeBKw0UwQJeKbr0DSwfAxLK3gKaCKlTuIKy5OwWYWdA+sMYem1gz8rpg3BhLETgZkfFjUJS0lhfzNLGyEiPA0rMawcyNIb07/R7LTLH5M0GkdgDrEMEFuIRdllJupKINxUIjAH1BlQZY0YXElpl35L0jJiGftny4A+q/manC2zANcy5jFuBiyFZQtq2kXfldSMtGbDn9wMrJiLWZkzC3Bh2hE3039g6fiBEq37WWQB2Ar77HD4fToD2szF5LSLvV+StQ5hNt/WGcA+tsqkWYBrmXHEMv1DWEpZa5EDSVGY928skwMWIBmbiJtZuWYhMAI8wDK5YAHYGIaWaX9lmXywEMusI5b5AiYnLPE60zZyzQKkvh1aZr13FDA3LADrbfi9ZfLDQmA6AYw2XuaahcCEbqa1d2HyxLJlmZ06kysWAlNWQpjt2bN8sQAsV0IY08o1izsKGLgZEqa5ZiEwoZuhuJvljiVuGXOaaxYCMwstY7ZyzRJ3MzTNNQvAoBu6mTbINQuBibiZ0Mo1C3GziGVQK9csxDKrSAOwTIIFc5sxxGARyc34s2CsG4bOCQeDUtifmVc5s0jWwn5/r8wMTqPuuBu6mcOZZYCghpAGhS6v6ZAiGzhH4x5PFuwQEOguBnPn6DnBBA2AsuDIgvWNhkynb7jZE1yc9+bBl8isc4ZMgx+L1FJQ25IwJj+Qx9bi0+RjY03dDBY5sswh7HqepZKvQ2Ne9X9ADaN1etxYVBOZ/lSbVHSrzIhTjVGD+VmDHwtE7/4t8Uhx10FwMoz3pLzav+TFgvsQTiiLO72rVTjNu2GLTbiWuLFYpLowFpO0M+0mp/Bv0NlZOOPIwtoubHnfwKnCYH3os2hzniwOZWm6AQ1NOVV+mc2EV7iy+KX3WXhFGCCXYQIsA2oXJ+csQIZw4d/SbzXzzKJCWPZDijTJPYug2T6L6jWauWZ5h23K4n0XdHi1Y/xZSG5pesuFsOH3Mbi1yQmwdKHghXrJX19v8kouE2DBUwV9uPeUvBVq3HKYRFhYQobnblxGQz3HLEbbW5OGjY3bjHHLk5NgAfIcbgwM8NL07j/hNS6aBAsJ95DUEexXfVjMNUsLKe5NZ95XcesjJ8JCKow2VoHs98cVbss5k2ABkq0pOouUArfVz8mwDBRYUv2hBTjnNiWSCAvWFdhRba/Xx/qYHJQICwA2fHf8kQXEb1V6Mixuc7zxogu/qJ8Yi8UW48MFv5XcCfkYLrNR+AG/2dCEWCS2ndbklSSDxFgApl+z5rhZIDEWevMZxwn3pOrL1P8ahVdf31XCdR/ya5GTbpPRkOc+oYRY6Pwb3/20ybDIG7YN1ci7jwXpvsBGY7komZyfrYskvTGODVki/Urdn3pHdgtqNr9NtQn19/3g0pI7kN/QRTJjSv53oLEhWSZSuHVgEhnrY/VeBlJR0So5HreU6LI7zUv3y5BbkEnCLv52dWS69sD9dw1y6vLzZ5EcapaOF1qkkYLafCImfxZ5TlNkOl6pTiDkMzzOnQU3Tf8LxrT8WB9C2OUR/vnbhS6FhgPmV17D3OLgZdxZZIGaJezoSx/uQr/zw/Bm8dYlunedR8vehdr7+XMZ7nah2X5spgKT9gCef0UsZxZp5Nd8QYsVHOuCppVyxsJ2QW1nlNhaa/Dc6y/4stDFb4Jgbld1PEKaYEW+CRNFDob7zdJ/viwqa5AnO5ekAYRDwyPAQJZlXR85xW63u5iTf1atpi7Lx/LwZZHZKtt9GVgXwbnet6bOR7czHCtE4SmK5BdzXlzKRxWFK4tEh19Qe+uCaw0VdDRoD9sCZBBKIOhtZVBMuwiOKAxPFqz7U2GCUopVDAno1mhlkwojeJsv2sNKebH6aBmW6qpvWYNuZ+yWS1OEj4ywOOwwB5YXu+bQneLENt1HT0whoOFyafVlNVLfvUYANLvemWMIHn7aHlcfe0fRmo+xrDcXG1PwHIpYYz4oCZow0/eecojByB/xgMVDmwCOLHhJR8XMkQSwJDcHZaSQigAVuJmUHENSJdVpQ9h2wN7SSk2a/hw6ZcORRaUdF21Oml1rYY99txqXHUuXqUPh5hpqwmS/aVR/7TScH9hB4MeCWfoCneZMIAaBimDPPnQ1FggluUMsBUvGHhqal6LxgYMdHO0S7BS0NUgaWGX4sdSlnSJjuUgsA9dFXd2+xHLsQ49C5MbCRpJcJ4Na227pX2yxxLg5dPeTmRNjKzSqM/o0DlygyY0lKAgJeZOBsWuQUJJcEkjrDGHZkSMfw3Qx0MHztZxYMMnqKYrd6ss/nC2LsVV2jxeGwmbhpWjYa8FnX2QNibJgQMrGYot6yCG5WG1OTOiGRmW8GpF2TtZH7AgY5dACcWDBqjUz2dKEg4fCMGjOkKIh19falU7HS3E8lIMHoM7OIslWGYWH0R4xREm8alBpkzZPQJpGjxtHUFgc3Jc+MwvGo4kWnqy9p9/yrUhMbS02UPHOTif3UcYz5/BznM/KQprXipttdRbUw4SjxyfcJLrfLM0m9mY+Ky71Y25wRhZS4+fkiaLJUqa5PvrViJ7fWSbJ/7E95bOxkBo/GUNNGY7CnvE82VM/z8WCjYVbT+wpeZZNahWT5yKLPToPC5Y/2oqm2FOShWC5sqc3mYTOwULCtk2iwnrk5VNsK23SHnYWFqyvSNu1KfnDDKwHhcaJH118Ootq2SQQzGjdwDo9S1P5SPxollNZMCA5LqxYLKKxhBCWkz/t90QWDIh/oVUQ0VR6uon2znMRzxc6jQXLC9KBt4KxBxycCs5xieiXOokFy0MFdvTg87hPKwsspXH8+ml2WSjKLJIxqbTLoRyZUp5Jp7DglQK70ZRpwubz0jng/wQWbKF4qUv+DTROSxF+1Aks7lkpERS2NJzf/smf9HsWbJnReOgeNOTdSGsmHiSpfs8ifWhm6EzSlK7g4bYL/GedwFKCG/aGBQxaCl2MNE0N5RSWAWR2keQurfYmvx1UP+uE+tI04cSbO5VH7zREbtKq9p5Oacc6UFlYuuXY1L/gRE/RKqfFF8OEsD1ca5pPIgxSfgfOSXG/OVQ05M8qwuFRU75cdFI+JvWLbW8O25w46bqXpxNzfizL01FTlkH677s7Qx8Z8zuE81gltV8sCV1YsqkLSzZ1YcmmLizZ1IUlm7qwZFMXlmzqwpJNXViyqQtLNnVh2VH8pdjVajV68ZtLO9fDD6bFUr27vQ/05+nv3cNbWJZa5NLn30btbbuYf+539efheJhzsTxehapfP169PANWmNpN/NLTc02M/fXt1a5uG+mx3BTiql+/sCdbu966VL9/jtnmtrCrLLEUCtd/atW9LIXC1eNrLVLUTLJc/Zfo8fGm7sO8gCjLDblwc12noFEYn+X6MarrlFmu/rjtT+3h7pMW+Lkastw3arXG3d+XW5+mfv+2xfJci+ntyy9LisX7pQr+Xnlmeq0FLFf3D6Q5ropvjadHH/Q/cZaru1+0wtxZ3N8/vafvO0rA4l+ioIVC4EUZZ6k9+s9+l4XIh6k/MTfKNgsA91fef+xlefMuFq7Z/2Sd5cl1sqvbvSzVhtfq1T/FfLD4FeZmLwszzFWe7PIVC/h7HXWyrLO8evXlfj9LtVGPxJ/Ms/i5yusXLA+ek9X/lwcW8Z9Xu6//fuFjtT8eyxOIsDTcvg1VhlhEv6yF+sMXLG+eC9Zf3kKWwudzqFoGWLzivr39u41Gwz0s4KW+w3JVD/WLxPLMLIXb//2H6PX2xs9SaJH2sIh++PHztT05fwZYCt5DpelW4eYz0n/ZyxKzS9ZYoqqzRHgPS/Up8z7GRPr1Nzf/WOp4YN1/fQr1i4GLs7Pc3Lh9ytv7Py//wh791yyf2Y0vhfu7f//uGneNh1o0ROQ1Vu6LdftyGM+K13eZZtl3cY+PPftpAQ2J+WZ5yVXOH9FuX+zBN0te+mIR7bL4nZt6XvrIEW2z0JrPokueWcSG30POy5hSTDGWKkHxO5UvwQdyyOL2BmqNFz+J3hmDzRFL4fbp8/Pl9Z6Om8fmV3LH4vUHrmh/oF5oRD6QP5aIrl9jiXBOWUhvoP74+ByfkPB61Bliuf6GpR6ykN7AZ2N7nMXvv2SFBdQarvZ3od4aoR5qb7sjRv6l3429RHWutQrfjWtVq9+PfJ0yJhbVZd1FNnVhyaYuLNnUhSWb+n/J0gHs+FZ+LwHiLLnjb73XyqBNWbpZ2cd6pLA+pGdqzgD9SUv83LAzCRtj5lkgOLI0peNdTlVw9rSyBB/0x4PPkc6YpOA0XQNY9Eeu75njKMze+TfsgR57ASBM+ayH34m9l0mACxGI7CDqfLbK8pwehotahGVEoyWXl+bwFntFJil9lbCIa+Zkndyx4L4ZvE1adFla7DWTyiLtsh0prL+zdzQIPY+lGv5HKe3SHSWsV9g5x7AreiyiE5zhjGaZOZLjZ+H+hqGg9z5lEVm4IW7WsXLSmmHQGofFdkTG0gtP14btkqxm3Tjui0usCQoKrUzEgEVsmqGbQWHSNHQgZVeybjjD8Ehw0gD3IiziNIQk15RxpVvMriZDpESLu9bFKIvY0iJX3Rc4ZFnxoq77YpxFdMzYJ3IjaDOUkEVcRjwwN0JKpSfusoi9mZA3GrhuRTYBxrYGTm1F+/kGWRGCcGGIX7GI1eXQhLnAQVAbT3qxwos7WzabK1tTYKbbAaSRqFEe9LaLvmf7ac8YzCrrcdol/lLtzXy17Fd3C/5/auuonMoOb8UAAAAASUVORK5CYII=" />
            </a>
            <a href="{{ $pdf->path }}" target="_blanck"
            <span class="text-muted">
                {{ $pdf->name }}
            </span>
            </a>

</div>
@endforeach
</div>

                  </div>
                </div>

              </div>

            </div>
          </section><!-- End Portfolio Details Section -->

    </main>
@endsection
