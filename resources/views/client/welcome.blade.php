@extends('client.index')

@section('content')
<!-- start slider -->

<section id="slider" class="services section-bg" style=" margin-top: 101px; ">
    <div class="container" data-aos="fade-up">

        <div class="row">
           

            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    @foreach ($ads as $ad)
                  <div class="carousel-item @if($loop->first) active @endif">
                    <img class="d-block w-100" src="{{$ad->image_url}}" alt="First slide" style=" height: 350px; ">
                  </div>
  @endforeach
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>

        </div>

    </div>
</section><!-- End Services Section -->
<!-- end slider -->
    <div id="home" class="header-hero bg_cover" style="background-image: url({{ $aboutus->image }})">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8 col-lg-10">
                    <div class="header-content text-center">
                        <h3 class="header-title">{{ $aboutus->title }}</h3>
                        <p class="text">{{ $aboutus->description }}</p>
                        {{-- <ul class="header-btn">
                        <li><a class="main-btn btn-one" href="#">GET IN TOUCH</a></li>
                        <li><a class="main-btn btn-two video-popup" href="https://www.youtube.com/watch?v=r44RKWyfcFw">WATCH THE VIDEO <i class="lni-play"></i></a></li>
                    </ul> --}}
                    </div> <!-- header content -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
        <div class="header-shape">
            <img src="assets/images/header-shape.svg" alt="shape">
        </div>
    </div> <!-- header content -->

    <main id="main">

        <section id="testimonials" class="testimonials">
            <div class="container" data-aos="zoom-in">

              <header class="section-header">
                <h3>@lang('navbar.staff')</h3>
              </header>

              <div class="row justify-content-center">
                <div class="col-lg-8">

                  <div class="testimonials-slider swiper-container" data-aos="fade-up" data-aos-delay="100">
                    <div class="swiper-wrapper">
                        @foreach ($staff as $st)

                      <div class="swiper-slide">

                            <a href="/staff/profile/{{ $st->id }}">

                                <img style="margin-left:30%; border-radius: 40%; max-width:40%" src="{{ $st->image ? $st->image : '/uploads/media/users/default.jpg' }}" class="testimonial-img" alt="">
                          <h3 style="
                          text-align: center;
">{{ $st->name }}</h3>
                          <h4 style="
                          text-align: center;
                      ">{{ $st->nationality }} </h4>
                          {{--  <p style="
                          text-align: center;
                      ">
                            {{ $st->age }}
                          </p>  --}}
                            </a>

                      </div><!-- End testimonial item -->

                      @endforeach

                    </div>
                    <div class="swiper-pagination"></div>
                  </div>
                </div>

              </div>
              <div class="row justify-content-center">

             <a href="/staff/all"> <span class="text-muted"> @lang('navbar.seeall') <i class="lni-chevron-down"></i></span></a>
              </div>
            </div>
          </section><!-- End Testimonials Section -->
     
          <section id="service" class="services-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-title pb-10">
                        <h4 class="title">Our Mission</h4>
                        <p class="text">“DELIVERING EXCELLENCE TO INDIVIDUALS, BUSINESS AND COMMUNITES.”
                                Our mission statement reflect our commitment to offering the highest quality service to our clients, candidates and all the other stakeholders we work with, as well as the delivery of our corporate social responsibility program to help the wider community. It demonstrate how we continually strive to go above and beyond to provide exceptional service
                                </p>
                    </div> <!-- section title -->
                </div>
            </div> <!-- row -->
    
        </div> <!-- conteiner -->
        <div class="services-image d-lg-flex align-items-center">
            <div class="image">
                <img src="assets/images/mission.png" alt="Services">
            </div>
        </div> <!-- services image -->
    </section>


    <section  class="services-area">
        <div class="container">
        <div class="services-image d-lg-flex align-items-center" style="left:0">
            <div class="image">
                <img src="assets/images/services.png" alt="Services">
            </div>
        </div> <!-- services image -->
            <div class="row" style=" justify-content: flex-end;">
                <div class="col-lg-6">
                    <div class="section-title pb-10">
                        <h4 class="title">Our Guarantee</h4>
                        <p class="text">We undertake to repatriate on our own expenses if any candidate deployed by Top Reach Human Resources Consultancy L.L.C., who is found professionally or medically unfit or refuses to work specified by company with in a probationary period of 90 days from the date of his deployment.</p>
                    </div> <!-- section title -->
                </div>
            </div> <!-- row -->
    
        </div> <!-- conteiner -->
    
    </section>
    <section id="service" class="services-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-title pb-10">
                        <h4 class="title">Our Vision</h4>
                        <p class="text">“ TO DELIVER WORLD CLASS PROFESSIONAL REQUIREMENT SOLUTIONS, TO EMPLOYERS & JOB SEEKERS IN EVERY RANGE OF DISCIPLINE ”</p>
                    </div> <!-- section title -->
                </div>
            </div> <!-- row -->
    
        </div> <!-- conteiner -->
        <div class="services-image d-lg-flex align-items-center">
            <div class="image">
                <img src="assets/images/vision.png" alt="Services" style="max-height: 300px;max-width: 300px; ">
            </div>
        </div> <!-- services image -->
    </section>


        <section id="service" class="services section-bg">
            <div class="container" data-aos="fade-up">

                <div class="section-title text-center pb-10">
                    <h4 class="title">
                        @lang('navbar.services')
                    </h4>
                </div>

                <div class="row">
                    @foreach ($services as $service)
                        <div class="col-md-6 col-lg-4 wow bounceInUp" data-aos="zoom-in" data-aos-delay="100">
                            <div class="box">
                                <div style="background: #fceef3;"><img style="max-width: 80%;" src="{{ $service->image_url }}"></div>
                                @if (\App::getlocale() == 'ar')
                                    <h4 class="title"><a
                                            href="">{{ $service->title ? $service->title : $service->title_en }}</a></h4>
                                    <p class="description"> {!! nl2br(e(mb_substr($service->description, 0, 100))) !!}@if (strlen(mb_substr($service->description, 100)) > 0)
                                            <p style="display:none" id="text{{ $service->id }}"> {!! nl2br(e(mb_substr($service->description, 100))) !!}
                                            </p>
                                            <br> <a style="padding: 0px;color: #007bff;cursor: pointer;" class="read-more" id="butto{{ $service->id }}"
                                                onclick="hideshow2('text{{ $service->id }}','butto{{ $service->id }}')">Read more
                                                 </a><a class="read-more" style="padding: 0px; "><i
                                                    class="bi bi-arrow-right"></i></a>
                                        @endif
                                    </p>
                                @else
                                    <h4 class="title"><a href="">{{ $service->title_en }}</a></h4>
                                    <p class="description">{!! nl2br(e(substr($service->description_en, 0, 100))) !!}@if (strlen(substr($service->description_en, 100)) > 0)
                                            <p style="display:none" id="text{{ $service->id }}"> {!! nl2br(e(substr($service->description_en, 100))) !!}
                                            </p>
                                            <br> <a style="padding: 0px;color: #007bff;cursor: pointer; " class="read-more" id="butto{{ $service->id }}"
                                                onclick="hideshow2('text{{ $service->id }}','butto{{ $service->id }}')">Read
                                                more </a><a class="read-more" style="padding: 0px; "><i
                                                    class="bi bi-arrow-right"></i></a>
                                        @endif
                                    </p>
                                @endif
                            </div>
                        </div>
                    @endforeach



                </div>

            </div>
        </section><!-- End Services Section -->


        
        <section id="pricing" class="pricing-area" style="background-color: white">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="section-title text-center pb-10">
                            <h4 class="title">@lang('navbar.jobvecancies')</h4>

                        </div> <!-- section title -->
                    </div>
                </div> <!-- row -->
                <div class="row justify-content-center">

                    @foreach ($jobvacanies as $job)
                        <div class="col-lg-4 col-md-7 col-sm-9 @if ($loop->index != 0 && $loop->index
                            != 1 && $loop->index != 2) HideItem @endif">
                            <div class="single-pricing mt-40">
                                <div class="pricing-header text-center">
                                    <h5 class="sub-title">{{ $job->title }}</h5>
                                    <div class="pricing-baloon">
                                        <img src="{{ $job->image }}" alt="baloon">
                                    </div>
                                </div>
                                <div class="pricing-list">
                                    <ul>
                                        @foreach (explode(" \r\n", $job->description) as
                                            $desc)
                                            <li><i class="lni-check-mark-circle"></i> {{ $desc }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="pricing-btn text-center">
                                    <a class="main-btn" href="#" onclick="$('#jobid').val({{ $job->id }})"
                                        data-toggle="modal" data-target="#exampleModalLong">@lang('navbar.applynow')</a>
                                </div>
                                <div class="buttom-shape">
                                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 350 112.35">
                                        <defs>
                                            <style>
                                                .color-1 {
                                                    fill: #2bdbdc;
                                                    isolation: isolate;
                                                }

                                                .cls-1 {
                                                    opacity: 0.1;
                                                }

                                                .cls-2 {
                                                    opacity: 0.2;
                                                }

                                                .cls-3 {
                                                    opacity: 0.4;
                                                }

                                                .cls-4 {
                                                    opacity: 0.6;
                                                }

                                            </style>
                                        </defs>
                                        <title>bottom-part1</title>
                                        <g id="bottom-part">
                                            <g id="Group_747" data-name="Group 747">
                                                <path id="Path_294" data-name="Path 294" class="cls-1 color-1"
                                                    d="M0,24.21c120-55.74,214.32,2.57,267,0S349.18,7.4,349.18,7.4V82.35H0Z"
                                                    transform="translate(0 0)" />
                                                <path id="Path_297" data-name="Path 297" class="cls-2 color-1"
                                                    d="M350,34.21c-120-55.74-214.32,2.57-267,0S.82,17.4.82,17.4V92.35H350Z"
                                                    transform="translate(0 0)" />
                                                <path id="Path_296" data-name="Path 296" class="cls-3 color-1"
                                                    d="M0,44.21c120-55.74,214.32,2.57,267,0S349.18,27.4,349.18,27.4v74.95H0Z"
                                                    transform="translate(0 0)" />
                                                <path id="Path_295" data-name="Path 295" class="cls-4 color-1"
                                                    d="M349.17,54.21c-120-55.74-214.32,2.57-267,0S0,37.4,0,37.4v74.95H349.17Z"
                                                    transform="translate(0 0)" />
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                            </div> <!-- single pricing -->
                        </div>
                    @endforeach
                    <a id="clickme" onclick="hideshowText()" href="#pricing">
                        <p class="text" style="font-size: 16px;line-height: 24px; color: #6c6c6c;margin-top: 24px;"> <span
                                id="seealltext"> @lang('navbar.seeall') </span><span id="seelesstext" style="display: none">
                                @lang('navbar.seeless') </span> <i class="lni-chevron-down"></i></p>
                    </a>

                </div> <!-- row -->
            </div> <!-- conteiner -->
        </section>

        <section id="team" class="team section-bg">
            <div class="container" data-aos="fade-up">
                <div class="section-header">
                    <h3>@lang('navbar.team')</h3>
                </div>

                <div class="row">
                    @foreach ($team as $t)

                        <div class="col-lg-3 col-md-6" data-aos="fade-up" data-aos-delay="100">
                            <div class="member">
                                <img src="{{ $t->image }}" class="img-fluid" alt="" style="height: 300px">
                                <div class="member-info">
                                    <div class="member-info-content">
                                        <h4>{{ $t->title }}</h4>
                                        <span>{{ $t->description }}</span>
                                        <span>{{ $t->vision }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endforeach
                </div>

            </div>
        </section><!-- End Team Section -->

        <section id="contact" class="contact-area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="section-title text-center pb-10">
                            <h4 class="title">Get In touch</h4>
                            <p class="text">{{App\models\Appinfo::first()->address}}</p>
                        </div> <!-- section title -->
                    </div>
                </div> <!-- row -->
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="contact-form">
                            <form id="contact-form" action="/contact/us" method="post" data-toggle="validator">
                            @csrf
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="single-form form-group">
                                            <input type="text" name="name" placeholder="Your Name"
                                                data-error="Name is required." required="required">
                                            <div class="help-block with-errors"></div>
                                        </div> <!-- single form -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="single-form form-group">
                                            <input type="email" name="email" placeholder="Your Email"
                                                data-error="Valid email is required." required="required">
                                            <div class="help-block with-errors"></div>
                                        </div> <!-- single form -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="single-form form-group">
                                            <input type="text" name="title" placeholder="Subject"
                                                data-error="Subject is required." required="required">
                                            <div class="help-block with-errors"></div>
                                        </div> <!-- single form -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="single-form form-group">
                                            <input type="text" name="phone" placeholder="Phone format: 971500001111"
                                                data-error="Phone is required." required="required" title="Format: 512345678">
                                            <div class="help-block with-errors"></div>
                                        </div> <!-- single form -->
                                    </div>
                                    <div class="col-md-12">
                                        <div class="single-form form-group">
                                            <textarea placeholder="Your Mesaage" name="message"
                                                data-error="Please, leave us a message." required="required"></textarea>
                                            <div class="help-block with-errors"></div>
                                        </div> <!-- single form -->
                                    </div>
                                    <p class="form-message"></p>
                                    <div class="col-md-12">
                                        <div class="single-form form-group text-center">
                                            <button type="submit" class="main-btn">send message</button>
                                        </div> <!-- single form -->
                                    </div>
                                </div> <!-- row -->
                            </form>
                        </div> <!-- row -->
                    </div>
                </div> <!-- row -->
            </div> <!-- conteiner -->
        </section>

        <div class="modal fade" id="exampleModalLong" data-backdrop="static" tabindex="-1" role="dialog"
            aria-labelledby="staticBackdrop" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="/cv/upload" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">@lang('navbar.jobvecancies')</h5>
                            <button type="button" class="btn" data-dismiss="modal" aria-label="Close">
                                X
                            </button>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label class="col-xl-6 col-lg-6 text-right col-form-label" data-required="yes">
                                    @lang('navbar.uploadyourcv')</label>
                                <input type="hidden" id="jobid" name="id">
                                <input type="file" name="cv" required class="form-control" accept=".pdf">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-primary font-weight-bold"
                                data-dismiss="modal">@lang('admin.close')</button>
                            <button type="submit" class="btn btn-primary font-weight-bold">@lang('admin.save')</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </main><!-- End #main -->
    <script src="/global/plugins.bundle.js"></script>
    <script>
        $('.HideItem').hide();
        $("#clickme").click(function() {
            $(".HideItem").toggle("slow", function() {
                // Animation complete.
            });
        });

        function hideshowText() {
            var btnTextall = document.getElementById('seealltext');
            var btnTextless = document.getElementById('seelesstext');

            if (btnTextall.style.display == "none") {
                btnTextall.style.display = "inline";
                btnTextless.style.display = "none";


            } else {

                btnTextall.style.display = "none";
                btnTextless.style.display = "inline";
            }
        }

        function hideshow2(id, readid) {
            var moreText = document.getElementById(id);
            var btnText = document.getElementById(readid);

            if (moreText.style.display === "none") {
                btnText.text = "Read less";
                moreText.style.display = "inline";

            } else {
                btnText.text = "Read more";
                moreText.style.display = "none";

            }
        }

        function replacebakn(x) {
            const regex = /\\n|\\r\\n|\\n\\r|\\r/g;
            return x.replace(regex, '<br>');
        }

    </script>
@endsection
