<!DOCTYPE html>
    <html>
    <head>
      <meta charset='utf-8'>
      <meta name='viewport' content='width=device-width'>
      <title>Privacy Policy</title>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

      <style> 
:root {
  --primary-color: #333;
  --secondary-color: #444;
  --overlay-color: rgba(0, 0, 0, 0.7);
}

* {
  box-sizing: border-box;
  margin: 0;
  padding: 0;
}

body {
  font-family: 'Catamaran', sans-serif;
  line-height: 1.6;
  color: #333;
  font-size: 1.1rem;
}

h1,
h2,
h3,
h4 {
  line-height: 1.3;
}

a {
  color: #444;
  text-decoration: none;
}

ul {
  list-style: none;
}

img {
  width: 100%;
}

.container {
  max-width: 1100px;
  margin: auto;
  overflow: hidden;
  padding: 0 2rem;
}

.navbar {
  font-size: 1.2rem;
  padding-top: 0.3rem;
  padding-bottom: 0.3rem;
}

.navbar .container {
  /* display: grid; */
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 2rem;
}

.navbar .logo {
  font-size: 2rem;
}

.navbar ul {
  justify-self: flex-end;
  display: flex;
  align-items: center;
  justify-content: center;
}

.navbar a {
  padding: 0 1rem;
}

.navbar a:hover {
  color: #555;
}

.section-a {
  margin: 2rem 0;
}

.section-a .container {
  /* display: grid; */
  grid-template-columns: 1fr 1fr;
  grid-gap: 3rem;
  align-items: center;
  justify-content: center;
}

.section-a h1 {
  font-size: 4rem;
  color: var(--primary-color);
}

.section-a p {
  margin: 1rem 0;
}

.section-b {
  position: relative;
  background: url('https://i.ibb.co/1RS1dqC/section-b.jpg') no-repeat bottom center/cover;
  height: 600px;
}

.section-b-inner {
  color: #fff;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: center;
  margin: auto;
  max-width: 860px;
  padding: 5rem 0;
}

.section-b-inner h3 {
  font-size: 2rem;
}

.section-b-inner h2 {
  font-size: 5rem;
  margin-top: 1rem;
}

.section-b-inner p {
  margin-top: 1rem;
  font-size: 1.5rem;
}

.section-c .gallery {
  display: grid;
  grid-template-columns: repeat(5, 1fr);
}
.section-c .gallery a:first-child {
  /* grid-row-start: 1;
  grid-row-end: 3; */
  grid-row: 1/3;
  grid-column: 1/3;
}

.section-c .gallery a:nth-child(2) {
  grid-column-start: 3;
  grid-column-end: 5;
}

.section-c .gallery img,
.section-c .gallery a {
  width: 100%;
  height: 100%;
}

.section-footer {
  background: var(--primary-color);
  color: #fff;
  padding: 4rem 0;
}

.section-footer .container {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 1rem;
}

.section-footer h2 {
  font-size: 2rem;
  margin-bottom: 1rem;
}

.section-footer h3 {
  margin-bottom: 0.7rem;
}

.section-footer a {
  line-height: 1.9;
  color: #ccc;
}

.section-footer a > i {
  color: #555;
  margin-right: 0.5rem;
}

.email-form {
  width: 100%;
  display: inline-block;
  background-color: #555;
  position: relative;
  border-radius: 20px;
  line-height: 0;
  margin-top: 1rem;
}

/* // .form-control-wrap {
// 	position: relative;
// 	display: inline-block;
// 	width: 100%;
// } */

.email-form .form-control {
  display: inline-block;
  border: 0;
  outline: 0;
  font-size: 1rem;
  color: #ddd;
  background-color: transparent;
  font-family: inherit;
  margin: 0;
  padding: 0 3rem 0 1.5rem;
  width: 100%;
  height: 45px;
  border-radius: 20px;
}

.email-form .submit {
  display: inline-block;
  position: absolute;
  top: 0;
  right: 0;
  width: 45px;
  height: 45px;
  background-color: #eee;
  font-size: 1rem;
  text-align: center;
  margin: 0;
  padding: 0;
  outline: 0;
  border: 0;
  color: #333;
  cursor: pointer;
  border-radius: 0 20px 20px 0;
}

.btn {
  display: inline-block;
  background: var(--primary-color);
  color: #fff;
  padding: 0.8rem 1.5rem;
  border: none;
  cursor: pointer;
  font-size: 1.1rem;
  border-radius: 30px;
}

.btn:hover {
  background: var(--secondary-color);
}

.overlay {
  height: 100%;
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
  background-color: var(--overlay-color);
}

@media (max-width: 700px) {
  .section-a .container {
    grid-template-columns: 1fr;
    text-align: center;
  }

  .section-a .container div:first-child {
    order: 2;
  }

  .section-a .container div:nth-child(2) {
    order: -1;
  }

  .section-a img {
    width: 80%;
    margin: auto;
  }

  .section-c .gallery {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
  }
  .section-c .gallery a:first-child {
    grid-row: 1/1;
    grid-column: 1/1;
  }

  .section-c .gallery a:nth-child(2) {
    grid-column: 2/4;
    grid-row: 2/2;
  }

  .section-c .gallery a:last-child {
    display: none;
  }

  .section-footer {
    padding: 2rem 0;
  }
  .section-footer .container {
    grid-template-columns: 1fr;
    text-align: center;
  }

  .section-footer div:nth-child(2),
  .section-footer div:nth-child(3) {
    display: none;
  }
}

       </style>
    </head>
<body>


  <!-- Showcase -->
  <section class="section-a">
    <div class="container" style="text-align: end;">
      <div>
        <img src="/uploads/logo.png" alt="" style="height: 33%;width:8%;" />
        <h1>Marhaba.</h1>
        <br>
        <h1>سياسة الخصوصية</h1>
        
        <p style="text-align: justify;">
 وُضعت سياسة الخصوصية هذه من أجل تقديم خدمة أفضل ومراعاة الأساسيات والمعايير العالمية لاستخدام "معلومات التعريف الشخصي" الخاصة بمستخدمي شبكة الإنترنت. ومعلومات التعريف الشخصي هي تلك المعلومات التي يمكن استخدامها بمفردها أو مع غيرها من المعلومات من أجل تحديد شخص ما أو التواصل معه أو تحديد مكانه، أو تحديد شخص بعينه فى سياق عام. من فضلك، اقرأ سياسة الخصوصية الخاصة بنا بعناية من أجل الحصول على فهم أوضح لكيفية جمعنا أو استخدامنا أو حمايتنا أو تعاملنا مع بيانات التعريف الشخصي خاصتك فيما يتعلق بموقعنا الإلكتروني. ما المعلومات الشخصية التي نجمعها من الأشخاص الذين يزورون مدونتنا أو موقعنا الإلكتروني أو تطبيقاتنا على الهواتف الذكية؟ يمكن للمستخدمين أن يزوروا موقعنا تحت هوية مجهولة، إلا أنه عند التسجيل في موقعنا الإلكتروني، سيُطلب منك إدخال اسمك وعنوان بريدك الإلكتروني وعنوانك البريدي ورقم هاتفك، أو غيرها من التفاصيل التي ستساعدنا على تقديم خدمة أفضل من أجلك. متى نجمع المعلومات؟ نحن نجمع منك المعلومات عندما تطلب التسجيل في موقعنا الإلكتروني أو تتواصل مع أحد مزودي الخدمة أو تشترك في خدمة الرسائل الإخبارية أو تشارك في استطلاع رأي، أو تملأ استمارة أو تدخل معلومات على موقعنا الإلكتروني. ويمكن للمستخدمين تغيير معلوماتهم الشخصية عن طريق الولوج إلى حسابهم، كما يمكنهم إلغاء اشتراكهم في الخدمة البريدية، بحيث لا تصلهم المزيد من الرسائل الإلكترونية عبر اتباع الخطوات المذكورة في نهاية كل رسالة. كيف نستخدم المعلومات؟ قد نستخدم المعلومات التي نجمعها منك عندما تسجل في الموقع الإلكتروني أو تتواصل مع أحد مزودي الخدمة أو تشترك في خدمة الرسائل الإخبارية أو تشارك في استطلاع رأي أو إحدى طرق التواصل التسويقية أو تتصفح الموقع أو تستخدم بعض السمات الأخرى المحددة في الموقع الإلكتروني؛ بالطرق التالية: من أجل إضفاء طابع شخصى على تجربة المستخدم والسماح لأنفسنا بتوصيل نوعية المحتويات وعروض الوجبات التي قد تهمك. من أجل تحسين موقعنا الإلكتروني لتقديم خدمة أفضل لك. من أجل السماح لأنفسنا بتقديم خدمة أفضل من أجلك عن طريق الإجابة عن استفساراتك المقدمة لخدمة العملاء. من أجل تنظيم مسابقات أو عروض خاصة أو استطلاعات رأي أو غيرها من سمات الموقع الإلكتروني. من أجل إجراء تعاملاتك بسرعة. من أجل إرسال رسائل بريد إلكتروني دورية بخصوص مشترياتك أو الوجبات والخدمات الأخرى. كيف نحمي معلومات الزوار؟ يتم عمل مسح شامل لموقعنا الإلكتروني بالكامل بصورة دورية من أجل اكتشاف الثغرات الأمنية ونقاط الضعف المعروفة من أجل جعل زيارتك لموقعنا الإلكتروني آمنة قدر الإمكان. يتم جمع معلوماتك الشخصية خلف شبكات مؤمَّنة ولا يمكن الدخول إليها إلا بواسطة عدد محدود من الأشخاص الذين يمتلكون تصريح دخول خاص إلى مثل هذه الأنظمة، كما يُطلب منهم الحفاظ على سرية المعلومات، بالإضافة إلى إن جميع معلومات بطاقات الائتمان/المعلومات الحساسة التي تمدنا بها يتم تشفيرها بواسطة تقنية طبقة المنافذ الآمنة (SSL). كما أننا نطبق مجموعة متنوعة من معايير الأمان عندما يضع المستخدم طلبات الشراء أو يسجل اشتراكه بالموقع أو يدخل إلى معلوماته. تتم معالجة جميع التعاملات عبر مزود خدمة البوابات الآمنة، ولا يتم تخزينها أو معالجتها على خوادمنا. هل نستخدم سجلات المتصفح (cookies)؟ نعم، إن سجلات المتصفح عبارة عن ملفات صغيرة الحجم ينقلها الموقع الإلكتروني أو مزود خدمته إلى القرص الصلب على حاسبك عبر متصفح الإنترنت الخاص بك (إن سمحت بهذا)، والتي تمكن أنظمة الموقع الإلكتروني أو مزود خدمته من التعرف على متصفحك وجمع وتخزين معلومات بعينها. على سبيل المثال، نستخدم سجلات المتصفح من أجل مساعدتنا على تذكر ومعالجة الأوامر التي قمت بها، كما أنها تُستخدم من أجل مساعدتنا على فهم مفضلاتك بناءً على نشاطاتك الحالية أو السابقة على الموقع، الأمر الذي يمكننا من تقديم خدمة محسنة لك. ونستخدم سجلات المتصفح أيضًا لمساعدتنا على تصنيف البيانات التي جمعناها عن حركة تصفح الموقع والتفاعلات التي تحدث عليه حتى نتمكن من تقديم تجربة وأدوات أفضل على الموقع في المستقبل. نستخدم سجلات المتصفح من أجل: المساعدة على تذكر ومعالجة الأوامر والخدمات التي طلبتها. فهم وتخزين مفضلات المستخدم من أجل الزيارات المستقبلية. متابعة الإعلانات. تصنيف البيانات التي تم جمعها عن حركة التصفح والتفاعلات على الموقع من أجل تقديم تجربة وأدوات أفضل في المستقبل. قد نستخدم أيضًا خدمات أخرى موثوق بها من أجل متابعة هذه المعلومات بالنيابة عنا. يمكنك أن تختار أن يحذرك حاسبك الإلكتروني في كل مرة تُرسل سجلات التتبع، ويمكنك أن تلغي استقبال جميع أنواع هذه السجلات. يمكنك أن تفعل هذا عبر إعدادات متصفحك (مثل متصفح إنترنت إكسبلورر). تختلف أنواع المتصفحات فيما بينها اختلافات طفيفة، لذا، ألق نظرة على قائمة المساعدة في متصفحك من أجل أن تتعلم الطريقة الصحيحة لتعديل استقبال جهازك لسجلات التتبع. إن أغلقت خاصية استقبال سجلات المتصفح، فستتوقف بعض السمات، والتي لن تؤثر على تجربة المستخدم التي تجعل من زيارتك لموقعنا أكثر فاعلية، وقد لا تعمل بعض من خدماتنا بشكل جيد. الكشف عن المعلومات السرية من قبل طرف خارجى إننا لا نبيع أو نبادل أو ننقل معلومات التعريف الشخصية الخاصة بك إلى أطراف خارجية إلا بعد إرسال إخطار مسبق لك، وهذا لا يتضمن شركاء استضافة الموقع الإلكتروني وغيرهم من الأطراف الذين يساعدوننا على تشغيل الموقع أو إدارة عملنا أو تقديم الخدمات لك، طالما وافقت هذه الأطراف على الحفاظ على سرية هذه المعلومات. قد نكشف عن معلوماتك في حال اعتقادنا أن هذا الأمر سيكون إذعانًا للقانون، أو بموجب سياسات موقعنا الإلكتروني أو لحماية حقوقنا أو ملكيتنا أو أمننا والآخرين. ولكن، يمكن تقديم بيانات الزوار غير الشخصية إلى أطراف أخرى من أجل أغراض التسويق أو الدعاية أو غيرها. روابط الأطراف الخارجية أحيانًا، وطبقًا لتقديرنا، قد نضع منتجات أو خدمات لأطراف أخرى أو نعلن عنها على موقعنا الإلكتروني، وتمتلك هذه الأطراف الأخرى مواقع إلكترونية خاصة بها ذات سياسات خصوصية منفصلة ومستقلة بذاتها، لذا فإننا لا نتحمل أية مسئولية أو التزام قانوني حيال محتويات أو أنشطة هذه المواقع المرتبطة بموقعنا الإلكتروني، ومع ذلك، فإننا نسعى لحماية نزاهة موقعنا ونرحب بأية تغذيات مرتدة عن هذه المواقع. كيف يتعامل موقعنا مع إشارات عدم التتبع؟ إننا نحترم إشارات عدم التتبع، ولا نتتبع أو نرسل سجلات تتبع أو نستخدم الدعاية عندما تكون خاصية عدم التتبع تعمل على المتفصح. هل يسمح موقعنا بالتتبع السلوكي للأطراف الخارجية؟ من المهم أيضًا أن نذكر أننا لا نسمح بخاصية التتبع السلوكي للأطراف الأخرى. ممارسات الاستخدام العادل للمعلومات إن حدث أي خرق أمني للبيانات، سنقوم باخبار جميع المستخدمين بذلك عن طريق البريد الإلكتروني وعبر موقعنا الإلكتروني في خلال سبعة أيام عمل.
        </p>
      </div>
    </div>
  </section>

  <script>
$(function() {
        const $gallery = $('.gallery a').simpleLightbox();
      });

  </script>
</body>
    </html>
      