@extends('layouts.app')


@section('css-links')

    <link rel="stylesheet" href="{{ asset('css/login/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/login/util.css') }}">

@endsection

@section('styles')

    <style>
        .container-login100
        {
            background-image: url("{{ asset('uploads/login_bg1.jpg') }}");
            background-size: cover;
        }

        .wrap-login100
        {
            opacity: 0.7;
        }
    </style>
@endsection

@section('content')



    <div class="limiter">

        <div class="container-login100">
            <div class="wrap-login100">
                <form class="login100-form validate-form" action="{{ route('login') }}" method="POST" autocomplete="off">
                    @csrf
                    <span class="login100-form-title p-b-48">
						<img src="{{ asset('uploads/Logo.png') }}"  alt="QO" height="150" width="200">
					</span>

                    <div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
                        <input class="input100" type="text" name="email" autocomplete="off">
                        <span class="focus-input100" data-placeholder="{{ trans('admin.email') }}"></span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate="Enter password">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
                        <input class="input100" type="password" name="password" autocomplete="off">
                        <span class="focus-input100" data-placeholder="{{ trans('admin.password') }}"></span>
                    </div>

                    <div >
                        @include('flash-message')
                    </div>

                    <div class="container-login100-form-btn">
                        <div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn"></div>
                            <button class="login100-form-btn" type="submit">
                                @lang('admin.login')
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@section('scripts')

    <script >
        $('.js-tilt').tilt({
            scale: 1.1
        })
    </script>

    <script src="{{ asset('js/login/main.js') }}"></script>

@endsection
