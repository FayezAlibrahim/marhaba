@extends('layouts.app')

@section('title',trans('complaints.complaints'))

@section('content')

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">@lang('complaints.complaints')</h1>
                <hr>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class=" table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>@lang('complaints.id')</th>
                            <th>@lang('complaints.title')</th>
                            <th>@lang('complaints.content')</th>
                            <th>@lang('complaints.user')</th>
                            <th>@lang('complaints.phone')</th>
                            <th>@lang('complaints.mark_as_read')</th>
                            <th>@lang('admin.settings')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($complaints as $complaint)
                            <tr class="odd gradeX">
                                <td>{{ $complaint->id }}</td>
                                <td>{{ $complaint->title ?? '' }}</td>
                                <td>{{ $complaint->content }}</td>
                                <td>{{ $complaint->user->username ?? ''   }}</td>
                                <td>{{ $complaint->user->phone ?? ''   }} </td>
                                <td>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox small">
                                            <input type="checkbox" class="custom-control-input" @if($complaint->mark_as_read == 'yes') checked @endif id="customCheck{{ $complaint->id }}" onchange="changeStatus({{ $complaint->id }})">
                                            <label class="custom-control-label" for="customCheck{{ $complaint->id }}"></label>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <a href="{{ route('admin.complaints.show',$complaint->id) }}" style="color: #28a745"><i class="fa fa-eye"></i></a>&nbsp;
                                    <a href="#" style="color: #dc3545" onclick="event.preventDefault();deleteItem('{{ $complaint->id }}','{{ route('admin.complaints.destroy',$complaint->id) }}')"><i class="fa fa-trash"></i></a>&nbsp;
                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
                <br>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>



@endsection


@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });


        function changeStatus(id) {
            var url = '{{ route('admin.complaints.status') }}';
            $.ajax({
                url: url,
                type: 'POST',
                data: {id: id, _token: '{!! csrf_token() !!}' }
            }).done(function(data) { }).fail(function(e) {
                swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")
            })
        }



        function deleteItem(id,url) {
            swal({
                title: '{!! trans('admin.are_you_sure') !!}',
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "{!! trans('admin.yes'); !!}",
                cancelButtonText: "{!! trans('admin.no'); !!}",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {



                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method : 'delete' }
                })
                    .done(function() {


                        swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.deleted_successfully') !!}", type: "success"},
                            function(){
                                location.reload();
                            }
                        );


                    })
                    .fail(function(e) {

                        swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")

                    })


            });
        }



    </script>

@endsection
