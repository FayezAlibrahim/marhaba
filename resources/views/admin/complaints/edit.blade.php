@extends('layouts.app')

@section('title',trans('categories.edit'))


@section('styles')

    <style>
        .wrapper-pic {
            position: relative;
            display: inline-block;
            opacity: 1;
        }

        .wrapper-pic:hover {
            transition: all .2s ease-in-out;
        }

        .wrapper-pic:hover .edit {
            transition: all .2s ease-in-out;
            display: block;
        }

        .edit {
            padding-top: 7px;
            padding-right: 7px;
            position: absolute;
            left: 20px;
            top: 10px;
            display: none;
        }
    </style>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">@lang('categories.edit')</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.categories.update',$category->id) }}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">@lang('categories.name')</label>

                                <div class="col-md-10">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $category->name }}">

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">@lang('categories.description')</label>

                                <div class="col-md-10">
                                    <textarea name="description" class="form-control @error('description') is-invalid @enderror" cols="20" rows="10">{{ $category->description }}</textarea>

                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">@lang('categories.status')</label>

                                <div class="col-md-10">
                                    <select name="status" class="form-control">
                                        <option value="active" {{ $category->status == "active" ? "selected" : "" }}>@lang('admin.active')</option>
                                        <option value="inactive" {{ $category->status == "inactive" ? "selected" : "" }}>@lang('admin.inactive')</option>
                                    </select>
                                </div>
                            </div>



                            @if($category->products()->count() == 0)
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">@lang('categories.secondary')</label>

                                <div class="col-md-10">
                                    <input type='checkbox' name="parent_id_checked" data-toggle='collapse' data-target='#mainCategories'>
                                </div>

                            </div>

                            <div class="form-group row collapse" id="mainCategories">
                                <label class="col-md-2 col-form-label text-md-right">@lang('categories.select_category')</label>
                                <div class="col-md-10" >
                                    <select name="parent_id" class="form-control" >
                                        @foreach($mainCategories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endif



                            <div class="form-group row">
                                <div class="col-md-12 offset-1">
                                    <button type="submit" class="btn btn-primary">
                                        @lang('admin.edit')
                                    </button>

                                </div>
                            </div>
                                </div>

                                <div class="col-md-4 wrapper-pic">
                                    <img src="{{ asset('uploads/'.$category->image_path) }}" class="img-fluid" id="category_image">
                                    <div class="edit">
                                        <a onclick="event.preventDefault();openChangeImage()" class="btn btn-success btn-xs">@lang('admin.edit')</a>
                                        <a  class="btn btn-danger btn-xs" onclick="event.preventDefault();removeImage()">@lang('admin.delete')</a>
                                    </div>
                                    <input type="file" id="category_image_id" onchange="previewImage(event)" name="image" style="display:none">
                                    <input type="hidden" name="image_flag" id="image_flag" >
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script>
        function openChangeImage()
        {
            $("#category_image_id").trigger('click');
        }

        function removeImage() {
            var default_image_url = "https://tsdist.com/wp-content/uploads/2018/07/Image_placeholder_1.jpg";
            $('#category_image').attr('src',default_image_url);
            change_image_flag('remove')
        }

        function change_image_flag(flag) {
            $("#image_flag").attr('value',flag);
        }


        function previewImage(event) {
         var image_src =  URL.createObjectURL(event.target.files[0]);
            $('#category_image').attr('src',image_src);
            change_image_flag('edit')

        }

    </script>

@endsection
