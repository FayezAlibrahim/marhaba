@extends('layouts.app')

@section('title','')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        {{ '#'.$object->id }}
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">{{ $object->title }}</h5>
                        <p class="card-text">{{ $object->content }}</p>
                        <p>{{ $object->user->username ?? ''   }}</p>
                        <p>{{ $object->user->phone ?? ''   }} </p>
                        <a href="#" onclick="changeStatus({{ $object->id }})" class="btn btn-primary">@lang('complaints.mark_as_read')</a>
                        <a href="#" onclick="event.preventDefault();deleteItem('{{ $object->id }}','{{ route('admin.complaints.destroy',$object->id) }}')" class="btn btn-danger">@lang('admin.delete')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


<script>
    function changeStatus(id) {
        var url = '{{ route('admin.complaints.status') }}';
        $.ajax({
            url: url,
            type: 'POST',
            data: {id: id, _token: '{!! csrf_token() !!}' }
        }).done(function(data) {
            swal("{!! trans('admin.done') !!}","{!! trans('admin.update_created_successfully') !!}", "success")

            swal({
                title: "{!! trans('admin.done') !!}",
                text: "{!! trans('admin.update_created_successfully') !!}",
                type: 'success',
                closeOnConfirm: false,
            }, function(){
                window.location.replace('{!! route('admin.complaints.index') !!}');
            });
        }).fail(function(e) {
            swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")
        })
    }



    function deleteItem(id,url) {
        swal({
            title: '{!! trans('admin.are_you_sure') !!}',
            type: "warning",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "{!! trans('admin.yes'); !!}",
            cancelButtonText: "{!! trans('admin.no'); !!}",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function () {



            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: {_token: '{!! csrf_token() !!}', _method : 'delete' }
            })
                .done(function() {


                    swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.deleted_successfully') !!}", type: "success"},
                        function(){
                            index_url = '{!! route('admin.complaints.index') !!}';
                            window.location.replace(index_url);
                        }
                    );


                })
                .fail(function(e) {

                    swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")

                })


        });
    }
</script>
