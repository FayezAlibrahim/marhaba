@extends('layouts.app')

@section('title',trans('orders.edit'))


@section('styles')
    <style>
        .variations_accordion {

            margin: 50px auto;
            box-shadow: 0 0 1px rgba(0, 0, 0, 0.1);
        }

        .variations_accordion .card,
        .variations_accordion .card:last-child .card-header {
            border: none;
        }

        .variations_accordion .card-header {
            border-bottom-color: #EDEFF0;
            background: transparent;
        }

        .variations_accordion .fa-stack {
            font-size: 18px;
        }

        .variations_accordion .btn {
            width: 100%;
            font-weight: bold;
            color: #004987;
            padding: 0;
        }

        .variations_accordion .btn-link:hover,
        .variations_accordion .btn-link:focus {
            text-decoration: none;
        }

        .variations_accordion li + li {
            margin-top: 10px;
        }



        .inc_wrapper {
            width: 300px;
            margin: 0 auto;
            text-align: center;
            padding-top: 50px;
        }



        .value-button {
            display: inline-block;
            border: 1px solid #ddd;
            margin: 0px;
            width: 40px;
            text-align: center;
            vertical-align: middle;
            padding: 11px 0;
            background: #eee;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .value-button:hover {
            cursor: pointer;
        }

        .inc_wrapper #decrease {
            margin-left: -4px;
            border-radius: 0 8px 8px 0;
        }

        .inc_wrapper #increase {


            margin-right: -4px;
            border-radius: 8px 0 0 8px;
        }

        /* form #input-wrap {
          margin: 0px;
          padding: 0px;
        } */

        .quantity_number {
            text-align: center;
            border: none;
            border-top: 1px solid #ddd;
            border-bottom: 1px solid #ddd;
            margin: 0px;
            width: 40px;
            height: 40px;
        }

        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    </style>
    @endsection

@section('content')
<div class="container">
    <div class="card card-custom">
        <!--Begin::Header-->
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <div class="card-header card-header-tabs-line">
            <div class="card-toolbar">
                <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x" role="tablist">
                    <li class="nav-item mr-3">
                        <a class="nav-link active" data-toggle="tab" href="#kt_apps_contacts_view_tab_2">
                            <span class="nav-icon mr-2">
                                <span class="svg-icon mr-3">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Chat-check.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24" />
                                            <path d="M4.875,20.75 C4.63541667,20.75 4.39583333,20.6541667 4.20416667,20.4625 L2.2875,18.5458333 C1.90416667,18.1625 1.90416667,17.5875 2.2875,17.2041667 C2.67083333,16.8208333 3.29375,16.8208333 3.62916667,17.2041667 L4.875,18.45 L8.0375,15.2875 C8.42083333,14.9041667 8.99583333,14.9041667 9.37916667,15.2875 C9.7625,15.6708333 9.7625,16.2458333 9.37916667,16.6291667 L5.54583333,20.4625 C5.35416667,20.6541667 5.11458333,20.75 4.875,20.75 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                            <path d="M2,11.8650466 L2,6 C2,4.34314575 3.34314575,3 5,3 L19,3 C20.6568542,3 22,4.34314575 22,6 L22,15 C22,15.0032706 21.9999948,15.0065399 21.9999843,15.009808 L22.0249378,15 L22.0249378,19.5857864 C22.0249378,20.1380712 21.5772226,20.5857864 21.0249378,20.5857864 C20.7597213,20.5857864 20.5053674,20.4804296 20.317831,20.2928932 L18.0249378,18 L12.9835977,18 C12.7263047,14.0909841 9.47412135,11 5.5,11 C4.23590829,11 3.04485894,11.3127315 2,11.8650466 Z M6,7 C5.44771525,7 5,7.44771525 5,8 C5,8.55228475 5.44771525,9 6,9 L15,9 C15.5522847,9 16,8.55228475 16,8 C16,7.44771525 15.5522847,7 15,7 L6,7 Z" fill="#000000" />
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span>
                            </span>
                            <span class="nav-text font-weight-bold">الطلب</span>
                        </a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link" data-toggle="tab" href="#kt_apps_contacts_view_tab_3">
                            <span class="nav-icon mr-2">
                                <span class="svg-icon mr-3">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Devices/Display1.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24" />
                                            <path d="M11,20 L11,17 C11,16.4477153 11.4477153,16 12,16 C12.5522847,16 13,16.4477153 13,17 L13,20 L15.5,20 C15.7761424,20 16,20.2238576 16,20.5 C16,20.7761424 15.7761424,21 15.5,21 L8.5,21 C8.22385763,21 8,20.7761424 8,20.5 C8,20.2238576 8.22385763,20 8.5,20 L11,20 Z" fill="#000000" opacity="0.3" />
                                            <path d="M3,5 L21,5 C21.5522847,5 22,5.44771525 22,6 L22,16 C22,16.5522847 21.5522847,17 21,17 L3,17 C2.44771525,17 2,16.5522847 2,16 L2,6 C2,5.44771525 2.44771525,5 3,5 Z M4.5,8 C4.22385763,8 4,8.22385763 4,8.5 C4,8.77614237 4.22385763,9 4.5,9 L13.5,9 C13.7761424,9 14,8.77614237 14,8.5 C14,8.22385763 13.7761424,8 13.5,8 L4.5,8 Z M4.5,10 C4.22385763,10 4,10.2238576 4,10.5 C4,10.7761424 4.22385763,11 4.5,11 L7.5,11 C7.77614237,11 8,10.7761424 8,10.5 C8,10.2238576 7.77614237,10 7.5,10 L4.5,10 Z" fill="#000000" />
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span>
                            </span>
                            <span class="nav-text font-weight-bold">تفاصيل الطلب</span>
                        </a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link" data-toggle="tab" href="#kt_apps_contacts_view_tab_4">
                            <span class="nav-icon mr-2">
                                <span class="svg-icon mr-3">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Home/Globe.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24" />
                                            <path d="M13,18.9450712 L13,20 L14,20 C15.1045695,20 16,20.8954305 16,22 L8,22 C8,20.8954305 8.8954305,20 10,20 L11,20 L11,18.9448245 C9.02872877,18.7261967 7.20827378,17.866394 5.79372555,16.5182701 L4.73856106,17.6741866 C4.36621808,18.0820826 3.73370941,18.110904 3.32581341,17.7385611 C2.9179174,17.3662181 2.88909597,16.7337094 3.26143894,16.3258134 L5.04940685,14.367122 C5.46150313,13.9156769 6.17860937,13.9363085 6.56406875,14.4106998 C7.88623094,16.037907 9.86320756,17 12,17 C15.8659932,17 19,13.8659932 19,10 C19,7.73468744 17.9175842,5.65198725 16.1214335,4.34123851 C15.6753081,4.01567657 15.5775721,3.39010038 15.903134,2.94397499 C16.228696,2.49784959 16.8542722,2.4001136 17.3003976,2.72567554 C19.6071362,4.40902808 21,7.08906798 21,10 C21,14.6325537 17.4999505,18.4476269 13,18.9450712 Z" fill="#000000" fill-rule="nonzero" />
                                            <circle fill="#000000" opacity="0.3" cx="12" cy="10" r="6" />
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span>
                            </span>
                            <span class="nav-text font-weight-bold">الوجبات</span>
                        </a>
                    </li>
                </ul>


            </div>
            <button type="submit" onclick="$('#update_order').submit()" class="btn btn-xs btn-primary" style="height: fit-content;  margin-top: 10px;">
                @lang('admin.edit')</button>

        </div>
        <!--end::Header-->
        <!--Begin::Body-->
        <div class="card-body">
        <form method="POST" id="update_order" action="/admin/order/edit/{{ $order->id }}">
            @csrf

            <div class="tab-content pt-5">
                <!--begin::Tab Content-->
                <div class="tab-pane active" id="kt_apps_contacts_view_tab_2" role="tabpanel">
                  @if($order->use_points)
                    <div class="alert alert-custom alert-notice alert-light-primary fade show" role="alert">
                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                        <div class="alert-text"> تم خصم مقدار{{ $order->use_points }} ل.س من الطلب</div>
                        <div class="alert-close">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="ki ki-close"></i></span>
                            </button>
                        </div>
                    </div>
                    @endif
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">@lang('orders.id')</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control @error('delivery_address') is-invalid @enderror"
                                       value="{{ $order->id ?? '' }}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">@lang('orders.status')</label>

                            <div class="col-md-6">
                                <select class="form-control" name="status">
                                    @foreach($orderStatuses as $orderStatus)
                                        <option
                                            value="{{ $orderStatus }}" {{ $orderStatus == $order->status ? "selected" : "" }}>{{ trans('orders.'.$orderStatus) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">@lang('orders.order_date')</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control @error('delivery_address') is-invalid @enderror"
                                       value="{{ $order->created_at->format('F j, Y, g:i a') ?? '' }}" readonly>
 
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">@lang('orders.client')</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control @error('delivery_address') is-invalid @enderror"
                                       value="{{ $order->user->username ?? '' }}" readonly>

                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">@lang('orders.phone')</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control @error('delivery_address') is-invalid @enderror"
                                       value="{{ $order->user->phone ?? '' }}" readonly>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">@lang('orders.city')</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control @error('delivery_address') is-invalid @enderror"
                                       value="{{ $order->user->city->name ?? '' }}" readonly>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">@lang('users.region')</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control @error('delivery_address') is-invalid @enderror"
                                       value="{{ $order->user->region->name ?? '' }}" readonly>
                            </div>
                        </div>



                </div>
                <!--end::Tab Content-->

                <!--begin::Tab Content-->
                <div class="tab-pane " id="kt_apps_contacts_view_tab_3" role="tabpanel">

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">@lang('orders.delivery_address')</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control @error('delivery_address') is-invalid @enderror" name="delivery_address"
                                       value="{{ $order->delivery_address }}">

                                @error('delivery_address')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right"></label>

                            <div class="col-md-6">
                                <input id="zoomLevel" type="hidden" value="14"/>
                                <input id="myloc"type="hidden" value="{{$order->lat.','.$order->lon}}" type="text"/>
                                <div id="map" style="width: 400px; height: 400px;margin:10px"></div>
                            </div>

                        </div>



                </div>
                <!--end::Tab Content-->
                <div class="tab-pane " id="kt_apps_contacts_view_tab_4" role="tabpanel">


                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label" >@lang('orders.total_price')</label>
                            <div class="col-lg-9 col-xl-6">
                                <div class="input-group input-group-lg">
                                    <div class="input-group-prepend">
									  <span class="input-group-text" style="color:gray;">
										ل.س
                                      </span>
                                    </div>
    <input readonly class="form-control form-control-lg  text-md-left @error('minimum_order') is-invalid @enderror"  required   value="{{ $order->total_price ?? '0' }} " name="total_price"/>

                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">@lang('orders.order_note')</label>

                            <div class="col-md-6">
                                <textarea class="form-control form-control-lg form-control-solid @error('order_note') is-invalid @enderror" name="order_note" cols="30" rows="10">{{ $order->order_note }}</textarea>

                                @error('order_note')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label">@lang('orders.details')</label>

                            <div class="col-lg-9 col-xl-6">

                                <div id="accordion" class="variations_accordion">
                                    @foreach($order_det as $detail)
                                    {{-- {{$order->get_details()[0]["product"]->get_images()[0]["media_url"]}}  --}}
                                        <div class="card">
                                            <div class="card-header" id="{{ $detail->id }}">
                                                <h2 class="mb-0">
                                                    <button
                                                        onclick="event.preventDefault();"
                                                        class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                                        data-toggle="collapse" data-target="#collapse_{{ $detail->id }}"
                                                        aria-expanded="false" aria-controls="collapse_{{ $detail->id }}">
                                                        <span class="badge badge-primary badge-pill">{{ $detail->quantity  }}</span>
                                                        {{ $detail->product->name ?? '' }}
                                                        <span class="fa-stack fa-sm">
                                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                                <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                                                              </span>
                                                        </span>
                                                    </button>
                                                </h2>
                                            </div>
                                            <div id="collapse_{{ $detail->id }}" class="collapse" aria-labelledby="{{ $detail->id }}"
                                                 data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="text-center">
                                                        <img src="{{$detail->product->media[0]['media_url']}}" class="rounded" alt="Img"
                                                         style="  max-width: 143px; max-height: 172px; margin: -21px 0px 28px 0px; ">
                                                    </div>
                                                    <div class="mb-7">
@if(isset($detail->product_size)))
                                                        <div class="d-flex justify-content-center align-items-center">
                                                            <span class="text-dark-75 font-weight-bolder mr-2">الحجم :  </span>
                                                            <span class="text-muted font-weight-bold">{{ $detail->product_size['name'] }}</span>
                                                            <span class="text-muted font-weight-bold">   *  {{ $detail->product_size['price'] }}</span>
                                                            <span class="text-muted font-weight-bold">  ل.س  </span>
                                                        </div>
@endif
                                                        @foreach($detail->product_extra as $extra)
                                                        <div class="d-flex justify-content-center align-items-center">
                                                            <span class="text-dark-75 font-weight-bolder mr-2">الزيادة :  </span>
                                                            <span class="text-muted font-weight-bold">{{ $extra['name'] }}</span>
                                                            <span class="text-muted font-weight-bold">   *  {{ $extra['price'] }}</span>
                                                            <span class="text-muted font-weight-bold">  ل.س  </span>
                                                        </div>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                </div>


                            </div>
                        </div>

                </div>
            </div>
        </form>
        </div>
        <!--end::Body-->
    </div>


</div>
@endsection

@section('scripts')
<script
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyABUP9n48lvINh62C_F2o04n8oiA-VI3CM&libraries=places&callback=initAutocomplete"
async defer>
</script>


<script>
    function initAutocomplete() {
        var markers = [];
        var input11 = document.getElementById("myloc").value
        var coords = input11.split(",");
        var input23 = new google.maps.LatLng(coords[0], coords[1]);
        var map = new google.maps.Map(document.getElementById('map'), {
            center: input23,
            zoom: 14,
            mapTypeId: 'roadmap'
        });

        placeMarker(map, map.getCenter());

        google.maps.event.addListener(map, 'click', function (event) {
            placeMarker(map, event.latLng);
        });

        function placeMarker(map, location) {
            deleteMarkers();
            var zoomL = map.getZoom();
            document.getElementById('zoomLevel').value = zoomL;
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            $("#myloc").val(location.lat() + ',' + location.lng());
            markers.push(marker);
        }

        function setMapOnAll() {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
            }
        }

        // Removes the markers from the map, but keeps them in the array.
        function clearMarkers() {
            setMapOnAll();
        }

        // Deletes all markers in the array by removing references to them.
        function deleteMarkers() {
            clearMarkers();
            markers = [];
        }

        // Create the search box and link it to the UI element.
        var input = document.getElementById('myloc');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }


            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
    }
</script>



    <script>
        function increaseValue(id,max) {
            var value = parseInt(document.getElementById(id).value, 10);
            value = isNaN(value) ? 0 : value;
            value++;
            if(value <= max)
                document.getElementById(id).value = value;
        }

        function decreaseValue(id) {
            var value = parseInt(document.getElementById(id).value, 10);
            value = isNaN(value) ? 0 : value;
            value < 1 ? value = 1 : '';
            value--;
            if(value >= 0)
                document.getElementById(id).value = value;
        }
    </script>

@endsection
