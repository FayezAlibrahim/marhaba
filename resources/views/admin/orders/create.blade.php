@extends('layouts.app')

@section('title',trans('products.create'))


@section('styles')

    <style>
        .myaccordion {

            margin: 50px auto;
            box-shadow: 0 0 1px rgba(0, 0, 0, 0.1);
        }

        .myaccordion .card,
        .myaccordion .card:last-child .card-header {
            border: none;
        }

        .myaccordion .card-header {
            border-bottom-color: #EDEFF0;
            background: transparent;
        }

        .myaccordion .fa-stack {
            font-size: 18px;
        }

        .myaccordion .btn {
            width: 100%;
            font-weight: bold;
            color: #004987;
            padding: 0;
        }

        .myaccordion .btn-link:hover,
        .myaccordion .btn-link:focus {
            text-decoration: none;
        }

        .myaccordion li + li {
            margin-top: 10px;
        }
    </style>


@endsection

@section('content')

    <div class="container">
        <form method="POST" action="{{ route('admin.products.store') }}" enctype="multipart/form-data">
            @csrf
            <h6 class="section-title h1">@lang('products.create')</h6>
            <div class="row">
                <div class="col">
                    <nav>
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-info-tab" data-toggle="tab" href="#nav-info"
                               role="tab" aria-controls="nav-info" aria-selected="true">@lang('products.info')</a>
                            <a class="nav-item nav-link" id="nav-tags-tab" data-toggle="tab" href="#nav-tags" role="tab"
                               aria-controls="nav-tags" aria-selected="false">@lang('products.related_info')</a>
                            <a class="nav-item nav-link" id="nav-images-tab" data-toggle="tab" href="#nav-images"
                               role="tab"
                               aria-controls="nav-images" aria-selected="false">@lang('products.images')</a>
                            <a class="nav-item nav-link" id="nav-attributes-tab" data-toggle="tab"
                               href="#nav-attributes"
                               role="tab" aria-controls="nav-attributes"
                               aria-selected="false">@lang('products.attributes')</a>
                        </div>
                    </nav>
                    <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-info" role="tabpanel"
                             aria-labelledby="nav-info-tab">


                            <div class="row">
                                <div class="form-group">
                                    <label class="col col-form-label text-md-left">@lang('products.name')</label>
                                    <div class="col">
                                        <input type="text" class="form-control @error('name') is-invalid @enderror"
                                               name="name">

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                {{-- <div class="form-group">
                                    <label class="col col-form-label text-md-left">@lang('products.sku')</label>
                                    <div class="col">
                                        <input type="text" class="form-control @error('sku') is-invalid @enderror"
                                               name="sku">

                                        @error('sku')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div> --}}

                                {{-- <div class="form-group">
                                    <label class="col col-form-label text-md-left">@lang('products.made_in')</label>
                                    <div class="col">
                                        <input type="text" class="form-control @error('made_in') is-invalid @enderror"
                                               name="made_in">

                                        @error('made_in')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div> --}}

                                <div class="form-group">
                                    <label class="col col-form-label text-md-left">@lang('products.currency')</label>
                                    <div class="col">
                                        <input type="text" class="form-control @error('currency') is-invalid @enderror"
                                               name="currency" value="{{ getCurrentCurrency(true) }}" readonly>

                                        @error('currency')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="col col-form-label text-md-left">@lang('products.description')</label>
                                    <div class="col">
                                    <textarea name="description"
                                              class="form-control @error('description') is-invalid @enderror" cols="20"
                                              rows="10"></textarea>

                                        @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group">
                                    <label class="col col-form-label text-md-left">@lang('products.unit_price')</label>
                                    <div class="col">
                                        <input type="text" class="form-control @error('name') is-invalid @enderror"
                                               name="unit_price">

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                {{-- <div class="form-group">
                                    <label
                                        class="col col-form-label text-md-left">@lang('products.unit_discount_price')</label>
                                    <div class="col">
                                        <input type="text" class="form-control @error('sku') is-invalid @enderror"
                                               name="unit_discount_price">

                                        @error('sku')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div> --}}


                            </div>

                            <div class="row">
                                {{-- <div class="form-group">
                                    <label
                                        class="col col-form-label text-md-left">@lang('products.stock_quantity')</label>
                                    <div class="col">
                                        <input type="text"
                                               class="form-control @error('stock_quantity') is-invalid @enderror"
                                               name="stock_quantity">

                                        @error('stock_quantity')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div> --}}

                                {{-- <div class="form-group">
                                    <label
                                        class="col col-form-label text-md-left">@lang('products.stock_status')</label>
                                    <div class="col">
                                        <select class="form-control" name="stock_status">
                                            <option value="in">@lang('products.in_stock')</option>
                                            <option value="out">@lang('products.out_of_stock')</option>
                                        </select>
                                    </div>
                                </div> --}}
                            </div>


                            <div class="row">
                                <div class="form-group">
                                    <label class="col col-form-label text-md-left">@lang('products.status')</label>
                                    <div class="col">
                                        <select class="form-control" name="status">
                                            <option value="active">@lang('products.active')</option>
                                            <option value="inactive">@lang('products.inactive')</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="tab-pane fade" id="nav-tags" role="tabpanel" aria-labelledby="nav-tags-tab">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="col col-form-label text-md-left">@lang('products.tags')</label>
                                    <div class="col">
                                        <input class="form-control" data-role="tagsinput" name="tags">

                                        @error('tags')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                </div>

                                <div class="form-group col-md-12">
                                    <label class="col col-form-label text-md-left">@lang('products.categories')</label>
                                    <div class="col">
                                        <select name="categories[]" class="form-control" id="product_categories_id"
                                                multiple>
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="tab-pane fade" id="nav-images" role="tabpanel" aria-labelledby="nav-images-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="file" class="form-control" id="images" name="images[]"
                                           onchange="preview_images();" multiple/>
                                </div>

                            </div>
                            <div class="row text-center text-lg-left" id="image_preview"></div>
                        </div>


                        <div class="tab-pane fade" id="nav-attributes" role="tabpanel"
                             aria-labelledby="nav-attributes-tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col col-form-label text-md-left">@lang('products.colors')</label>
                                        <div class="col">
                                            <input class="form-control" data-role="tagsinput" name="colors">

                                            @error('colors')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col col-form-label text-md-left">@lang('products.sizes')</label>
                                        <div class="col">
                                            <input class="form-control" data-role="tagsinput" name="sizes">

                                            @error('sizes')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col">
                                        <button type="submit" class="btn btn-primary">
                                            @lang('admin.create')
                                        </button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="tab-pane fade" id="nav-variations" role="tabpanel"
                             aria-labelledby="nav-variations-tab">

                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>

@endsection


@section('scripts')


    <script>
        function preview_images() {
            var total_file = document.getElementById("images").files.length;
            for (var i = 0; i < total_file; i++) {
                template = `<div class="col-lg-3 col-md-4 col-6">
                <a href="#" class="d-block mb-4 h-100">
                    <img class="img-fluid img-thumbnail" src="${URL.createObjectURL(event.target.files[i])}" alt="">
                </a>
            </div>`;
                $('#image_preview').append(template);
            }
        }


        $("#accordion").on("hide.bs.collapse show.bs.collapse", e => {
            $(e.target)
                .prev()
                .find("i:last-child")
                .toggleClass("fa-minus fa-plus");
        });

        $("#product_categories_id").select2();


    </script>
@endsection
