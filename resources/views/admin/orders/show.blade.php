@extends('layouts.app')

@section('title','')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        {{ '#'.$object->id }}
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">{{ $object->user->username ?? '' }}</h5>
                        <p class="card-text">{{ empty($object->order_note) ? "لا يوجد ملاحظات" : $object->order_note }}</p>
                        <p class="card-text">{{ $object->total_price . ' ' . trans('orders.SYP') }}</p>
                        <p class="card-text">{{ $object->delivery_address }}</p>
                        <p class="card-text">
                            <select class="form-control" onchange="changeOrderStatus(event,{{ $object->id }})">
                                @foreach($orderStatuses as $orderStatus)
                                    <option value="{{ $orderStatus }}" {{ $orderStatus == $object->status ? "selected" : "" }}>{{ trans('orders.'.$orderStatus) }}</option>
                                @endforeach
                            </select>
                        </p>
                        <a href="#" class="btn btn-danger">@lang('admin.delete')</a>
                        <a href="{{ route('admin.orders.index') }}" class="btn btn-warning">@lang('admin.back')</a>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
    <script>
        function changeOrderStatus(event,order_id) {

            var target = $(event.target).find("option:selected");
            var selected_value = target.val();
            url = '{!! route('admin.orders.status') !!}';

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: {_token: '{!! csrf_token() !!}', order_id : order_id , value : selected_value }
            })
                .done(function() {


                    swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.update_created_successfully') !!}", type: "success"},
                        function(){
                            // location.reload();
                        }
                    );


                })
                .fail(function(e) {

                    swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")

                })

        }
    </script>
@endsection
