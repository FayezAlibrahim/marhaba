@extends('layouts.app')

@section('title',trans('orders.orders'))

@section('content')
<!--begin::Card-->
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="container-fluid">
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h2 class="card-label">
                @lang('orders.orders')
                        {{-- <span class="d-block text-muted pt-2 font-size-sm">Datatable initialized from HTML table</span> --}}
            </h2>
        </div>
        <div class="card-toolbar">
           @if(Auth::check() && Auth::user()->role=='restaurant')
            <span class="label pulse pulse-success mr-10 max-h-70 max-w-70" title="عمال التوصيل المتاحين">
                <span class="position-relative" style="font-size: large;">{{ $restaurant_deliveries_staff }}</span>
                <span class="pulse-ring"></span>
            </span>
            @endif
											<!--begin::Dropdown-->
											<div class="dropdown dropdown-inline mr-2">
												<button type="button" class="btn btn-light-primary font-weight-bolder dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<span class="svg-icon svg-icon-md">
													<!--begin::Svg Icon | path:assets/media/svg/icons/Design/PenAndRuller.svg-->
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect x="0" y="0" width="24" height="24" />
															<path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z" fill="#000000" opacity="0.3" />
															<path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z" fill="#000000" />
														</g>
													</svg>
													<!--end::Svg Icon-->
												</span>Export</button>
												<!--begin::Dropdown Menu-->
												<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
													<!--begin::Navigation-->
													<ul class="navi flex-column navi-hover py-2">
														<li class="navi-header font-weight-bolder text-uppercase font-size-sm text-primary pb-2">تصدير طلبات:</li>
														<li class="navi-item">
															<a href="{{ route('admin.reports.sales') }}" data-filter="today" class="navi-link">
																<span class="navi-icon">
																	<i class="la la-print"></i>
																</span>
																<span class="navi-text">اليوم</span>
															</a>
														</li>
														<li class="navi-item">
															<a href="{{ route('admin.reports.sales','filter=yesterday') }}" data-filter="yesterday" class="navi-link">
																<span class="navi-icon">
																	<i class="la la-copy"></i>
																</span>
																<span class="navi-text">أمس</span>
															</a>
														</li>
														<li class="navi-item">
															<a href="{{ route('admin.reports.sales','filter=weekly') }}" data-filter="weekly" class="navi-link">
																<span class="navi-icon">
																	<i class="la la-file-excel-o"></i>
																</span>
																<span class="navi-text">الأسبوع الماضي</span>
															</a>
														</li>
														<li class="navi-item">
															<a href="{{ route('admin.reports.sales','filter=monthly') }}" data-filter="monthly" class="navi-link">
																<span class="navi-icon">
																	<i class="la la-file-text-o"></i>
																</span>
																<span class="navi-text">الشهر الماضي</span>
															</a>
														</li>
													</ul>
													<!--end::Navigation-->
												</div>
												<!--end::Dropdown Menu-->
											</div>
                                            <!--end::Dropdown-->

										</div>
          </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-9 col-xl-8">
                    <div class="row align-items-center">
                        <div class="col-md-8 my-2 my-md-0">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 my-2 my-md-0">
                            <div class="d-flex align-items-center">
                                <label class="mr-3 mb-0 d-none d-md-block">الحالة</label>
                                <select class="form-control" id="result_filter">
                                    <option value="all" {{ $filter == "all" ? "selected" : "" }}>@lang('admin.all')</option>
                     @foreach($orderStatuses as $orderStatus)
                        <option value="{{ $orderStatus }}" {{ $filter == $orderStatus ? "selected" : "" }}>{{ trans('orders.'.$orderStatus) }}</option>
                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <!--end::Search Form-->
        <!--end: Search Form-->
        <!--begin: Datatable-->
        <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
            <thead>
                <tr>
                    <th >#</th>
                    <th>@lang('orders.client')</th>
                    <th>@lang('orders.total_price')</th>
                    <th>الحالة</th>
                    <th>@lang('orders.deliveryman')</th>
                    <th>@lang('orders.delivered_at')</th>
                    <th>@lang('orders.created_at')</th>
                    <th>@lang('admin.settings')</th>

                </tr>
            </thead>
            <tbody>
                @foreach($orders as $order)
                <tr>
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->user->username ?? '' }}</td>
                        <td>{{ $order->total_price ?? '' }}</td>
                        <td>
                            <select class="form-control"  onchange="changeOrderStatus(event,{{ $order->id }})">
                                @foreach($orderStatuses as $orderStatus)
                                    <option value="{{ $orderStatus }}" {{ $orderStatus == $order->status ? "selected" : "" }}>{{ trans('orders.'.$orderStatus) }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>{{$order->delivery? $order->delivery->username:''  }} </td>
                        <td>{{ $order->delivered_at ?$order->delivered_at->format('H:i:s') : '' }}</td>
                        <td>{{ $order->created_at->format('d-m-Y') ?? '' }}
                            <span class="d-block text-muted pt-2 font-size-sm" style="font-weight: 600">{{ $order->created_at->format('H:i:s') ?? '' }}
                        </span></td>


                <td>
                    <span style="overflow: visible; position: relative; width: 125px;">
                        <div class="dropdown dropdown-inline">

                        <a href="{{ route('admin.orders.edit',$order->id)}}" class="btn btn-sm btn-light btn-text-primary btn-icon mr-2" title="Edit details">
                           <span class="svg-icon svg-icon-md">
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                 <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"></rect>
                                    <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "></path>
                                    <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"></rect>
                                 </g>
                              </svg>
                           </span>
                        </a>
                        <a href="#"class="btn btn-sm btn-light btn-text-primary btn-icon mr-2"  style="color: #dc3545;@if($order->status != "canceled") display:none;@endif" onclick="deleteItem('{{ $order->id }}','{{ route('admin.orders.destroy',$order->id) }}')">
                           <span class="svg-icon svg-icon-md">
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                 <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"></rect>
                                    <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"></path>
                                    <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"></path>
                                 </g>
                              </svg>
                           </span>
                        </a>
                     </span>
                </td>
                </tr>
                @endforeach

            </tbody>
        </table>
        <!--end: Datatable-->
    </div>

</div>

</div>

@endsection


@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable({
                "order": [],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });



        function deleteItem(id,url) {



                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method : 'delete' }

                    ,
                    success: function (response) {
                    location.reload();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);

                    }


            });
        }

        function changeOrderStatus(event,order_id) {

            var target = $(event.target).find("option:selected");
            var selected_value = target.val();
            url = '{!! route('admin.orders.status') !!}';

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: {
                    _token: '{!! csrf_token() !!}',
                     order_id : order_id ,
                      value : selected_value
                       }
                       ,
                       success: function (response) {
                       location.reload();
                       },
                       error: function(jqXHR, textStatus, errorThrown) {
                       console.log(textStatus, errorThrown);

                       }
            });


        }
        $('#result_filter').on('change', function() {
            url = '{!! route('admin.orders.index','@status@') !!}';
            status = $(this).find(":checked").val();
            url = url.replace('@status@','filter='+status);
            window.location.replace(url);

        });
    </script>

@endsection
