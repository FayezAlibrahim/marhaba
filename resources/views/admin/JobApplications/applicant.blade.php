@extends('layouts.app')

@section('title', 'Applicant')
@section('styles')

    <style>
        .wrapper-pic {
            position: relative;
            display: inline-block;
            opacity: 1;
        }

        .wrapper-pic:hover {
            transition: all .2s ease-in-out;
        }

        .wrapper-pic:hover .edit {
            transition: all .2s ease-in-out;
            display: block;
        }

        .edit {
            padding-top: 7px;
            padding-right: 7px;
            position: absolute;
            left: 20px;
            top: 10px;
            display: none;
        }

    </style>

@endsection
@section('content')


    <div class="container-fluid">
    
                    <div class="card card-custom">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">
                                  {{$job->title}}
                                </h3>
                            </div>
                        </div>

                        <div class="card-body">
                     
       <div class="row">
                                    @foreach ($job->applicants as $pdf)
                                        <div class="col-3" id="p{{ $pdf->id }}">
                                            <div class="row" style="justify-content: center;">
                                                <a href="{{ $pdf->path }}" target="_blanck"
                                                    style="max-width: 150px; max-height: 150px;"> <img
                                                        style="max-width: 60px;"
                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMsAAAD5CAMAAAC+lzGnAAAAzFBMVEX19fX/IRb///8sLCz1+/v/CwD6m5n6oJ317u38dHD/ZF3/Ggz/AAD1+Pj6+vooKCgeHh7a2toZGRkzMzOurq70//9DQ0PQ0NAkJCRPT0/AwMD/GAkTExNra2v24+L23dyfn5/3zcv5r6z+PTX+LyVjY2NYWFj8Z2H6j4v/7ez24N/21NL9WFL5qKX4u7j9U0z4wb/p6emEhIT+Myr9SkP7eHP4v737g375rKn8bGf7h4T/zcv6k4/+Qjr9S0QHBweRkZGGhoanp6d3d3fUHSfIAAAOzElEQVR4nO2da1visNaG09JAIQ2KWmWotgJVDi0FNwJy3LPH//+f3rRNegBUENLDe/F8mEunWHJ3rWStHAvEHfWMj1llPRZSkLmZd51+b7dMBwls/S47KxspEKVBQoQ0qJiV7ugMLL3SWIBaShyBNCi8L6snsjhjJS2DbAkptnM0TYSlN9MyQuJKQ+Vj603IsoQw7fLHBWHzlywfQoaM4guZpV+xrOJNlwbTUdzLEZz9gqUV9S+NNIuLYgpalDvteDyAkyNaAJ9laYY3gIq9NHQgpSAgy/1mtx2tuHB+JEsvfBaaObNUjEFawlhSjZkZxjhlLh/FMgkeBKw0UwQJeKbr0DSwfAxLK3gKaCKlTuIKy5OwWYWdA+sMYem1gz8rpg3BhLETgZkfFjUJS0lhfzNLGyEiPA0rMawcyNIb07/R7LTLH5M0GkdgDrEMEFuIRdllJupKINxUIjAH1BlQZY0YXElpl35L0jJiGftny4A+q/manC2zANcy5jFuBiyFZQtq2kXfldSMtGbDn9wMrJiLWZkzC3Bh2hE3039g6fiBEq37WWQB2Ar77HD4fToD2szF5LSLvV+StQ5hNt/WGcA+tsqkWYBrmXHEMv1DWEpZa5EDSVGY928skwMWIBmbiJtZuWYhMAI8wDK5YAHYGIaWaX9lmXywEMusI5b5AiYnLPE60zZyzQKkvh1aZr13FDA3LADrbfi9ZfLDQmA6AYw2XuaahcCEbqa1d2HyxLJlmZ06kysWAlNWQpjt2bN8sQAsV0IY08o1izsKGLgZEqa5ZiEwoZuhuJvljiVuGXOaaxYCMwstY7ZyzRJ3MzTNNQvAoBu6mTbINQuBibiZ0Mo1C3GziGVQK9csxDKrSAOwTIIFc5sxxGARyc34s2CsG4bOCQeDUtifmVc5s0jWwn5/r8wMTqPuuBu6mcOZZYCghpAGhS6v6ZAiGzhH4x5PFuwQEOguBnPn6DnBBA2AsuDIgvWNhkynb7jZE1yc9+bBl8isc4ZMgx+L1FJQ25IwJj+Qx9bi0+RjY03dDBY5sswh7HqepZKvQ2Ne9X9ADaN1etxYVBOZ/lSbVHSrzIhTjVGD+VmDHwtE7/4t8Uhx10FwMoz3pLzav+TFgvsQTiiLO72rVTjNu2GLTbiWuLFYpLowFpO0M+0mp/Bv0NlZOOPIwtoubHnfwKnCYH3os2hzniwOZWm6AQ1NOVV+mc2EV7iy+KX3WXhFGCCXYQIsA2oXJ+csQIZw4d/SbzXzzKJCWPZDijTJPYug2T6L6jWauWZ5h23K4n0XdHi1Y/xZSG5pesuFsOH3Mbi1yQmwdKHghXrJX19v8kouE2DBUwV9uPeUvBVq3HKYRFhYQobnblxGQz3HLEbbW5OGjY3bjHHLk5NgAfIcbgwM8NL07j/hNS6aBAsJ95DUEexXfVjMNUsLKe5NZ95XcesjJ8JCKow2VoHs98cVbss5k2ABkq0pOouUArfVz8mwDBRYUv2hBTjnNiWSCAvWFdhRba/Xx/qYHJQICwA2fHf8kQXEb1V6Mixuc7zxogu/qJ8Yi8UW48MFv5XcCfkYLrNR+AG/2dCEWCS2ndbklSSDxFgApl+z5rhZIDEWevMZxwn3pOrL1P8ahVdf31XCdR/ya5GTbpPRkOc+oYRY6Pwb3/20ybDIG7YN1ci7jwXpvsBGY7komZyfrYskvTGODVki/Urdn3pHdgtqNr9NtQn19/3g0pI7kN/QRTJjSv53oLEhWSZSuHVgEhnrY/VeBlJR0So5HreU6LI7zUv3y5BbkEnCLv52dWS69sD9dw1y6vLzZ5EcapaOF1qkkYLafCImfxZ5TlNkOl6pTiDkMzzOnQU3Tf8LxrT8WB9C2OUR/vnbhS6FhgPmV17D3OLgZdxZZIGaJezoSx/uQr/zw/Bm8dYlunedR8vehdr7+XMZ7nah2X5spgKT9gCef0UsZxZp5Nd8QYsVHOuCppVyxsJ2QW1nlNhaa/Dc6y/4stDFb4Jgbld1PEKaYEW+CRNFDob7zdJ/viwqa5AnO5ekAYRDwyPAQJZlXR85xW63u5iTf1atpi7Lx/LwZZHZKtt9GVgXwbnet6bOR7czHCtE4SmK5BdzXlzKRxWFK4tEh19Qe+uCaw0VdDRoD9sCZBBKIOhtZVBMuwiOKAxPFqz7U2GCUopVDAno1mhlkwojeJsv2sNKebH6aBmW6qpvWYNuZ+yWS1OEj4ywOOwwB5YXu+bQneLENt1HT0whoOFyafVlNVLfvUYANLvemWMIHn7aHlcfe0fRmo+xrDcXG1PwHIpYYz4oCZow0/eecojByB/xgMVDmwCOLHhJR8XMkQSwJDcHZaSQigAVuJmUHENSJdVpQ9h2wN7SSk2a/hw6ZcORRaUdF21Oml1rYY99txqXHUuXqUPh5hpqwmS/aVR/7TScH9hB4MeCWfoCneZMIAaBimDPPnQ1FggluUMsBUvGHhqal6LxgYMdHO0S7BS0NUgaWGX4sdSlnSJjuUgsA9dFXd2+xHLsQ49C5MbCRpJcJ4Na227pX2yxxLg5dPeTmRNjKzSqM/o0DlygyY0lKAgJeZOBsWuQUJJcEkjrDGHZkSMfw3Qx0MHztZxYMMnqKYrd6ss/nC2LsVV2jxeGwmbhpWjYa8FnX2QNibJgQMrGYot6yCG5WG1OTOiGRmW8GpF2TtZH7AgY5dACcWDBqjUz2dKEg4fCMGjOkKIh19falU7HS3E8lIMHoM7OIslWGYWH0R4xREm8alBpkzZPQJpGjxtHUFgc3Jc+MwvGo4kWnqy9p9/yrUhMbS02UPHOTif3UcYz5/BznM/KQprXipttdRbUw4SjxyfcJLrfLM0m9mY+Ky71Y25wRhZS4+fkiaLJUqa5PvrViJ7fWSbJ/7E95bOxkBo/GUNNGY7CnvE82VM/z8WCjYVbT+wpeZZNahWT5yKLPToPC5Y/2oqm2FOShWC5sqc3mYTOwULCtk2iwnrk5VNsK23SHnYWFqyvSNu1KfnDDKwHhcaJH118Ootq2SQQzGjdwDo9S1P5SPxollNZMCA5LqxYLKKxhBCWkz/t90QWDIh/oVUQ0VR6uon2znMRzxc6jQXLC9KBt4KxBxycCs5xieiXOokFy0MFdvTg87hPKwsspXH8+ml2WSjKLJIxqbTLoRyZUp5Jp7DglQK70ZRpwubz0jng/wQWbKF4qUv+DTROSxF+1Aks7lkpERS2NJzf/smf9HsWbJnReOgeNOTdSGsmHiSpfs8ifWhm6EzSlK7g4bYL/GedwFKCG/aGBQxaCl2MNE0N5RSWAWR2keQurfYmvx1UP+uE+tI04cSbO5VH7zREbtKq9p5Oacc6UFlYuuXY1L/gRE/RKqfFF8OEsD1ca5pPIgxSfgfOSXG/OVQ05M8qwuFRU75cdFI+JvWLbW8O25w46bqXpxNzfizL01FTlkH677s7Qx8Z8zuE81gltV8sCV1YsqkLSzZ1YcmmLizZ1IUlm7qwZFMXlmzqwpJNXViyqQtLNnVh2VH8pdjVajV68ZtLO9fDD6bFUr27vQ/05+nv3cNbWJZa5NLn30btbbuYf+539efheJhzsTxehapfP169PANWmNpN/NLTc02M/fXt1a5uG+mx3BTiql+/sCdbu966VL9/jtnmtrCrLLEUCtd/atW9LIXC1eNrLVLUTLJc/Zfo8fGm7sO8gCjLDblwc12noFEYn+X6MarrlFmu/rjtT+3h7pMW+Lkastw3arXG3d+XW5+mfv+2xfJci+ntyy9LisX7pQr+Xnlmeq0FLFf3D6Q5ropvjadHH/Q/cZaru1+0wtxZ3N8/vafvO0rA4l+ioIVC4EUZZ6k9+s9+l4XIh6k/MTfKNgsA91fef+xlefMuFq7Z/2Sd5cl1sqvbvSzVhtfq1T/FfLD4FeZmLwszzFWe7PIVC/h7HXWyrLO8evXlfj9LtVGPxJ/Ms/i5yusXLA+ek9X/lwcW8Z9Xu6//fuFjtT8eyxOIsDTcvg1VhlhEv6yF+sMXLG+eC9Zf3kKWwudzqFoGWLzivr39u41Gwz0s4KW+w3JVD/WLxPLMLIXb//2H6PX2xs9SaJH2sIh++PHztT05fwZYCt5DpelW4eYz0n/ZyxKzS9ZYoqqzRHgPS/Up8z7GRPr1Nzf/WOp4YN1/fQr1i4GLs7Pc3Lh9ytv7Py//wh791yyf2Y0vhfu7f//uGneNh1o0ROQ1Vu6LdftyGM+K13eZZtl3cY+PPftpAQ2J+WZ5yVXOH9FuX+zBN0te+mIR7bL4nZt6XvrIEW2z0JrPokueWcSG30POy5hSTDGWKkHxO5UvwQdyyOL2BmqNFz+J3hmDzRFL4fbp8/Pl9Z6Om8fmV3LH4vUHrmh/oF5oRD6QP5aIrl9jiXBOWUhvoP74+ByfkPB61Bliuf6GpR6ykN7AZ2N7nMXvv2SFBdQarvZ3od4aoR5qb7sjRv6l3429RHWutQrfjWtVq9+PfJ0yJhbVZd1FNnVhyaYuLNnUhSWb+n/J0gHs+FZ+LwHiLLnjb73XyqBNWbpZ2cd6pLA+pGdqzgD9SUv83LAzCRtj5lkgOLI0peNdTlVw9rSyBB/0x4PPkc6YpOA0XQNY9Eeu75njKMze+TfsgR57ASBM+ayH34m9l0mACxGI7CDqfLbK8pwehotahGVEoyWXl+bwFntFJil9lbCIa+Zkndyx4L4ZvE1adFla7DWTyiLtsh0prL+zdzQIPY+lGv5HKe3SHSWsV9g5x7AreiyiE5zhjGaZOZLjZ+H+hqGg9z5lEVm4IW7WsXLSmmHQGofFdkTG0gtP14btkqxm3Tjui0usCQoKrUzEgEVsmqGbQWHSNHQgZVeybjjD8Ehw0gD3IiziNIQk15RxpVvMriZDpESLu9bFKIvY0iJX3Rc4ZFnxoq77YpxFdMzYJ3IjaDOUkEVcRjwwN0JKpSfusoi9mZA3GrhuRTYBxrYGTm1F+/kGWRGCcGGIX7GI1eXQhLnAQVAbT3qxwos7WzabK1tTYKbbAaSRqFEe9LaLvmf7ac8YzCrrcdol/lLtzXy17Fd3C/5/auuonMoOb8UAAAAASUVORK5CYII=" />
                                                </a>
                                            </div>
                                            <div class="row"  style="justify-content: center;">
                                                <span>{{ $pdf->name }}</span>
                                         
                                            </div>
                                              <div class="row"  style="justify-content: center;">
                                               
                                                <span><br>{{ $pdf->created_at }}</span>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                        

                        </div>
                    </div>
            
    </div>


    <script>
        function openChangeImage() {
            $("#category_image_id").trigger('click');
        }

        function removeImage() {
            var default_image = "https://tsdist.com/wp-content/uploads/2018/07/Image_placeholder_1.jpg";
            $('#category_image').attr('src', default_image);
            $('#category_image_id').val('');
            change_image_flag('remove')
        }
        removeImage();

        function change_image_flag(flag) {
            $("#image_flag").attr('value', flag);
        }


        function previewImage(event) {
            var image_src = URL.createObjectURL(event.target.files[0]);
            $('#category_image').attr('src', image_src);
            change_image_flag('edit')

        }

    </script>
@endsection
