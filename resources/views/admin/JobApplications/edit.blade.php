@extends('layouts.app')
@section('title', 'Edit Jab Vacancy')
@section('styles')
<style>
   .wrapper-pic {
   position: relative;
   display: inline-block;
   opacity: 1;
   }
   .wrapper-pic:hover {
   transition: all .2s ease-in-out;
   }
   .wrapper-pic:hover .edit {
   transition: all .2s ease-in-out;
   display: block;
   }
   .edit {
   padding-top: 7px;
   padding-right: 7px;
   position: absolute;
   left: 20px;
   top: 10px;
   display: none;
   }
</style>
@endsection
@section('content')
<div class="container-fluid">
   @if ($errors->any())
   <div class="alert alert-custom alert-outline-danger fade show mb-5" role="alert"
      style="width: fit-content;margin-right:35%">
      <div class="alert-icon"><i class="flaticon-warning"></i></div>
      <div class="alert-text">
         <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
         </ul>
      </div>
      <div class="alert-close">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
         <span aria-hidden="true"><i class="ki ki-close"></i></span>
         </button>
      </div>
   </div>
   @endif
   <form id="create_form" method="POST" action="{{ route('admin.JobApplications.update') }}"
      enctype="multipart/form-data">
      @csrf
      <div class="card card-custom">
         <div class="card-header">
            <div class="card-title">
               <h3 class="card-label">
                  Update Job Info
               </h3>
            </div>
            <div class="card-toolbar">
               <button type="submit" class="btn btn-sm btn-primary font-weight-bold">
               Save
               </button>
            </div>
         </div>
         <div class="card-body">
            <div class="row ">
               <div class="col-sm-6">
                  <div class="form-group">
                     <label class="col-md-2 col-form-label text-center" data-required="yes">Title</label>
                     <div class="col-md-10 ">
                        <input type="text" style="direction: ltr;" class="form-control @error('title') is-invalid @enderror"
                           name="title" value="{{ $JobApplication->title }}" placeholder="title" required autofocus>
                        @error('title')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-2 col-form-label" data-required="yes" style="padding: 0px;margin: 0px;font-size: 0.9rem;margin-bottom: 7px;">Description</label>
                     <div class="col-md-10">
                        <textarea name="description"
                           style="direction: ltr;"   class="form-control @error('description') is-invalid @enderror" cols="20" rows="10"
                           placeholder="Description"     required autofocus>{{ $JobApplication->description }}</textarea>
                        @error('description')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                     </div>
                     <input type="hidden" name="id" value="{{ $JobApplication->id }}"/>
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="col-sm-12 wrapper-pic" style=" margin-top: 35px; ">
                     <img src="{{ $JobApplication->image }}" class="img-fluid" id="category_image">
                     <div class="edit">
                        <a onclick="event.preventDefault();openChangeImage()"
                           class="btn btn-success btn-xs">Edit</a>
                     </div>
                     <input type="file" id="category_image_id" onchange="previewImage(event)"
                        name="image" style="display:none">
                     <input type="hidden" name="image_flag" id="image_flag">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </form>
</div>
<script>
   function openChangeImage() {
       $("#category_image_id").trigger('click');
   }
   
   function removeImage() {
       var default_image = "https://tsdist.com/wp-content/uploads/2018/07/Image_placeholder_1.jpg";
       $('#category_image').attr('src', default_image);
       $('#category_image_id').val('');
       change_image_flag('remove')
   }
   
   function change_image_flag(flag) {
       $("#image_flag").attr('value', flag);
   }
   
   
   function previewImage(event) {
       var image_src = URL.createObjectURL(event.target.files[0]);
       $('#category_image').attr('src', image_src);
       change_image_flag('edit')
   
   }
   
</script>
@endsection