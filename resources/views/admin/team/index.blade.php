@extends('layouts.app')

@section('title','JOBs')

@section('content')
<div class="container">

@if(session('success'))
    <div class="alert alert-custom alert-outline-2x alert-outline-primary fade show mb-5" role="alert">
        <div class="alert-icon"><i class="flaticon-warning"></i></div>
        <div class="alert-text">{{session('success')}}</div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="ki ki-close"></i></span>
            </button>
        </div>
    </div>
@endif
<div class="row" style="justify-content: flex-end;">
    <a href="/admin/team/create" class="btn btn-primary" ><i class="fa fa-plus"></i>Add New</a>

</div><br>
   <div class="row">
       @foreach($JobApplications as $JobApplication)
    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6">
        <!--begin::Card-->
        <div class="card card-custom gutter-b card-stretch">
            <!--begin::Body-->
            <div class="card-header" >
           <lable class="text-md-right" style="margin-top: 25px;">
                   {{ $JobApplication->title ?? '' }}</lable>
                <div class="card-toolbar">
                    <a href="/admin/team/edit/{{ $JobApplication->id }}" class="btn btn-sm btn-light btn-text-primary btn-icon mr-2" title="Edit details">
                        <span class="svg-icon svg-icon-md">
                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                              <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                 <rect x="0" y="0" width="24" height="24"></rect>
                                 <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "></path>
                                 <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"></rect>
                              </g>
                           </svg>
                        </span>
                     </a>

                    <a href="#" onclick="$('#item_id').val({{ $JobApplication->id }});" data-toggle="modal" data-target="#delete_modal"  class="btn btn-sm btn-light btn-text-primary btn-icon" title="Delete">
                        <span class="svg-icon svg-icon-md">
                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                              <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                 <rect x="0" y="0" width="24" height="24"></rect>
                                 <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"></path>
                                 <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"></path>
                              </g>
                           </svg>
                        </span>
                     </a>
                </div>
            </div>
            <a href="/admin/team/edit/{{ $JobApplication->id }}" style=" color: inherit; ">
            <img class="card-img-top" src="{{$JobApplication->image}}" alt="Card image cap">
            <div class="card-body">
                <p class="card-text">
                    {{substr($JobApplication->description,0,30)}} @if(strlen(substr($JobApplication->description,31))>0)<p style="display:none" id="text{{$JobApplication->id }}">{{substr($JobApplication->description,31)}}</p>
                    <br> <a href="#"id="butto{{$JobApplication->id }}" onclick="hideshow2('text{{$JobApplication->id}}','butto{{$JobApplication->id}}')">Read more</a> @endif
                </p>
                <!--end::Desc-->

            </div>
            </a>
            <!--end::Body-->
        </div>
        <!--end::Card-->
    </div>
    @endforeach
   </div>

</div>


<!-- Modal-->
<div class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title">Delete confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this Application?
                <form action="/admin/team/destroy" id="delete_form" method="POST">
                    @csrf
                    <input type="hidden" id="item_id" name="id">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="$('#delete_form').submit()" class="btn btn-primary">Yes</button>

                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
@endsection

<script>
    function hideshow2(id,readid)
    {

      var moreText = document.getElementById(id);
      var btnText = document.getElementById(readid);

      if (moreText.style.display === "none") {
          btnText.text = "Read less";
          moreText.style.display = "inline";

      } else {
          btnText.text = "Read more";
          moreText.style.display = "none";

      }
    }
</script>
