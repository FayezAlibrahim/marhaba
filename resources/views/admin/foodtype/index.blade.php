@extends('layouts.app')

@section('title','أصناف المأكولات')
<style>

.modal.loading .modal-content:before {
    content: 'Loading...';
    text-align: center;
    line-height: 155px;
    font-size: 20px;
    background: rgba(0, 0, 0, .8);
    position: absolute;
    top: 55px;
    bottom: 0;
    left: 0;
    right: 0;
    color: #EEE;
    z-index: 1000;
}
</style>
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('flash-message')
    </div>
</div>
<div class="form-group row">;
    <div class="col-lg-9 col-md-9 col-sm-12">
     <a href="" class="btn btn-light-danger font-weight-bold" data-toggle="modal" data-target="#kt_switch_modal">إضافة جديد</a>
    </div>
   </div>
<input type="hidden" id="idHolder">
<div class="row">
    <div class="col-lg-10" style=" margin-right: 50px;">
        <div id="list">
        @foreach($foodtypes as $food_type)
     <!--begin::Card-->
     <div class="card card-custom gutter-b  listitem" id="{{ $food_type->id }}">
      <div class="card-header">
       <div class="card-title">
       <h3 class="card-label" style="direction: rtl;">{{$food_type->name}} ({{$food_type->products->count()}} وجبة) </h3>
       </div>
       <div class="card-toolbar">
        <a href="#" class="btn btn-icon btn-hover-light-primary ">
        <i class="ki ki-menu "></i>
        </a>
            <button class="btn btn-default"  data-href="/admin/delete/foodtype/{{$food_type->id}}" data-toggle="modal" data-target="#confirm-delete" onclick="$('#idHolder').val({{$food_type->id}})">
                 Delete record #{{$food_type->id}}
            </button>
       </div>
      </div>
     </div>
     <!--end::Card-->
     @endforeach
    </div>
     <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>
                <div class="modal-body">
                    <p>هل أنت متأكد من الحذف؟ <b><i class="title"></i></b> عند حذف هذاالتصنيف سوف يتم حذف جميع المنتجات</p>
                    <p>المتعلقة به?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger btn-ok">Delete</button>
                </div>
            </div>
        </div>
    </div>


    </div>

   </div>


<script>
    var KTCardDraggable = function () {
 return {
  //main function to initiate the module
  init: function () {
   var containers = document.querySelectorAll('.draggable-zone');

   if (containers.length === 0) {
    return false;
   }

   var swappable = new Sortable.default(containers, {
    draggable: '.draggable',
    handle: '.draggable .draggable-handle',
    mirror: {
     appendTo: 'body',
     constrainDimensions: true
    }
   });
  }
 };
}();

jQuery(document).ready(function () {
 KTCardDraggable.init();
});

</script>
<script>
    $('#confirm-delete').on('click', '.btn-ok', function(e) {
        var $modalDiv = $(e.delegateTarget);
        var id = $('#idHolder').val();
        $.ajax({url: '/admin/foodtypes/destroy/' + id, type: 'POST',data:{_token: '{!! csrf_token() !!}'}})
        // $.post('/api/record/' + id).then()
        $modalDiv.addClass('loading');
        setTimeout(function() {
            $modalDiv.modal('hide').removeClass('loading');
        }, 10000)
  location.reload();
    });
    $('#confirm-delete').on('show.bs.modal', function(e) {
        var data = $(e.relatedTarget).data();
        $('.title', this).text(data.recordTitle);
        $('.btn-ok', this).data('recordId', data.recordId);
    });
</script>

<script src="/custom/draggable/draggable.bundle.js" type="text/javascript"></script>

<link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" />
<script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script>
    $("#list").sortable();
    $("#list").sortable({
        stop: function(e, ui) {
          $("#list div").each(function(i, elm) {

            $elm = $(elm); // cache the jquery object
if($elm.index("#list .listitem")!=-1){
    console.log($elm.attr("id"));
$.ajax({
    url: '/admin/foodtype/proiority',
    type: 'POST',
    data: {id:$elm.attr("id"),priority:$elm.index("#list .listitem") , _token: '{!! csrf_token() !!}'}
}).done(function (data) {
    console.log('success');
}).fail(function (e) {
   console.log("error")
})

}
          });

        }
      });

</script>
								<!--begin::Modal-->
								<div class="modal fade" id="kt_switch_modal" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">إضافة تصنيف جديد</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<i aria-hidden="true" class="ki ki-close"></i>
												</button>
											</div>
											<form class="form form--fit"  method="POST" id="add_foodtype" action="/admin/products/addfoodtypes">
												<div class="modal-body">
@csrf
                                                    <div class="form-group row">
                                                        <label class="col-lg-3 col-form-label">اسم التصنيف</label>
                                                        <div class="col-lg-6">
                                                         <input type="text" name="name" class="form-control" placeholder="سندويش, مشروبات .." required/>
                                                         <span class="form-text text-muted">الاسم مطلوب</span>
                                                        </div>
                                                       </div>

												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-primary mr-2" data-dismiss="modal">إلغاء</button>
													<button type="submit" class="btn btn-secondary" data-dismiss="modal" onclick="$('#add_foodtype').submit();">إضافة</button>
												</div>
											</form>
										</div>
									</div>
								</div>
                                <!--end::Modal-->


@endsection





