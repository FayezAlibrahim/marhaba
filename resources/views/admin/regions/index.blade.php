@extends('layouts.app')

@section('title',trans('regions.regions'))

@section('content')
<!--begin::Card-->
<div class="container">
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h2 class="card-label">@lang('regions.regions')
            </h2>
        </div>
        <div class="card-toolbar">


            <!--begin::Button-->
            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#add_city_modal">
            <span class="svg-icon svg-icon-md">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <circle fill="#000000" cx="9" cy="15" r="6" />
                        <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>@lang('admin.add_new')</a>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->


        <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
            <thead>
                <tr>
                    <th title="Field #1" style="max-width: 50px !important">#</th>

                    <th title="Field #3">@lang('cities.name')</th>
                    <th >@lang('admin.settings')</th>
                </tr>
            </thead>
            <tbody>
                @foreach($regions as $region)
                <tr>
                    <td style="max-width: 50px !important">{{ $region->id }}</td>
                    <td>{{ $region->city->name ?? '' }}
                        <span class="text-muted font-weight-bold"> /  {{ $region->name ?? '' }}</span></td>

                <td>
                    <span style="overflow: visible; position: relative; width: 125px;">
                        <div class="dropdown dropdown-inline">
                            <a href="#" data-toggle="modal" data-target="#edit_modal" data-item_id="{{ $region->id }}" data-item_name="{{ $region->name }}" class="btn btn-sm btn-light btn-text-primary btn-icon mr-2" title="Edit details">
                           <span class="svg-icon svg-icon-md">
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                 <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"></rect>
                                    <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "></path>
                                    <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"></rect>
                                 </g>
                              </svg>
                           </span>
                        </a>
                        <a href="#"  onclick="deleteItem('{{ $region->id }}','{{ route('admin.regions.destroy',$region->id) }}')"  class="btn btn-sm btn-light btn-text-primary btn-icon" title="Delete">
                           <span class="svg-icon svg-icon-md">
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                 <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"></rect>
                                    <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"></path>
                                    <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"></path>
                                 </g>
                              </svg>
                           </span>
                        </a>
                     </span>
                </td>
                </tr>
                @endforeach

            </tbody>
        </table>
        <!--end: Datatable-->
    </div>

</div>
<!--end::Card-->
</div>


    <div class="modal" id="add_city_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">@lang('regions.create')</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col">
                            <form action="{{ route('admin.regions.store') }}" method="POST" id="add_city_form">
                                @csrf
                                <div class="form-group row">
                                    <label class="col-md-5 col-form-label text-md-left">@lang('regions.name')</label>

                                    <div class="col-md-12">
                                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" required autofocus>

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                {{-- <div class="form-group row">
                                    <label class="col-md-5 col-form-label text-md-left">@lang('regions.en_name')</label>

                                    <div class="col-md-12">
                                        <input type="text" class="form-control @error('english_name') is-invalid @enderror" name="english_name" required autofocus>

                                        @error('english_name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div> --}}
                                <div class="form-group row">
                                    <div class="col-md-12">

                                            <label class="col-md-4 col-form-label text-sm-left" >@lang('cities.cities')</label>
                                        <select name="city_id" class="custom-select" >
                                          @foreach( $cities as $city )
                                            <option value={{$city->id}}>{{$city->name}}</option>
                                          @endforeach
                                        </select>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">

                    <button type="button" onclick="$('#add_city_form').submit()" class="btn btn-primary">@lang('admin.save')</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('admin.close')</button>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">@lang('regions.edit')</h4>
                </div>
                <div class="modal-body">
                    <form action="#" id="edit_regions_form" method="POST">
                        @csrf
                        @method('PUT')
                        <input type="hidden" id="item_id">
                        <div class="form-group">
                            <input type="text" class="form-control" id="item_name" name="name">
                        </div>
                        {{-- <div class="form-group row">
                            <label class="col-md-5 col-form-label text-md-right">@lang('regions.en_name')</label>

                            <div class="col-md-12">
                                <input type="text" id="item_en_name" class="form-control @error('english_name') is-invalid @enderror" name="english_name" required autofocus>

                                @error('english_name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> --}}
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">@lang('admin.close')</button>
                    <button type="button" onclick="$('#edit_regions_form').submit()" class="btn btn-primary">@lang('admin.save')</button>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });



        function deleteItem(id,url) {

                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method : 'delete' },
                    success: function (response) {
                        location.reload();
                        },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);

                        }
                })

        }

        $('#edit_modal').on('show.bs.modal', function (event) {

            edit_url = '{!! route('admin.regions.update','@id@') !!}';
            var button = $(event.relatedTarget);
            var item_id = button.data('item_id');
            var item_name = button.data('item_name');
            // var item_en_name = button.data('english_name');
            url = edit_url.replace("@id@", item_id);

            var modal = $(this)
            modal.find('.modal-body #item_id').val(item_id);
            modal.find('.modal-body #item_name').val(item_name);
            // modal.find('.modal-body #item_en_name').val(item_en_name);
            $('#edit_regions_form').attr('action',url);
        })



    </script>

@endsection
