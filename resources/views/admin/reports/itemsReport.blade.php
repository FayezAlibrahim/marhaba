@extends('layouts.app')

@section('title',trans('reports.itemsReportDetails'))



@section('styles')

    <style type="text/css">
        .table thead th {
            border-bottom: none !important;
        }

        .removeRole {
            color: #dc3545;
            margin: 4px 0px 0px 4px;
            padding: 2px;
        }

        .editUser {
            visibility: hidden;
            cursor: pointer;
            font-size: 18px;
            color: #3e3e3e;
        }

        .editUser:hover {
            color: #212121;
            transition: 0.2s;
        }

        tr:hover .editUser {
            transition: 0.2s;
            visibility: visible;
        }


        /** Carusel Indicator **/
        .carousel-indicators li {
            position: relative;
            -ms-flex: 0 1 auto;
            flex: 0 1 auto;
            width: 30px;
            height: 3px;
            margin-right: 3px;
            margin-left: 3px;
            text-indent: -999px;
            background-color: rgba(0,0,0,0.2);
        }

        .carousel-indicators .active {
            background-color: rgba(0,0,0,0.4);
        }

        .saveGraphPng {
            position: absolute;
            top: 15px;
            right: 20px;
            color: #b0b0b0;
        }

        .saveGraphPng:hover {
            transition: 0.2s;
            color: #3e3e3e;
        }

        .totalResult span {
            background-color: #ddd;
            color: #3e3e3e;
            border-radius: 50px;
            padding: 2px 16px;
            float: left;
            font-size: 14px ;
        }
    </style>
@endsection
@section('content')


<div class="container-fluid">
    <div class="contents" style="margin: 0rem 2rem 0rem 0rem !important">
        <div class="row">
            <div class="col-12">
                <div class="contents" style="margin:2rem 3rem 2rem 6rem !important;">
                    <div class="card border-0 noBorderRadius">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-10" style="padding: 0 ; margin-top: 4px">
                                    <form id="filter-form">
                                        <div class="col text-left" id="myBtnFilter">
                                            <a  href="{{ route('admin.reports.items') }}" data-filter="today" class="btn btn-outline-secondary filter_item btn-sm active">اليوم</a>
                                            <a href="{{ route('admin.reports.items','filter=yesterday') }}" data-filter="yesterday"  class="btn btn-outline-secondary filter_item btn-sm">أمس</a>
                                            <a href="{{ route('admin.reports.items','filter=weekly') }}" data-filter="weekly" class="btn btn-outline-secondary filter_item btn-sm">الأسبوع الماضي</a>
                                            <!--button type="button" onclick="getInvoices('daily')" class="btn btn-outline-secondary filter_item btn-sm">Same Day Last Week</button-->
                                            <a href="{{ route('admin.reports.items','filter=monthly') }}" data-filter="monthly" class="btn btn-outline-secondary filter_item btn-sm">الشهر الماضي</a>
                                            <!--button type="button" onclick="getInvoices('daily')" class="btn btn-outline-secondary filter_item btn-sm">Same Day Last Month</button-->
                                            <button type="button" class="btn btn-outline-secondary filter_item btn-sm" data-filter="custom" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample"> متقدم <i class="fas fa-search-plus"></i></button>
                                        </div>
                                        <div class="col text-left">
                                            <span>{{ 'من : ' . $from_date->format("F j, Y, g:i a") . '  الى : ' . $to_date->format("F j, Y, g:i a")}}</span>
                                        </div>
                                        <input type="hidden" name="filter" id="filter">
                                        <div class="collapse" id="collapseExample" style="margin-top: 1rem">
                                            <div class="row" style="margin: 0">
                                                <div class="col">
                                                    <input class="form-control" id="from-date" name="from_date" type="text" placeholder="من .." autocomplete="off">
                                                </div>
                                                <div class="col">
                                                    <input class="form-control" id="to-date" name="to_date" type="text" placeholder="الى .." autocomplete="off">
                                                </div>
                                                <div class="col">
                                                    <button type="button" onclick="get_report()" class="btn btn-secondary">انشاء</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-2">
                                    <a  href="{{ route('admin.reports.items',['filter' => request()->filter,'export' => 'yes']) }}"><i class="fas fa-file-excel" style="font-size: 50px;color: green"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="contents" id="reportTable" style="margin: 0 3rem 2rem 6rem !important;">
        <div class="card">
            <div class="card-body">
                <h4>@lang('reports.itemsReportDetails')</h4>
                <hr>
                <table class="table table-hover table-striped" id="data-table">
                    <thead>
                    <tr>
                        <th scope="col">@lang('orders.client')</th>
                        <th scope="col">@lang('orders.item_name')</th>
                        <th scope="col">@lang('orders.paid_price')</th>
                        <th scope="col">@lang('orders.quantity')</th>
                        <th scope="col">@lang('orders.total_price')</th>
                        <th scope="col">@lang('orders.attributes')</th>
                        <th scope="col">@lang('orders.order_date')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr>
                            <td>{{ $item["client"] ?? '' }}</td>
                            <td>{{ $item["item_name"] ?? '' }}</td>
                            <td>{{ $item["paid_price"] ?? '' }}</td>
                            <td>{{ $item["quantity"] ?? '' }}</td>
                            <td>{{ $item["total_price"] ?? '' }}</td>
                            <td >
                                @foreach($item["attributes"] as $key => $value)
                                @if(strpos($value, "#") ==  0 && $key == "color")

                                        <p>{{$key}} :</p>
                                        <span class="color-btn" style="background-color:{{$value ?? "Value"}}; width:24px;float:right"></span>

                                        <br>
                                @else
                                <br>
                                <p style="float: right">{{$value}} :</p>
                                <p > {{$key}}</p>

                                @endif
                                @endforeach
                            </td>
                            <td>{{ $item["order_date"] ?? '' }}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

@endsection

@section('scripts')



    <script>

        $('#from-date').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        });
        $('#to-date').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        });


        function submitFilter (filter) {
            $('#filter').val(filter);
            //$('#filter-form').submit();
        }

        function get_report() {
            var from_date = $('#from-date').val();
            var to_date = $('#to-date').val();

            url = '{!! route('admin.reports.items') !!}';
            url = url + '?from_date='+from_date+'&to_date='+to_date;
            window.location.replace(url);
        }

        $('.filter_item').removeClass('active');
        var filter_name = '{!! $filter_name !!}';
        $( `.filter_item[data-filter='${filter_name}']` ).addClass('active')
    </script>


@endsection
