@extends('layouts.app')
@section('title',trans('users.users'))
@section('content')

<div class="container">

    <div class="card card-custom">
        <div class="card-header">
            <div class="card-title">
                <span class="card-icon">
                    <i class="flaticon2-chat-1 text-primary"></i>
                </span>
                <h2 class="card-label">العنوان</h2>

            </div>
            <div class="card-toolbar">
                <a onclick="$('#add_address_form').submit();" class="btn btn-primary">@lang('admin.update')</a>
            </div>

        </div>
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <form action="/admin/address/store" method="POST" id="add_address_form">
                        @csrf
                        <div class="form-group row">
                            @if($address)
                            <input type="hidden" name="address_id" value="{{ $address->id??'' }}"/>

                            @endif
                            <input type="hidden" name="user_id" value="{{ $user_id }}"/>

                            <label class="col-lg-4 text-right col-form-label"data-required="yes">@lang('users.city')</label>
                            <div class="col-lg-6">
                                <select id="city_drop" name="city_id" class="form-control form-control-lg "
                                        onfocus="GetRegion(this.value)" onchange="GetRegion(this.value)">
                                       @foreach($cities as $city)

                                       <option @if($address && ($city->id==$address->city_id))  selected @endif value="{{ $city->id }}">{{ $city->name ?? '' }}</option>

                                       @endforeach

                                </select>
                            </div>

                        </div>
<div class="form-group row">
    <label class="col-lg-4 text-right col-form-label"data-required="yes">@lang('users.region')</label>
                            <div class="col-lg-6">
                                <select id="region_drop" name="region_id" class="form-control form-control-lg ">

                                </select>
                            </div>
</div>
                        <div class="form-group row">
                        <label class="col-lg-4 text-right col-form-label"data-required="yes">العنوان</label>
                        <div class="col-lg-6">
                        <textarea name="address" class="form-control form-control-lg form-control-solid" id="exampleTextarea" rows="3" placeholder="Type notes">
                            {{ $address->address??'' }}
                        </textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-4 text-right col-form-label">الموقع</label>
                        <div class="col-lg-8">

                            <input id="zoomLevel" name="zoom" type="hidden" value="12"/>
                            @if(!empty($address->lat))
                            <input id="myloc" name="map_location" type="hidden" value="{{ $address->lat }},{{ $address->lan }}" type="text"/>
                            @else
                            <input id="myloc" name="map_location" type="hidden" value="33.4967793,36.2476908"type="text"/>


                            @endif
                            <div id="map" style="width: 450px; height: 200px;margin:10px"></div>
                        </div>

                    </div>


                    </form>
                </div>
            </div>
            </div>
    </div>

</div>



@endsection
@section('scripts')
    <script>

        $(document).ready(function () {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });


        function changeStatus(id) {
            var url = '{{ route('admin.users.status') }}';
            $.ajax({
                url: url,
                type: 'POST',
                data: {id: id, _token: '{!! csrf_token() !!}'}
            }).done(function (data) {
            }).fail(function (e) {
                swal("{!! trans('admin.fail') !!}", e.responseJSON.message, "error")
            })
        }


        function deleteItem(id, url) {
            swal({
                title: '{!! trans('admin.are_you_sure') !!}',
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "{!! trans('admin.yes'); !!}",
                cancelButtonText: "{!! trans('admin.no'); !!}",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {


                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method: 'delete'}
                })
                    .done(function () {


                        swal({
                                title: "{!! trans('admin.done') !!}",
                                text: "{!! trans('admin.deleted_successfully') !!}",
                                type: "success"
                            },
                            function () {
                                location.reload();
                            }
                        );


                    })
                    .fail(function (e) {

                        swal("{!! trans('admin.fail') !!}", e.responseJSON.message, "error")

                    })


            });
        }


    </script>
    <script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyABUP9n48lvINh62C_F2o04n8oiA-VI3CM&libraries=places&callback=initAutocomplete"
    async defer></script>
    <script>

        function initAutocomplete() {
            var markers = [];
            var input11 = document.getElementById("myloc").value
            var coords = input11.split(",");
            var input23 = new google.maps.LatLng(coords[0], coords[1]);
            var map = new google.maps.Map(document.getElementById('map'), {
                center: input23,
                zoom: 14,
                mapTypeId: 'roadmap'
            });

            placeMarker(map, map.getCenter());

            google.maps.event.addListener(map, 'click', function (event) {
                placeMarker(map, event.latLng);
            });

            function placeMarker(map, location) {
                deleteMarkers();
                var zoomL = map.getZoom();
                document.getElementById('zoomLevel').value = zoomL;
                var marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
                $("#myloc").val(location.lat() + ',' + location.lng());
                markers.push(marker);
            }

            function setMapOnAll() {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(null);
                }
            }

            // Removes the markers from the map, but keeps them in the array.
            function clearMarkers() {
                setMapOnAll();
            }

            // Deletes all markers in the array by removing references to them.
            function deleteMarkers() {
                clearMarkers();
                markers = [];
            }

            // Create the search box and link it to the UI element.
            var input = document.getElementById('myloc');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
            });

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }


                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location
                    }));

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });
        }
    </script>
    <script>
        $('#edit_address_modal').on('show', function(e) {
            var link     = e.relatedTarget(),
                modal    = $(this),
                address = link.data('address'),
                city_id    = link.data('city_id'),
                region_id= link.data('region_id'),
                lat=link.data('lat'),
                lot=link.data('lon');

            modal.find("#address_mod").val(address);
            modal.find("#city_id_mod").val(city_id);
            modal.find("#redion_id_mod").val(region_id);
            modal.find("#lat_mod").val(lat);
            modal.find("#lon_mod").val(lon);
        });
    </script>
    <script>
          function GetRegion(id) {
        var regions = document.getElementById("region_drop");
        $("#region_drop").empty();
        $.ajax(
            {

                type: "GET",
                url: "/admin/city/" + id + "/regions",
                dataType: "text",
                data: "",
                success: function (response) {
                    var JSONArray = $.parseJSON(response);
                    var str = '';
                    //  console.log(response);
                    for (var i = 0; i < JSONArray.length; i++) {
                        var opt = document.createElement('option');
                        opt.value = JSONArray[i]['id'];
                        opt.innerHTML = JSONArray[i]['name'];
                        if({{ $address->region_id ?? '-1'}}== JSONArray[i]['id'])
                                opt.selected=true;
                        regions.appendChild(opt);
                    }
                }
            }
        );
    }

</script>

@if($address)
<script>
    GetRegion({{$address->city_id}});
</script>
@else
<script>
GetRegion({{$cities[0]->id}});
</script>
@endif
@endsection

