@extends('layouts.app')

@section('title',trans('users.create'))

@section('content')

    <div class="container-fluid">
                <form method="POST" action="{{ route('admin.users.store') }}" id="form">
                    @csrf
                    <div class="row">
                        <div class="col-lg-8">
                        
                        <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right" data-required="yes">@lang('users.role')</label>
                                <div class="col-lg-8">
                                    <select name="role" id="role" class="form-control">
                                        <option value="client">@lang('users.client')</option>
                                        <option value="admin">@lang('users.admin')</option>
                                        <option value="delivery">@lang('users.delivery')</option> 
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group row" id="phoneContainer">
                                <label class="col-lg-2 col-form-label text-lg-right" data-required="yes">@lang('users.phone')</label>

                                <div class="col-lg-8">
                                    <input type="text" class="form-control  @error('phone') is-invalid @enderror"
                                    required
                                           name="phone"
                                           id="phone"
                                           >

                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right" data-required="yes">@lang('users.username')</label>

                                <div class="col-lg-8">
                                    <input type="text" class="form-control @error('username') is-invalid @enderror"
                                           name="username" required>

                                    @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right" data-required="yes">@lang('users.password')</label>

                                <div class="col-lg-8">
                                    <input type="password" class="form-control @error('password') is-invalid @enderror"
                                           name="password" required>

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row"  id="addressContainer" style="display:none">
                                <label class="col-lg-2 col-form-label text-lg-right">@lang('users.address')</label>

                                <div class="col-lg-8">
                                    <textarea name="address" class="form-control @error('address') is-invalid @enderror"
                                              cols="7" rows="4" placeholder="(optional)"></textarea>

                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row" id="emailContainer" style="display:none">
                                <label class="col-lg-2 col-form-label text-lg-right" data-required="yes">@lang('users.email')</label>

                                <div class="col-lg-8">
                                    <input type="email" class="form-control @error('email') is-invalid @enderror"
                                    id="email"  name="email" autocomplete="off" placeholder="Aaaa@bb.cc">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>



                            <div class="form-group row mb-0">
                                <div class="col-lg-8 offset-lg-2">
                                    <button type="submit" class="btn btn-primary">
                                        @lang('admin.create')
                                    </button>

                                </div>
                            </div>
                        </div>


                        <div class="col-lg-4">




                            <div class="form-group row" id="cityContainer">
                                <label class="col-lg-2 col-form-label text-lg-right" data-required="yes">@lang('users.city')</label>

                                <div class="col-lg-10">
                                    <select 
                                    onfocus="GetRegions(this.value)"
                                     onchange="GetRegions(this.value)"
                                      id="city_drop" name="city_id" class="form-control">
                                        @foreach($cities as $city)
                                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row" id="regionContainer">
                                <label class="col-lg-2 col-form-label text-lg-right" data-required="yes">@lang('users.region')</label>

                                <div class="col-lg-10">
                                    <select id="region_drop" name="region_id" class="form-control">

                                    </select>
                                </div>
                            </div>

                            <div class="form-group row" id="deliveryTypeContainer" style="display:none">
                                <label class="col-lg-2 col-form-label text-lg-right" data-required="yes">@lang('users.delivery_type')</label>
                                <div class="col-lg-8">
                                    <select name="delivery_type" id="delivery_type" class="form-control" onchange="">
                                        <option value="general">@lang('users.general')</option>
                                        <option value="specific">@lang('users.specific')</option>
                                    </select> 
                                </div>
                            </div>

                            <div class="form-group row" id="restaurantsContainer" style="display:none">
                                <label class="col-lg-2 col-form-label text-lg-right" data-required="yes">المطعم</label>
                                <div class="col-lg-8">
                                    <select name="rest_id" id="rest_id" class="form-control" onchange="">
                                    @foreach($restaurants as $rest)
                                        <option value="{{$rest->id}}">{{$rest->user->username}}</option>
                                        @endforeach
                                    </select> 
                                </div>
                            </div>




                        </div>

                    </div>


                </form>
    </div>
    <script>
                                function GetRegions(id) {
                                   var  regions = document.getElementById("region_drop");
                                    $("#region_drop").empty();
                                    $.ajax(
                                        {
                                            type:"GET",
                                            url : "/admin/city/"+id+"/regions",
                                            dataType: "text",
                                            data : "",
                                            success: function(response)
                                            {
                                                var JSONArray = $.parseJSON( response );
                                                var str = '';
                                              //  console.log(response);
                                                 for (var i = 0; i < JSONArray.length; i++)
                                                    {
                                                        var opt = document.createElement('option');
                                                        opt.value = JSONArray[i]['id'];
                                                        opt.innerHTML = JSONArray[i]['name'];;
                                                        regions.appendChild(opt);
                                                    }
                                            }
                                        }
                                    );
                                }
 
                                $('#role').change(function(){
                                    if($(this).val() == "client"){
                                        $('#emailContainer').hide();
                                        $('#restaurantsContainer').hide();
                                        $('#deliveryTypeContainer').hide();
                                        $('#phoneContainer').show();
                                        $('#phone').attr('required',true);
                                        $('#email').removeAttr('required');
                                        $('#cityContainer').show();
                                        $('#regionContainer').show();
                                    }
                                    else if($(this).val() == "admin"){
                                        $('#emailContainer').show();
                                        $('#phoneContainer').hide();
                                        $('#restaurantsContainer').hide();
                                        $('#deliveryTypeContainer').hide();
                                        $('#phone').removeAttr('required');
                                        $('#email').attr('required',true);
                                        $('#cityContainer').hide();
                                        $('#regionContainer').hide();
                                        }
                                        else if($(this).val() == "delivery"){
                                            $('#emailContainer').hide();
                                        $('#phoneContainer').show();
                                        $('#restaurantsContainer').hide();
                                        $('#deliveryTypeContainer').show();
                                        $('#phone').attr('required',true);
                                        $('#email').removeAttr('required');
                                        $('#cityContainer').show();
                                        $('#regionContainer').hide();
                                        }
                                    })

                                    $('#delivery_type').change(function(){
                                    if($(this).val() == "general"){
                                        $('#restaurantsContainer').hide();
                                    }
                                    else if($(this).val() == "specific"){
                                        $('#restaurantsContainer').show();
                                    }
                                    })
                            </script>
@endsection
