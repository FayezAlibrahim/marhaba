@extends('layouts.app')
@section('title',trans('users.users'))
@section('content')
<div class="container">

    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">   عمال التوصيل
          </h3>
            </div>
            <div class="card-toolbar">
                <div class="col-md-10 my-2 my-md-0">
                    <div class="input-icon">
                        <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
                        <span>
                            <i class="flaticon2-search-1 text-muted"></i>
                        </span>
                    </div>
                </div>

                <!-- <div class="col-md-2 my-2 my-md-0">
                    <div class="input-icon">
                    <a href="{{ route('admin.users.create') }}" class="btn btn-light-warning font-weight-bolder btn-sm">Add New</a>
                    </div>
                </div> -->

            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
                <thead>
                    <tr>
                        <th title="Field #1">#</th>
                        <th title="Field #2">@lang('users.username')</th>
                        <th title="Field #3">@lang('users.phone')</th>

                        <th title="Field #4">@lang('users.city')</th>
                        <th title="Field #5">@lang('users.active')</th>
                        <th title="Field #6">
                            <small>الحالة</small>
                        </th>
                        <th title="Field #7">@lang('users.settings')</th>


                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->user->username ?? '' }}</td>
                        <td> {{ $user->user->phone ?? '' }} </td>

                      <td>  {{ $user->user->city->name ?? '' }}  </td>
                        <td class="text-right" >
                            @if( $user->availability == "1")
                         <span  style="width: 110px;"><span class="label font-weight-bold label-lg label-light-primary label-inline">متاح</span></span>
                        @else
                        <span style="width: 110px;"><span class="label font-weight-bold label-lg label-light-danger label-inline">غير متاح</span></span>
                        @endif
                        </td>
                        </td>

<td style="width: 131px !important;padding: 0px;">
@if( $user->type == 'general')
                         <span  style="width: 131px !important;padding: 0px;">
                             <span class="label font-weight-bold label-lg label-light-primary label-inline" style="width: 130px;height:30px;font-size: smaller;">توصيل عام ضمن المدينة</span></span>
                        @else
                        <span style="width: 131px !important;padding: 0px;"><span class="label font-weight-bold label-lg label-light-info label-inline" style="width: 130px;height:30px;font-size: smaller;">توصيل لمطعم {{$user->restaurant->user->username}}</span></span>
                        @endif
</td>
<td>
    <span style="overflow: visible; position: relative; width: 150px;">
 <a href="{{ route('admin.delivery.edit',$user->id) }}"
    class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2"
    title="Edit details">
    <span class="svg-icon svg-icon-md">
       <svg xmlns="http://www.w3.org/2000/svg"
            xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
            height="24px" viewBox="0 0 24 24" version="1.1">
          <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
             <rect x="0" y="0" width="24" height="24"></rect>
             <path
                 d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z"
                 fill="#000000" fill-rule="nonzero"
                 transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) "></path>
             <path
                 d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z"
                 fill="#000000" fill-rule="nonzero" opacity="0.3">
             </path>
          </g>
       </svg>
    </span>
 </a>
    </span>
</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>



</div>


<!-- Modal-->
<div class="modal fade" id="exampleModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('admin.delete')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form action="/api/users/delete/user" method="POST">
                {{ method_field('POST') }}
            <div class="modal-body">
                @csrf
               <input type="hidden" name="id" id="delete_id"/>
@lang('admin.are_you_sure')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">@lang('admin.no')</button>
                <button type="submit"  class="btn btn-primary font-weight-bold">@lang('admin.yes')</button>
            </div>
        </form>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });


        function changeStatus(id) {
            var url = '{{ route('admin.users.status') }}';
            $.ajax({
                url: url,
                type: 'POST',
                data: {id: id, _token: '{!! csrf_token() !!}'}
            }).done(function (data) {
            }).fail(function (e) {
                swal("{!! trans('admin.fail') !!}", e.responseJSON.message, "error")
            })
        }


        function deleteItem() {
var $id=$('#delete_id').val();
var url="/admin/users/destroy/"+$id;
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method: 'delete'}
               ,
               success: function (response) {
                location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);

                }


            });
        }


    </script>
@endsection

