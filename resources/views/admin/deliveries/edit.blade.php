@extends('layouts.app')

@section('title', 'تعديل عمال التوصيل')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form method="POST" id="update_form" action="{{ route('admin.delivery.update', $user->id) }}">
                    @csrf
                <div class="card">
                    <div class="card-header" style="
                        font-weight: 800;
                        font-size: medium;">
                       تعديل @lang('users.delivery')

                       <div class="card-toolbar">
                            <button type="submit"  class="btn btn-primary" style="float: left;
                                 margin-top: -20px;">
                                @lang('admin.edit')
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-form-label text-md-right">@lang('users.username')</label>

                                        <input type="text" class="form-control @error('username') is-invalid @enderror"
                                            name="username" required value="{{ $user->user->username }}">

                                        @error('username')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    {{-- <div class="form-group">
                                        <label class="col-form-label text-md-right">@lang('users.email')</label>

                                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" autocomplete="off">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div> --}}
                                    {{-- <div class="form-group ">
                                        <label class="col-form-label text-md-right" data-required="yes">@lang('users.wallet')</label>

                                            <input type="number"
                                                   class="form-control @error('wallet') is-invalid @enderror"
                                                   name="wallet" value="{{ $user->wallet }}" required>
                                            @error('wallet')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                    </div> --}}
                                    <div class="form-group">
                                        <label class="col-form-label text-md-right">@lang('users.phone')</label>

                                        <input type="text" required class="form-control @error('phone') is-invalid @enderror"
                                            name="phone" value="{{ $user->user->phone }}" autocomplete="off">

                                        @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label text-md-right">@lang('users.password')</label>

                                        <input type="password" class="form-control @error('password') is-invalid @enderror"
                                            name="password" value="" autocomplete="off">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-form-label text-md-right">@lang('users.city')</label>

                                        <select name="city_id" required onchange="GetRegions(this.value)" class="form-control">
                                            @foreach ($cities as $city)
                                                <option value="{{ $city->id }}"
                                                    {{ isset($user->user->city) && $user->user->city->id == $city->id ? 'selected' : '' }}>
                                                    {{ $city->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label text-md-right">متاح</label>
                                        <select name="availability"  required class="form-control">
                                            <option value="1" {{ $user->availability == '1' ? 'selected' : '' }}>
                                               متاح</option>
                                            <option value="0" {{ $user->availability == '0' ? 'selected' : '' }}>
                                               غير متاح</option>
                                        </select>
                                    </div>
                                    <div class="form-group" id="deliveryTypeContainer">
                                        <label class="col-form-label text-md-right" >@lang('users.delivery_type')</label>

                                            <select name="delivery_type" required id="delivery_type" class="form-control" onchange="">
                                                <option {{ $user->type == 'general' ? 'selected' : '' }} value="general">@lang('users.general')</option>
                                                <option {{ $user->type == 'specific' ? 'selected' : '' }}  value="specific">@lang('users.specific')</option>
                                            </select>
                                    </div>
                                    <div class="form-group" id="restaurantsContainer" @if($user->type == 'general') style="display:none" @endif>
                                        <label class="col-form-label text-md-right">المطعم</label>
                                            <select name="rest_id" required id="rest_id" class="form-control" onchange="">
                                            @foreach($restaurants as $rest)
                                                <option {{ $user->staurant_id == $rest->id ? 'selected' : '' }}  value="{{$rest->id}}">{{$rest->user->username}}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="row">
                                <div class="col-md-12">
                                    <h3><i class="fa fa-lock"></i> @lang('sidebar.roles')</h3>
                                </div>

                                @foreach ($roles as $role)
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" name="roles[]" class="custom-control-input" value="{{ $role->id }}" @if (in_array($role->id, $user_roles)) checked @endif id="customCheck{{ $role->id }}">
                                                <label class="custom-control-label" for="customCheck{{ $role->id }}">{{ $role->name }}</label>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach

                            </div> --}}

                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
<script>
    $('#delivery_type').change(function(){
        if($(this).val() == "general"){
            $('#restaurantsContainer').hide();
        }
        else if($(this).val() == "specific"){
            $('#restaurantsContainer').show();
        }
        })
</script>
@endsection
