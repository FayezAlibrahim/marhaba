@extends('layouts.app')

@section('title',trans('categories.create'))

@section('content')





    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">@lang('categories.create')</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.categories.store') }}">
                            @csrf

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">@lang('admin.categories.name')</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" required autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">@lang('admin.categories.description')</label>

                                <div class="col-md-6">
                                    <textarea name="description" class="form-control @error('description') is-invalid @enderror" cols="20" rows="10"></textarea>

                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">@lang('categories.status')</label>

                                <div class="col-md-6">
                                    <select name="status" class="form-control">
                                        <option value="active">@lang('categories.active')</option>
                                        <option value="inactive">@lang('categories.inactive')</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">@lang('categories.secondary')</label>

                                <div class="col-md-6">
                                    <input type='checkbox' name="parent_id_checked" data-toggle='collapse' data-target='#mainCategories'>
                                </div>

                            </div>

                            <div class="form-group row collapse" id="mainCategories">
                                <label class="col-md-4 col-form-label text-md-right">@lang('categories.select_category')</label>
                                <div class="col-md-6 " >
                                    <select name="parent_id" class="form-control" >
                                       @foreach($mainCategories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>



                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                       @lang('admin.create')
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
