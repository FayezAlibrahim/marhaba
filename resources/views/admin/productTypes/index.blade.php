@extends('layouts.app')

@section('title',trans('productTypes.productTypes'))

@section('content')

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">@lang('admin.productTypes')</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class=" table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>@lang('productTypes.id')</th>
                            <th>@lang('productTypes.name')</th>
                            <th>@lang('admin.settings')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($productTypes as $city)
                            <tr class="odd gradeX">
                                <td>{{ $city->id }}</td>
                                <td>{{ $city->name ?? '' }}</td>
                                <td>
                                    <a href="{{ route('admin.productTypes.edit',$city->id) }}"><i class="fa fa-edit"></i> </a> &nbsp;
                                    <a href="#" style="color: #dc3545" onclick="event.preventDefault();deleteItem('{{ $city->id }}','{{ route('admin.productTypes.destroy',$city->id) }}')"><i class="fa fa-trash"></i></a>&nbsp;

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
                <br>
                <div class="panel-footer">
                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#add_city_modal"><i class="fa fa-plus"></i> @lang('admin.add_new')</a>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>


    <div class="modal" id="add_city_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">@lang('productTypes.create')</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col">
                            <form action="{{ route('admin.productTypes.store') }}" method="POST" id="add_city_form">
                                @csrf
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">@lang('admin.productTypes.name')</label>

                                    <div class="col-md-12">
                                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" required autofocus>

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">

                    <button type="button" onclick="$('#add_city_form').submit()" class="btn btn-primary">@lang('admin.save')</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('admin.close')</button>
                </div>

            </div>
        </div>
    </div>

@endsection


@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });



        function deleteItem(id,url) {
            swal({
                title: '{!! trans('admin.are_you_sure') !!}',
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "{!! trans('admin.yes'); !!}",
                cancelButtonText: "{!! trans('admin.no'); !!}",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {



                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method : 'delete' }
                })
                    .done(function() {


                        swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.deleted_successfully') !!}", type: "success"},
                            function(){
                                location.reload();
                            }
                        );


                    })
                    .fail(function(e) {

                        swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")

                    })


            });
        }



    </script>

@endsection
