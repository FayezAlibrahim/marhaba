@extends('layouts.app')

@section('title','المطاعم')

@section('content')
                                <!--begin::Card-->
                                <div class="container">
								<div class="card card-custom">
									<div class="card-header flex-wrap border-0 pt-6 pb-0">
										<div class="card-title">
											<h2 class="card-label">المطاعم
                                            {{-- <span class="d-block text-muted pt-2 font-size-sm">Datatable initialized from HTML table</span> --}}
											</h2>
										</div>
										<div class="card-toolbar">
											<!-- <div class="dropdown dropdown-inline mr-2">
												<button type="button" class="btn btn-light-primary font-weight-bolder"  aria-haspopup="true" aria-expanded="false">
												<span class="svg-icon svg-icon-md">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect x="0" y="0" width="24" height="24" />
															<path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z" fill="#000000" opacity="0.3" />
															<path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z" fill="#000000" />
														</g>
													</svg>
								
												</span>Export</button>

											</div> -->
											<!--end::Dropdown-->
                                            <!--begin::Button-->
                                            <a href="#"  onclick="restaurant()" data-toggle="modal" data-target="#exampleModalLong" class="btn btn-primary font-weight-bolder" style=" margin: 2px; ">
                                                <span class="svg-icon svg-icon-md">
                                                    <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Design\Color-profile.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"/>
                                                            <path d="M12,10.9996338 C12.8356605,10.3719448 13.8743941,10 15,10 C17.7614237,10 20,12.2385763 20,15 C20,17.7614237 17.7614237,20 15,20 C13.8743941,20 12.8356605,19.6280552 12,19.0003662 C11.1643395,19.6280552 10.1256059,20 9,20 C6.23857625,20 4,17.7614237 4,15 C4,12.2385763 6.23857625,10 9,10 C10.1256059,10 11.1643395,10.3719448 12,10.9996338 Z M13.3336047,12.504354 C13.757474,13.2388026 14,14.0910788 14,15 C14,15.9088933 13.7574889,16.761145 13.3336438,17.4955783 C13.8188886,17.8206693 14.3938466,18 15,18 C16.6568542,18 18,16.6568542 18,15 C18,13.3431458 16.6568542,12 15,12 C14.3930587,12 13.8175971,12.18044 13.3336047,12.504354 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                            <circle fill="#000000" cx="12" cy="9" r="5"/>
                                                        </g>
                                                    </svg><!--end::Svg Icon-->
                                                </span>spin</a>

											<a href="/admin/restaurants/create" class="btn btn-primary font-weight-bolder">
											<span class="svg-icon svg-icon-md">
												<!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24" />
														<circle fill="#000000" cx="9" cy="15" r="6" />
														<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
													</g>
												</svg>
												<!--end::Svg Icon-->
											</span>@lang('admin.add_new')</a>
                                            <!--end::Button-->

										</div>
									</div>
									<div class="card-body">
										<!--begin: Search Form-->
										<!--begin::Search Form-->
										<div class="mb-7">
											<div class="row align-items-center">
												<div class="col-lg-9 col-xl-8">
													<div class="row align-items-center">
														<div class="col-md-4 my-2 my-md-0">
															<div class="input-icon">
																<input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
																<span>
																	<i class="flaticon2-search-1 text-muted"></i>
																</span>
															</div>
														</div>
														<div class="col-md-4 my-2 my-md-0">
															<div class="d-flex align-items-center">
																<label class="mr-3 mb-0 d-none d-md-block">الحالة</label>
																<select class="form-control" id="kt_datatable_search_status">
																	<option value="">All</option>
																	<option value="1">Published</option>
																	<option value="2">unPublished</option>
																</select>
															</div>
														</div>
														<div class="col-md-4 my-2 my-md-0">
															<div class="d-flex align-items-center">
																<label class="mr-3 mb-0 d-none d-md-block">المنطقة</label>
																<select class="form-control" id="kt_datatable_search_type">
																	<option value="">All</option>
                                                                @foreach ($cities as $city)
																	<option >{{ $city->name }}</option>
                                                                @endforeach

																</select>
															</div>
														</div>
													</div>
												</div>
												{{-- <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
													<a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
												</div> --}}
											</div>
										</div>
										<!--end::Search Form-->
										<!--end: Search Form-->
										<!--begin: Datatable-->
										<table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
											<thead>
												<tr>
													<th title="Field #1">#</th>
													<th title="Field #2">الاسم</th>
													<th title="Field #3">المنطقة</th>
                                                    <th title="Field #4">العنوان</th>
                                                    <th title="Field #5">الدفع بالنقاط</th>
													<th title="Field #5">الوصف</th>
													<th title="Field #6">الهاتف</th>
                                                <th title="Field #7">الحالة</th>
                                                <th  title="Field #8"></th>

												</tr>
											</thead>
											<tbody>
                                                @foreach($restaurants as $restaurant)
                                                <tr>
                                                    <td>{{ $restaurant->id }}
                                                        @if($restaurant->checkIfOpen()=='Yes')
                                                        <span style="width: 110px;"><span class="label label-primary label-dot mr-2"></span><span class="font-weight-bold text-primary">Online</span></span>
                                                        @else
                                                        <span style="width: 110px;"><span class="label label-danger label-dot mr-2"></span><span class="font-weight-bold text-danger">Offline</span></span>
                                                      @endif

                                                    </td>
													<td>{{ $restaurant->user->username ?? '' }}</td>
                                                    <td>{{ $restaurant->user->city->name ?? '' }}
                                                        <span class="d-block text-muted pt-2 font-size-sm">{{ $restaurant->user->region->name ?? '' }}
                                                        </span>
                                                    </td>
                                                    <td>{{ $restaurant->user->address  ?? '' }}</td>
                                                    <td>{{ $restaurant->points_cost()??'0' }}</td>
													<td>{{ $restaurant->bio ?? '' }}</td>
                                                    <td>{{ $restaurant->user->phone ?? '' }}</td>
                                                    <td>

                                                            @if( $restaurant->user->active == "yes")
                                                            <span  style="width: 110px;"><span class="label font-weight-bold label-lg label-light-primary label-inline">Published</span></span>
                                                        @else
                                                        <span style="width: 110px;"><span class="label font-weight-bold label-lg label-light-danger label-inline">unPublished</span></span>
                                                        @endif

                                                    </td>
                                                <td>
                                                    <span style="overflow: visible; position: relative; width: 125px;">
                                                        <div class="dropdown dropdown-inline">

                                                        <a href="{{ route('admin.restaurants.edit',$restaurant->id) }}" class="btn btn-sm btn-light btn-text-primary btn-icon mr-2" title="Edit details">
                                                           <span class="svg-icon svg-icon-md">
                                                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                 <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "></path>
                                                                    <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"></rect>
                                                                 </g>
                                                              </svg>
                                                           </span>
                                                        </a>
                                                        <a href="#" onclick="$('#restaurant_id').val({{ $restaurant->id }})" data-toggle="modal" data-target="#Deletemodal" class="btn btn-sm btn-light btn-text-primary btn-icon" title="Delete">
                                                           <span class="svg-icon svg-icon-md">
                                                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                 <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"></path>
                                                                    <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"></path>
                                                                 </g>
                                                              </svg>
                                                           </span>
                                                        </a>
                                                     </span>
                                                </td>
                                                </tr>
                                                @endforeach

											</tbody>
										</table>
										<!--end: Datatable-->
                                    </div>

								</div>
                                <!--end::Card-->

                                <input id="restaurant_id" type="hidden" />
                                <!-- Modal-->
<div class="modal fade" id="Deletemodal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">هل أنت متأكد من الحذف؟</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary " ><a id="deleteurl" onclick="deleteItem()" style="color: aliceblue;" href="#">حذف</a></button>
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="exampleModalLong" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Spiner</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form method="post" action="/admin/spiner">
                @csrf
            <div class="modal-body">
<div class="form-group row">
    <label class="col-lg-4 text-right col-form-label"data-required="yes">الطلبات من تاريخ:</label>
    <div class="col-lg-6">
        <input name="from" type="date"  class="form-control form-control-lg" required/>
    </div>
</div>
<div class="form-group row">
    <label class="col-lg-4 text-right col-form-label"data-required="yes">الطلبات إلى تاريخ:</label>
    <div class="col-lg-6">
        <input name="to" type="date"  class="form-control form-control-lg" required />
    </div>
</div>
<div class="form-group row">
    <label class="col-xl-3 col-lg-3 text-right col-form-label"data-required="yes">المطاعم :</label>
    <div class="col-lg-6">
        <select name="restaurants[]"  required class="form-control form-control-lg" id="restaurant_select" style="width:300px" multiple>
            <option value="all"> All</option>
            @foreach($restaurants as $restaurant)
            <option value="{{ $restaurant->id }}">{{ $restaurant->user->username }}</option>
            @endforeach
        </select>
    </div>
</div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">@lang('admin.close')</button>
                <button type="submit" class="btn btn-primary font-weight-bold">التالي</button>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection
<script>


    function deleteItem() {
var id=$('#restaurant_id').val();
$.ajax({
url: "/api/restaurant/delete",
type: "post",
data:{
_token: '{!! csrf_token() !!}'
,id:id
} ,
success: function (response) {
location.reload();
},
error: function(jqXHR, textStatus, errorThrown) {
console.log(textStatus, errorThrown);

}
});
    }
    function restaurant(){
    $('#restaurant_select').select2();
    }


</script>
