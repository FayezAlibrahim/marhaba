@extends('layouts.app')

@section('title','تعديل مطعم')


@section('styles')

    <style>
        .myaccordion {

            margin: 50px auto;
            box-shadow: 0 0 1px rgba(0, 0, 0, 0.1);
        }

        .myaccordion .card,
        .myaccordion .card:last-child .card-header {
            border: none;
        }

        .myaccordion .card-header {
            border-bottom-color: #EDEFF0;
            background: transparent;
        }

        .myaccordion .fa-stack {
            font-size: 18px;
        }

        .myaccordion .btn {
            width: 100%;
            font-weight: bold;
            color: #004987;
            padding: 0;
        }

        .myaccordion .btn-link:hover,
        .myaccordion .btn-link:focus {
            text-decoration: none;
        }

        .myaccordion li + li {
            margin-top: 10px;
        }

        select {
            height: 100% !important;
        }

        hr {
            border-top: 2px solid #769d69;
            margin-right: -16px;
        }

        .subTitle {
            margin-top: 35px;
            margin-bottom: -11px;
        }
    </style>


@endsection
@section('content')
    <!--begin::Card-->
<div class="container">
    <div class="card card-custom">
        <!--Begin::Header-->
        <div class="card-header card-header-tabs-line">
            <div class="card-toolbar">
                <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x" role="tablist">
                    <li class="nav-item mr-3">
                        <a class="nav-link active" data-toggle="tab" href="#kt_apps_contacts_view_tab_2">
														<span class="nav-icon mr-2">
															<span class="svg-icon mr-3">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Chat-check.svg-->
																<svg xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     width="24px" height="24px" viewBox="0 0 24 24"
                                                                     version="1.1">
																	<g stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24"/>
																		<path
                                                                            d="M4.875,20.75 C4.63541667,20.75 4.39583333,20.6541667 4.20416667,20.4625 L2.2875,18.5458333 C1.90416667,18.1625 1.90416667,17.5875 2.2875,17.2041667 C2.67083333,16.8208333 3.29375,16.8208333 3.62916667,17.2041667 L4.875,18.45 L8.0375,15.2875 C8.42083333,14.9041667 8.99583333,14.9041667 9.37916667,15.2875 C9.7625,15.6708333 9.7625,16.2458333 9.37916667,16.6291667 L5.54583333,20.4625 C5.35416667,20.6541667 5.11458333,20.75 4.875,20.75 Z"
                                                                            fill="#000000" fill-rule="nonzero"
                                                                            opacity="0.3"/>
																		<path
                                                                            d="M2,11.8650466 L2,6 C2,4.34314575 3.34314575,3 5,3 L19,3 C20.6568542,3 22,4.34314575 22,6 L22,15 C22,15.0032706 21.9999948,15.0065399 21.9999843,15.009808 L22.0249378,15 L22.0249378,19.5857864 C22.0249378,20.1380712 21.5772226,20.5857864 21.0249378,20.5857864 C20.7597213,20.5857864 20.5053674,20.4804296 20.317831,20.2928932 L18.0249378,18 L12.9835977,18 C12.7263047,14.0909841 9.47412135,11 5.5,11 C4.23590829,11 3.04485894,11.3127315 2,11.8650466 Z M6,7 C5.44771525,7 5,7.44771525 5,8 C5,8.55228475 5.44771525,9 6,9 L15,9 C15.5522847,9 16,8.55228475 16,8 C16,7.44771525 15.5522847,7 15,7 L6,7 Z"
                                                                            fill="#000000"/>
																	</g>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>
														</span>
                            <span class="nav-text font-weight-bold">معلومات المطعم</span>
                        </a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link" data-toggle="tab" href="#kt_apps_contacts_view_tab_3">
														<span class="nav-icon mr-2">
															<span class="svg-icon mr-3">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Devices/Display1.svg-->
																<svg xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     width="24px" height="24px" viewBox="0 0 24 24"
                                                                     version="1.1">
																	<g stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24"/>
																		<path
                                                                            d="M11,20 L11,17 C11,16.4477153 11.4477153,16 12,16 C12.5522847,16 13,16.4477153 13,17 L13,20 L15.5,20 C15.7761424,20 16,20.2238576 16,20.5 C16,20.7761424 15.7761424,21 15.5,21 L8.5,21 C8.22385763,21 8,20.7761424 8,20.5 C8,20.2238576 8.22385763,20 8.5,20 L11,20 Z"
                                                                            fill="#000000" opacity="0.3"/>
																		<path
                                                                            d="M3,5 L21,5 C21.5522847,5 22,5.44771525 22,6 L22,16 C22,16.5522847 21.5522847,17 21,17 L3,17 C2.44771525,17 2,16.5522847 2,16 L2,6 C2,5.44771525 2.44771525,5 3,5 Z M4.5,8 C4.22385763,8 4,8.22385763 4,8.5 C4,8.77614237 4.22385763,9 4.5,9 L13.5,9 C13.7761424,9 14,8.77614237 14,8.5 C14,8.22385763 13.7761424,8 13.5,8 L4.5,8 Z M4.5,10 C4.22385763,10 4,10.2238576 4,10.5 C4,10.7761424 4.22385763,11 4.5,11 L7.5,11 C7.77614237,11 8,10.7761424 8,10.5 C8,10.2238576 7.77614237,10 7.5,10 L4.5,10 Z"
                                                                            fill="#000000"/>
																	</g>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>
														</span>
                            <span class="nav-text font-weight-bold">الوصف</span>
                        </a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link" data-toggle="tab" href="#kt_apps_contacts_view_tab_4">
														<span class="nav-icon mr-2">
															<span class="svg-icon mr-3">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Home/Globe.svg-->
																<svg xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     width="24px" height="24px" viewBox="0 0 24 24"
                                                                     version="1.1">
																	<g stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24"/>
																		<path
                                                                            d="M13,18.9450712 L13,20 L14,20 C15.1045695,20 16,20.8954305 16,22 L8,22 C8,20.8954305 8.8954305,20 10,20 L11,20 L11,18.9448245 C9.02872877,18.7261967 7.20827378,17.866394 5.79372555,16.5182701 L4.73856106,17.6741866 C4.36621808,18.0820826 3.73370941,18.110904 3.32581341,17.7385611 C2.9179174,17.3662181 2.88909597,16.7337094 3.26143894,16.3258134 L5.04940685,14.367122 C5.46150313,13.9156769 6.17860937,13.9363085 6.56406875,14.4106998 C7.88623094,16.037907 9.86320756,17 12,17 C15.8659932,17 19,13.8659932 19,10 C19,7.73468744 17.9175842,5.65198725 16.1214335,4.34123851 C15.6753081,4.01567657 15.5775721,3.39010038 15.903134,2.94397499 C16.228696,2.49784959 16.8542722,2.4001136 17.3003976,2.72567554 C19.6071362,4.40902808 21,7.08906798 21,10 C21,14.6325537 17.4999505,18.4476269 13,18.9450712 Z"
                                                                            fill="#000000" fill-rule="nonzero"/>
																		<circle fill="#000000" opacity="0.3" cx="12"
                                                                                cy="10" r="6"/>
																	</g>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>
														</span>
                            <span class="nav-text font-weight-bold">تفاصيل الطلب</span>
                        </a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link" data-toggle="tab" href="#kt_apps_contacts_view_tab_1">
														<span class="nav-icon mr-2">
															<span class="svg-icon mr-3">
																<!--begin::Svg Icon | path:assets/media/svg/icons/General/Notification2.svg-->
																<svg xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     width="24px" height="24px" viewBox="0 0 24 24"
                                                                     version="1.1">
																	<g stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24"/>
																		<path
                                                                            d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z"
                                                                            fill="#000000"/>
																		<circle fill="#000000" opacity="0.3" cx="18.5"
                                                                                cy="5.5" r="2.5"/>
																	</g>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>
														</span>
                            <span class="nav-text font-weight-bold">الأصناف</span>
                        </a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link" data-toggle="tab" href="#kt_apps_contacts_view_tab_10">
														<span class="nav-icon mr-2">
															<span class="svg-icon mr-3">
																<!--begin::Svg Icon | path:assets/media/svg/icons/General/Notification2.svg-->
																<svg xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     width="24px" height="24px" viewBox="0 0 24 24"
                                                                     version="1.1">
																	<g stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24"/>
																		<path
                                                                            d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z"
                                                                            fill="#000000"/>
																		<circle fill="#000000" opacity="0.3" cx="18.5"
                                                                                cy="5.5" r="2.5"/>
																	</g>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>
														</span>
                            <span class="nav-text font-weight-bold">الصور</span>
                        </a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link" data-toggle="tab" href="#kt_apps_contacts_view_tab_5">
														<span class="nav-icon mr-2">
															<span class="svg-icon mr-3">
														<!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Design\Image.svg-->
                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     width="24px" height="24px" viewBox="0 0 24 24"
                                                                     version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path
            d="M6,5 L18,5 C19.6568542,5 21,6.34314575 21,8 L21,17 C21,18.6568542 19.6568542,20 18,20 L6,20 C4.34314575,20 3,18.6568542 3,17 L3,8 C3,6.34314575 4.34314575,5 6,5 Z M5,17 L14,17 L9.5,11 L5,17 Z M16,14 C17.6568542,14 19,12.6568542 19,11 C19,9.34314575 17.6568542,8 16,8 C14.3431458,8 13,9.34314575 13,11 C13,12.6568542 14.3431458,14 16,14 Z"
            fill="#000000"/>
                </g>
                                   </svg><!--end::Svg Icon-->
                                                            </span>

		</span>

                            <span class="nav-text font-weight-bold">معرض الصور</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div>
                <button type="button" class="btn btn-primary font-weight-bold"
                        style="margin-top: 15px;"onclick="sub()">Save Changes</button>
            </div>
        </div>
        <!--end::Header-->
        <!--Begin::Body-->
        <form id="update_form"  method="POST" action="{{route('admin.restaurants.update')}}" enctype="multipart/form-data">
            @csrf
        <div class="card-body">
@if($restaurant->user->active == "yes")
        <span class="label label-primary label-pill label-inline mr-2" onclick="changeStatus(0)">مفعٌل</span>
        @else
     <span class="label label-warning label-pill label-inline mr-2" onclick="changeStatus(1)">غير مفعٌل</span>
     @endif


            @if( Session::has( 'success' ))

            <div class="row">
                <div class="col-lg-9 col-xl-6 offset-xl-3">
                    <div class="alert alert-custom alert-light-primary fade show mb-9" role="alert">
                        <div class="alert-icon">
                            <i class="flaticon-warning"></i>
                        </div>
                        <div class="alert-text">

                            <h5>{{session('success')}}</h5>
                    </div>
                        <div class="alert-close">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">
                                    <i class="ki ki-close"></i>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
             @endif
             @if ($errors->any())
             <div class="row">
                <div class="col-lg-9 col-xl-6 offset-xl-3">
                    <div class="alert alert-custom alert-light-danger fade show mb-9" role="alert">
                        <div class="alert-icon">
                            <i class="flaticon-warning"></i>
                        </div>
                        <div class="alert-text">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>

                    </div>
                        <div class="alert-close">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">
                                    <i class="ki ki-close"></i>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
         @endif
            <div class="tab-content pt-5">
                <!--begin::Tab Content-->
                <div class="tab-pane active" id="kt_apps_contacts_view_tab_2" role="tabpanel">
                    <input  name="restaurant_id" value="{{$restaurant->id}}" hidden>
<!-- {{--                    <input  name="id" value="{{$restaurant->user->id}}" hidden>--}} -->
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label"data-required="yes">اسم المطعم</label>
                            <div class="col-lg-9 col-xl-6">
                                <input name="username"class="form-control form-control-lg   @error('username') is-invalid @enderror" placeholder="اسم المطعم"  required type="text"
                                       value="{{$restaurant->user->username}}"/>
                                @error('username')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>
                        {{--password--}}
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label" data-required="yes">@lang('users.password')</label>
                            <div class="col-lg-9 col-xl-6">
                                <input name="password" class="form-control form-control-lg  @error('password') is-invalid @enderror"
                                       type="password" required value=""/>
                                      @error('password')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror

                            </div>
                        </div>
                        {{-- phone--}}
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label" data-required="yes">@lang('users.phone')</label>
                            <div class="col-lg-9 col-xl-6">
                                <div class="input-group input-group-lg ">
                                    <div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="la la-phone"></i>
                                      </span>
                                    </div>
                                    <input name="phone"type="text" class="form-control form-control-lg  text-md-left @error('phone') is-invalid @enderror"
                                           value="{{$restaurant->user->phone}}" placeholder="09********" style="direction: ltr;"/>
                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label"data-required="yes">@lang('users.city')</label>
                            <div class="col-lg-9 col-xl-6">
                                <select id="city_drop" name="city_id" class="form-control form-control-lg "
                                        onfocus="GetRegions(this.value)" onchange="GetRegions(this.value)">

                                       @foreach($cities as $city)
                                      <option @if($city->id==$restaurant->user->city_id)selected @endif value="{{ $city->id }}">{{ $city->name }}</option>
                                      @endforeach

                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label"data-required="yes">@lang('users.region')</label>
                            <div class="col-lg-9 col-xl-6">
                                <select id="region_drop" name="region_id" class="form-control form-control-lg ">

                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label" data-required="yes">إشعار</label>
                            <div class="col-lg-9 col-xl-6">
                                <div class="input-group input-group-lg ">
                                    <div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="la la-alert"></i>
                                      </span>
                                    </div>
                                    <input name="alert" type="text" class="form-control form-control-lg  text-md-left @error('alert') is-invalid @enderror"
                                           value="{{$restaurant->alert}}" placeholder="هذا الاشعار سيظهر عند عرض صفحة المطعم" style="direction: ltr;"/>
                                    @error('alert')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                </div>
                <!--end::Tab Content-->
                <!--begin::Tab Content-->
                <div class="tab-pane" id="kt_apps_contacts_view_tab_3" role="tabpanel">
                        <!--end::Heading-->
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label" data-required="yes">@lang('users.address')</label>
                            <div class="col-lg-9 col-xl-6">
{{--                                <div class="spinner spinner-sm spinner-success spinner-right">--}}
                                    <input name="address" class="form-control form-control-lg   @error('address') is-invalid @enderror" type="text"
                                           value="{{$restaurant->user->address}}" required/>
                                @error('address')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
{{--                                </div>--}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Bio</label>
                            <div class="col-lg-9 col-xl-6">
                                <textarea name="bio" class="form-control form-control-lg  @error('bio') is-invalid @enderror"
                                          rows="3" placeholder="Type Bio"
                                          style="font-weight: bold; font-size: small;font-family: cairo;">{{$restaurant->bio}}</textarea>
                                @error('bio')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label">الموقع</label>
                            <div class="col-lg-9 col-xl-6">
                                <input id="zoomLevel" name="zoom" type="hidden" value="{{$restaurant->user->zoom}}"/>
                                <input id="myloc" name="map_location" value="{{$restaurant->user->lat.','.$restaurant->user->lon}}" type="text"/>
                                <div id="map" style="width: 400px; height: 400px;margin:10px"></div>
                            </div>

                        </div>

                </div>
                <!--end::Tab Content-->
                <!--begin::Tab Content-->
                <div class="tab-pane" id="kt_apps_contacts_view_tab_4" role="tabpanel">

                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 text-right col-form-label" data-required="yes">الضريبة لكل طلب</label>
                        <div class="col-lg-9 col-xl-6">
                            <div class="input-group input-group-lg">
                                <div class="input-group-prepend">
                                  <span class="input-group-text" style="color:gray; font-weight:300">
                                   %
                                  </span>
                                </div>
                                <input name="tax"type="number" class="form-control form-control-lg  text-md-left @error('tax') is-invalid @enderror"
                                    required   value="{{ $restaurant->tax ?? '' }}"  placeholder="ex: 1" />
                                @error('tax')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            {{--                                <span class="form-text text-muted">We'll never share your email with anyone else.</span>--}}
                        </div>
                    </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label" data-required="yes">الحد الأدنى للطلب</label>
                            <div class="col-lg-9 col-xl-6">
                                <div class="input-group input-group-lg">
                                    <div class="input-group-prepend">
									  <span class="input-group-text" style="color:gray;">
										ل.س
                                      </span>
                                    </div>
                                    <input name="minimum_order"type="number" class="form-control form-control-lg  text-md-left @error('minimum_order') is-invalid @enderror"
                                        required   value="{{ $restaurant->minimum_order }}"  placeholder="ex: 3000" />
                                    @error('minimum_order')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                {{--                                <span class="form-text text-muted">We'll never share your email with anyone else.</span>--}}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label">يتوفر توصيل؟</label>
                            <div class="col-lg-1 col-xl-1">
								<span class="switch">
								    <label>
										<input type="checkbox"
                                          {{$restaurant->has_delivery=="Yes"?'checked':''}}
                                           id="exampleCheck2" name="has_delivery"
                                               onchange="showdelivery_time(this,'delivery_time_div')"/>
											<span></span>
                                    </label>
								</span>
                            </div>
                            <div id="delivery_time_div" class="form-group row" style="display:none">
                                <label class="col-xl-7 col-lg-7 text-right col-form-label">وقت التوصيل التقريبي</label>
                                <div class="col-lg-5 col-xl-5">
                                    <div class="input-group input-group-lg">
                                        <div class="input-group-prepend">
									  <span class="input-group-text" style="color:gray;">
										دقيقة
                                      </span>
                                        </div>
                                        <input required name="delivery_time" type="number" class="form-control form-control-lg  text-md-left" value="{{$restaurant->delivery_time}}">
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label">متوفر 24 ساعة؟</label>
                            <div class="col-lg-1 col-xl-1">
								<span class="switch">
									<label>
										<input type="checkbox" onchange="show_start_time(this)"
                                           {{$restaurant->is_fullday=="Yes"?'checked':''}}
                                           id="exampleCheck1"
                                           name="is_fullday"/>
											<span></span>
                                    </label>
								</span>
                            </div>
                            <div id="start_end_div" class="form-group " style="display:none">

                                    <div class="form-group row">
                                        <label class="col-form-label text-right col-lg-3 col-sm-12">وقت البدء</label>
                                        <div class="col-lg-7 col-md-9 col-sm-12">
                                            <div class="input-group timepicker">
                                                <div class="input-group-prepend">
															<span class="input-group-text">
																<i class="la la-clock-o"></i>
															</span>
                                                </div>
                                                <input class="form-control" name="open_time"value="{{$restaurant->open_time}}" id="kt_timepicker_3" readonly="readonly" placeholder="Select time" type="text">
                                            </div>
                                        </div>
                                    </div>
                                <br>
                                <br>
                                <div class="form-group row">
                                    <label class="col-form-label text-right col-lg-3 col-sm-12">وقت النتهاء</label>
                                    <div class="col-lg-7 col-md-9 col-sm-12">
                                        <div class="input-group timepicker">
                                            <div class="input-group-prepend">
															<span class="input-group-text">
																<i class="la la-clock-o"></i>
															</span>
                                            </div>
                                            <input class="form-control" name="close_time" value="{{$restaurant->close_time}}" id="kt_timepicker_3" readonly="readonly" placeholder="Select time" type="text">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <!--end::Tab Content-->
                <!--begin::Tab Content-->
                <div class="tab-pane" id="kt_apps_contacts_view_tab_1" role="tabpanel">
                    <div class="container">


                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 text-right col-form-label"data-required="yes">@lang('products.categories')</label>
                                <div class="col-lg-9 col-xl-6">
                                    <select name="categories[]"  class="form-control form-control-lg" id="product_categories_id" style="width:300px" multiple>
                                        @foreach($categories as $category)
                             <option value="{{ $category->id ?? '1' }}" {{in_array($category->id,$restaurant->categories->pluck('id')->toArray())?'selected':''}}>{{ $category->name }}</option>

                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 text-right col-form-label"data-required="yes">الكلمات مفتاحية</label>
                                <div class="col-lg-9 col-xl-6">
                                    <select name="tags[]"  class="form-control form-control-lg" id="tags_select" style="width:300px"multiple>
                                        @foreach($restaurant->tags as $tag)
                                            <option value="{{ $tag->tag_value}}" selected>{{ $tag->tag_value}}</option>
                                        @endforeach


                                    </select>
                                    <small id="tagsHelp" class="form-text text-muted">تساعد الكلمات مفتاحية في عملية البحث, يرجى ادخال أكبر قدر من الكلمات مفتاحية للمطعم (مثال: اسم المطعم/الوجبات الرئيسية بالعربي والأنجليزي + أسماء تصنيفات الطعام المتوفرة لدى المطعم)</small>

                                </div>
                            </div>

                        <!--end::Timeline-->

                    </div>
                </div>
                <div class="tab-pane" id="kt_apps_contacts_view_tab_5" role="tabpanel">
                    <div class="container">
                            <div class="form-group row">
                                <input type="file" id="images" onchange="preview_images()" name="images[]"  accept="image/*" multiple/>
                                <button type="button" style="margin-right:auto" class="btn btn-outline-primary btn-sm" onclick="clearFiles()">@lang('products.clear_files')</button>
                                <input type="hidden" name="deletephotos" id="delphoto" />
                            </div>
                                <div class="form-group row">
                                <div class="row text-center" id="image_preview" style="margin-top: 15px;">
                               @foreach($restaurant->images as $image)
                                <img class="img-fluid img-thumbnail " src="{{$image->media_url}}" alt=""style="height:300px;width:300px">
                               @endforeach
                                </div>
                            </div>
                            </div>
                </div>

                <div class="tab-pane" id="kt_apps_contacts_view_tab_10" role="tabpanel">
                    <div class="container">
                            <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label"data-required="yes"> صورة الغلاف</label>


                            <div class="image-input image-input-outline" id="kt_image_4" style="background-image: url(/uploads/media/users/blank.png)">
                            <div class="image-input-wrapper" style="background-image: url({{ $restaurant->cover }})"></div>

                            <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                             <i class="fa fa-pen icon-sm text-muted"></i>
                             <input type="file" name="cover" accept=".png, .jpg, .jpeg"/>
                             <input type="hidden" name="profile_avatar_remove"/>
                            </label>

                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                             <i class="ki ki-bold-close icon-xs text-muted"></i>
                            </span>

                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="Remove avatar">
                             <i class="ki ki-bold-close icon-xs text-muted"></i>
                            </span>
                           </div>


                            </div>

                         <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label"data-required="yes"> صورة العرض</label>

                            <div class="image-input image-input-outline" id="kt_image_3" style="background-image: url(/uploads/media/users/blank.png)">
                            <div class="image-input-wrapper" style="background-image: url({{ $restaurant->image_url }})"></div>

                            <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                             <i class="fa fa-pen icon-sm text-muted"></i>
                             <input type="file" name="image_url" accept=".png, .jpg, .jpeg"/>
                             <input type="hidden" name="profile_avatar_remove"/>
                            </label>

                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                             <i class="ki ki-bold-close icon-xs text-muted"></i>
                            </span>

                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="Remove avatar">
                             <i class="ki ki-bold-close icon-xs text-muted"></i>
                            </span>
                           </div>

                         </div>
                    </div>
                </div>
            </div>
            <!--end::Tab Content-->
            <!--end::Body-->
        </div>
        </form>
        <!--end::Card-->
    </div>

</div>

@endsection

<script>
    function sbmt() {
        $("#submitForm").submit();
    }
</script>
@section('scripts')
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyABUP9n48lvINh62C_F2o04n8oiA-VI3CM&libraries=places&callback=initAutocomplete"
        async defer></script>


    <script>

        function preview_images() {
            var total_file = document.getElementById("images").files.length;
            console.log(total_file);
            if(total_file<{{$restaurant->images_limit}}){
            $('#image_preview').empty();
            for (var i = 0; i < total_file; i++) {
                template = `<div class="col-md-3" style="padding: 0px; margin:5px;">
                <a href="#" >
                <img class="img-fluid img-thumbnail " src="${URL.createObjectURL(event.target.files[i])}" alt=""
                style="height:300px;width:300px"
                    >
                </a>
            </div>`;
                $('#image_preview').append(template);
            }
        }
        else {
            alert('عدد الصور أكثر من المتوقع');
            }
        }


        $("#accordion").on("hide.bs.collapse show.bs.collapse", e => {
            $(e.target)
                .prev()
                .find("i:last-child")
                .toggleClass("fa-minus fa-plus");
        });

        $("#product_categories_id").select2();
        $("#tags_select").select2({
            tags: true
        });
        $(".attributes_select").select2();


        function clearFiles() {
            var total_file = document.getElementById("images").value = "";
            $('#image_preview').empty();
            $('#delphoto').val('true');
        }





    </script>

    <script type="text/javascript">

        $('.select2-selection__rendered').click(function () {
            // attributes_select
            $(document).ready(function () {
                function setCurrency(currency) {
                    if (!currency.id) {
                        return currency.text;
                    }
                    var n = currency.element.value.startsWith("#");
                    if (n == true) {
                        var $currency = $('<span>' + currency.text + '</span><span class="color-btn" style="background-color:' + currency.element.value + '"></span>');

                    } else {
                        $currency = currency.text;
                    }
                    return $currency;
                }

                $(".attributes_select").select2({
                    // placeholder: "What currency do you use?", //placeholder
                    templateResult: setCurrency,
                    templateSelection: setCurrency
                });
            })
        });
    </script>

    <script>
        function GetRegions(id) {
            var regions = document.getElementById("region_drop");
            $("#region_drop").empty();
            $.ajax(
                {
                    type: "GET",
                    url: "/admin/city/" + id + "/regions",
                    dataType: "text",
                    data: "",
                    success: function (response) {
                        var JSONArray = $.parseJSON(response);
                        var str = '';
                        //  console.log(response);
                        for (var i = 0; i < JSONArray.length; i++) {
                            var opt = document.createElement('option');
                            opt.value = JSONArray[i]['id'];
                            opt.innerHTML = JSONArray[i]['name'];

                            regions.appendChild(opt);
                        }
                    }
                }
            );
        }
        GetRegions({{$restaurant->user->city_id}});
    </script>
    <script>
        function initAutocomplete() {
            var markers = [];
            var input11 = document.getElementById("myloc").value
            var coords = input11.split(",");
            var input23 = new google.maps.LatLng(coords[0], coords[1]);
            var map = new google.maps.Map(document.getElementById('map'), {
                center: input23,
                zoom: 14,
                mapTypeId: 'roadmap'
            });

            placeMarker(map, map.getCenter());

            google.maps.event.addListener(map, 'click', function (event) {
                placeMarker(map, event.latLng);
            });

            function placeMarker(map, location) {
                deleteMarkers();
                var zoomL = map.getZoom();
                document.getElementById('zoomLevel').value = zoomL;
                var marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
                $("#myloc").val(location.lat() + ',' + location.lng());
                markers.push(marker);
            }

            function setMapOnAll() {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(null);
                }
            }

            // Removes the markers from the map, but keeps them in the array.
            function clearMarkers() {
                setMapOnAll();
            }

            // Deletes all markers in the array by removing references to them.
            function deleteMarkers() {
                clearMarkers();
                markers = [];
            }

            // Create the search box and link it to the UI element.
            var input = document.getElementById('myloc');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
            });

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }


                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location
                    }));

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });
        }
    </script>


    <script>
        function showdelivery_time(ob,elem)
        {
            var s= document.getElementById(elem);
            if(ob.checked)
            {

                s.style.display="inline-flex";
            }
            else{
                s.style.display="none";
            }
        }
        function show_start_time(ob)
        {
            var s= document.getElementById("start_end_div");
            if(!ob.checked)
            {

                s.style.display="inline-flex";
            }
            else{
                s.style.display="none";
            }
        }
        showdelivery_time(document.getElementById("exampleCheck2"),"delivery_time_div");
        show_start_time(document.getElementById("exampleCheck1"));
function sub()
{
    $('#update_form').submit();
}
    </script>

    <script>
        var avatar4 = new KTImageInput('kt_image_4');
  avatar4.on('cancel', function(imageInput) {
 swal.fire({
  title: 'Image successfully canceled !',
  type: 'success',
  buttonsStyling: false,
  confirmButtonText: 'Awesome!',
  confirmButtonClass: 'btn btn-primary font-weight-bold'
 });
});

avatar4.on('change', function(imageInput) {
 swal.fire({
  title: 'Image successfully changed !',
  type: 'success',
  buttonsStyling: false,
  confirmButtonText: 'Awesome!',
  confirmButtonClass: 'btn btn-primary font-weight-bold'
 });
});

avatar4.on('remove', function(imageInput) {
 swal.fire({
  title: 'Image successfully removed !',
  type: 'error',
  buttonsStyling: false,
  confirmButtonText: 'Got it!',
  confirmButtonClass: 'btn btn-primary font-weight-bold'
 });
});
var avatar3 = new KTImageInput('kt_image_3');
  avatar3.on('cancel', function(imageInput) {
 swal.fire({
  title: 'Image successfully canceled !',
  type: 'success',
  buttonsStyling: false,
  confirmButtonText: 'Awesome!',
  confirmButtonClass: 'btn btn-primary font-weight-bold'
 });
});

avatar3.on('change', function(imageInput) {
 swal.fire({
  title: 'Image successfully changed !',
  type: 'success',
  buttonsStyling: false,
  confirmButtonText: 'Awesome!',
  confirmButtonClass: 'btn btn-primary font-weight-bold'
 });
});

avatar3.on('remove', function(imageInput) {
 swal.fire({
  title: 'Image successfully removed !',
  type: 'error',
  buttonsStyling: false,
  confirmButtonText: 'Got it!',
  confirmButtonClass: 'btn btn-primary font-weight-bold'
 });
});
    </script>

    <script>

    function changeStatus($status){

        Swal.fire({
        title: "Are you sure?",
        text: "changing Restaurant Status will also change status of all related meals!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, update!"
    }).then(function(result) {
        if (result.value) {
            Swal.fire(
                "Updated!",
                "Restaurant has been Updated.",
                "success"
            );
            $.ajax({
                url: "/api/restaurant/update_status/"+$status+"/"+{{$restaurant->id}},
                type: 'POST',
                dataType: 'JSON',
                data: {_token: '{!! csrf_token() !!}', _method : 'post' },
                success: function (response) {
                    location.reload();
                    // console.log(response);
                    },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert(textStatus, errorThrown);
                    }
            })
        }
        // else{

        // }
    });
    }
    </script>
    <script src="{{asset('js/pages/crud/forms/widgets/bootstrap-timepicker.js')}}"></script>
    <script src="{{asset('js/pages/custom/contacts/edit-contact.js')}}"></script>
@endsection
