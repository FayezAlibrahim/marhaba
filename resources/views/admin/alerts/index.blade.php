@extends('layouts.app')
@section('title',trans('alerts.alerts'))

@section('content')

<div class="container-fluid">
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flexf flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 class="card-label">@lang('alerts.alerts')
                        </div>
                        <div class="card-toolbar">
                            <!--begin::Button-->
                            <a href="/admin/alerts/create" class="btn btn-primary font-weight-bolder"  >
                            <span class="svg-icon svg-icon-md">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <circle fill="#000000" cx="9" cy="15" r="6" />
                                        <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>@lang('alerts.create')</a>
                            <!--end::Button-->
                        </div>
                    </div>
                    <div class="card-body">

                        <!--begin: Datatable-->
                        <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('alerts.title')</th>
                                    <th>@lang('alerts.message')</th>
                                    <th>نوع الاشعار</th>
                                    <th>@lang('admin.settings')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($app_alerts as $alert)
                                <tr>

                                    <td>{{ $alert->id }}</td>
                                    <td>{{ $alert->title ?? '' }}</td>
                                    <td>{{ $alert->message ?? '' }}</td>

                                <td> {{ $alert->product_id?   'إشعار لمنتج' :'إشعار عام' }}
                                    <span class="d-block text-muted pt-2 font-size-sm">{{ $alert->product->name ?? '' }}
                                    </span>
                                </td>
                                    <td>
                                        <span style="overflow: visible; position: relative; width: 125px;">
                                            <div class="dropdown dropdown-inline">

                                            <a href="#"  onclick="event.preventDefault();sendAlert('{{ $alert->id }}','{{ route('admin.alerts.send') }}')" class="btn btn-sm btn-light btn-text-primary btn-icon mr-2" title="Send notification">
                                                <span class="svg-icon svg-icon-md svg-icon-primary"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Communication\Send.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <path d="M3,13.5 L19,12 L3,10.5 L3,3.7732928 C3,3.70255344 3.01501031,3.63261921 3.04403925,3.56811047 C3.15735832,3.3162903 3.45336217,3.20401298 3.70518234,3.31733205 L21.9867539,11.5440392 C22.098181,11.5941815 22.1873901,11.6833905 22.2375323,11.7948177 C22.3508514,12.0466378 22.2385741,12.3426417 21.9867539,12.4559608 L3.70518234,20.6826679 C3.64067359,20.7116969 3.57073936,20.7267072 3.5,20.7267072 C3.22385763,20.7267072 3,20.5028496 3,20.2267072 L3,13.5 Z" fill="#000000"/>
                                                    </g>
                                                </svg><!--end::Svg Icon--></span>
                                            </a>
                                            <a href="#"  onclick="deleteItem('{{ $alert->id }}','{{ route('admin.alerts.destroy',$alert->id) }}')" class="btn btn-sm btn-light btn-text-primary btn-icon" title="Delete">
                                               <span class="svg-icon svg-icon-md svg-icon-primary">
                                                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                     <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"></path>
                                                        <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"></path>
                                                     </g>
                                                  </svg>
                                               </span>
                                            </a>
                                         </span>
                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
</div>


@endsection


@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });



        function deleteItem(id,url) {




                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method : 'delete' }
                    ,
                    success: function (response) {
                    location.reload();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);

                    }
                });
        }



        function sendAlert(id,url) {

                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', alert_id : id }
                })
                    .done(function() {

                    alert("{!! trans('admin.sent_successfully') !!}");
                        swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.sent_successfully') !!}", type: "success"},
                            function(){
                                location.reload();
                            }
                        );


                    })
                    .fail(function(e) {
                        console.log(e)
                    })
        }

    </script>

@endsection
