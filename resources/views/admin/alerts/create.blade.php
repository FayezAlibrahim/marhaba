@extends('layouts.app')

@section('title',trans('ads.create'))


@section('styles')

    <style>

        .wrapper-pic {
            position: relative;
            display: inline-block;
            opacity: 1;
        }

        .wrapper-pic:hover {
            transition: all .2s ease-in-out;
        }

        .wrapper-pic:hover .edit {
            transition: all .2s ease-in-out;
            display: block;
        }

        .edit {
            padding-top: 7px;
            padding-right: 7px;
            position: absolute;
            left: 20px;
            top: 10px;
            display: none;
        }


    </style>


@endsection

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-custom">
                    <div class="card-header">
                            <div class="card-title">
                                <span class="card-icon">
                                    <i class="flaticon2-notification text-primary"></i>
                                </span>
                                <h3 class="card-label">@lang('alerts.create')
                            </h3>
                            </div>
                            <div class="card-toolbar">
                                <a href="#" onclick="$('#create_form').submit();" class="btn btn-sm btn-success font-weight-bold">
                                <i class="flaticon2-cube"></i>@lang('admin.add_new')</a>
                            </div>

                    </div>

                    <div class="card-body">
                        <form id="create_form" method="POST" action="{{ route('admin.alerts.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-8">


                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right" data-required="yes">@lang('alerts.title')</label>

                                <div class="col-md-10">
                                    <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" required autofocus>

                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">@lang('ads.type')</label>

                                <div class="col-md-10">
                                    <select name="type" id="select_type" class="form-control select2" onchange="display_productsay(event)">
                                        <option value="public" selected>إشعار عام</option>
                                        <option value="product">إشعار لمنتج</option>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group row" id="products" style="display: none">
                                <label class="col-md-2 col-form-label text-md-right" data-required="yes">@lang('ads.item')</label>

                                <div class="col-lg-10 col-md-9 col-sm-12">
                                    <select class="form-control select2" id="kt_select2_1" name="product_id">
                                        @foreach ($products as $product )
                                        <option value="{{ $product->id }}">{{ $product->name }} - {{$product->restaurant->user->username}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right" data-required="yes">@lang('alerts.message')</label>

                                <div class="col-md-10">
                                    <textarea name="message" required autofocus class="form-control form-control-lg form-control-solid" cols="30" rows="5"></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">@lang('ads.image')</label>

                                <div class="col-md-10">

                                    <div class="image-input image-input-empty image-input-outline" id="kt_image_4" style="background-image: url(assets/media/users/blank.png)">
                                        <div class="image-input-wrapper"></div>

                                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                         <i class="fa fa-pen icon-sm text-muted"></i>
                                         <input type="file" name="image" accept=".png, .jpg, .jpeg"/>
                                         <input type="hidden" name="profile_avatar_remove"/>
                                        </label>

                                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                         <i class="ki ki-bold-close icon-xs text-muted"></i>
                                        </span>

                                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="Remove avatar">
                                         <i class="ki ki-bold-close icon-xs text-muted"></i>
                                        </span>
                                       </div>
                                </div>
                            </div>




                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts')

    <script>
        $('#select_type').select2({ width: '100%',});
        $('#kt_select2_1').select2({
            width: '100%',
           });
           $('#kt_select2_1').data('select2').$selection.css('height', '34px');
           $('#select_type').data('select2').$selection.css('height', '34px');
       function display_productsay(event)
        {
            var x=document.getElementById("products");
            var selected_value  = $(event.target).val();
            if(selected_value === "product"){
                x.style.display="flex";

            }
            else  x.style.display="none";

        }




    </script>
@endsection
