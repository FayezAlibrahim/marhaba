@extends('layouts.app')

@section('title',trans('attributes.trans'))

@section('content')

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">@lang('attributes.trans')</h1>
                <hr>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table  class=" table table-striped table-bordered table-hover" id="dataTables">
                        <thead>
                        <tr>
                            <th>@lang('attributes.id')</th>
                            <th>@lang('attributes.values')</th>
                            <th>@lang('attributes.trans')</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($attribute->values()->get() as $value)
                            <tr class="odd gradeX">
                                <td>{{ $value->id }}</td>
                                <td>{{ $value->value}}</td>
                                <td>
                                    <div class="form-group">

                                        <input type="text" data-id="{{ $value->id }}" value="{{$value->get_attribute_trans('value')->value}}"
                                               class="form-control trans_values" >
                                    </div>
                                </td>

                            </tr>
                        @endforeach


                        </tbody>

                    </table>

                </div>
                <!-- /.table-responsive -->
                <br>
                <div class="panel-footer">
                    <button class="btn btn-primary" onclick="save_attribute()" >@lang('admin.save')</button>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>






@endsection


@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });

        $(".attribute_value_select select").select2({
            tags: true,
        });

        function save_attribute() {

            var data = [];

                $('.trans_values').each(function (key,element) {
                    ele = $(element);
                    let id = ele.attr('data-id');
                    let value = ele.val();
                    data.push({
                        'id' : id,
                        'value' : value
                    })
            });

            var url = '{!! route("admin.attributes.value_trans") !!}';

         console.log(JSON.stringify(data));
           // console.log(JSON.stringify(data[0]->name));

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: {_token: '{!! csrf_token() !!}', data : JSON.stringify(data)}
            })
                .done(function(data) {

                    console.log(data);
                    swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.created_successfully') !!}", type: "success"},
                        function(){
                         location.reload();
                        }
                    );

                })
                .fail(function(e) {
                   console.log(e);

                })



        }
    </script>

@endsection
