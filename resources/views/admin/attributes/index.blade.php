@extends('layouts.app')

@section('title',trans('attributes.attributes'))

@section('content')

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">@lang('attributes.attributes')</h1>
                <hr>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class=" table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>@lang('attributes.id')</th>
                            <th>@lang('attributes.name')</th>
                            <th>@lang('admin.settings')</th>
                            {{-- <th>@lang('attributes.trans')</th> --}}
                            <th>@lang('attributes.values')</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($attributes as $attribute)
                            <tr class="odd gradeX">
                                <td>{{ $attribute->id }}</td>
                                <td>{{ $attribute->name }}</td>
                                <td>
                                <a  href="#"  data-toggle="modal" data-target="#edit_modal" 
                                    data-item_id="{{ $attribute->id }}"
                                    data-item_name="{{ $attribute->name }}">
                                    <i class="fa fa-edit"></i> 
                                </a>
                   @if($attribute->name !="colors" && $attribute->name !="sizes")
                                <a  
                                onclick="checkDelete('{{$attribute->id}}','{{ $attribute->name }}')"
                                >
                                <i class="fa fa-trash" style="color:#ff4747;"></i> 
                            </a>                  
                    @endif
                                 &nbsp;
                                </td>
                                {{-- <td>
                                    <a href="./attribute/trans/{{$attribute->id}}"   ><i class="fa fa-edit"></i> </a> &nbsp;
                                </td> --}}
                                <td class="attribute_value_select">
                                    <select class="form-control" data-id="{{ $attribute->id }}"  multiple>
                                        @foreach($attribute->values()->get() as $value)
                                            <option value="{{ $value->id }}"  selected >{{ $value->value }}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                        @endforeach
                        <script>
                            function checkDelete($id,$name) {
                              var r = confirm("متأكد من حذف الخاصية ("+$name+")؟, سيتم حذفها من جميع الوجبات ايضا");
                              if (r == true) {
                                $.ajax(
                                        {
                                            type:"post",
                                            url : "/api/attribute/remove",
                                            data : {
                                                "id":$id
                                            },
                                            success: function(response)
                                            {
                                                // var JSONArray = $.parseJSON( response );
                                                location.reload();
                                            //    console.log(response);
          
                                            }
                                        }
                                    );
                              }
                            }
                            </script>

                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
                <br>
                <div class="panel-footer">
                    <button class="btn btn-primary" onclick="save_attribute()">@lang('admin.save')</button>
                    <button class="btn btn-primary" data-toggle="modal" data-target="#add_attribute_modal">@lang('admin.create')</button>

                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>

    <div class="modal" id="add_attribute_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">@lang('cities.create')</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col">
                            <form action="{{ route('admin.attributes.store') }}" method="POST" id="add_attribute_form">
                                @csrf
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">@lang('attributes.name')</label>

                                    <div class="col-md-8">
                                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" required autofocus>

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                </div>
                                {{-- <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">@lang('attributes.en_name')</label>


                                    <div class="col-md-8">
                                        <input type="text" class="form-control @error('english_name') is-invalid @enderror" name="english_name" required autofocus>

                                        @error('english_name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                </div> --}}
                            </form>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">

                    <button type="button" onclick="$('#add_attribute_form').submit()" class="btn btn-primary">@lang('admin.save')</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('admin.close')</button>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">@lang('attributes.edit')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="#" id="edit_attribute_form" method="POST">
                        @csrf
                        @method('PUT')
                        <input type="hidden" id="item_id">
                        <div class="form-group">
                            <input type="text" class="form-control" id="item_name" name="name">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('admin.close')</button>
                    <button type="button" onclick="$('#edit_attribute_form').submit()" class="btn btn-primary">@lang('admin.save')</button>
                </div>
            </div>
        </div>
    </div>


@endsection


@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });

        $(".attribute_value_select select").select2({
            tags: true,
        });


        function save_attribute() {

            var data = [];

            $('.attribute_value_select').each(function (key,ele) {
                var id = $(ele).find('select').attr('data-id');
                var att_data  = $(ele).find('select').select2('data');
                data[id] = att_data;
            });

            var url = '{!! route('admin.attributes.save') !!}';

            console.log(JSON.stringify(data));

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: {_token: '{!! csrf_token() !!}', data : JSON.stringify(data)}
            })
                .done(function(data) {


                    swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.created_successfully') !!}", type: "success"},
                        function(){
                            location.reload();
                        }
                    );

                })
                .fail(function(e) {
                    swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")

                })



        }


        function deleteItem(id,url) {
            swal({
                title: '{!! trans('admin.are_you_sure') !!}',
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "{!! trans('admin.yes'); !!}",
                cancelButtonText: "{!! trans('admin.no'); !!}",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {



                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method : 'delete' }
                })
                    .done(function() {


                        swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.deleted_successfully') !!}", type: "success"},
                            function(){
                                location.reload();
                            }
                        );


                    })
                    .fail(function(e) {

                        swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")

                    })


            });
        }

        $('#edit_modal').on('show.bs.modal', function (event) {

            edit_url = '{!! route('admin.attributes.update','@id@') !!}';
            var button = $(event.relatedTarget);
            var item_id = button.data('item_id');
            var item_name = button.data('item_name');
            console.log(item_name);
            url = edit_url.replace("@id@", item_id);

            var modal = $(this);
            modal.find('.modal-body #item_id').val(item_id);
            modal.find('.modal-body #item_name').val(item_name);
            $('#edit_attribute_form').attr('action',url);
        })



    </script>

@endsection
