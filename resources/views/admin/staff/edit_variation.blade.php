@extends('layouts.app')

@section('title',trans('products.edit'))

@section('css-links')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
    @endsection

@section('styles')

    <style>
        .variations_accordion {

            margin: 50px auto;
            box-shadow: 0 0 1px rgba(0, 0, 0, 0.1);
        }

        .variations_accordion .card,
        .variations_accordion .card:last-child .card-header {
            border: none;
        }

        .variations_accordion .card-header {
            border-bottom-color: #EDEFF0;
            background: transparent;
        }

        .variations_accordion .fa-stack {
            font-size: 18px;
        }

        .variations_accordion .btn {
            width: 100%;
            font-weight: bold;
            color: #004987;
            padding: 0;
        }

        .variations_accordion .btn-link:hover,
        .variations_accordion .btn-link:focus {
            text-decoration: none;
        }

        .variations_accordion li + li {
            margin-top: 10px;
        }


        .editable-pic {
            position: relative;
            display: inline-block;
            opacity: 1;
        }

        .editable-pic:hover {
            transition: all .2s ease-in-out;
        }

        .editable-pic:hover .edit {
            transition: all .2s ease-in-out;
            display: block;
        }

        .edit {
            padding-top: 7px;
            padding-right: 7px;
            position: absolute;
            right: 10px;
            bottom: 5px;
            display: none;
        }

    </style>


@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        
        <h6 class="section-title h1">@lang('products.edit_variation')</h6>
        <i class="fas fa-arrow-left" style="float: left;color: deeppink;font-size: 22px;" 
        onclick="window.location.href='/admin/products/{{$product->parent_id}}/edit?selected=var'"></i>
        <form method="POST" id="edit_product_form" action="{{ route('admin.products.update',$product->id) }}" enctype="multipart/form-data">
            @csrf 
            @method('PUT')
            
            <div class="row">
                <div class="col">
                    <nav>
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-info-tab" data-toggle="tab" href="#nav-info"
                               role="tab" aria-controls="nav-info" aria-selected="true">@lang('products.info')</a>
                            <a class="nav-item nav-link" id="nav-images-tab" data-toggle="tab" href="#nav-images"
                               role="tab"
                               aria-controls="nav-images" aria-selected="false">@lang('products.images')</a>
                            <a class="nav-item nav-link" id="nav-attributes-tab" data-toggle="tab"
                               href="#nav-attributes"
                               role="tab" aria-controls="nav-attributes"
                               aria-selected="false">@lang('products.attributes')</a>
                        </div>
                    </nav>
                    <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-info" role="tabpanel"
                             aria-labelledby="nav-info-tab">


                            <div class="row">
                                <div class="form-group">
                                    <label class="col col-form-label text-md-left">@lang('products.name')</label>
                                    <div class="col">
                                        <input type="text" class="form-control @error('name') is-invalid @enderror"
                                               name="name"
                                               value="{{ $product->name }}">

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                {{-- <div class="form-group">
                                    <label class="col col-form-label text-md-left">@lang('products.sku')</label>
                                    <div class="col">
                                        <input type="text" class="form-control @error('sku') is-invalid @enderror"
                                               name="sku"
                                               value="{{ $product->sku }}">

                                        @error('sku')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div> --}}

                                {{-- <div class="form-group">
                                    <label class="col col-form-label text-md-left">@lang('products.made_in')</label>
                                    <div class="col">
                                        <input type="text" class="form-control @error('made_in') is-invalid @enderror"
                                               name="made_in"
                                               value="{{ $product->made_in }}">

                                        @error('made_in')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div> --}}

                                <div class="form-group">
                                    <label class="col col-form-label text-md-left">@lang('products.currency')</label>
                                    <div class="col">
                                        <input type="text"  class="form-control @error('currency') is-invalid @enderror" value="{{ getCurrentCurrency() }}" readonly>
                                        <input type="hidden" name="currency" value="{{ getCurrentCurrency(true) }}">

                                        @error('currency')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="col col-form-label text-md-left">@lang('products.description')</label>
                                    <div class="col">
                                    <textarea name="description"
                                              class="form-control @error('description') is-invalid @enderror" cols="20"
                                              rows="10">{{ $product->description }}</textarea>

                                        @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group">
                                    <label class="col col-form-label text-md-left">@lang('products.unit_price')</label>
                                    <div class="col">
                                        <input type="text" class="form-control @error('name') is-invalid @enderror"
                                               name="unit_price"
                                               value="{{ $product->unit_price }}">

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label
                                        class="col col-form-label text-md-left">@lang('products.unit_discount_price')</label>
                                    <div class="col">
                                        <input type="text" class="form-control @error('sku') is-invalid @enderror"
                                               name="unit_discount_price"
                                               value="{{ $product->unit_discount_price }}">

                                        @error('unit_discount_price')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>


                            </div>

                            <div class="row">
                                {{-- <div class="form-group">
                                    <label
                                        class="col col-form-label text-md-left">@lang('products.stock_quantity')</label>
                                    <div class="col">
                                        <input type="text"
                                               class="form-control @error('stock_quantity') is-invalid @enderror"
                                               name="stock_quantity"
                                               value="{{ $product->stock_quantity }}">

                                        @error('stock_quantity')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div> --}}

                                {{-- <div class="form-group">
                                    <label
                                        class="col col-form-label text-md-left">@lang('products.stock_status')</label>
                                    <div class="col">
                                        <select class="form-control" name="stock_status">
                                            <option
                                                value="in" {{ $product->stock_status == "in" ? "selected" : "" }}>@lang('products.in_stock')</option>
                                            <option
                                                value="out" {{ $product->stock_status == "out" ? "selected" : "" }}>@lang('products.out_of_stock')</option>
                                        </select>
                                    </div>
                                </div> --}}
                            </div>


                            <div class="row">
                                <div class="form-group">
                                    <label class="col col-form-label text-md-left">@lang('products.status')</label>
                                    <div class="col">
                                        <select class="form-control" name="status">
                                            <option
                                                value="active" {{ $product->status == "active" ? "selected" : "" }}>@lang('products.active')</option>
                                            <option
                                                value="inactive" {{ $product->status == "inactive" ? "selected" : "" }}>@lang('products.inactive')</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="tab-pane fade" id="nav-images" role="tabpanel" aria-labelledby="nav-images-tab">
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <input type="file" class="form-control" id="images" name="images[]"
                                           onchange="preview_images();" multiple/>
                                </div>

                            </div>
                            <div class="row text-center text-lg-left" id="image_preview">
                                @foreach($product->get_images() as $image)

                                    <div class="editable-pic col-md-3" id="image_{{ $image->id }}">
                                        <a href="{{ $image->media_url }}" data-toggle="lightbox" data-gallery="gallery">
                                            <img src="{{ $image->media_url }}" class="img-fluid img-thumbnail">
                                            <div class="edit">
                                                <a  class="btn btn-danger btn-xs" onclick="event.preventDefault();removeImage(event,{{ $image->id }})">@lang('admin.delete')</a>
                                            </div>
                                        </a>
                                    </div>



                                @endforeach
                            </div>


                        </div>

                        <div class="tab-pane fade" id="nav-attributes" role="tabpanel"
                             aria-labelledby="nav-attributes-tab">

                            <div class="row">
    @foreach($attributes as $attribute)
        <div class="form-group">
            <label class="col col-form-label text-md-left">{{ $attribute->name }}</label>
            <div class="col">
                <select class="form-control attribute_select" name="variant_attributes[{{ $attribute->id }}]" data-name="{{ $attribute->name }}" data-id="{{ $attribute->id }}">
                    @if($attribute->name!= "colors" && $attribute->name!= "sizes")
                    <option value="any">@lang('products.any')</option>
                    @endif
                    @foreach($product->parent()->get_attribute_values($attribute->id) as $product_attr)

    <option value="{{ $product_attr }}" {{ $product->check_variant_value($attribute->id,$product_attr) ? "selected" : "" }} style="background-color:{{ $product_attr }}" >
{{$product_attr}}
</option>
                    @endforeach
                </select>
            </div>
        </div>
    @endforeach
                        </div>
                        <div class="row">
                            <div class="col">
                                <button type="submit" class="btn btn-primary">
                                    @lang('admin.edit')
                                </button>
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </form>
    </div>



    <div class="modal fade" id="add_variation_modal" tabindex="-1" role="dialog" aria-labelledby="add_variant_modal"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">@lang('products.add_variation')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                            {{-- <div class="form-group">
                                <label
                                    class="col col-form-label text-md-left">@lang('products.sku')</label>
                                <div class="col">
                                    <input type="text" class="form-control" value="{{ $product->sku }}" id="variation_sku">
                                </div>
                            </div> --}}

                        <div class="form-group">
                            <label
                                class="col col-form-label text-md-left">@lang('products.unit_price')</label>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $product->unit_price }}" id="variation_unit_price">
                            </div>
                        </div>

                        <div class="form-group">
                            <label
                                class="col col-form-label text-md-left">@lang('products.unit_discount_price')</label>
                            <div class="col">
                                <input type="text" class="form-control" value="{{ $product->unit_discount_price }}"  id="variation_unit_discount_price">
                            </div>
                        </div>

                        {{-- <div class="form-group">
                            <label
                                class="col col-form-label text-md-left">@lang('products.stock_status')</label>
                            <div class="col">
                                <select class="form-control" id="variation_stock_status">
                                    <option
                                        value="in" {{ $product->stock_status == "in" ? "selected" : "" }}>@lang('products.in_stock')</option>
                                    <option
                                        value="out" {{ $product->stock_status == "out" ? "selected" : "" }}>@lang('products.out_of_stock')</option>
                                </select>
                            </div>
                        </div> --}}

                        <div class="form-group">
                            <label class="col col-form-label text-md-left">@lang('products.status')</label>
                            <div class="col">
                                <select class="form-control" id="variation_status">
                                    <option
                                        value="active" {{ $product->status == "active" ? "selected" : "" }}>@lang('products.active')</option>
                                    <option
                                        value="inactive" {{ $product->status == "inactive" ? "selected" : "" }}>@lang('products.inactive')</option>
                                </select>
                            </div>
                        </div>



                </div>

                    <div class="row">
                        @foreach($attributes as $attribute)
                        <div class="form-group">
                            <label class="col col-form-label text-md-left">{{ $attribute->name }}</label>
                            <div class="col">
                                <select class="form-control attribute_select" data-name="{{ $attribute->name }}" data-id="{{ $attribute->id }}">
                                    <option value="any">@lang('products.any')</option>
                                    @foreach($product->get_attribute_values($attribute->id) as $product_attr)
                                        <option value="{{ $product_attr }}">{{ $product_attr }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @endforeach


                        <div class="form-group">
                            <label class="col col-form-label text-md-left">@lang('products.variation_images')</label>
                                <div class="col">
                                    <input type="file" class="form-control" id="variation_images" name="variation_images[]"
                                           onchange="preview_variation_images();" multiple/>
                                </div>
                            <div class="row text-center text-lg-left" id="variation_image_preview"></div>
                        </div>
                    </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('admin.close')</button>
                    <button type="button" class="btn btn-primary" onclick="add_to_variations()">@lang('admin.save')</button>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>


    <script>
        function preview_images() {
            var total_file = document.getElementById("images").files.length;
            for (var i = 0; i < total_file; i++) {
                template = `<div class="col-lg-3 col-md-4 col-6">
                <a href="#" class="d-block mb-4 h-100">
                    <img class="img-fluid img-thumbnail" src="${URL.createObjectURL(event.target.files[i])}" alt="">
                </a>
            </div>`;
                $('#image_preview').append(template);
            }
        }


        function preview_variation_images() {
            var total_file = document.getElementById("variation_images").files.length;
            for (var i = 0; i < total_file; i++) {
                template = `<div class="col-lg-3 col-md-4 col-6">
                <a href="#" class="d-block mb-4 h-100">
                    <img class="img-fluid img-thumbnail" src="${URL.createObjectURL(event.target.files[i])}" alt="">
                </a>
            </div>`;
                $('#variation_image_preview').append(template);
            }
        }


        $("#accordion").on("hide.bs.collapse show.bs.collapse", e => {
            $(e.target)
                .prev()
                .find("i:last-child")
                .toggleClass("fa-minus fa-plus");
        });

        $("#product_categories_id").select2();
        $("#tags_select").select2({
            tags: true
        });


        function add_to_variations() {
            var list_elements = '';
            product_name = '{!! $product->name !!}';
            // sku = $('#variation_sku').val();
            unit_price = $('#variation_unit_price').val();
            unit_discount_price = $('#variation_unit_discount_price').val();
            status = $('#variation_status').val();
            // stock_status = $('#variation_stock_status').val();

            header_rand = "heading_" + (""+Math.random()).substring(2,7);
            collapse_rand = "collapse_" + (""+Math.random()).substring(2,7);
            rand_index = Math.floor(Math.random() * (100 - 1 + 1)) + 1;


            $('.attribute_select').each(function(key,ele){

                var attr_name = $(ele).attr('data-name');
                var attr_id = $(ele).attr('data-id');
                var attr_value = $(ele).val() === "any" ? "" : $(ele).val();
                list_elements += `<li>${attr_name} : ${attr_value} <input type="hidden" name="variations[${rand_index}][attributes][${attr_id}]" value="${attr_value}"></li>`;

            });


            edit_form = $("#edit_product_form");

            $("#variation_images").clone().prop('class','d-none').appendTo(edit_form);


            var new_product_name = product_name;

            template = `<div class="card">
                                            <div class="card-header" id="${header_rand}">
                                                <h2 class="mb-0">
                                                    <button
                                                        onclick="event.preventDefault();"
                                                        class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                                                        data-toggle="collapse" data-target="#${collapse_rand}"
                                                        aria-expanded="false" aria-controls="${collapse_rand}">
                                                        ${new_product_name}

                                                        <span class="fa-stack fa-sm">
                                                            <i class="fas fa-circle fa-stack-2x"></i>
                                                            <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                                                          </span>
                                                          </span>
                                                    </button>
                                                </h2>
                                            </div>
                                            <div id="${collapse_rand}" class="collapse" aria-labelledby="${header_rand}"
                                                 data-parent="#accordion">
                                                <div class="card-body">
                                                    <ul>
                                                        <li>السعر : ${unit_price} <input type="hidden" name="variations[${rand_index}][unit_price]" value="${unit_price}"></li>
                                                        <li>سعر الخصم : ${unit_discount_price} <input type="hidden" name="variations[${rand_index}][unit_discount_price]" value="${unit_discount_price}"></li>
                                                        <li>حالة المنتج : ${status} <input type="hidden" name="variations[${rand_index}][status]" value="${status}"></li>
                                                        ${list_elements}
                                                        <input type="hidden" name="variations[${rand_index}][variation_name]" value="${new_product_name}">

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>`;

            $('#accordion').append(template);

            $('#add_variation_modal').modal('hide');

        }


        $('#add_variation_modal').on('hidden.bs.modal', function (event) {
            var modal = $(this);
            modal.find('#variation_images').val('');
            modal.find('#variation_image_preview').empty();

        });


        function removeImage(event,id) {

            element = $('#image_'+id);

            removed_image = `<input type="hidden" name="removed_images_ids[]" value="${id}">`;

            $('#edit_product_form').append($(removed_image));

            element.hide();


        }

        $(".attributes_select").select2();

        $(document).on("click", '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });


        function deleteItem(id,url) {
            swal({
                title: '{!! trans('admin.are_you_sure') !!}',
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "{!! trans('admin.yes'); !!}",
                cancelButtonText: "{!! trans('admin.no'); !!}",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {



                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method : 'delete' }
                })
                    .done(function() {


                        swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.deleted_successfully') !!}", type: "success"},
                            function(){
                                location.reload();
                            }
                        );


                    })
                    .fail(function(e) {

                        swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")

                    })


            });
        }


    </script>
@endsection
