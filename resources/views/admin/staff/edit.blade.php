@extends('layouts.app')

@section('title', 'Edit staff')


@section('styles')

    <style>
        .myaccordion {

            margin: 50px auto;
            box-shadow: 0 0 1px rgba(0, 0, 0, 0.1);
        }

        .myaccordion .card,
        .myaccordion .card:last-child .card-header {
            border: none;
        }

        .myaccordion .card-header {
            border-bottom-color: #EDEFF0;
            background: transparent;
        }

        .myaccordion .fa-stack {
            font-size: 18px;
        }

        .myaccordion .btn {
            width: 100%;
            font-weight: bold;
            color: #004987;
            padding: 0;
        }

        .myaccordion .btn-link:hover,
        .myaccordion .btn-link:focus {
            text-decoration: none;
        }

        .myaccordion li+li {
            margin-top: 10px;
        }

        select {
            height: 100% !important;
        }

    </style>


@endsection



@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container">

        <form id="store_form" method="POST" action="/admin/staff/update/{{ $staff->id }}" enctype="multipart/form-data">
            @csrf
            <div class="card card-custom" style="width: 1000px;margin-right: 30px;">
                <!--Begin::Header-->
                <div class="card-header card-header-tabs-line">
                    <div class="card-toolbar">
                        <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x"
                            role="tablist">
                            <li class="nav-item mr-3">
                                <a class="nav-link active" data-toggle="tab" href="#kt_apps_contacts_view_tab_2">
                                    <span class="nav-icon mr-2">
                                        <span class="svg-icon mr-3">
                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Chat-check.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <path
                                                        d="M4.875,20.75 C4.63541667,20.75 4.39583333,20.6541667 4.20416667,20.4625 L2.2875,18.5458333 C1.90416667,18.1625 1.90416667,17.5875 2.2875,17.2041667 C2.67083333,16.8208333 3.29375,16.8208333 3.62916667,17.2041667 L4.875,18.45 L8.0375,15.2875 C8.42083333,14.9041667 8.99583333,14.9041667 9.37916667,15.2875 C9.7625,15.6708333 9.7625,16.2458333 9.37916667,16.6291667 L5.54583333,20.4625 C5.35416667,20.6541667 5.11458333,20.75 4.875,20.75 Z"
                                                        fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                    <path
                                                        d="M2,11.8650466 L2,6 C2,4.34314575 3.34314575,3 5,3 L19,3 C20.6568542,3 22,4.34314575 22,6 L22,15 C22,15.0032706 21.9999948,15.0065399 21.9999843,15.009808 L22.0249378,15 L22.0249378,19.5857864 C22.0249378,20.1380712 21.5772226,20.5857864 21.0249378,20.5857864 C20.7597213,20.5857864 20.5053674,20.4804296 20.317831,20.2928932 L18.0249378,18 L12.9835977,18 C12.7263047,14.0909841 9.47412135,11 5.5,11 C4.23590829,11 3.04485894,11.3127315 2,11.8650466 Z M6,7 C5.44771525,7 5,7.44771525 5,8 C5,8.55228475 5.44771525,9 6,9 L15,9 C15.5522847,9 16,8.55228475 16,8 C16,7.44771525 15.5522847,7 15,7 L6,7 Z"
                                                        fill="#000000" />
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </span>
                                    <span class="nav-text font-weight-bold">Basic information</span>
                                </a>
                            </li>
                            <li class="nav-item mr-3">
                                <a class="nav-link" data-toggle="tab" href="#kt_apps_contacts_view_tab_3">
                                    <span class="nav-icon mr-2">
                                        <span class="svg-icon mr-3">
                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Devices/Display1.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <path
                                                        d="M11,20 L11,17 C11,16.4477153 11.4477153,16 12,16 C12.5522847,16 13,16.4477153 13,17 L13,20 L15.5,20 C15.7761424,20 16,20.2238576 16,20.5 C16,20.7761424 15.7761424,21 15.5,21 L8.5,21 C8.22385763,21 8,20.7761424 8,20.5 C8,20.2238576 8.22385763,20 8.5,20 L11,20 Z"
                                                        fill="#000000" opacity="0.3" />
                                                    <path
                                                        d="M3,5 L21,5 C21.5522847,5 22,5.44771525 22,6 L22,16 C22,16.5522847 21.5522847,17 21,17 L3,17 C2.44771525,17 2,16.5522847 2,16 L2,6 C2,5.44771525 2.44771525,5 3,5 Z M4.5,8 C4.22385763,8 4,8.22385763 4,8.5 C4,8.77614237 4.22385763,9 4.5,9 L13.5,9 C13.7761424,9 14,8.77614237 14,8.5 C14,8.22385763 13.7761424,8 13.5,8 L4.5,8 Z M4.5,10 C4.22385763,10 4,10.2238576 4,10.5 C4,10.7761424 4.22385763,11 4.5,11 L7.5,11 C7.77614237,11 8,10.7761424 8,10.5 C8,10.2238576 7.77614237,10 7.5,10 L4.5,10 Z"
                                                        fill="#000000" />
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </span>
                                    <span class="nav-text font-weight-bold">Experience</span>
                                </a>
                            </li>
                            <li class="nav-item mr-3">
                                <a class="nav-link" data-toggle="tab" href="#kt_apps_contacts_view_tab_income">
                                    <span class="nav-icon mr-2">
                                        <span class="svg-icon mr-3">
                                            <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Navigation\Down-left.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24"/>
                                                    <rect fill="#000000" opacity="0.3" transform="translate(12.353553, 12.146447) rotate(-135.000000) translate(-12.353553, -12.146447) " x="11.3535534" y="5.14644661" width="2" height="14" rx="1"/>
                                                    <path d="M15.8890873,16.0961941 C16.441372,16.0961941 16.8890873,16.5439093 16.8890873,17.0961941 C16.8890873,17.6484788 16.441372,18.0961941 15.8890873,18.0961941 L7.40380592,18.0961941 C6.86841446,18.0961941 6.42800568,17.6745174 6.40474976,17.1396313 L6.05119637,9.00790332 C6.02720666,8.45613984 6.45505183,7.98939965 7.00681531,7.96540994 C7.55857879,7.94142022 8.02531897,8.36926539 8.04930869,8.92102887 L8.36127239,16.0961941 L15.8890873,16.0961941 Z" fill="#000000" fill-rule="nonzero"/>
                                                </g>
                                            </svg><!--end::Svg Icon-->
                                        </span>

                                    </span>

                                    <span class="nav-text font-weight-bold">Income</span>
                                </a>
                            </li>
                            <li class="nav-item mr-3">
                                <a class="nav-link" data-toggle="tab" href="#kt_apps_contacts_view_tab_outcome">
                                    <span class="nav-icon mr-2">
                                        <span class="svg-icon mr-3">
                                            <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Navigation\Up-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24"/>
                                                    <rect fill="#000000" opacity="0.3" transform="translate(11.646447, 12.853553) rotate(-315.000000) translate(-11.646447, -12.853553) " x="10.6464466" y="5.85355339" width="2" height="14" rx="1"/>
                                                    <path d="M8.1109127,8.90380592 C7.55862795,8.90380592 7.1109127,8.45609067 7.1109127,7.90380592 C7.1109127,7.35152117 7.55862795,6.90380592 8.1109127,6.90380592 L16.5961941,6.90380592 C17.1315855,6.90380592 17.5719943,7.32548256 17.5952502,7.8603687 L17.9488036,15.9920967 C17.9727933,16.5438602 17.5449482,17.0106003 16.9931847,17.0345901 C16.4414212,17.0585798 15.974681,16.6307346 15.9506913,16.0789711 L15.6387276,8.90380592 L8.1109127,8.90380592 Z" fill="#000000" fill-rule="nonzero"/>
                                                </g>
                                            </svg><!--end::Svg Icon-->
                                        </span>

                                    </span>

                                    <span class="nav-text font-weight-bold">Outcome</span>
                                </a>
                            </li>
                            <li class="nav-item mr-3">
                                <a class="nav-link" data-toggle="tab" href="#kt_apps_contacts_view_tab_5">
                                    <span class="nav-icon mr-2">
                                        <span class="svg-icon mr-3">
                                            <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Design\Image.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <path
                                                        d="M6,5 L18,5 C19.6568542,5 21,6.34314575 21,8 L21,17 C21,18.6568542 19.6568542,20 18,20 L6,20 C4.34314575,20 3,18.6568542 3,17 L3,8 C3,6.34314575 4.34314575,5 6,5 Z M5,17 L14,17 L9.5,11 L5,17 Z M16,14 C17.6568542,14 19,12.6568542 19,11 C19,9.34314575 17.6568542,8 16,8 C14.3431458,8 13,9.34314575 13,11 C13,12.6568542 14.3431458,14 16,14 Z"
                                                        fill="#000000" />
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>

                                    </span>

                                    <span class="nav-text font-weight-bold">Images</span>
                                </a>
                            </li>
                            <li class="nav-item mr-3">
                                <a class="nav-link" data-toggle="tab" href="#kt_apps_contacts_view_tab_5_video">
                                    <span class="nav-icon mr-2">
                                        <span class="svg-icon mr-3">
                                            <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Media\Airplay-video.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"/>
                                                    <path d="M7,15 C7.55228475,15 8,15.4477153 8,16 C8,16.5522847 7.55228475,17 7,17 L6,17 C4.34314575,17 3,15.6568542 3,14 L3,7 C3,5.34314575 4.34314575,4 6,4 L18,4 C19.6568542,4 21,5.34314575 21,7 L21,14 C21,15.6568542 19.6568542,17 18,17 L17,17 C16.4477153,17 16,16.5522847 16,16 C16,15.4477153 16.4477153,15 17,15 L18,15 C18.5522847,15 19,14.5522847 19,14 L19,7 C19,6.44771525 18.5522847,6 18,6 L6,6 C5.44771525,6 5,6.44771525 5,7 L5,14 C5,14.5522847 5.44771525,15 6,15 L7,15 Z" fill="#000000" fill-rule="nonzero"/>
                                                    <polygon fill="#000000" opacity="0.3" points="8 20 16 20 12 15"/>
                                                </g>
                                            </svg><!--end::Svg Icon-->
                                        </span>

                                    </span>

                                    <span class="nav-text font-weight-bold">Videos</span>
                                </a>
                            </li>

                            <li class="nav-item mr-3">
                                <a class="nav-link" data-toggle="tab" href="#kt_apps_contacts_view_tab_5_pdf">
                                    <span class="nav-icon mr-2">
                                        <span class="svg-icon mr-3">
                                            <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\General\Attachment1.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"/>
                                                    <path d="M12.4644661,14.5355339 L9.46446609,14.5355339 C8.91218134,14.5355339 8.46446609,14.9832492 8.46446609,15.5355339 C8.46446609,16.0878187 8.91218134,16.5355339 9.46446609,16.5355339 L12.4644661,16.5355339 L12.4644661,17.5355339 C12.4644661,18.6401034 11.5690356,19.5355339 10.4644661,19.5355339 L6.46446609,19.5355339 C5.35989659,19.5355339 4.46446609,18.6401034 4.46446609,17.5355339 L4.46446609,13.5355339 C4.46446609,12.4309644 5.35989659,11.5355339 6.46446609,11.5355339 L10.4644661,11.5355339 C11.5690356,11.5355339 12.4644661,12.4309644 12.4644661,13.5355339 L12.4644661,14.5355339 Z" fill="#000000" opacity="0.3" transform="translate(8.464466, 15.535534) rotate(-45.000000) translate(-8.464466, -15.535534) "/>
                                                    <path d="M11.5355339,9.46446609 L14.5355339,9.46446609 C15.0878187,9.46446609 15.5355339,9.01675084 15.5355339,8.46446609 C15.5355339,7.91218134 15.0878187,7.46446609 14.5355339,7.46446609 L11.5355339,7.46446609 L11.5355339,6.46446609 C11.5355339,5.35989659 12.4309644,4.46446609 13.5355339,4.46446609 L17.5355339,4.46446609 C18.6401034,4.46446609 19.5355339,5.35989659 19.5355339,6.46446609 L19.5355339,10.4644661 C19.5355339,11.5690356 18.6401034,12.4644661 17.5355339,12.4644661 L13.5355339,12.4644661 C12.4309644,12.4644661 11.5355339,11.5690356 11.5355339,10.4644661 L11.5355339,9.46446609 Z" fill="#000000" transform="translate(15.535534, 8.464466) rotate(-45.000000) translate(-15.535534, -8.464466) "/>
                                                </g>
                                            </svg><!--end::Svg Icon-->
                                        </span>

                                    </span>

                                    <span class="nav-text font-weight-bold">Pdf</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <button type="submit" class="btn btn-primary font-weight-bold" style="margin-top: 15px;">Save</button>
                    </div>
                </div>
                <!--end::Header-->
                <!--Begin::Body-->

                <div class="card-body">

                    <div class="tab-content pt-5">
                        <!--begin::Tab Content-->
                        <div class="tab-pane active" id="kt_apps_contacts_view_tab_2" role="tabpanel">
                            <!--begin::Heading-->

                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 text-right col-form-label" data-required="yes">Name</label>
                                <div class="col-lg-9 col-xl-6">
                                    <input name="name" id="product_new_name" class="form-control form-control-lg
                                                            @error('name') is-invalid @enderror" required type="text"
                                        value="{{ $staff->name }}" />
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 text-right col-form-label"
                                    data-required="yes">Nationality</label>
                                <div class="col-lg-9 col-xl-6">
                                    <input name="nationality" class="form-control form-control-lg
                                                @error('nationality') is-invalid @enderror" required type="text"
                                        value="{{ $staff->nationality }}" />
                                    @error('nationality')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 text-right col-form-label" data-required="yes">Age</label>
                                <div class="col-lg-9 col-xl-6">
                                    <input name="age" class="form-control form-control-lg
                                                @error('age') is-invalid @enderror" required type="number" min="10"
                                        value="{{ $staff->age }}" />
                                    @error('age')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 text-right col-form-label" data-required="yes">Passport
                                    number</label>
                                <div class="col-lg-9 col-xl-6">
                                    <input name="passport_number" class="form-control form-control-lg
                                                @error('passport_number') is-invalid @enderror" required type="number"
                                        min="10" value="{{ $staff->passport_number }}" />
                                    @error('passport_number')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 text-right col-form-label"
                                    data-required="yes">Gender</label>
                                <div class="col-lg-9 col-xl-6">
                                    <select name="gender" required class="form-control selectpicker">
                                        <option @if ($staff->gender == 'male') {{ 'selected' }} @endif value="male">Male</option>
                                        <option @if ($staff->gender == 'female') {{ 'selected' }} @endif value="female">Female
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 text-right col-form-label">
                                    Image</label>
                                <div class="image-input image-input-outline" id="kt_image_4"
                                    style="background-image: url(/uploads/media/users/blank.png)">
                                    <div class="image-input-wrapper"
                                        style="background-image: url({{ $staff->image ?? '' }})"></div>

                                    <label
                                        class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                        data-action="change" data-toggle="tooltip" title=""
                                        data-original-title="Change avatar">
                                        <i class="fa fa-pen icon-sm text-muted"></i>
                                        <input type="file" name="image_url" accept=".png, .jpg, .jpeg" />
                                        <input type="hidden" name="profile_avatar_remove" />
                                    </label>

                                    <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                        data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                                    </span>

                                    <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                        data-action="remove" data-toggle="tooltip" title="Remove avatar">
                                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                                    </span>
                                </div>


                            </div>
                        </div>


                        <!--end::Tab Content-->
                        <!--begin::Tab Content-->
                        <div class="tab-pane" id="kt_apps_contacts_view_tab_3" role="tabpanel">
                            <!--end::Heading-->
                            <div id="kt_repeater_1">
                                <div class="form-group row" id="kt_repeater_1">
                                    <div data-repeater-list="exper" class="col-lg-10">
                                        @foreach ($staff->experience as $exp)
                                            <div data-repeater-item class="form-group row align-items-center">
                                                <div class="col-md-3">
                                                    <label>Title:</label>
                                                    <input type="text" name="title" value="{{ $exp->title }}"
                                                        class="form-control" />
                                                    <div class="d-md-none mb-2"></div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>From:</label>
                                                    <input name="from" type="date" value="{{ $exp->from_date }}"
                                                        class="form-control" />
                                                    <div class="d-md-none mb-2"></div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>TO:</label>
                                                    <input name="to" type="date" value="{{ $exp->to_date }}"
                                                        class="form-control" />
                                                    <div class="d-md-none mb-2"></div>
                                                </div>
                                                <div class="col-md-3">
                                                    <a href="javascript:;" data-repeater-delete=""
                                                        class="btn btn-sm font-weight-bolder btn-light-danger">
                                                        <i class="la la-trash-o"></i>Delete
                                                    </a>
                                                </div>
                                            </div>

                                        @endforeach
                                        @if ($staff->experience->count() == 0)
                                            <div data-repeater-item class="form-group row align-items-center">
                                                <div class="col-md-3">
                                                    <label>Title:</label>
                                                    <input type="text" name="title" class="form-control" />
                                                    <div class="d-md-none mb-2"></div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>From:</label>
                                                    <input name="from" type="date" class="form-control" />
                                                    <div class="d-md-none mb-2"></div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>TO:</label>
                                                    <input name="to" type="date" class="form-control" />
                                                    <div class="d-md-none mb-2"></div>
                                                </div>
                                                <div class="col-md-3">
                                                    <a href="javascript:;" data-repeater-delete=""
                                                        class="btn btn-sm font-weight-bolder btn-light-danger">
                                                        <i class="la la-trash-o"></i>Delete
                                                    </a>
                                                </div>
                                            </div>

                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right"></label>
                                    <div class="col-lg-4">
                                        <a href="javascript:;" data-repeater-create=""
                                            class="btn btn-sm font-weight-bolder btn-light-primary">
                                            <i class="la la-plus"></i>Add
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane" id="kt_apps_contacts_view_tab_income" role="tabpanel">
                            <div id="kt_repeater_22">
                                <div class="form-group row" id="kt_repeater_22">
                                    <div data-repeater-list="income" class="col-lg-10">
                                        @foreach ($staff->incomes as $in)
                                            <div data-repeater-item class="form-group row align-items-center">
                                                <input type="hidden" value="{{ $in->id }}" name="id">

                                                <div class="col-md-4">
                                                    <label>Title:</label>
                                                    <input type="text" name="title" value="{{ $in->title }}"
                                                        class="form-control" />
                                                    <div class="d-md-none mb-2"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Cost:</label>
                                                    <input name="cost" type="number" value="{{ $in->cost }}"
                                                        class="form-control" />
                                                    <div class="d-md-none mb-2"></div>
                                                </div>

                                                <div class="col-md-4">
                                                    <a href="javascript:;" data-repeater-delete=""
                                                        class="btn btn-sm font-weight-bolder btn-light-danger">
                                                        <i class="la la-trash-o"></i>Delete
                                                    </a>
                                                </div>
                                            </div>

                                        @endforeach
                                        @if ($staff->incomes->count() == 0)
                                            <div data-repeater-item class="form-group row align-items-center">
                                                <div class="col-md-4">
                                                    <label>Title:</label>
                                                    <input type="text" name="title"
                                                        class="form-control" />
                                                    <div class="d-md-none mb-2"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Cost:</label>
                                                    <input name="cost" type="number"
                                                        class="form-control" />
                                                    <div class="d-md-none mb-2"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="javascript:;" data-repeater-delete=""
                                                        class="btn btn-sm font-weight-bolder btn-light-danger">
                                                        <i class="la la-trash-o"></i>Delete
                                                    </a>
                                                </div>
                                            </div>

                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right"></label>
                                    <div class="col-lg-4">
                                        <a href="javascript:;" data-repeater-create=""
                                            class="btn btn-sm font-weight-bolder btn-light-primary">
                                            <i class="la la-plus"></i>Add
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="kt_apps_contacts_view_tab_outcome" role="tabpanel">
                            <div id="kt_repeater_33">
                                <div class="form-group row" id="kt_repeater_33">
                                    <div data-repeater-list="outcome" class="col-lg-10">
                                        @foreach ($staff->outcomes as $in)
                                        <div data-repeater-item class="form-group row align-items-center">
                                        <input type="hidden" value="{{ $in->id }}" name="id">

                                                <div class="col-md-4">
                                                    <label>Title:</label>
                                                    <input type="text" name="title" value="{{ $in->title }}"
                                                       class="form-control" />
                                                    <div class="d-md-none mb-2"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Cost:</label>
                                                    <input name="cost" type="number" value="{{ $in->cost }}"
                                                        class="form-control" />
                                                    <div class="d-md-none mb-2"></div>
                                                </div>

                                                <div class="col-md-4">
                                                    <a href="javascript:;" data-repeater-delete=""
                                                        class="btn btn-sm font-weight-bolder btn-light-danger">
                                                        <i class="la la-trash-o"></i>Delete
                                                    </a>
                                                </div>
                                            </div>

                                        @endforeach
                                        @if ($staff->outcomes->count() == 0)
                                            <div data-repeater-item class="form-group row align-items-center">

                                                <div class="col-md-4">
                                                    <label>Title:</label>
                                                    <input type="text" name="title"
                                                        class="form-control" />
                                                    <div class="d-md-none mb-2"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Cost:</label>
                                                    <input name="cost" type="number"
                                                        class="form-control" />
                                                    <div class="d-md-none mb-2"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="javascript:;" data-repeater-delete=""
                                                        class="btn btn-sm font-weight-bolder btn-light-danger">
                                                        <i class="la la-trash-o"></i>Delete
                                                    </a>
                                                </div>
                                            </div>

                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right"></label>
                                    <div class="col-lg-4">
                                        <a href="javascript:;" data-repeater-create=""
                                            class="btn btn-sm font-weight-bolder btn-light-primary">
                                            <i class="la la-plus"></i>Add
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="kt_apps_contacts_view_tab_5" role="tabpanel">
                            <div class="container">
                                <div class="custom-file">
                                    <input name="images[]" type="file" accept="image/*" multiple class="custom-file-input"
                                        id="customFile">
                                    <label class="custom-file-label" for="customFile">Choose Images</label>
                                </div>
                                <div class="row" style=" margin-top: 10px; ">
                                    @foreach ($staff->images as $image)
                                        <div class="col-3" id="{{ $image->id }}">
                                            <a href="#"
                                                onclick="$('#input{{ $image->id }}').val('deleted');document.getElementById('{{ $image->id }}').style.display='none';">
                                                <i class="flaticon2-trash text-danger"></i></a>
                                            <img src="{{ $image->path }}" style="max-width: 150px; max-height: 150px;" />
                                            <input name="attachment[]" value="{{ $image->id }}"
                                                id="input{{ $image->id }}" type="hidden"/>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="kt_apps_contacts_view_tab_5_video" role="tabpanel">
                            <div class="container">
                                <div class="custom-file">
                                    <input name="videos[]" type="file" accept=".mp4" multiple class="custom-file-input"
                                        id="customFile">
                                    <label class="custom-file-label" for="customFile">Choose Videos</label>
                                </div>
                                <div class="row" style=" margin-top: 10px; ">
                                    @foreach ($staff->videos as $video)
                                    <div class="col-lg-3 col-md-6 portfolio-item filter-app" id="v{{ $video->id }}">
                                        <a href="#"
                                        onclick="$('#inputv{{ $video->id }}').val('deleted');document.getElementById('v{{ $video->id }}').style.display='none';">
                                        <i class="flaticon2-trash text-danger"></i></a>
                                        <div class="embed-responsive embed-responsive-1by1">
                                            <video width="320" height="240" controls>
                                                <source src="{{ $video->path }}" type="video/mp4" />
                                            </video>
                                            <input name="attachment[]" value="{{ $video->id }}" type="hidden"
                                                id="inputv{{ $video->id }}" />
                                          </div>
                                      </div>

                                    @endforeach

                                </div>

                            </div>
                        </div>
                        <div class="tab-pane" id="kt_apps_contacts_view_tab_5_pdf" role="tabpanel">
                            <div class="container">
                                <div class="custom-file">
                                    <input name="files[]" type="file" accept=".pdf" multiple class="custom-file-input"
                                        id="customFile">
                                    <label class="custom-file-label" for="customFile">Choose PDF</label>
                                </div>
                                <div class="row" style=" margin-top: 10px; ">
                                @foreach ($staff->pdfs as $pdf)
                                <div class="col-3" id="p{{ $pdf->id }}">
                                 <div class="row">
                                    <a href="#"
                                        onclick="$('#inputp{{ $pdf->id }}').val('deleted');document.getElementById('p{{ $pdf->id }}').style.display='none';">
                                        <i class="flaticon2-trash text-danger"></i></a>
                     <a href="{{ $pdf->path }}" target="_blanck" style="max-width: 150px; max-height: 150px;">   <img style="max-width: 60px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMsAAAD5CAMAAAC+lzGnAAAAzFBMVEX19fX/IRb///8sLCz1+/v/CwD6m5n6oJ317u38dHD/ZF3/Ggz/AAD1+Pj6+vooKCgeHh7a2toZGRkzMzOurq70//9DQ0PQ0NAkJCRPT0/AwMD/GAkTExNra2v24+L23dyfn5/3zcv5r6z+PTX+LyVjY2NYWFj8Z2H6j4v/7ez24N/21NL9WFL5qKX4u7j9U0z4wb/p6emEhIT+Myr9SkP7eHP4v737g375rKn8bGf7h4T/zcv6k4/+Qjr9S0QHBweRkZGGhoanp6d3d3fUHSfIAAAOzElEQVR4nO2da1visNaG09JAIQ2KWmWotgJVDi0FNwJy3LPH//+f3rRNegBUENLDe/F8mEunWHJ3rWStHAvEHfWMj1llPRZSkLmZd51+b7dMBwls/S47KxspEKVBQoQ0qJiV7ugMLL3SWIBaShyBNCi8L6snsjhjJS2DbAkptnM0TYSlN9MyQuJKQ+Vj603IsoQw7fLHBWHzlywfQoaM4guZpV+xrOJNlwbTUdzLEZz9gqUV9S+NNIuLYgpalDvteDyAkyNaAJ9laYY3gIq9NHQgpSAgy/1mtx2tuHB+JEsvfBaaObNUjEFawlhSjZkZxjhlLh/FMgkeBKw0UwQJeKbr0DSwfAxLK3gKaCKlTuIKy5OwWYWdA+sMYem1gz8rpg3BhLETgZkfFjUJS0lhfzNLGyEiPA0rMawcyNIb07/R7LTLH5M0GkdgDrEMEFuIRdllJupKINxUIjAH1BlQZY0YXElpl35L0jJiGftny4A+q/manC2zANcy5jFuBiyFZQtq2kXfldSMtGbDn9wMrJiLWZkzC3Bh2hE3039g6fiBEq37WWQB2Ar77HD4fToD2szF5LSLvV+StQ5hNt/WGcA+tsqkWYBrmXHEMv1DWEpZa5EDSVGY928skwMWIBmbiJtZuWYhMAI8wDK5YAHYGIaWaX9lmXywEMusI5b5AiYnLPE60zZyzQKkvh1aZr13FDA3LADrbfi9ZfLDQmA6AYw2XuaahcCEbqa1d2HyxLJlmZ06kysWAlNWQpjt2bN8sQAsV0IY08o1izsKGLgZEqa5ZiEwoZuhuJvljiVuGXOaaxYCMwstY7ZyzRJ3MzTNNQvAoBu6mTbINQuBibiZ0Mo1C3GziGVQK9csxDKrSAOwTIIFc5sxxGARyc34s2CsG4bOCQeDUtifmVc5s0jWwn5/r8wMTqPuuBu6mcOZZYCghpAGhS6v6ZAiGzhH4x5PFuwQEOguBnPn6DnBBA2AsuDIgvWNhkynb7jZE1yc9+bBl8isc4ZMgx+L1FJQ25IwJj+Qx9bi0+RjY03dDBY5sswh7HqepZKvQ2Ne9X9ADaN1etxYVBOZ/lSbVHSrzIhTjVGD+VmDHwtE7/4t8Uhx10FwMoz3pLzav+TFgvsQTiiLO72rVTjNu2GLTbiWuLFYpLowFpO0M+0mp/Bv0NlZOOPIwtoubHnfwKnCYH3os2hzniwOZWm6AQ1NOVV+mc2EV7iy+KX3WXhFGCCXYQIsA2oXJ+csQIZw4d/SbzXzzKJCWPZDijTJPYug2T6L6jWauWZ5h23K4n0XdHi1Y/xZSG5pesuFsOH3Mbi1yQmwdKHghXrJX19v8kouE2DBUwV9uPeUvBVq3HKYRFhYQobnblxGQz3HLEbbW5OGjY3bjHHLk5NgAfIcbgwM8NL07j/hNS6aBAsJ95DUEexXfVjMNUsLKe5NZ95XcesjJ8JCKow2VoHs98cVbss5k2ABkq0pOouUArfVz8mwDBRYUv2hBTjnNiWSCAvWFdhRba/Xx/qYHJQICwA2fHf8kQXEb1V6Mixuc7zxogu/qJ8Yi8UW48MFv5XcCfkYLrNR+AG/2dCEWCS2ndbklSSDxFgApl+z5rhZIDEWevMZxwn3pOrL1P8ahVdf31XCdR/ya5GTbpPRkOc+oYRY6Pwb3/20ybDIG7YN1ci7jwXpvsBGY7komZyfrYskvTGODVki/Urdn3pHdgtqNr9NtQn19/3g0pI7kN/QRTJjSv53oLEhWSZSuHVgEhnrY/VeBlJR0So5HreU6LI7zUv3y5BbkEnCLv52dWS69sD9dw1y6vLzZ5EcapaOF1qkkYLafCImfxZ5TlNkOl6pTiDkMzzOnQU3Tf8LxrT8WB9C2OUR/vnbhS6FhgPmV17D3OLgZdxZZIGaJezoSx/uQr/zw/Bm8dYlunedR8vehdr7+XMZ7nah2X5spgKT9gCef0UsZxZp5Nd8QYsVHOuCppVyxsJ2QW1nlNhaa/Dc6y/4stDFb4Jgbld1PEKaYEW+CRNFDob7zdJ/viwqa5AnO5ekAYRDwyPAQJZlXR85xW63u5iTf1atpi7Lx/LwZZHZKtt9GVgXwbnet6bOR7czHCtE4SmK5BdzXlzKRxWFK4tEh19Qe+uCaw0VdDRoD9sCZBBKIOhtZVBMuwiOKAxPFqz7U2GCUopVDAno1mhlkwojeJsv2sNKebH6aBmW6qpvWYNuZ+yWS1OEj4ywOOwwB5YXu+bQneLENt1HT0whoOFyafVlNVLfvUYANLvemWMIHn7aHlcfe0fRmo+xrDcXG1PwHIpYYz4oCZow0/eecojByB/xgMVDmwCOLHhJR8XMkQSwJDcHZaSQigAVuJmUHENSJdVpQ9h2wN7SSk2a/hw6ZcORRaUdF21Oml1rYY99txqXHUuXqUPh5hpqwmS/aVR/7TScH9hB4MeCWfoCneZMIAaBimDPPnQ1FggluUMsBUvGHhqal6LxgYMdHO0S7BS0NUgaWGX4sdSlnSJjuUgsA9dFXd2+xHLsQ49C5MbCRpJcJ4Na227pX2yxxLg5dPeTmRNjKzSqM/o0DlygyY0lKAgJeZOBsWuQUJJcEkjrDGHZkSMfw3Qx0MHztZxYMMnqKYrd6ss/nC2LsVV2jxeGwmbhpWjYa8FnX2QNibJgQMrGYot6yCG5WG1OTOiGRmW8GpF2TtZH7AgY5dACcWDBqjUz2dKEg4fCMGjOkKIh19falU7HS3E8lIMHoM7OIslWGYWH0R4xREm8alBpkzZPQJpGjxtHUFgc3Jc+MwvGo4kWnqy9p9/yrUhMbS02UPHOTif3UcYz5/BznM/KQprXipttdRbUw4SjxyfcJLrfLM0m9mY+Ky71Y25wRhZS4+fkiaLJUqa5PvrViJ7fWSbJ/7E95bOxkBo/GUNNGY7CnvE82VM/z8WCjYVbT+wpeZZNahWT5yKLPToPC5Y/2oqm2FOShWC5sqc3mYTOwULCtk2iwnrk5VNsK23SHnYWFqyvSNu1KfnDDKwHhcaJH118Ootq2SQQzGjdwDo9S1P5SPxollNZMCA5LqxYLKKxhBCWkz/t90QWDIh/oVUQ0VR6uon2znMRzxc6jQXLC9KBt4KxBxycCs5xieiXOokFy0MFdvTg87hPKwsspXH8+ml2WSjKLJIxqbTLoRyZUp5Jp7DglQK70ZRpwubz0jng/wQWbKF4qUv+DTROSxF+1Aks7lkpERS2NJzf/smf9HsWbJnReOgeNOTdSGsmHiSpfs8ifWhm6EzSlK7g4bYL/GedwFKCG/aGBQxaCl2MNE0N5RSWAWR2keQurfYmvx1UP+uE+tI04cSbO5VH7zREbtKq9p5Oacc6UFlYuuXY1L/gRE/RKqfFF8OEsD1ca5pPIgxSfgfOSXG/OVQ05M8qwuFRU75cdFI+JvWLbW8O25w46bqXpxNzfizL01FTlkH677s7Qx8Z8zuE81gltV8sCV1YsqkLSzZ1YcmmLizZ1IUlm7qwZFMXlmzqwpJNXViyqQtLNnVh2VH8pdjVajV68ZtLO9fDD6bFUr27vQ/05+nv3cNbWJZa5NLn30btbbuYf+539efheJhzsTxehapfP169PANWmNpN/NLTc02M/fXt1a5uG+mx3BTiql+/sCdbu966VL9/jtnmtrCrLLEUCtd/atW9LIXC1eNrLVLUTLJc/Zfo8fGm7sO8gCjLDblwc12noFEYn+X6MarrlFmu/rjtT+3h7pMW+Lkastw3arXG3d+XW5+mfv+2xfJci+ntyy9LisX7pQr+Xnlmeq0FLFf3D6Q5ropvjadHH/Q/cZaru1+0wtxZ3N8/vafvO0rA4l+ioIVC4EUZZ6k9+s9+l4XIh6k/MTfKNgsA91fef+xlefMuFq7Z/2Sd5cl1sqvbvSzVhtfq1T/FfLD4FeZmLwszzFWe7PIVC/h7HXWyrLO8evXlfj9LtVGPxJ/Ms/i5yusXLA+ek9X/lwcW8Z9Xu6//fuFjtT8eyxOIsDTcvg1VhlhEv6yF+sMXLG+eC9Zf3kKWwudzqFoGWLzivr39u41Gwz0s4KW+w3JVD/WLxPLMLIXb//2H6PX2xs9SaJH2sIh++PHztT05fwZYCt5DpelW4eYz0n/ZyxKzS9ZYoqqzRHgPS/Up8z7GRPr1Nzf/WOp4YN1/fQr1i4GLs7Pc3Lh9ytv7Py//wh791yyf2Y0vhfu7f//uGneNh1o0ROQ1Vu6LdftyGM+K13eZZtl3cY+PPftpAQ2J+WZ5yVXOH9FuX+zBN0te+mIR7bL4nZt6XvrIEW2z0JrPokueWcSG30POy5hSTDGWKkHxO5UvwQdyyOL2BmqNFz+J3hmDzRFL4fbp8/Pl9Z6Om8fmV3LH4vUHrmh/oF5oRD6QP5aIrl9jiXBOWUhvoP74+ByfkPB61Bliuf6GpR6ykN7AZ2N7nMXvv2SFBdQarvZ3od4aoR5qb7sjRv6l3429RHWutQrfjWtVq9+PfJ0yJhbVZd1FNnVhyaYuLNnUhSWb+n/J0gHs+FZ+LwHiLLnjb73XyqBNWbpZ2cd6pLA+pGdqzgD9SUv83LAzCRtj5lkgOLI0peNdTlVw9rSyBB/0x4PPkc6YpOA0XQNY9Eeu75njKMze+TfsgR57ASBM+ayH34m9l0mACxGI7CDqfLbK8pwehotahGVEoyWXl+bwFntFJil9lbCIa+Zkndyx4L4ZvE1adFla7DWTyiLtsh0prL+zdzQIPY+lGv5HKe3SHSWsV9g5x7AreiyiE5zhjGaZOZLjZ+H+hqGg9z5lEVm4IW7WsXLSmmHQGofFdkTG0gtP14btkqxm3Tjui0usCQoKrUzEgEVsmqGbQWHSNHQgZVeybjjD8Ehw0gD3IiziNIQk15RxpVvMriZDpESLu9bFKIvY0iJX3Rc4ZFnxoq77YpxFdMzYJ3IjaDOUkEVcRjwwN0JKpSfusoi9mZA3GrhuRTYBxrYGTm1F+/kGWRGCcGGIX7GI1eXQhLnAQVAbT3qxwos7WzabK1tTYKbbAaSRqFEe9LaLvmf7ac8YzCrrcdol/lLtzXy17Fd3C/5/auuonMoOb8UAAAAASUVORK5CYII=" />
                   </a>
                </div>
                <div class="row">
                   <span>{{ $pdf->name }}</span>
                </div>
                                    <input name="attachment[]" value="{{ $pdf->id }}" type="hidden"
                                        id="inputp{{ $pdf->id }}" />
                                </div>
                            @endforeach
                        </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Tab Content-->
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
        </form>
    </div>
@endsection

<script>
    function sbmt() {
        $('#store_form').submit();
    }

</script>
@section('scripts')
    <script>
        document.getElementById('media').onchange = function() {
            var url = URL.createObjectURL(this.files[0]);
            console.log(url);
            document.getElementById('image_preview').src = url;
        };
        $("#accordion").on("hide.bs.collapse show.bs.collapse", e => {
            $(e.target)
                .prev()
                .find("i:last-child")
                .toggleClass("fa-minus fa-plus");
        });

        $("#product_categories_id").select2();
        $("#product_tags_id").select2({
            tags: true
        });

        $('#product_food_types').select2({
            width: '100%'
        });
        $('#product_food_types').data('select2').$selection.css('height', '30px');

        $(".attributes_select").select2();

    </script>
    <script>
        $(function() {
            $(document).on('click', '.btn-add', function(e) {
                e.preventDefault();

                var dynaForm = $('.dynamic-wrap form:first'),
                    currentEntry = $(this).parents('.entry:first'),
                    newEntry = $(currentEntry.clone()).appendTo(dynaForm);

                newEntry.find('input').val('');
                dynaForm.find('.entry:not(:last) .btn-add')
                    .removeClass('btn-add').addClass('btn-remove')
                    .removeClass('btn-success').addClass('btn-danger')
                    .html('<span class="glyphicon glyphicon-minus"></span>');
            }).on('click', '.btn-remove', function(e) {
                $(this).parents('.entry:first').remove();

                e.preventDefault();
                return false;
            });
        });

    </script>


    <script>
        function add_sizes(parent) {
            $(parent).before('<div id="sizes_div" class="form-group col-lg-12" >\n' +
                '    <div class="entry input-group">\n' +
                '        <div class="col-lg-4 input-group input-group-lg">\n' +
                '            <input name="sizes[]" placeholder="وسط"type="text" class="form-control form-control-lg  text-md-left" value="">\n' +
                '        </div>\n' +
                '        <div class="input-group input-group-lg col col-lg-8 ">\n' +
                '            <div class="input-group-prepend">\n' +
                '    <span class="input-group-text">\n' +
                ' ل.س' +
                '                                          </span>\n' +
                '            </div>\n' +
                '            <input name="size_price[]" type="number" class="form-control form-control-lg  text-md-left" required="" placeholder="ex:2000">\n' +
                ' <button type="button" class="btn btn-danger btn-sm" style="margin-right:10px" onclick="$(this).parent().parent().parent().remove()"><i class="fa fa-minus"></i></button>      ' +
                ' </div>\n' +
                '    </div>\n' +
                '</div>');
        }

        function add_extras(parent) {
            $(parent).before('<div id="sizes_div" class="form-group col-lg-12" >\n' +
                '    <div class="entry input-group">\n' +
                '        <div class="col-lg-4 input-group input-group-lg">\n' +
                '            <input name="extras[]" placeholder="وسط"type="text" class="form-control form-control-lg  text-md-left" value="">\n' +
                '        </div>\n' +
                '        <div class="input-group input-group-lg col col-lg-8 ">\n' +
                '            <div class="input-group-prepend">\n' +
                '    <span class="input-group-text">\n' +
                ' ل.س' +
                '                                          </span>\n' +
                '            </div>\n' +
                '            <input name="extras_price[]" type="number" class="form-control form-control-lg  text-md-left" required="" placeholder="ex:2000">\n' +
                ' <button type="button" class="btn btn-danger btn-sm" style="margin-right:10px" onclick="$(this).parent().parent().parent().remove()"><i class="fa fa-minus"></i></button>      ' +
                ' </div>\n' +
                '    </div>\n' +
                '</div>');
        }
        var KTFormRepeater = function() {

            // Private functions
            var demo1 = function() {
                $('#kt_repeater_1').repeater({
                    initEmpty: false,

                    defaultValues: {
                        'text-input': 'foo'
                    },

                    show: function() {
                        $(this).slideDown();
                    },

                    hide: function(deleteElement) {
                        $(this).slideUp(deleteElement);
                    }
                });
            }
            var demo2 = function() {
                $('#kt_repeater_22').repeater({
                    initEmpty: false,

                    defaultValues: {
                        'text-input': 'foo'
                    },

                    show: function() {
                        $(this).slideDown();
                    },

                    hide: function(deleteElement) {
                        $(this).slideUp(deleteElement);
                    }
                });
            }
            var demo3 = function() {
                $('#kt_repeater_33').repeater({
                    initEmpty: false,

                    defaultValues: {
                        'text-input': 'foo'
                    },

                    show: function() {
                        $(this).slideDown();
                    },

                    hide: function(deleteElement) {
                        $(this).slideUp(deleteElement);
                    }
                });
            }
            return {
                // public functions
                init: function() {
                    demo1();
                    demo2();
                    demo3();

                }
            };
        }();

        jQuery(document).ready(function() {
            KTFormRepeater.init();
        });

    </script>

@endsection
