@extends('layouts.app')

@section('title', 'Staff')


@section('styles')
    <link href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection


@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('success'))
        <div class="alert alert-success">

            {{ session('success') }}
        </div>
        @endif

        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="fa fa-user"></i>
                    </span>
                    <h3 class="card-label">All Staff</h3>
                </div>
                <div class="card-toolbar">
                    <!--begin::Button-->
                    <a href="/admin/staff/create" class="btn btn-primary font-weight-bolder">
                        <i class="la la-plus"></i>New Staff</a>
                    <!--end::Button-->
                </div>
            </div>
            <div class="card-body">
                <!--begin: Datatable-->
                <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Nationality</th>
                            <th>Age</th>
                            <th>Gender</th>
                            <th>Passport Number</th>
                            <th>Settings</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($staff as $sta)
                            <tr>
                                <td>{{ $sta->id }}</td>
                                <td> {{ $sta->name }}</td>
                                <td> {{ $sta->nationality }}</td>
                                <td> {{ $sta->age }}</td>
                                <td> @if( $sta->gender=="female") <span class="font-weight-bold text-success">FEMALE</span>@else  <span class="font-weight-bold text-info">MAlE</span> @endif</td>
                                <td> {{ $sta->passport_number }}</td>
                                <td>
                                    <span style="overflow: visible; position: relative; width: 125px;">
                                        <div class="dropdown dropdown-inline">
                                            <a href="/admin/staff/show/{{ $sta->id }}"  class="btn btn-sm btn-clean btn-icon" title="View">
                                                <span class="svg-icon svg-icon-dark svg-icon-md">
                                                    <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\General\Visible.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"/>
                                                            <path d="M3,12 C3,12 5.45454545,6 12,6 C16.9090909,6 21,12 21,12 C21,12 16.9090909,18 12,18 C5.45454545,18 3,12 3,12 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                            <path d="M12,15 C10.3431458,15 9,13.6568542 9,12 C9,10.3431458 10.3431458,9 12,9 C13.6568542,9 15,10.3431458 15,12 C15,13.6568542 13.6568542,15 12,15 Z" fill="#000000" opacity="0.3"/>
                                                        </g>
                                                    </svg><!--end::Svg Icon-->
                                                </span>
                                             </a>
                                        <a href="/admin/staff/edit/{{ $sta->id }}" class="btn btn-sm btn-clean btn-icon" title="Edit">
                                           <span class="svg-icon svg-icon-md">
                                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                 <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "></path>
                                                    <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"></rect>
                                                 </g>
                                              </svg>
                                           </span>
                                        </a>
                                        <a href="#"  class="btn btn-sm btn-clean btn-icon"  data-toggle="modal" data-target="#delete_modal"  onclick="$('#item_id').val({{ $sta->id }});"title="Delete">
                                           <span class="svg-icon svg-icon-md">
                                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                 <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"></path>
                                                    <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"></path>
                                                 </g>
                                              </svg>
                                           </span>
                                        </a>
                                     </span>
                                </td>
                            </tr>

                        @endforeach

                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Nationality</th>
                            <th>Age</th>
                            <th>Gender</th>
                            <th>Passport Number</th>
                            <th>Settings</th>
                        </tr>
                    </tfoot>
                </table>
                <!--end: Datatable-->
            </div>
        </div>
    </div>
    <!-- Modal-->
    {{--  <div class="modal fade" id="exampleModal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Confiramtion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form action="/admin/staff/delete" method="POST">

                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="id" id="delete_id" />
                       Are You sure you want delete this Staff??
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary font-weight-bold">Yes</button>
                        <button type="button" class="btn btn-light-primary font-weight-bold"
                            data-dismiss="modal">No</button>
                    </div>
                </form>
            </div>
        </div>
    </div>  --}}

    <div class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">Delete confirmation</h4>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete this record?
                    <form action="/admin/staff/destroy" id="delete_form" method="POST">
                        @csrf
                        <input type="hidden" id="item_id" name="id">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="$('#delete_form').submit()" class="btn btn-primary">Yes</button>

                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
<script>
    function deleteItem(id,url) {
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {_token: '{!! csrf_token() !!}',id : id}
            ,
            success: function (response) {
            location.reload();
            },
            error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            }
    });
}
</script>
    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js" defer></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });

    </script>

@endsection
