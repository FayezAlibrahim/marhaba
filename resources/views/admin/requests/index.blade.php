@extends('layouts.app')

@section('title','Requests')

@section('content')

<!--begin::Card-->
<div class="container">
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h2 class="card-label">Requests
            </h2>
        </div>
        
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->


        <table class="table datatable datatable-bordered datatable-head-custom" id="kt_datatable">
            <thead>
                <tr>
                    <th title="Field #1">#</th>
                    <th title="Field #2">Customer name</th>
                    <th title="Field #3">Subject</th>
                    <th title="Field #4">Message</th>
                    <th title="Field #5">Email/Phone</th>
                </tr>
            </thead>
            <tbody>
                @foreach($requests as $request)
                <tr>
                    <td>{{ $request->id }}</td>
                    <td>{{ $request->name ?? '' }}</td>
                    <td>{{ $request->title ?? '' }}</td>
                    <td>{{ $request->message ?? '' }}</td>
                    <td>
                @if(isset($request->phone))
                   <a target="_blank" href="https://api.whatsapp.com/send/?phone={{$request->phone}}&text=Hi&app_absent=0"> <i class="flaticon-whatsapp icon-md" style=" color: green; "></i></a>
                @endif

                @if(isset($request->email)) 
                <a target="_blank" href="mailto:{{$request->email}}">   <img src="/uploads/media/svg/icons/Communication/Mail.svg"/></a>
                @endif
</td>
                </tr>
                @endforeach

            </tbody>
        </table>
        <!--end: Datatable-->
    </div>

</div>
<!--end::Card-->
</div>



@endsection


@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });
    </script>

@endsection
