@extends('layouts.app')

@section('title','ADs')

@section('styles')

    <style>
        .wrapper-pic {
            position: relative;
            display: inline-block;
            opacity: 1;
        }

        .wrapper-pic:hover {
            transition: all .2s ease-in-out;
        }

        .wrapper-pic:hover .edit {
            transition: all .2s ease-in-out;
            display: block;
        }

        .edit {
            padding-top: 7px;
            padding-right: 7px;
            position: absolute;
            left: 20px;
            top: 10px;
            display: none;
        }
    </style>

@endsection


@section('content')


    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-custom">
                    <div class="card-header">
                     <div class="card-title">

                      <h3 class="card-label">
                        Create Ad
                                    </h3>
                     </div>
                           <div class="card-toolbar">
                               <a href="#" onclick="sub();" class="btn btn-sm btn-primary font-weight-bold">
                                   @lang('admin.create')
                               </a>
                           </div>
                    </div>
                    <div class="card-body">
                        <form id="create" method="POST" action="{{ route('admin.ads.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">

                            <div class="col-sm-12 wrapper-pic">
                                    <img src="" class="img-fluid" id="category_image" style=" height: 300px; ">
                                <div class="edit">
                                    <a onclick="event.preventDefault();openChangeImage()" class="btn btn-success btn-xs">@lang('admin.edit')</a>
                                    <a  class="btn btn-danger btn-xs" onclick="event.preventDefault();removeImage()">@lang('admin.delete')</a>
                                </div>
                                <input type="file" id="category_image_id" onchange="previewImage(event)" name="image" style="display:none" accept="image/*">
                                <input type="hidden" name="image_flag" id="image_flag" >

                        </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts')
    <script>
        $("#region_select").select2();
function sub()
{
    $('#create').submit();
}
        function enableCategorySearching() {
            var searchUrl = '{{ route('admin.categories.search') }}';
            searchingFor(searchUrl);
            attachCategory();


        }

        function attachCategory(){
            $('#ads_item').off('select2:select').on('select2:select', function (event) {
                var id = $(event.currentTarget).find("option:selected").val();
                if(confirm('@lang('admin.sure_to_add_category')'))
                {
                    $('#selected_item').attr('value',id);
                }
            });
        }

        function attachProduct(){
            $('#ads_item').off('select2:select').on('select2:select', function (event) {
                var id = $(event.currentTarget).find("option:selected").val();
                if(confirm('@lang('admin.sure_to_add_product')'))
                {
                    $('#selected_item').attr('value',id);
                }
            });
        }

        function enableProductSearching() {
            var searchUrl = '{{ route('admin.products.search') }}';
            searchingFor(searchUrl);
            attachProduct();
        }


        function searchingFor(url)
        {
            //console.log(url);
            if(url === '')
            {
                return;
            }
            $('#ads_item').empty();
            $('#ads_item').select2({
                placeholder: "اكتب اسم الوجبة",
                minimumInputLength: 2,
                ajax: {
                    url: url,
                    dataType: 'json',
                    data: function (params) {
                        return {
                            q: $.trim(params.term),
                            d:true
                        };
                    },
                    processResults: function (data) {
                      //  console.log(data);
                        return {
                            results: data
                        };
                    },
                    error: function (err) {
                        console.log(err);
                    },
                    cache: true
                }
            });
        }






       function get_item_data(event)
        {
            var selected_value  = $(event.target).val();
            if(selected_value === "product"){
                $("#adForId").css("display", "flex");
                enableProductSearching();}
            // else if(selected_value === "category")
            //     enableCategorySearching();
            else if(selected_value === "ads")
            {
                $("#adForId").css("display", "none");
                $('#ads_item').empty();
                var ele = `<option value="ads">اعلان مجرد</option>`;
                $('#ads_item').append(ele);
            }
            else if(selected_value === "offers")
            {
                $('#ads_item').empty();
                var ele = `<option value="offers">اعلان لصفحة العروض</option>`;
                $('#ads_item').append(ele);
            }
        }

        enableProductSearching();


    </script>
    <script>
        function openChangeImage()
        {
            $("#category_image_id").trigger('click');
        }

        function removeImage() {
            var default_image_url = "https://tsdist.com/wp-content/uploads/2018/07/Image_placeholder_1.jpg";
            $('#category_image').attr('src',default_image_url);
            $('#category_image_id').val('');
            change_image_flag('remove')
        }
        removeImage();
        function change_image_flag(flag) {
            $("#image_flag").attr('value',flag);
        }


        function previewImage(event) {
            var image_src =  URL.createObjectURL(event.target.files[0]);
            $('#category_image').attr('src',image_src);
            change_image_flag('edit')

        }
    </script>
@endsection
