@extends('layouts.app')

@section('title','Ads')

@section('content')

@if(session('success'))
    <div class="alert alert-custom alert-outline-2x alert-outline-primary fade show mb-5" role="alert">
        <div class="alert-icon"><i class="flaticon-warning"></i></div>
        <div class="alert-text">{{session('success')}}</div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="ki ki-close"></i></span>
            </button>
        </div>
    </div>
@endif
<div class="d-flex flex-column-fluid">

    <div class="container">
     <div class="row" style="margin: auto">
         <a href="{{ route('admin.ads.create') }}" class="btn btn-primary" ><i class="fa fa-plus"></i> @lang('admin.add_new')</a>
     </div>
        <div class="row">
            @foreach($ads as $ad)
         <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6">
             <!--begin::Card-->
             <div class="card card-custom gutter-b card-stretch">
                 <!--begin::Body-->


                 <div class="card-header" >
                <lable class="text-md-right" style="margin-top: 25px;">
                        {{ $ad->title ?? '' }}</lable>
                     <div class="card-toolbar">
                         <a href="{{ route('admin.ads.edit',$ad->id) }}" class="btn btn-icon btn-xs btn-light-primary mr-1" data-card-tool="edit">
                             <i class="fa fa-edit"></i>
                         </a>

                         <a href="#" onclick="deleteItem_new('{{ $ad->id }}','{{route('admin.ads.destroy',$ad->id)}}')" class="btn btn-icon btn-xs btn-light-danger" data-card-tool="remove">
                             <i class="fa fa-trash"></i>
                         </a>
                     </div>
                 </div>
                 <a href="{{ route('admin.ads.edit',$ad->id) }}">
                 <div class="card-body pt-4">

                     <!--begin::User-->
                     <div class="d-flex align-items-end mb-7">
                         <!--begin::Pic-->
                         <div class="d-flex align-items-center">
                             <!--begin::Pic-->
                             <div class="flex-shrink-0 mr-4 mt-lg-0 mt-3">
                                 <div class="symbol symbol-circle symbol-lg-150">
                                     <img src="{{$ad->image_url}}" alt="image">
                                 </div>
                             </div>
                             <!--end::Pic-->
                             <!--begin::Title-->

                             <!--end::Title-->
                         </div>


                         <!--end::Title-->
                     </div>
                     <!--end::User-->
                     <!--begin::Desc-->
                     <p class="mb-7">
                        <div class="d-flex flex-column">
                            <div class="text-dark font-weight-bold text-hover-primary font-size-h4 mb-0">
                                {{ $ad->get_item()->name ?? trans('ads.'.$ad->type) }}
                            </div>
                            <span class="text-muted font-weight-bold">{{ trans('ads.'.$ad->type) }}</span>
                        </div>



                     </p>
                     <!--end::Desc-->

                 </div>
                 </a>
                 <!--end::Body-->
             </div>
             <!--end::Card-->
         </div>
         @endforeach
        </div>
    </div>
 </div>

@endsection


@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });
        function deleteItem_new(id,url) {

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: {_token: '{!! csrf_token() !!}', _method : 'delete' },
                success: function (response) {
                    location.reload();
                    },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);

                    }
            })

    }

    </script>

@endsection
