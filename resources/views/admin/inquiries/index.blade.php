@extends('layouts.app')

@section('title',trans('sidebar.inquiries'))

@section('content')

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">الاستفسارات</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class=" table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>@lang('inquiries.id')</th>
                            <th>@lang('inquiries.user')</th>
                            <th style="width: 100px;">المحادثة</th>
                        </tr>
                        </thead>
                        
                        <tbody>
                        @foreach($userToReturn as $user)
                            <tr class="odd gradeX">
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->username ?? '' }}
                                    @if($user->inquiries_count != "0")
                                <span class="badge badge-danger badge-counter">{{$user->inquiries_count}}</span>
                                @endif
                                </td> 
                                <td>
                                <a href="/admin/inquiries/user/{{$user->id}}" style="cursor: pointer; color: #4e73df"  data-toggle="tooltip" title="فتح المحادثة"><i class="far fa-comment-dots"></i> </a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                     
                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
                <br>

            </div>
            {{-- <div class="col-md-12 text-center">
                <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#exampleModal">بدء محادثة</button>

            </div> --}}
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          {{-- <h5 class="modal-title" id="exampleModalLabel"></h5> --}}
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form>
                <div class="form-group">
                  <label for="exampleInputEmail1">ادخل ID المستخدم</label>
                  <input type="text" class="form-control input-sm" id="exampleInputEmail1" name="user_id" aria-describedby="emailHelp" placeholder="User ID">
                </div>
                             
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">إلغاء</button>
          <button type="submit" class="btn btn-primary">بدء</button>
        </form>
        </div>
      </div>
    </div>
  </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>

@endsection


@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable({
                "aaSorting": [],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });
        });
    </script>

@endsection
