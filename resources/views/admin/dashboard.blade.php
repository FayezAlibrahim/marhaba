@extends('layouts.app')

@section('content')

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            <div class="dropdown dropdown-inline mr-2" style="float: right; ">
                <button type="button" class="btn btn-light-primary font-weight-bolder dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="svg-icon svg-icon-md">
                    <!--begin::Svg Icon | path:assets/media/svg/icons/Design/PenAndRuller.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24" />
                            <path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z" fill="#000000" opacity="0.3" />
                            <path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z" fill="#000000" />
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>CSV Export</button>
                <!--begin::Dropdown Menu-->
                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                    <!--begin::Navigation-->
                    <ul class="navi flex-column navi-hover py-2">
                        <li class="navi-header font-weight-bolder text-uppercase font-size-sm text-primary pb-2"> Log Export:</li>

                        <li class="navi-item">
                            <a href="{{ route('admin.reports.sales') }}" data-filter="today" class="navi-link">
                                <span class="navi-icon">
                                    <i class="la la-file-text-o"></i>
                                </span>
                                <span class="navi-text">Today</span>
                            </a>
                        </li>
                        <li class="navi-item">
                            <a href="{{ route('admin.reports.sales','filter=yesterday') }}" data-filter="yesterday" class="navi-link">
                                <span class="navi-icon">
                                    <i class="la la-file-text-o"></i>
                                </span>
                                <span class="navi-text">Yesterday</span>
                            </a>
                        </li>
                        <li class="navi-item">
                            <a href="{{ route('admin.reports.sales','filter=weekly') }}" data-filter="weekly" class="navi-link">
                                <span class="navi-icon">
                                    <i class="la la-file-text-o"></i>
                                </span>
                                <span class="navi-text"> Last week</span>
                            </a>
                        </li>
                        <li class="navi-item">
                            <a href="{{ route('admin.reports.sales','filter=monthly') }}" data-filter="monthly" class="navi-link">
                                <span class="navi-icon">
                                    <i class="la la-file-text-o"></i>
                                </span>
                                <span class="navi-text">Last month </span>
                            </a>
                        </li>
                        <li class="navi-item">
                            <a href="#" data-toggle="modal" data-target="#daterangepicker_modal" data-filter="advanced" class="navi-link">
                                <span class="navi-icon">
                                    <i class="la la-file-text-o"></i>
                                </span>
                                <span class="navi-text">Advanced</span>
                            </a>
                        </li>
                    </ul>
                    <!--end::Navigation-->
                </div>
                <!--end::Dropdown Menu-->
            </div>
        </div>
        <div class="card card-custom gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label"> General Fees</h3>
                    <form class="form-inline" action="/admin/add_fee" method="post">
                        @csrf
                        <div class="form-group mb-2" style=" margin-right: 10px; ">
                            <input type="number" min="0" class="form-control" name="amount" placeholder="Amount" required>
                        </div>
                        <div class="form-group mb-2" style=" margin-right: 10px; ">
                            <input type="text" class="form-control" name="title" placeholder="Title" required>
                        </div>

                        <div class="form-group mb-2" style=" margin-right: 10px; ">
                            <input name="feeDate" type="date" class="form-control" id="feeDate"
                                value="{{ date('Y-m-d') }}" />
                            <div class="d-md-none mb-2"></div>
                        </div>
                        <button type="submit" class="btn btn-primary mb-2">Add New</button>
                    </form>
                </div>

                <h6 style="margin-top:10px">Total Fees: {{ $fees->sum('amount') }}</h6>

            </div>
            <div class="card-body">

                <!--begin::Chart-->
                <div class="row">

                    <div class="col-sm-12">
                        <!--begin::Advance Table Widget 2-->
                        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                            <thead>
                                <tr>
                                    <th>Record ID</th>
                                    <th>Date</th>
                                    <th>Title</th>
                                    <th>Amount (in dirham)</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($fees as $fee)
                                    <tr>
                                        <td>{{ $fee->id }}</td>
                                        <td>{{ $fee->created_at ?? '' }}</td>
                                        <td>{{ $fee->title ?? '' }}</td>
                                        <td>{{ $fee->amount ?? '0' }}</td>
                                        <td>

                                            <a href="#" onclick="deleteFee({{ $fee->id }})" data-toggle="modal"
                                                data-target="#delete_modal"
                                                class="btn btn-sm btn-light btn-text-primary btn-icon" title="Delete">
                                                <span class="svg-icon svg-icon-md">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                        xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                        height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <path
                                                                d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z"
                                                                fill="#000000" fill-rule="nonzero"></path>
                                                            <path
                                                                d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z"
                                                                fill="#000000" opacity="0.3"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </a>

                                            <!-- /admin/fee/edit/{{ $fee->id }} -->
                                            <a href="#" class="btn btn-sm btn-light btn-text-primary btn-icon mr-2"
                                            data-toggle="modal" onclick="$('#idhold').val('{{ $fee->id }}')"
                                            data-target="#exampleModal" title="Edit details">
                                                <span class="svg-icon svg-icon-md">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                        xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                        height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <path
                                                                d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z"
                                                                fill="#000000" fill-rule="nonzero"
                                                                transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) ">
                                                            </path>
                                                            <rect fill="#000000" opacity="0.3" x="5" y="20" width="15"
                                                                height="2" rx="1"></rect>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </a>

                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>

                        <!--end::Advance Table Widget 2-->
                    </div>

                    <div class="col-sm-2">
                        <!--begin::Advance Table Widget 2-->


                        <!--end::Advance Table Widget 2-->
                    </div>
                </div>

                <!--end::Chart-->
            </div>
        </div>
        <div class="card card-custom bg-gray-100 gutter-b">
            <!--begin::Header-->
            <div class="card-header h-auto border-0">
                <!--begin::Title-->
                <div class="card-title py-5">
                    <h3 class="card-label">
                        <span class="d-block text-dark font-weight-bolder">Staff Income & Outcome</span>
                    </h3>
                </div>
                <!--end::Title-->
                <!--begin::Toolbar-->
                <div class="card-toolbar">
                    <ul class="nav nav-pills nav-pills-sm nav-dark-75" role="tablist">

                        <li class="nav-item">
                            <a class="nav-link py-2 px-4" data-toggle="tab" href="#kt_tab_pane_11_1">Month</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link py-2 px-4" data-toggle="tab" href="#kt_tab_pane_11_2">Week</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link py-2 px-4 active" data-toggle="tab" href="#kt_tab_pane_11_3">Day</a>
                        </li>

                    </ul>
                </div>
                <!--end::Toolbar-->
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body">
                <div class="tab-content mt-5" id="myTabTables11">
                    <!--begin::Tap pane-->
                    <div class="tab-pane fade" id="kt_tab_pane_11_1" role="tabpanel" aria-labelledby="kt_tab_pane_11_1">
                        <div id="chart_month"></div>

                    </div>
                    <!--end::Tap pane-->
                    <!--begin::Tap pane-->
                    <div class="tab-pane fade" id="kt_tab_pane_11_2" role="tabpanel" aria-labelledby="kt_tab_pane_11_2">
                        <!--begin::Table-->
                        <div id="chart_week"></div>
                        <!--end::Table-->
                    </div>
                    <!--end::Tap pane-->
                    <!--begin::Tap pane-->
                    <div class="tab-pane fade show active" id="kt_tab_pane_11_3" role="tabpanel"
                        aria-labelledby="kt_tab_pane_11_3">
                        <!--begin::Table-->
                        <div id="chart_day"></div>
                        <!--end::Table-->
                    </div>
                    <!--end::Tap pane-->
                </div>
            </div>
            <!--end::Body-->
        </div>


    </div>



    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Update Fee</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
          <form class="form" action="/admin/update_fee" method="post">
                                @csrf
                                <input type="hidden" id="idhold" name="id">

        <div class="form-group mb-2" style=" margin-right: 10px; ">
          <input type="number" min="0" class="form-control" name="amount" placeholder="New Amount Value" required>
        </div>
        <div class="form-group mb-2" style=" margin-right: 10px; ">
          <input type="text" class="form-control" name="title" placeholder="New Title Value" required>
        </div>

        <div class="form-group mb-2" style=" margin-right: 10px; ">
        <input name="feeDate" type="date" class="form-control"  value="{{date('Y-m-d')}}"/>
        <div class="d-md-none mb-2"></div>
      </div>
        <button type="submit" class="btn btn-primary mb-2">Update</button>
      </form>
            </div>
          </div>
        </div>
      </div>


      <div class="modal fade" id="daterangepicker_modal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form method="POST" action="/admin/reports/custom">
                @csrf

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Choose Custom Date</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                   Date From:
                     <input name="from_date" type="date"  class="form-control" id="fom_date"
                   value="{{ date('Y-m-d') }}" required/>
                   Date To:     <input name="to_date"  type="date" class="form-control" id="t_date"
                   value="{{ date('Y-m-d') }}" required />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                    <button type="submit"   class="btn btn-primary font-weight-bold">Submit</button>
                </div>
            </div>
        </form>

        </div>
    </div>
@endsection


@section('scripts')
    <script>
        function redirectTo(url) {
            window.location.href = url;
        }

    </script>
    <script>
        const primary = '#6993FF';
        const success = '#1BC5BD';

        var KTApexChartsDemo = function() {

            var _demo2 = function() {
                const apexChart = "#chart_day";
                var options = {
                    series: [{
                        name: 'Income',
                        data: @json($daily_income[1])
                    }, {
                        name: 'Outcome',
                        data: @json($daily_outcome[1])
                    }],
                    chart: {
                        height: 350,
                        type: 'area'
                    },
                    dataLabels: {
                        enabled: false
                    },
                    stroke: {
                        curve: 'smooth'
                    },
                    xaxis: {
                        type: 'date',
                        categories: @json($daily_income[0]),
                    },
                    tooltip: {
                        x: {
                            format: 'dd/MM/yy'
                        },
                    },
                    colors: [primary, success]
                };

                var chart = new ApexCharts(document.querySelector(apexChart), options);
                chart.render();
            }

            var _demo4 = function() {
                const apexChart = "#chart_week";
                var options = {
                    series: [{
                        name: 'Income',
                        data: @json($weekly_income[1])
                    }, {
                        name: 'Outcome',
                        data: @json($weekly_outcome[1])
                    }],
                    chart: {
                        height: 350,
                        type: 'area'
                    },
                    dataLabels: {
                        enabled: false
                    },
                    stroke: {
                        curve: 'smooth'
                    },
                    xaxis: {
                        type: 'date',
                        categories: @json($weekly_income[0]),
                    },
                    tooltip: {
                        x: {
                            format: 'dd/MM/yy'
                        },
                    },
                    colors: [primary, success]
                };

                var chart = new ApexCharts(document.querySelector(apexChart), options);
                chart.render();
            }
            var _demo5 = function() {
                const apexChart = "#chart_month";
                var options = {
                    series: [{
                        name: 'Income',
                        data: @json($monthly_income[1])
                    }, {
                        name: 'Outcome',
                        data: @json($monthly_outcome[1])
                    }],
                    chart: {
                        height: 350,
                        type: 'area'
                    },
                    dataLabels: {
                        enabled: false
                    },
                    stroke: {
                        curve: 'smooth'
                    },
                    xaxis: {
                        type: 'date',
                        categories: @json($monthly_income[0]),
                    },
                    tooltip: {
                        x: {
                            format: 'dd/MM/yy'
                        },
                    },
                    colors: [primary, success]
                };

                var chart = new ApexCharts(document.querySelector(apexChart), options);
                chart.render();
            }
            return {
                // public functions
                init: function() {
                    _demo2();
                    _demo5();
                    _demo4();
                }
            };
        }();

        jQuery(document).ready(function() {
            KTApexChartsDemo.init();
        });

        function deleteFee(id) {
            var r = confirm("Are You Sure You Want to delete this record? this can't be undo");
            if (r == true) {
                txt = "You pressed OK!";

                $.ajax({
                    type: "POST",
                    url: '/admin/fee/delete',
                    data: {
                        'id': id,
                        "_token": "{{ csrf_token() }}"
                    },
                    success: function() {
                        location.reload();
                    },
                    error: function() {
                        alert('Error occured:deleteing Fee!');
                    }
                });
            } else {
                // txt = "You pressed Cancel!";
            }

        }

    </script>

@endsection