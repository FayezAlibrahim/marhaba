@extends('layouts.app')
@section('title',trans('users.users'))
@section('content')
<div class="container">

    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">   @lang('users.users')
          </h3>
            </div>
            <div class="card-toolbar">
                <div class="col-md-10 my-2 my-md-0">
                    <div class="input-icon">
                        <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
                        <span>
                            <i class="flaticon2-search-1 text-muted"></i>
                        </span>
                    </div>
                </div>

                <div class="col-md-2 my-2 my-md-0">
                    <div class="input-icon">
                    <a href="{{ route('admin.users.create') }}" class="btn btn-light-warning font-weight-bolder btn-sm">Add New</a>
                    </div>
                </div>

            </div>
        </div>
        <div class="card-body">


            <!--begin: Datatable-->
            <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
                <thead>
                    <tr>
                        <th title="Field #1">#</th>
                        <th title="Field #2">@lang('users.username')</th>
                        <th title="Field #3">@lang('users.phone')</th>

                        <th title="Field #4">@lang('users.city')</th>
                        <th title="Field #5">@lang('users.active')</th>
                        <th title="Field #6">
                            مؤكد
                            <small>برمز الهاتف</small>
                        </th>
                        <th title="Field #7"></th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->username ?? '' }}</td>
                        <td> {{ $user->phone ?? '' }} </td>

                      <td>  {{ $user->city->name ?? '' }}  </td>
                        <td class="text-right">
                            @if( $user->active == "yes")
                         <span  style="width: 110px;"><span class="label font-weight-bold label-lg label-light-primary label-inline">مفعل</span></span>
                        @else
                        <span style="width: 110px;"><span class="label font-weight-bold label-lg label-light-danger label-inline">غير مفعل</span></span>
                        @endif</td>
                        </td>
                        <td class="text-right">
                        @if( $user->status == "1")
                            <span  style="width: 110px;"><span class="label font-weight-bold label-lg label-light-primary label-inline">مؤكد</span></span>
                        @else
                        <span style="width: 110px;"><span class="label font-weight-bold label-lg label-light-danger label-inline">غير مؤكد</span></span>
                        @endif</td>
                        <td>
                            <span style="overflow: visible; position: relative; width: 150px;">
                                <a href="{{ route('admin.users.logs.show',$user->id) }}"
                                   class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2"
                                   title="User Log">
                               <span class="svg-icon svg-icon-md">
                                     <svg xmlns="http://www.w3.org/2000/svg"
                                          xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                          height="24px" viewBox="0 0 24 24" version="1.1"
                                          class="svg-icon">
                                        <g stroke="none" stroke-width="1" fill="none"
                                           fill-rule="evenodd">
                                           <rect x="0" y="0" width="24" height="24"></rect>
                                           <path
                                               d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z"
                                               fill="#000000"></path>
                                           <path
                                               d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z"
                                               fill="#000000" opacity="0.3"></path>
                                        </g>
                                     </svg>
                                  </span>
                            </a>
                            <a href="{{ route('admin.users.edit',$user->id) }}"
                               class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2"
                               title="Edit details">
                               <span class="svg-icon svg-icon-md">
                                  <svg xmlns="http://www.w3.org/2000/svg"
                                       xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                       height="24px" viewBox="0 0 24 24" version="1.1">
                                     <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"></rect>
                                        <path
                                            d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z"
                                            fill="#000000" fill-rule="nonzero"
                                            transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) "></path>
                                        <path
                                            d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z"
                                            fill="#000000" fill-rule="nonzero" opacity="0.3">
                                        </path>
                                     </g>
                                  </svg>
                               </span>
                            </a>
                            <a href="#" style="color: #dc3545" onclick="$('#delete_id').val({{ $user->id }})" data-toggle="modal" data-target="#exampleModal"
                               class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon"
                               title="Delete">
                               <span class="svg-icon svg-icon-md">
                                  <svg xmlns="http://www.w3.org/2000/svg"
                                       xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                       height="24px" viewBox="0 0 24 24" version="1.1">
                                     <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"></rect>
                                        <path
                                            d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z"
                                            fill="#000000" fill-rule="nonzero"></path>
                                        <path
                                            d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z"
                                            fill="#000000" opacity="0.3"></path>
                                     </g>
                                  </svg>
                               </span>
                            </a>
                             <a href="{{ route('admin.users.address',$user->id) }}"
                                class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2"
                                title="view Address">
<span class="svg-icon svg-icon-md">
    <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Home\Home.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <rect x="0" y="0" width="24" height="24"/>
            <path d="M3.95709826,8.41510662 L11.47855,3.81866389 C11.7986624,3.62303967 12.2013376,3.62303967 12.52145,3.81866389 L20.0429,8.41510557 C20.6374094,8.77841684 21,9.42493654 21,10.1216692 L21,19.0000642 C21,20.1046337 20.1045695,21.0000642 19,21.0000642 L4.99998155,21.0000673 C3.89541205,21.0000673 2.99998155,20.1046368 2.99998155,19.0000673 L2.99999828,10.1216672 C2.99999935,9.42493561 3.36258984,8.77841732 3.95709826,8.41510662 Z M10,13 C9.44771525,13 9,13.4477153 9,14 L9,17 C9,17.5522847 9.44771525,18 10,18 L14,18 C14.5522847,18 15,17.5522847 15,17 L15,14 C15,13.4477153 14.5522847,13 14,13 L10,13 Z" fill="#000000"/>
        </g>
    </svg><!--end::Svg Icon-->
</span>
                                 </a>
               </span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>



</div>


<!-- Modal-->
<div class="modal fade" id="exampleModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('admin.delete')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form action="/api/users/delete/user" method="POST">
                {{ method_field('POST') }}
            <div class="modal-body">
                @csrf
               <input type="hidden" name="id" id="delete_id"/>
@lang('admin.are_you_sure')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">@lang('admin.no')</button>
                <button type="submit"  class="btn btn-primary font-weight-bold">@lang('admin.yes')</button>
            </div>
        </form>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });


        function changeStatus(id) {
            var url = '{{ route('admin.users.status') }}';
            $.ajax({
                url: url,
                type: 'POST',
                data: {id: id, _token: '{!! csrf_token() !!}'}
            }).done(function (data) {
            }).fail(function (e) {
                swal("{!! trans('admin.fail') !!}", e.responseJSON.message, "error")
            })
        }


        function deleteItem() {
var $id=$('#delete_id').val();
var url="/admin/users/destroy/"+$id;
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method: 'delete'}
               ,
               success: function (response) {
                location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);

                }


            });
        }


    </script>
@endsection

