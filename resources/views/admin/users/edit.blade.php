@extends('layouts.app')

@section('title',trans('users.edit'))

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" style="
                    font-weight: 800;
                    font-size: medium;
                ">
                        @lang('users.edit')
           <div class="card-toolbar" >

        <button type="submit"  onclick="$('#update_form').submit();" class="btn btn-primary"  style="float: left;
        margin-top: -20px;">
            @lang('admin.edit')
        </button>


</div>

                    </div>

                    <div class="card-body">
                        <form method="POST" id="update_form" action="{{ route('admin.users.update',$user->id) }}">
                            @csrf
                            @method('PUT')

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="col-form-label text-md-right">@lang('users.username')</label>

                                        <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ $user->username }}">

                                        @error('username')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>


                                    {{-- <div class="form-group">
                                        <label class="col-form-label text-md-right">@lang('users.email')</label>

                                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" autocomplete="off">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div> --}}
                                    {{-- <div class="form-group ">
                                        <label class="col-form-label text-md-right" data-required="yes">@lang('users.wallet')</label>

                                            <input type="number"
                                                   class="form-control @error('wallet') is-invalid @enderror"
                                                   name="wallet" value="{{ $user->wallet }}" required>
                                            @error('wallet')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                    </div> --}}
                                    <div class="form-group">
                                        <label class="col-form-label text-md-right">@lang('users.phone')</label>

                                        <input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ $user->phone }}" autocomplete="off">

                                        @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label text-md-right">@lang('users.password')</label>

                                        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="" autocomplete="off">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-form-label text-md-right">@lang('users.city')</label>

                                        <select name="city_id" onchange="GetRegions(this.value)" class="form-control">
                                            @foreach($cities as $city)
                                                <option value="{{ $city->id }}" {{ (isset($user->city) && $user->city->id == $city->id) ? "selected" : "" }}>{{ $city->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label text-md-right">@lang('users.status')</label>

                                        <select name="active" class="form-control">
                                            <option value="yes" {{ $user->active == "yes" ? "selected" : "" }}>@lang('users.active')</option>
                                            <option value="no" {{ $user->active == "no" ? "selected" : "" }}>@lang('users.inactive')</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label text-md-right">النقاط</label>
                                        <input type="number" class="form-control" id="points" name="points" value="{{$user->points}}" placeholder="enter amount">
                                    </div>
 

                                </div>

                            </div>
                            {{-- <div class="row">
                                <div class="col-md-12">
                                    <h3><i class="fa fa-lock"></i> @lang('sidebar.roles')</h3>
                                </div>

                                @foreach($roles as $role)
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" name="roles[]" class="custom-control-input" value="{{ $role->id }}" @if(in_array($role->id, $user_roles)) checked @endif id="customCheck{{ $role->id }}">
                                                <label class="custom-control-label" for="customCheck{{ $role->id }}">{{ $role->name }}</label>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach

                            </div> --}}




                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
