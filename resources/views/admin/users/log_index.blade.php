 @extends('layouts.app')

@section('title','System Log')

@section('content')

                        <!--begin::Card-->
                        <div class="container">
                  <div class="card card-custom">
                     <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                           <h2 class="card-label">
                           System Log
                           </h2>
                           <div class="card-toolbar">


                                      <div class="row" style="
                                      margin-right: 400px;
                                  ">
                                         <div class="col-md-12 my-2 my-md-0">
                                            <div class="input-icon">
                                               <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
                                               <span>
                                                  <i class="flaticon2-search-1 text-muted"></i>
                                               </span>
                                            </div>
                                         </div>


                                </div>
                             </div>

                        </div>


                     </div>
                     <div class="card-body">

                        <!--begin: Datatable-->
                        <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
                           <thead>
                              <tr>
                                 <th title="Field #1">#ID</th>
                                 <th title="Field #2">@lang('users.username')</th>
                                 <th title="Field #3">activity type</th>
                                 <th title="Field #4">Date</th>

                              </tr>
                           </thead>
                           <tbody>
                            @foreach($activity_logs as $log)
                                          <tr>


                                             <td>{{ $log->id }}</td>
                                             <td>{{ $log->user->username ?? '' }}</td>
                                             <td>{{ $log->activity_name ?? '' }}</td>
                                             <td>{{ $log->created_at ?? '' }}</td>
                                          </tr>
                                          @endforeach

                           </tbody>
                        </table>
                        <!--end: Datatable-->
                     </div>
                  </div>
                </div>
                  <!--end::Card-->



@endsection
