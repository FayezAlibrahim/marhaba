<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bianka Village : Coming Soon </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="{{ asset('css/uploads/faviconhawary1.ico') }}"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome-free/css/all.min.css') }}">
    <!--===============================================================================================-->

    <link rel="stylesheet" type="text/css" href="{{ asset('css/coming soon/util.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/coming soon/main.css') }}">

    <!--===============================================================================================-->
</head>
<body>

<!--  -->
<div class="simpleslide100">
    <div class="simpleslide100-item bg-img1" style="background-image: url('{{ asset('uploads/login_bg1.jpg') }}');"></div>
    <div class="simpleslide100-item bg-img1" style="background-image: url('{{ asset('uploads/login_bg2.jpg') }}');"></div>
    <div class="simpleslide100-item bg-img1" style="background-image: url('{{ asset('uploads/login_bg3.jpg') }}');"></div>
</div>

<div class="flex-col-c-sb size1 overlay1">
    <!--  -->
    <div class="w-full flex-w flex-sb-m p-l-80 p-r-80 p-t-22 p-lr-15-sm">


        <div class="flex-w m-t-10 m-b-10">
            <!-- <a href="#" class="size2 m1-txt1 flex-c-m how-btn1 trans-04">
                Sign Up
            </a> -->
        </div>
    </div>

    <!--  -->
    <div class="flex-col-c-m p-l-15 p-r-15 p-t-50 p-b-120">
        <div class="wrappic1 m-r-30 m-t-10 m-b-10">
            <a><img src="{{ asset('uploads/o.svg') }}" alt="LOGO"></a>
        </div>
        <h3 class="l1-txt1 txt-center p-b-40 respon1">
            Bianka Village <br>
        </h3>

        <h3 class="coming-soon">
            Coming Soon
        </h3>
        <!-- <h3 class="l1-txt1 txt-center p-b-40 respon1">
            Coming Soon
        </h3> -->

        <h3 class="contact-us">
            Contact us: <a href="mailto:info@Marhaba.com">info@Marhaba.com</a>
        </h3>
    </div>

    <!--  -->
    {{-- <div class="flex-w flex-c-m p-b-35 buttom-p">
        <p><a href="#">Marhaba.net</a> 2020 all rights reserved. Created by <a href="http://Marhaba.com" target="_blank">Marhaba</a></p>
    </div> --}}
</div>





<!--===============================================================================================-->
<script src="{{ asset('comingsoon/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('comingsoon/vendor/bootstrap/js/popper.js') }}"></script>
<script src="{{ asset('comingsoon/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('comingsoon/js/main.js') }}"></script>

</body>
</html>
