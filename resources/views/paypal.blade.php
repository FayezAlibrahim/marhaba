<div id="paypal-button2"></div>
<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script>
  paypal.Button.render({
    env: 'sandbox',
    style: {
      size: 'medium',
      color: 'blue',
      shape: 'rect',
    },
    // Set up the payment:
    // 1. Add a payment callback
    payment: function (data, actions) {
        // 2. Make a request to your server
        return actions.request.post('/api/create-paypal-transaction-test',
        {
           _token: '{!! csrf_token() !!}',
           user_id:'{{ $user_id }}',
           price:'{{ $price }}',
            currency:'{{ $currency }}',
            name:'{{ $name }}',



        }
        )  .then(function (res) {
          // 3. Return res.id from the response
           console.log("result"+res);
          return res.id;
        })

    },
    // Execute the payment:
    // 1. Add an onAuthorize callback
    onAuthorize: function (data, actions) {
      // 2. Make a request to your server
      return actions.request.post('/api/confirm-paypal-transaction-test', {
          user_id:{{ $user_id }},
        payment_id: data.paymentID,
        payer_id: data.payerID,
        _token: '{!! csrf_token() !!}',
      })
        .then(function (res) {
          console.log(res);
          window.location.replace(res.redirect_urls.return_url);
          alert('Payment successfully done!!');
        })
    }
  }, '#paypal-button2')
</script>
