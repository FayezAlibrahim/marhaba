function submitForm(formid)
{
    $('#'+formid).submit();
}

function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element);
    if (typeof(node) === 'undefined' || node === null)
    {
        return;
    }



    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}

animateCSS('.sidebar-brand-icon', 'fadeInDown');
animateCSS('.page-header', 'bounceIn');


$(document).ready(function(){

    $('#overlay').fadeOut('slow',function(){
        $('#content').fadeIn();
        $('#footer').fadeIn();
    });


    required_fields = $('label[data-required]');

    $.each(required_fields,function (key,ele) {
        $(ele).append('<span class="required-indicator" style="color: red"> * </span>');
    });


});

