<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $guarded = ['id','created_at','updated_at'];

    public function getCreatedAtAttribute($attr)
    {
        return formatDateAsName($attr);
    }
    public function uid()
    {
        return $this->user_id;
    }
}
