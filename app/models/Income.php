<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\models\Staff;
class Income extends Model
{
    //
    public function staff()
    {
        return $this->belongsTo('App\models\Staff');
    }
}
