<?php

namespace App\models;
use App\models\WorkExperianc;
use Illuminate\Database\Eloquent\Model;
use App\models\Attachment;
use App\models\Outcome;
use App\models\Income;
class Staff extends Model
{
    //
    public function experience()
    {
        return $this->hasMany('App\models\WorkExperianc');
    }
    public function pdfs()
    {
        return $this->hasMany('App\models\Attachment')->where('type','pdf');
    }
    public function videos()
    {
        return $this->hasMany('App\models\Attachment')->where('type','video');
    }
    public function images()
    {
        return $this->hasMany('App\models\Attachment')->where('type','img');
    }
    public function incomes()
    {
        return $this->hasMany('App\models\Income');
    }
    public function outcomes()
    {
        return $this->hasMany('App\models\Outcome');
    }

}
