<?php

namespace App\models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use PHPZen\LaravelRbac\Traits\Rbac;
use App\models\UserFavoriteProduct;
use App\models\Address;
use App\models\Delivery;



class User extends Authenticatable
{

    use Notifiable;
    use Rbac;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id','created_at','updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function userAsDelivery()
    {
        return $this->hasOne('App\models\User');
    }

    public function orders()
    {
        return $this->hasMany('App\models\Order');
    }

    public function city()
    {
        return $this->belongsTo('App\models\City');
    }

    // public function roles()
    // {
    //     return $this->belongsToMany('App\models\Role');
    // }


    public function fav_ids($id)
    {
        return DB::table('user_favorites')->where('user_id', $id)->pluck('restaurant_id')->toarray();
    }


      public function inquiries()
    {
        return $this->hasMany('App\models\Inquiry');
    }

    public function activities()
    {
        return $this->hasMany('App\models\UserActivity');
    }

    public function logActivity($activity_name)
    {
        $activity = UserActivity::create([
            'user_id' => $this->getKey(),
            'activity_name' => $activity_name
        ]);

        return $this;
    }

    public function checkUserRole($role)
    {
        return $this->role == $role;
    }

    public static function checkType($user_id,$type)
    {
        return self::find($user_id)->role == $type;
    }
    public function sendNotification($title,$message,$image_url)
    {

        if($this->app_token)
        {
            $notification = [
                'title' => $title,
                'body' => $message,
                'image_url'=>$image_url,
                'priority' => 'high',
                'click_action'=> 'FLUTTER_NOTIFICATION_CLICK'
            ];
            SendNotifications($notification,[$this->app_token]);
        }

        return true;
    }


    // public function sendinquiriesCountNotification($title,$message,$count)
    // {

    //     if($this->app_token)
    //     {
    //         $notification = [
    //             'title' => $title,
    //             'body' => $message,
    //             'priority' => 'high',
    //             'click_action'=> 'FLUTTER_NOTIFICATION_CLICK',

    //         ];
    //         SendInqNotifications($notification,$count,[$this->app_token]);
    //     }

    //     return true;
    // }

    // public static function sendPublicNotifications($notification)
    // {
    //     $visitors_tokens = DB::table('app_visitors')->distinct('token')->pluck('token')->toArray();
    //     SendNotifications($notification,$visitors_tokens);
    // }

    public function region()
    {
        return $this->belongsTo('App\models\Region');
    }

    public function addresses()
    {
        return $this->hasMany('App\models\Address');
    }

    public function restaurant()
    {
        return $this->hasMany('App\models\Restaurant');
    }
    public function favoriteProducts()
    {
        return $this->belongsTo('App\models\UserFavoriteProduct');

    }
}
