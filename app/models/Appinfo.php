<?php
namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Appinfo extends Model
{
   protected $fillable=[ 'delivery_cost','call_center_number', 'quick_access','about_us','points_value','email_info'];
    //
}
?>
