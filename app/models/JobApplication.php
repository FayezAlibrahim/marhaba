<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\models\Applicant;
class JobApplication extends Model
{
    //
    public function applicants()
    {
        return $this->hasMany('App\models\Applicant');
    }
}
