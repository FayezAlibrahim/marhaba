<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    protected $table = 'ads';
    protected $guarded = ["id","image","created_at","updated_at"];
    protected $hidden = ["image_path"];

    public function get_item()
    {
            return false;
    }

    // public function region()
    // {
    //     return $this->belongsTo('App\Models\Region');
    // }
}
