<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    protected $fillable = ['user_id','title','content'];


    public function user()
    {
        return $this->belongsTo('App\models\User');
    }
}
