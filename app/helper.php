<?php

use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\models\User;
use App\models\Restaurant;
function formatValidationMessages($errors)
{


    return Arr::flatten($errors);
}


function getMediaUrl($path)
{
    return url('/uploads/'.$path);
}

function returnDeliveryUserInfo($delivery)
{
    return [
        "id"=>$delivery->id,
        "user_id"=> $delivery->user->id,
        "name"=> $delivery->user->username,
        "is_available"=> $delivery->availability==1?"Yes":"No",
        "phone"=> $delivery->user->phone,
        "city"=> $delivery->user->city->name,
        "type"=> $delivery->type,
        "restaurant"=> isset($delivery->restaurant_id)?$delivery->restaurant->id:null,
        "role"=> $delivery->user->role,

    ];
}

function CheckAndDecodeJson($json)
{
    if(!isset($json) || $json == "")
        return [];

    return json_decode($json);
}

function hl_uploadFileTo($file,$path)
{
    $file_path = Storage::disk('public_images')->put($path, $file);
    return [
        'media_path' => $file_path,
        'media_url' => getMediaUrl($file_path)
    ];


}

function hl_deleteFile($file_path)
{
    return Storage::disk('public_images')->delete($file_path);
}

function calcPercentage($base,$number)
{

    if($base == 0)
        return 0;

    return intval($number * 100 / $base);
}


function formatDateAsName($date)
{
    $d    = new DateTime($date);
    return $d->format('l');
}

function GetReportDates($filter_name)
{
    if ($filter_name == "weekly")
    {
        $from_date = Carbon::yesterday()->subWeek(1);
        $to_date = Carbon::yesterday();
    }
    elseif ($filter_name == "monthly")
    {
        $from_date = Carbon::yesterday()->subMonth(1);
        $to_date = Carbon::yesterday();
    }
    elseif ($filter_name = "yesterday")
    {
        $from_date = Carbon::yesterday();
        $to_date = Carbon::today();
    }
    else
    {
        $from_date = Carbon::today();
        $to_date = Carbon::now();
    }

    return [
        'from_date' => $from_date,
        'to_date' => $to_date,
    ];
}


function getCsv($rows, $fileName = 'file.csv') {


    $columnNames = array_keys(current($rows));
    $columnNamesTrans = array_map(function($col){
        return trans('orders.'.$col);
    },$columnNames);



    $headers = [
        "Content-type" => "text/csv",
        "Content-Disposition" => "attachment; filename=" . $fileName,
        "Pragma" => "no-cache",
        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        "Expires" => "0"
    ];



        $file = fopen($fileName, 'w');
    fputs( $file, $bom = chr(0xEF) . chr(0xBB) . chr(0xBF) );
        fputcsv($file, $columnNamesTrans);
        foreach ($rows as $row) {
            if(isset($row["attributes"]))
                $row["attributes"]=json_encode($row["attributes"]);
                // $row["attributes"]= formantAttributeArray($row["attributes"]);
            fputcsv($file, array_values($row));
        }
        fclose($file);



    return response()->download($fileName, 'sales-' . date('d-m-Y-H:i:s').'.csv', $headers)->deleteFileAfterSend(true);
}



function getCsvForOrders($income,$outcome,$fee, $fileName = 'file.csv') {

    $columnNames = ['Id','Title','Cost','Type','Staff Name','Date'];

    $headers = [
        "Content-type" => "text/csv",
        "Content-Disposition" => "attachment; filename=" . $fileName,
        "Pragma" => "no-cache",
        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        "Expires" => "0"
    ];

    $file = fopen($fileName, 'w');
    fputs( $file, $bom = chr(0xEF) . chr(0xBB) . chr(0xBF) );
    fputcsv($file, $columnNames);
    $row_details=array();
    foreach ($income as $collection) {

            $row = [
                $collection->id,$collection->title,$collection->cost,'Income',$collection->staff? $collection->staff->name: '',  $collection->created_at  ];
            fputcsv($file, array_values($row));

    }
    foreach ($outcome as $collection) {

        $row = [
            $collection->id,$collection->title,$collection->cost,'Outcome',$collection->staff? $collection->staff->name: '',  $collection->created_at  ];
        fputcsv($file, array_values($row));

}
foreach ($fee as $collection) {

    $row = [
        $collection->id,$collection->title,$collection->amount,'FEE','',  $collection->created_at  ];
    fputcsv($file, array_values($row));

}
    fclose($file);

    return response()->download($fileName, 'Fees-income-outcome-' . date('d-m-Y-H:i:s').'.csv', $headers)->deleteFileAfterSend(true);
}


function formantAttributeArray($attributes)
{
    if(!is_array($attributes))
        return;
    $result = '';
    foreach ($attributes as $attribute)
    {
        $result .= $attribute->attribute_name . ' : ' . $attribute->value . PHP_EOL;
    }

    return $result;
}


function formatNumber($number)
{
    return number_format($number);
}


function SendSMS($request) {
    $text = urlencode($request['message']);
    $to = $request['numbers'];
    $url = "https:///messages.php?message=$text&phone=$to";
try {
    $ch = curl_init();
    if ($ch === false) {
        throw new Exception('failed to initialize');
    }
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $content = curl_exec($ch);
    if ($content === false) {
        throw new Exception(curl_error($ch), curl_errno($ch));
    }
    curl_close($ch);
    return $content;
}
 catch(Exception $e)
{
    trigger_error(sprintf('Curl failed with error #%d: %s',$e->getCode(), $e->getMessage()),E_USER_ERROR);
 }
}

function SendNewProductNotification($id,$token,$prod_id,$foodtype_id,$prod_name,$rest_name){
    $notification = [
        'title' => $prod_name,
        'body' => $prod_name." أصبحت متوفرة الان من ".$rest_name,
        'type'=>'Product',
        'resId'=>null,
        'product_id'=>$prod_id,
        'food_type_id'=>$foodtype_id,
        'priority' => 'high',
        'contentAvailable'=>true,
        'click_action'=> 'FLUTTER_NOTIFICATION_CLICK'
    ];
    SendNotifications($notification,[$token]);
}


function SendNewRestaurantNotification($id,$image,$name,$text,$city_id){
    $tokens=USER::where('city_id',$city_id)->where('role','client')->where('status',1)->pluck('app_token')->toArray();
    $notification = [
        'title' => $name,
        'body' => $text,
        'image_url'=>$image,
        'type'=>'Restaurant',
        'resId'=>$id,
        'product_id'=>null,
        'food_type_id'=>null,
        'priority' => 'high',
        'contentAvailable'=>true,
        'click_action'=> 'FLUTTER_NOTIFICATION_CLICK'
    ];
    SendNotifications($notification,$tokens);

}

function SendDeliveryNotifications($notification,$tokens)
{

     $API_ACCESS_KEY ="AAAApIMBGj4:APA91bHFpMx7WO2qofC4tCfCO9XwWDbxhERZpUnAzcV-DwS-FjRphbzcQx7bH2x8ah0S5g1DRbaiw6Ef-woFGYjkimDp0542lz05kJTC2Ouy1bqby0UW3gSekUfGo8QoheAIGeEq-K5y";

    $url = 'https://fcm.googleapis.com/fcm/send';
       // prepare the message
    $fields = array(
        'registration_ids' => $tokens,
        'notification' => $notification

    );
    // dd($tokens);
    $headers = array(
        "Authorization: key=$API_ACCESS_KEY",
        'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL,$url);
    curl_setopt( $ch,CURLOPT_POST,true);
    curl_setopt( $ch,CURLOPT_HTTPHEADER,$headers);
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER,false);
    curl_setopt( $ch,CURLOPT_POSTFIELDS,json_encode($fields,true));
    $result = curl_exec($ch);
    curl_close($ch);
    return  $result;
}
function SendNotifications($notification,$tokens)
{

    //dd($tokens);
    //$API_ACCESS_KEY ="AAAApIMBGj4:APA91bHFpMx7WO2qofC4tCfCO9XwWDbxhERZpUnAzcV-DwS-FjRphbzcQx7bH2x8ah0S5g1DRbaiw6Ef-woFGYjkimDp0542lz05kJTC2Ouy1bqby0UW3gSekUfGo8QoheAIGeEq-K5y";
     $API_ACCESS_KEY ="AAAApIMBGj4:APA91bHFpMx7WO2qofC4tCfCO9XwWDbxhERZpUnAzcV-DwS-FjRphbzcQx7bH2x8ah0S5g1DRbaiw6Ef-woFGYjkimDp0542lz05kJTC2Ouy1bqby0UW3gSekUfGo8QoheAIGeEq-K5y";

    $url = 'https://fcm.googleapis.com/fcm/send';
       // prepare the message
    $fields = array(
        'registration_ids' => $tokens,
        // 'notification' => $notification
        'data' => $notification

    );
    // dd($tokens);
    $headers = array(
        "Authorization: key=$API_ACCESS_KEY",
        'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL,$url);
    curl_setopt( $ch,CURLOPT_POST,true);
    curl_setopt( $ch,CURLOPT_HTTPHEADER,$headers);
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER,false);
    curl_setopt( $ch,CURLOPT_POSTFIELDS,json_encode($fields,true));
    $result = curl_exec($ch);
    curl_close($ch);
    return  $result;
}

function convertToHoursMins($mins) {
return Carbon::now()->addMinutes($mins)->format("g:i A");
}
function has_access ($slugs) {
    if (!Auth::user()->canDo($slugs)) {
        abort(403);
    }
    return true;
}

function invalidUserNameMessage()
{
    if(request()->has('lang') && request('lang') == "en")
    return "اسم المستخدم او كلمة المرور غير صحيحة";

        // return "invalid username or password";
    if(request()->has('lang') && request('lang') == "ar")
        return "اسم المستخدم او كلمة المرور غير صحيحة";
}
function numberFromatToInteger($number)
{
   return  (int)str_replace(',','',$number);
}

function isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
   }

function formatPhoneNumber($phone){
    $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    $arabic = ['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨','٩'];
    $num = range(0, 9);
    $res=str_replace($arabic, $num,$phone);
    return $res;
}

function distance($lat1, $lon1, $lat2, $lon2) {
    $theta = $lon1 - $lon2;
    $miles = (sin(deg2rad($lat1)) * sin(deg2rad($lat2))) + (cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)));
    $miles = acos($miles);
    $miles = rad2deg($miles);
    $miles = $miles * 60 * 1.1515;
    $feet = $miles * 5280;
    $yards = $feet / 3;
    $kilometers = $miles * 1.609344;
    $meters = $kilometers * 1000;
    // return compact('miles','feet','yards','kilometers','meters');
    return $meters;
}


function GoogleDistance($lat1, $lon1, $lat2, $lon2){
    $API_ACCESS_KEY ="AIzaSyBoNx0lZSVmTUatcvCJsQB8_DOwmhjRup4";

    $url = 'https://maps.googleapis.com/maps/api/distancematrix/xml';
       // prepare the message
       $url=$url.'?units=imperial&origins='.$lat1.','.$lon1.'&destinations='.$lat2.','.$lon2.'&mode=walking&language=en&key='.$API_ACCESS_KEY;
    $headers = array();
$headers[] = "Accept: application/json";

    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER,false);
    $result = curl_exec($ch);
    curl_close($ch);
    $xml = simplexml_load_string($result);
$json = json_encode($xml);
$array = json_decode($json,TRUE);
// dd($array);
    return  $array;
}

function randomPassword() {
    $alphabet = '8934567890123456789012345678901223456789013456789012345678901234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 5; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}


function calcDeliveryCost($user_id,$rest_id){
    $fee=265; //fee per 1000 meter if distanse is bigger than 2000 meters
    $feeInSmallDistance=530; //fee for orders in less than 1000 meters
    $feeInMidDistance=300; //fee for orders in range of [1150 - 2000] meters
    $min_cost=300; // minimum fee
    $Corrective_value=150;
    $cost=$expected_delivery_time=0;
    $restaurants = Restaurant::where('id',$rest_id)->first();
    $user = User::where('id',$user_id)->first();
    $result= GoogleDistance($user->addresses()->first()->lat,$user->addresses()->first()->lan, $restaurants->user->lat, $restaurants->user->lon);
//    return $result;
    if($result && $result['status']=='OK' && $result['row']['element']['status']!="ZERO_RESULTS"){ //google return valid result
    if( isset( $result['row'] ) ){
        $expected_delivery_time= $result['row']['element']['duration']['value'];
        $cost=$result['row']['element']['distance']['value'];
        // return $expected_delivery_time;
        // return $cost;

                $fee=$cost < 1150 ?$feeInSmallDistance :$fee;
                // return $fee;
                $fee=(1550 <= $cost) && ($cost <= 2000) ?$feeInMidDistance :$fee;
                $fee=$cost >6800 ? $fee+45:$fee;
                $d=(int)($cost/1000)==0?1:(int)($cost/1000);
                $cost =$d*$fee;
                $cost=(int)ceil($cost /25)*25;
                $edt=ceil((int)$expected_delivery_time/60/10) * 10;
                $rest_default_delivery_time=$restaurants->delivery_time;
                $time=$edt ." Min";
                if($rest_default_delivery_time ==null){
                $extra_time=2;
                $avg=(($edt+$extra_time)/2) > 40?(($edt+$extra_time)/2)-17:(($edt+$extra_time)/2);
                $time= convertToHoursMins($avg);
                }
                elseif(isset($rest_default_delivery_time) && $edt!= $rest_default_delivery_time) {

    $avg=(($edt+$rest_default_delivery_time)/2) > 40?(($edt+$rest_default_delivery_time)/2)-17:(($edt+$rest_default_delivery_time)/2);
    $time= convertToHoursMins($avg);
            }
              else{
                   $time= convertToHoursMins($rest_default_delivery_time);
              }
                }
                $cost=$cost<$min_cost?$min_cost:$cost;
                return ["cost"=>$cost,"expected_delivery_time"=> $time];

   }else{
    $result=  (int)distance($user->addresses()->first()->lat,$user->addresses()->first()->lan, $restaurants->user->lat, $restaurants->user->lon);
    $d=(int)$result/1000;
    $fee=$d < 1000 ?$feeInSmallDistance :$fee;
    $cost =$d*$fee;
    $cost=(int)$cost + (25 -  $cost % 25);
    $cost=$cost<$fee ?400:$cost;
    $expected_delivery_time=$restaurants->delivery_time ." Min";
    $cost=$cost<$min_cost?$min_cost:$cost;
    return ["cost"=>$cost,"expected_delivery_time"=>$expected_delivery_time];
   }


}
