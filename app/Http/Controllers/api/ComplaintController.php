<?php

namespace App\Http\Controllers\api;

use App\models\Complaint;
use App\models\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ComplaintController extends Controller
{
    private function storeValidation()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'title' => 'required|string|max:200',
            'content' => 'required|string|max:1000'
        ];
    } 

    public function store(Request $request)
    {
        Log::error(json_encode($request->all()));
        $validator = Validator::make($request->all(), $this->storeValidation());
        if ($validator->fails()) {
            Log::error($validator->errors());

            return response()->json(['status' => 'fail','errors' => formatValidationMessages($validator->errors()->getMessages())]);
        }


        $complaint = Complaint::create($validator->validated());
        $notification_data = [

        ];
        Notification::create($notification_data);
        return response()->json(['status' => 'success'], 200);


    }
}
