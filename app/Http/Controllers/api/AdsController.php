<?php

namespace App\Http\Controllers\api;

use App\Http\Resources\Category as CategoryResource;
use App\Http\Resources\Product as ProductResource;
use App\models\Ads;
use App\models\User;
use App\models\Category;
use App\models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdsController extends Controller
{ 

    public function index($id)
    {
        $user=User::where('id',$id)->first();
$ads = Ads::get(); 
$adsToReturn=[];
$ad_type="";
foreach($ads as $ad){
$ad['foodtype_id']=null;
    if($ad["type"] == "product"){
        $ad["ad_type"]="product"; 
        $ad['foodtype_id']=Product::where('id',$ad["item_id"])->first()->food_type_id;
    }
    else if($ad["type"] == "category"){
        $ad["ad_type"]="category";
    }
    else{
        $ad["ad_type"]="";
    } 
    if(in_array($user->city_id,json_decode($ad->city_id,true))){
        array_push($adsToReturn,$ad);
    }
    
}
        return response()->json($adsToReturn);
    }
}
