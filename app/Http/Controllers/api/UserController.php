<?php

namespace App\Http\Controllers\api;

use App\Http\Resources\OrderCollection;
use App\models\User;
use App\models\Order;
use App\models\Inquiry;
use App\models\Product;
use App\models\Fee;
use App\models\Appinfo;
use App\models\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\ProductCollection;
use App\models\UserActivity;
use App\models\Delivery;
class UserController extends Controller
{

    private function RegisterValidations()
    {
        return [
            'username' => 'required|string|unique:users,username',
            'phone' => 'required|unique:users,phone|min:10|max:10|regex:/[0-9]/',
            'app_token' => 'nullable|string',
            'address' => 'nullable|string',
            'city_id' => 'required|exists:cities,id',
            'password' => 'required',
            'lat' => 'nullable|string',
            'lon' => 'nullable|string',
        ];
    }
        private function InqueryValidations()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'question' => 'required|string|min:3|max:100'

        ];
    }

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), $this->RegisterValidations());
        if ($validator->fails()) {
        Log::error($validator->errors());
        return response()->json(['status' => 'fail','errors' => formatValidationMessages($validator->errors()->getMessages())]);
        }

        $validated_data = $validator->validated();
        $validated_data["password"] = Hash::make($validated_data["password"]);
        $validated_data['active']='yes';
        $validated_data['status']='0';
        $rp=randomPassword();
        $request['message']="Your Activation Code is: (".$rp.") ";
        $request['numbers']="963".substr($request['phone'], 1);
        $sms=SendSMS($request);
        $user_created = User::create($validated_data);
        $user = User::find($user_created->id);
        $user->code=$rp;
        $user->save();
        if($request['app_token']){
            $request['token']=$request['app_token'];
            $request['user_id']=$user_created->id;
            $this->setToken($request);
        }
        return response()->json(['status' => 'success','data' => $user->toArray()]);
    }


    private function LoginValidations()
    {
        return [
            'phone' => 'required',
            'password' => 'required',
            'app_token' => 'nullable|string'
        ];
    }

    public function login(Request $request)
    {
// dd($request["env"]);
        $request["phone"]=formatPhoneNumber($request["phone"]);
        if($request["password"]){
            $request["password"]=formatPhoneNumber($request["password"]);
            }
        $validator = Validator::make($request->all(), $this->LoginValidations());
        if ($validator->fails()) {
            Log::error($validator->errors());
            return response()->json(['status' => 'fail','errors' => formatValidationMessages($validator->errors()->getMessages())]);
        }

        try
        {
                $user = User::where(['phone' => $request->phone])->with('region')->first();
                if(isset($user) && Hash::check($request->password, $user->password))
                {
                    if($user->status == "0"){
                        $rp=randomPassword();
                        $user->code=$rp;
                        $user->save();
                        $request['message']="Your Activation Code is: (".$rp.") ";
                        $request['numbers']="963".substr($request->phone, 1);
                        $sms=SendSMS($request);
                return response()->json(['status' => 'fail','errors' => ["Account Not Verified, new verification code has been sent"]]);
                    }

                    if(isset($request['app_token'])&&!empty($request['app_token'])){
                        $old_token=$user->app_token;
                        DB::table('app_visitors')->where('token',$old_token)->update(['token'=>$request['app_token']]);
                        $user->app_token=$request['app_token'];
                        $user->save();
                    }
                    if($user->active == "yes"){
if($user->role =="delivery"){
    $user->is_available=returnDeliveryUserInfo(Delivery::where('user_id', $user->id)->first())["is_available"];
} 
// Delivery::where('user_id', $id)->first()
                        $user->has_address=$user->addresses()->count()>0?true:false;
                        if($user->addresses()->count()>0){
                        $user->lat=$user->addresses()->first()->lat;
                        $user->lon=$user->addresses()->first()->lan;
                    }
                    else{
                        $user->lat=null;
                        $user->lon=null;
                    }
                        return response()->json(['status' => 'success','data' => $user->toArray()]);
                    }
                    return response()->json(['status' => 'fail','errors' => [trans('admin.account_not_active')]]);
                }
                else{

                 return response()->json(['status' => 'fail','errors' => ["اسم المستخدم او كلمة المرور غير صحيحة"]]);
                }


        }
        catch (\Throwable $exception)
        {
            Log::error($exception->getMessage());
            return response()->json(['error' => $exception->getMessage()], 500);
        }
    }



    private function uploadProfileValidations()
    {
        return [
            'username' => 'sometimes|string|exists:users,username',
            'email' => 'nullable|sometimes|email|string',
            'address' => 'nullable|sometimes|string',
            'city_id' => 'nullable|sometimes|exists:cities,id',
            'region_id' => 'sometimes|exists:regions,id',
            'lat' => 'nullable|sometimes|nullable|string',
            'lon' => 'nullable|sometimes|nullable|string',
        ];
    }
    public function uploadProfile(Request $request,$id)
    {
        // return $request;
        $validator = Validator::make($request->all(), $this->uploadProfileValidations());
        if ($validator->fails()) {
            Log::error($validator->errors());

            return response()->json(['status' => 'fail','errors' => formatValidationMessages($validator->errors()->getMessages())]);
        }
        $validated_data = $validator->validated();
        $user = User::findOrFail($id);

        try
        {
            // $user->update($request->toArray());
            $user->update($validated_data);
            // return $user;
        }
        catch (\Throwable $exception)
        {
            Log::error($exception->getMessage());
            return response()->json(['error' => $exception->getMessage()], 500);
        }

        return response()->json(['status' => 'success','data' => $user->toArray()], 200);

    }


    public function get_orders($id)
    {

        $per_page = request()->has('per_page') ? request('per_page') : 10;
        $order_by =  request()->has('order_by') ? request('order_by') : 'created_at';
        $order =  request()->has('order') ? request('order') : 'desc';

        $user = User::findOrFail($id);
        $orders = $user->orders()->orderBy($order_by,$order)->paginate($per_page);

        return new OrderCollection($orders);
    }

    public function user_info($id)
    {

        $user = User::findOrFail($id);
        $user->has_address = $user->addresses()->count() > 0 ? true : false;
        $user->lat = $user->has_address?$user->addresses()->first()->lat:null;
        $user->lon = $user->has_address?$user->addresses()->first()->lan:null;
        if($user->role =="delivery"){
            $user->is_available=returnDeliveryUserInfo(Delivery::where('user_id', $user->id)->first())["is_available"];
        } 
            $user->city_id= strval($user->city_id);
            $user->region_id= strval($user->region_id);
            return response()->json(['status' => 'success', 'data' =>$user->toArray()]);
        }

    public function setToken(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'token' => 'required|string'
        ]);
        if ($validator->fails()) {
            Log::error($validator->errors());

            return response()->json(['status' => 'fail','errors' => formatValidationMessages($validator->errors()->getMessages())]);
        }

        if($request->has('user_id') && !empty($request->user_id))
            $user = User::findOrFail($request->user_id)->update([
                'app_token' => $request->token
            ]);

        $token = DB::table('app_visitors')->where('token',$request->token)->first();

        if(!$token)
        {

           $x= DB::table('app_visitors')->insert([
                [
                    "token" => $request->token,
                ]
            ]);
        }
        return response()->json(['status' => 'success']);
    }
    public function get_user_wallet($id,Request $request)
    {
        $wallet=User::findOrFail($id)->wallet;
        $currency="SYP";
        if($request->has('lang') && $request->lang=='ar')
            $currency= getCurrentCurrency() ;
        return response()->json(['wallet' => $wallet." ".$currency]);
    }

           public function inquiry(Request $request)
    {

        $validator = Validator::make($request->all(), $this->InqueryValidations());
        if ($validator->fails()) {
            Log::error($validator->errors());

            return response()->json(['status' => 'fail','errors' => formatValidationMessages($validator->errors()->getMessages())],422);
        }
        DB::beginTransaction();
        $validated_data = $validator->validated();

        $inquery = Inquiry::create($validated_data);
        $notification_data = [

        ];
        Notification::create($notification_data);
        DB::commit();
        return response()->json(['status' => 'success','data' => $inquery->toArray()]);
    }
        public function myInquiries ($id) {
        $user = User::find($id);
        $inqueries = $user->inquiries()->with(['user','admin'])->get()->toArray();
        $user->inquiries()->update(['notified' => 'yes']);

        return response()->json(['status' => 'success','data' => $inqueries]);
    }


    public function newAnsweredInquiries ($id) {
        $user = User::find($id);

        return response()->json(['status' => 'success','data' => $user->inquiries()->where('notified', 'no')->count()]);
    }

    private function addToBasketValidations()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'product_id' => 'required|exists:products,id'

        ];
    }
    public function addProductToBasket(Request $request){
$prod_id=$request['product_id'];
$user_id=$request['user_id'];
$validator = Validator::make($request->all(), $this->addToBasketValidations());
if ($validator->fails()) {
    Log::error($validator->errors());
    return response()->json(['status' => 'fail','errors' => formatValidationMessages($validator->errors()->getMessages())]);
}
$validated_data = $validator->validated();
DB::table('user_basket')->insert($validated_data);
return response()->json(['status' => 'success','data' => null]);
}

    public function emptyBasket($id){
        $validator = Validator::make(["id"=>$id],["id"=>'required|exists:users,id']);
        if ($validator->fails()) {
            Log::error($validator->errors());
            return response()->json(['status' => 'fail','errors' => formatValidationMessages($validator->errors()->getMessages())]);
        }
        DB::table('user_basket')->where('user_id', $id)->delete();
        return response()->json(['status' => 'success','data' => null]);
    }

    public function getUserBasketProducts($id){
return new ProductCollection(Product::whereIn('id',DB::table('user_basket')->where('user_id',$id)->pluck('product_id'))->get());
    }


    public function code_verify(Request $request)
    {
        if($request["phone"]){
            $request["phone"]=formatPhoneNumber($request["phone"]);
            }
            if($request["code"]){
                $request["code"]=formatPhoneNumber($request["code"]);
                }
        $result="Invalid Code";
        $validator = Validator::make($request->all(),[
            'phone' => 'required|string|max:10|regex:/(09)[0-9]{8}/',
            'code' => 'required|string'
        ]);
        if ($validator->fails()) {
            Log::error($validator->errors());

            return response()->json(['status' => 'fail','errors' => formatValidationMessages($validator->errors()->getMessages())]);
        }
        $user = User::where(['phone' => $request->phone])->first();
        $result= $user->code === $request->code ?["MATCH"]:['Invalid Code'];
if($result==["MATCH"]){
    $user->status=1;
    $user->save();
    return response()->json(['status' => 'success','data' => $result]);
}
return response()->json(['status' => 'fail','errors' => $result]);
    }


    public function code_resend(Request $request)
    {
        if($request["phone"]){
            $request["phone"]=formatPhoneNumber($request["phone"]);
            }

        $validator = Validator::make($request->all(),[
            'phone' => 'required|string|max:10|regex:/(09)[0-9]{8}/'
        ]);
        if ($validator->fails()) {
            Log::error($validator->errors());
            return response()->json(['status' => 'fail','errors' => formatValidationMessages($validator->errors()->getMessages())]);
        }

        $user = User::where(['phone' => $request->phone])->first();
if(!$user){
    return response()->json(['status' => 'fail','errors' => ["Account Doesn't exist"]]);
}

$rp=randomPassword();
$user->code=$rp;
$user->save();
$request['message']="Your Activation Code is: (".$rp.") ";
$request['numbers']="963".substr($request->phone, 1);
$sms=SendSMS($request);

return response()->json(['status' => 'success','data' => $user->toArray()]);

    }
    public function forget_password(Request $request)
    {
        if($request["phone"]){
            $request["phone"]=formatPhoneNumber($request["phone"]);
            }
        $validator = Validator::make($request->all(),[
            'phone' => 'required|string|max:10|regex:/(09)[0-9]{8}/'
        ]);
        if ($validator->fails()) {
            Log::error($validator->errors());
            return response()->json(['status' => 'fail','errors' => formatValidationMessages($validator->errors()->getMessages())]);
        }

        $user = User::where(['phone' => $request->phone])->first();
if(!$user){
    return response()->json(['status' => 'fail','errors' => ["Account Doesn't exist"]]);
}

$rp=randomPassword();
$user->code=$rp;
$user->save();
$request['message']="Your Activation Code is: (".$rp.") ";
$request['numbers']="963".substr($request->phone, 1);
$sms=SendSMS($request);

return response()->json(['status' => 'success','data' => $user->toArray()]);

    }

    public function new_password(Request $request)
    {
        if($request["phone"]){
            $request["phone"]=formatPhoneNumber($request["phone"]);
            }
            if($request["password"]){
                $request["password"]=formatPhoneNumber($request["password"]);
                }
        $validator = Validator::make($request->all(),[
            'phone' => 'required|string|max:10|regex:/(09)[0-9]{8}/',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            Log::error($validator->errors());
            return response()->json(['status' => 'fail','errors' => formatValidationMessages($validator->errors()->getMessages())]);
        }

        $user = User::where(['phone' => $request->phone])->first();
if(!$user){
    return response()->json(['status' => 'fail','errors' => ["Account Doesn't exist"]]);
}

$user->password = Hash::make($request->password);
$user->save();

return response()->json(['status' => 'success','data' => $user->toArray()]);

    }



    public function newusers()
    {
        $newusers=User::whereDate('created_at', \Carbon\Carbon::today())->count();
        return $newusers;
    }

    public function users_orders($id){
        $info = Appinfo::where('id', 31)->first();
        $env = $info->environment;
        if (isset($env) && $env == "test")
        {
            if (User::find($id)->environment == "test")
            {
                $orders=Order::where('id',0)->get();
            }
            else {
                $orders=Order::where('user_id',$id)->orderBy('created_at','DESC')->get();
            }
        }
        else
        {
            $orders=Order::where('user_id',$id)->orderBy('created_at','DESC')->get();
        }
         return new OrderCollection($orders);
    }

    public function order_details($id){
         $order=Order::where('id',$id)->get();
         Request()->chk=true;
         return new OrderCollection($order);
    }


    public function test ($user_id,$rest_id){
       return  calcDeliveryCost($user_id,$rest_id);
    }

    public function destroy(Request $request)
    {
            $object = User::findOrFail($request['id']);
            $deleted = $object->delete();
            if ($deleted) {
                $log_message = trans('users.delete_log') . '#' . $object->id;
                UserActivity::logActivity($log_message);
                return redirect()->back()->withSuccess('تم الحذ بنجاح');
            } else {
                return response()->json(['status' => 'fail', 'message' => 'fail_while_delete']);
            }


        return redirect()->route($this->index_route);
    }

}
