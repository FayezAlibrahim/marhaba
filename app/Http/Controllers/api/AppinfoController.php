<?php
namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Appinfo;
use App\models\User;
class AppinfoController extends Controller
{
    public function index()
    {
        $info = Appinfo::where('id', 31)->first();
        $env = $info->environment;
        if (isset($env) && $env == "test")
        {
            if (isset(Request()->user_id) && Request()->user_id != "0" &&  User::find(Request()->user_id)->environment == "test")
            {
                return json_decode($info->f_appinfo,true);
            }
            else {
            return response()->json(['status' => 'success', 'data' => $info->only(['about_us', 'call_center_number', 'facebook', 'instagram']) ]);

            }
        }
        else
        {
            return response()->json(['status' => 'success', 'data' => $info->only(['about_us', 'call_center_number', 'facebook', 'instagram']) ]);
        }
    }

}

