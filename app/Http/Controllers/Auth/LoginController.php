<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\models\UserActivity;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    // use RedirectsUsers, ThrottlesLogins;

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function loginForm()
    {

        if(Auth::check())
        {
            $log_message = trans('admin.login_log') ;
            UserActivity::logActivity($log_message);
            return redirect()->route('admin.dashboard');
        }

        return view('auth.login');
    }


    public function username()
{
     $login = request()->input('identity');

     $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
     request()->merge([$field => $login]);

     return $field;
}


}
