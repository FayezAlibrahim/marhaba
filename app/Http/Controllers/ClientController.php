<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Aboutus;
use App\models\Appinfo;
use App\models\JobApplication;
use App\models\Service;
use App\models\Staff;
use App\models\Team;
use App\models\Ads;

class ClientController extends Controller
{
    public function home(){
        $staff=Staff::limit(5)->get();
        $services=Service::all();
        $jobvacanies=JobApplication::get();
        $appinfo=Appinfo::first();
        $aboutus=Aboutus::first();
        $team=Team::all();
        $ads=Ads::all();
        return view('client.welcome',compact('staff','services','jobvacanies','appinfo','aboutus','team','ads'));
    }
    public function allstaff()
    {
        $staff=Staff::paginate(9);
        $appinfo=Appinfo::first();

        return view('client.staffindex',compact('staff','appinfo'));
    }
    public function profilestaff($id)
    {
        $staff=Staff::findorfail($id);
        $appinfo=Appinfo::first();

        return view('client.staffprofile',compact('staff','appinfo'));
    }
    public function alljob()
    {
        $jobvacanies =JobApplication::paginate(9);
        return view('client.jobvacancies',compact('jobvacanies'));
    }

}
