<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Team;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
class TeamController extends Controller
{
    private function StoreValidationRules()
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'image' => 'required|mimes:jpg,jpeg,png,webp',
        ];
    }
    private function UpdateValidationRules()
    {
        return [
            'id'=>'required|int|exists:teams,id',
            'title' => 'required|string',
            'description' => 'required|string',
            'Vision' => 'required|string',
            'image' => 'nullable|mimes:jpg,jpeg,png,webp',
        ];
    }


public function index()
{
    $JobApplications=Team::all();
    return view('admin.team.index',compact('JobApplications'));
}
public function create()
{
    return view('admin.team.create');
}
public function store(Request $request)
{
    $validated_data = $request->validate($this->StoreValidationRules());
    try {
     DB::beginTransaction();
    $JobApplication=new Team;
    $JobApplication->title=$validated_data['title'];
    $JobApplication->description=$validated_data['description'];

    if($request->has('image'))
    {
        $image = $validated_data['image'];
        $img_file_path = Storage::disk('public_images')->put('Team', $image);
        $JobApplication->image = getMediaUrl($img_file_path);
    }
    $JobApplication->save();
    DB::table('user_activities')->insert([
        'user_id' => Auth::user()->id,
        'activity_name' => 'Add New Team Member',
    ]);
    DB::commit();
    return redirect()->route('admin.team.index')->withSuccess('Team member was added successfully!!');
} catch (\Exception $ex) {
    DB::rollBack();
    dd($ex->getMessage());
    Log::error($ex->getMessage());
    return redirect()->back()->with('errors', $ex->getMessage());
}

}
public function edit($id)
{
    $JobApplication=Team::find($id);
    return view('admin.team.edit',compact('JobApplication'));
}
public function update(Request $request)
{

        $validated_data = $request->validate($this->UpdateValidationRules());
        try {
            DB::beginTransaction();
           $JobApplication=Team::find($validated_data['id']);
           $JobApplication->title=$validated_data['title'];
           $JobApplication->description=$validated_data['description'];
           $JobApplication->Vision=$validated_data['Vision'];
           if($request->has('image'))
           {
               $image = $validated_data['image'];
               $img_file_path = Storage::disk('public_images')->put('posts', $image);
               $JobApplication->image = getMediaUrl($img_file_path);
           }
           $JobApplication->save();
           DB::table('user_activities')->insert([
            'user_id' => Auth::user()->id,
            'activity_name' => 'Update Team Member Info',
        ]);

           DB::commit();
           return redirect()->route('admin.team.index')->withSuccess('Team member was updated successfully!!');
       } catch (\Exception $ex) {
           DB::rollBack();
           dd($ex->getMessage());
           Log::error($ex->getMessage());
           return redirect()->back()->with('errors', $ex->getMessage());
       }

}

    public function destroy(Request $request)
    {

        $JobApplication=Team::findorfail($request['id']);
        $delted=$JobApplication->delete();
        if($delted)
        {
            return redirect()->back()->withSuccess('Team member was deleted successfully!!');
        }
        else return redirect()->back()->with('An error occured');

    }
}
