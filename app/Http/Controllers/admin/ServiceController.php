<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Service;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
class ServiceController extends Controller
{
    private function StoreValidationRules()
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'title_en' => 'required|string',
            'description_en' => 'required|string',
            'image_url' => 'required|mimes:jpg,jpeg,png,webp',
        ];
    }
    private function UpdateValidationRules()
    {
        return [
            'id'=>'required|int|exists:services,id',
            'title' => 'required|string',
            'description' => 'required|string',
            'title_en' => 'required|string',
            'description_en' => 'required|string',
            'image_url' => 'nullable|mimes:jpg,jpeg,png,webp',
        ];
    }


public function index()
{
    $services=Service::all();
    return view('admin.services.index',compact('services'));
}
public function create()
{
    return view('admin.services.create');
}
public function store(Request $request)
{
    $validated_data = $request->validate($this->StoreValidationRules());
    try {
     DB::beginTransaction();
    $service=new Service;
    $service->title=$validated_data['title'];
    $service->description=$validated_data['description'];
    $service->title_en=$validated_data['title_en'];
    $service->description_en=$validated_data['description_en'];

    if($request->has('image_url'))
    {
        $image = $validated_data['image_url'];
        $img_file_path = Storage::disk('public_images')->put('services', $image);
        $service->image_url = getMediaUrl($img_file_path);
    }
    $service->save();

    DB::table('user_activities')->insert([
        'user_id' => Auth::user()->id,
        'activity_name' => 'Add New Service',
    ]);
    DB::commit();
    return redirect()->route('admin.services.index')->withSuccess('Service was added successfully!!');
} catch (\Exception $ex) {
    DB::rollBack();
    dd($ex->getMessage());
    Log::error($ex->getMessage());
    return redirect()->back()->with('errors', $ex->getMessage());
}

}
public function edit($id)
{
    $service=Service::find($id);
    return view('admin.services.edit',compact('service'));
}
public function update(Request $request)
{

        $validated_data = $request->validate($this->UpdateValidationRules());
        try {
            DB::beginTransaction();
           $service=Service::find($validated_data['id']);
           $service->title=$validated_data['title'];
           $service->description=$validated_data['description'];
           $service->title_en=$validated_data['title_en'];
           $service->description_en=$validated_data['description_en'];
           if($request->has('image_url'))
           {
               $image = $validated_data['image_url'];
               $img_file_path = Storage::disk('public_images')->put('posts', $image);
               $service->image_url = getMediaUrl($img_file_path);
           }
           $service->save();

           DB::table('user_activities')->insert([
            'user_id' => Auth::user()->id,
            'activity_name' => 'Update Service Details',
        ]);

           DB::commit();
           return redirect()->route('admin.services.index')->withSuccess('Service was updated successfully!!');
       } catch (\Exception $ex) {
           DB::rollBack();
           dd($ex->getMessage());
           Log::error($ex->getMessage());
           return redirect()->back()->with('errors', $ex->getMessage());
       }

}

    public function destroy(Request $request)
    {

        $service=Service::findorfail($request['id']);
        $delted=$service->delete();
        if($delted)
        {
            return redirect()->back()->withSuccess('Service was deleted successfully!!');
        }
        else return redirect()->back()->with('An error occured');

    }
}
