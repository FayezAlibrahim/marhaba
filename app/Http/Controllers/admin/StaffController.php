<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Attachment;
use App\models\Income;
use App\models\Outcome;
use App\models\Staff;
use App\models\WorkExperianc;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class StaffController extends Controller
{
    private $index_view;
    private $create_view;
    private $edit_view;
    private $edit_variation_view;
    private $show_view;
    private $index_route;
    private $model_instance;
    private $success_message;
    private $error_message;
    private $update_success_message;
    private $update_error_message;
    private $model_instance_translation;


    public function __construct()
    {
        $this->index_view = 'admin.staff.index';
        $this->create_view = 'admin.staff.create';
        $this->show_view = 'admin.staff.show';
        $this->edit_view = 'admin.staff.edit';
        $this->edit_variation_view = 'admin.staff.edit_variation';
        $this->index_route = 'admin.staff.index';
        $this->success_message = trans('admin.created_successfully');
        $this->update_success_message = trans('admin.update_created_successfully');
        $this->error_message = trans('admin.fail_while_create');
        $this->update_error_message = trans('admin.fail_while_update');
    }

    private function StoreValidationRules()
    {
        return [
            'name' => 'required|string|min:1|max:200',
            'image_url' => 'nullable|mimes:jpg,jpeg,png,webp',
            'nationality' => 'required|string|min:1|max:200',
            'passport_number' => 'required|numeric',
            'age' => 'required|numeric',
            'gender' => 'required|in:male,female',
            'attachment' => 'nullable|array',
            'exper' => 'nullable',
        ];
    }

    private function UpdateValidationRules()
    {
        return [
            'name' => 'required|string|min:1|max:200',
            'image_url' => 'nullable|mimes:jpg,jpeg,png,webp',
            'nationality' => 'required|string|min:1|max:200',
            'passport_number' => 'required|numeric',
            'age' => 'required|numeric',
            'gender' => 'required|in:male,female',
            'attachment' => 'nullable|array',
            'images' => 'nullable|array',
            'videos' => 'nullable|array',
            'files' => 'nullable|array',

            'exper' => 'nullable',
        ];
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $staff = Staff::orderby('created_at', 'desc')->get();
        return view($this->index_view, compact('staff'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view($this->create_view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated_data = $request->validate($this->StoreValidationRules());


        try {
            DB::beginTransaction();
            $staff = new staff;
            $staff->name = $validated_data['name'];
            $staff->nationality = $validated_data['nationality'];
            $staff->gender = $validated_data['gender'];
            $staff->age = $validated_data['age'];
            $staff->passport_number = $validated_data['passport_number'];
            $staff->save();
            if ($request->has('image_url')) {
                $image = $validated_data['image_url'];
                $img_file_path = Storage::disk('public_images')->put('staff', $image);
                $staff->image = getMediaUrl($img_file_path);
                $staff->save();
            }
            if ($request->has('attachment')) {
                foreach ($validated_data['attachment'] as $fileatach) {
                    $file =  $fileatach;
                    // Upload video
                    $destinationPath = 'uploads/attachments';
                    $fileName = $file->getClientOriginalName();
                    $uploadSuccess = $file->move($destinationPath, time() . $fileName);
                    $Src = '/' . $destinationPath . '/' . time() . $fileName;
                    $attachment = new Attachment;
                    $attachment->staff_id = $staff->id;
                    $attachment->name=$fileName;
                    $attachment->path = $Src;
                    if ($file->getClientOriginalExtension() == "pdf")
                        $attachment->type = "pdf";
                    else if ($file->getClientOriginalExtension() == "mp4")
                        $attachment->type = "video";
                    else  $attachment->type = "img";
                    $attachment->save();
                }
            }
            if ($request->has('exper')) {
                foreach ($validated_data['exper'] as $exp) {
                    if (isset($exp['title'])) {
                        $experiance = new WorkExperianc;
                        $experiance->title = $exp['title'];
                        $experiance->from_date = $exp['from'];
                        $experiance->to_date = $exp['to'];
                        $experiance->total = Carbon::parse($exp['to'])->diffInMonths(Carbon::parse($exp['from']));
                        $experiance->staff_id = $staff->id;
                        $experiance->save();
                    }
                }
            }
            // $log_message = trans('staff.create_log') . '#' . $staff->id;
            // UserActivity::logActivity($log_message);

            DB::table('user_activities')->insert([
                'user_id' => Auth::user()->id,
                'activity_name' => 'Add New Staff',
            ]);

            DB::commit();

            return redirect()->route($this->index_view, $staff->id)->with('success', 'Staff was added successfully!!');
        } catch (\Exception $ex) {
            // dd($this->error_message);
            DB::rollBack();
            dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $staff = Staff::findOrFail($id);
        $staff->experience = $staff->experience()->orderby('from_date', 'DESC')->get();
        return view($this->show_view, compact('staff'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $staff = Staff::findOrFail($id);
        return view($this->edit_view, compact('staff'));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated_data = $request->validate($this->UpdateValidationRules());
        try {
            DB::beginTransaction();


            $staff = Staff::findOrFail($id);
            $staff->name = $validated_data['name'];
            $staff->nationality = $validated_data['nationality'];
            $staff->gender = $validated_data['gender'];
            $staff->age = $validated_data['age'];
            $staff->passport_number = $validated_data['passport_number'];
            $staff->save();
            if ($request->has('image_url')) {
                $image = $validated_data['image_url'];
                $img_file_path = Storage::disk('public_images')->put('staff', $image);
                $staff->image = getMediaUrl($img_file_path);
                $staff->save();
            }

            if ($request->has('attachment')) {
               $deleteatta=  Attachment::where('staff_id', $id)->whereNotIn('id',$validated_data['attachment'])->delete();
            }
            $outcomeid=[];
if($request->has('outcome'))
{

    foreach ($request['outcome'] as $out) {

        if(array_key_exists('id',$out))
        {
            $outs= Outcome::find($out['id']);
        }


        if (!isset($outs))
          $outs = new Outcome;

        if (isset($out['title'])) {


        $outs->title=$out['title'];
        $outs->cost=$out['cost'];
        $outs->staff_id=$staff->id;
        $outs->save();
        array_push($outcomeid,$outs->id);

        }
    }
    if($outcomeid!=[])
 { $dele= Outcome::where('staff_id', $staff->id)->whereNotIn('id',$outcomeid)->delete();
 }
}
$incomeid=[];
if($request->has('income'))
{
    foreach ($request['income'] as $in) {

        if(array_key_exists('id',$in))
        {
            $income= Income::find($in['id']);
        }
        if (!isset($income))
        {  $income=new Income;
        }
        if (isset($in['title'])) {
        $income->title=$in['title'];
        $income->staff_id=$staff->id;
        $income->cost=$in['cost'];
        $income->save();
        array_push($incomeid,$income->id);

        }
    }
    if($incomeid!=[])
  { Income::where('staff_id', $staff->id)->whereNotIn('id',$incomeid)->delete();
  }

}
            if ($request->has('files')) {

                foreach ($validated_data['files'] as $fileatach) {
                    $file =  $fileatach;
                    // Upload video
                    $destinationPath = 'uploads/attachments';
                    $fileName = $file->getClientOriginalName();
                    $uploadSuccess = $file->move($destinationPath, time() . $fileName);
                    $Src = '/' . $destinationPath . '/' . time() . $fileName;
                    $attachment = new Attachment;
                    $attachment->staff_id = $staff->id;
                    $attachment->name=$fileName;

                    $attachment->path = $Src;
                    $attachment->type = "pdf";
                    $attachment->save();
                }
            }
            if ($request->has('images')) {

                foreach ($validated_data['images'] as $fileatach) {
                    $file =  $fileatach;
                    // Upload video
                    $destinationPath = 'uploads/attachments';
                    $fileName = $file->getClientOriginalName();
                    $uploadSuccess = $file->move($destinationPath, time() . $fileName);
                    $Src = '/' . $destinationPath . '/' . time() . $fileName;
                    $attachment = new Attachment;
                    $attachment->staff_id = $staff->id;
                    $attachment->path = $Src;
                    $attachment->name=$fileName;

                    $attachment->type = "img";
                    $attachment->save();
                }
            }
            if ($request->has('videos')) {

                foreach ($validated_data['videos'] as $fileatach) {
                    $file =  $fileatach;
                    // Upload video
                    $destinationPath = 'uploads/attachments';
                    $fileName = $file->getClientOriginalName();
                    $uploadSuccess = $file->move($destinationPath, time() . $fileName);
                    $Src = '/' . $destinationPath . '/' . time() . $fileName;
                    $attachment = new Attachment;
                    $attachment->staff_id = $staff->id;
                    $attachment->path = $Src;
                    $attachment->name=$fileName;

                    $attachment->type = "video";
                    $attachment->save();
                }
            }
            if ($request->has('exper')) {
                WorkExperianc::where('staff_id', $id)->delete();
                foreach ($validated_data['exper'] as $exp) {

                    if (isset($exp['title'])) {
                        $experiance = new WorkExperianc;
                        $experiance->title = $exp['title'];
                        $experiance->from_date = $exp['from'];
                        $experiance->to_date = $exp['to'];
                        if (isset($exp['to']) && isset($exp['from']))
                            $experiance->total = Carbon::parse($exp['to'])->diffInMonths(Carbon::parse($exp['from']));
                        $experiance->staff_id = $staff->id;
                        $experiance->save();
                    }
                }
            }
            // $log_message = trans('staff.create_log') . '#' . $staff->id;
            // UserActivity::logActivity($log_message);

            DB::table('user_activities')->insert([
                'user_id' => Auth::user()->id,
                'activity_name' => 'Add Staff Details',
            ]);

            DB::commit();
            return redirect()->route($this->index_view, $staff->id)->with('success', 'Staff was updated successfully!!');
        } catch (\Exception $ex) {
           //  dd($this->error_message);
            DB::rollBack();
            dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', 'An error occured');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {


        try {
            $id = $request['id'];
            DB::beginTransaction();
            $staff = staff::findOrFail($id);
            Attachment::where('staff_id', $id)->delete();
            WorkExperianc::where('staff_id', $id)->delete();
            $deleted = staff::where('id', $staff->id)->delete();

            if ($deleted) {
                // $log_message = trans('staff.delete_log') . '#' . $id;
                // UserActivity::logActivity($log_message);
                DB::commit();
                return  redirect()->back()->withSuccess('Record was Deleted successfully!!');
            } else {
                //rollback??
               return  redirect()->back()->with('error','An error occured');

            }
        } catch (\Exception $ex) {
            // dd($this->error_message);
            DB::rollBack();
            dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }
    }

    public function search(Request $request)
    {
        $term = trim($request->q);
        $isAdsSearch = $request->d ? true : false;

        if (empty($term)) {
            return response()->json([]);
        }

        $staff = staff::where('name', 'LIKE', '%' . $term . '%')->where('status', 'active')->limit(5)->get();
        $map = $staff->map(function ($items) {
            $data["id"] = $items->id;
            $data["text"] = $items->name . " - " . $items->restaurant->user->username;
            return $data;
        });

        return response()->json($map->toArray());
    }

    public function add_foodtypes(Request $request)
    {
        try {
            $user = Auth::User()->id;
            $restaurant = Restaurant::where('user_id', $user)->first();
            $last_priority = FoodType::where('restaurant_id', $restaurant->id)->max('priority');
            $foodtype = new FoodType;
            $foodtype->name = $request['name'];
            $foodtype->restaurant_id = $restaurant->id;
            $foodtype->priority = ++$last_priority;
            $foodtype->save();

            return redirect()->back()->withSuccess('تم إضافة الأصناف بنجاح');
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return redirect()->back()->with('error', $this->error_message);
        }
    }


    public function resizeAllImages()
    {
        $restaurnats = Restaurnats::get();
        foreach ($restaurnat as $restaurnat) {

            $info = pathinfo($restaurnat->image_url);
            $sub = $info['basename'];
            // start cloning thumb image(16/9)
            $img = Image::make(public_path('uploads/restaurants/' . $sub));
            if ($img->height() > 200) {
                $img = $img->resize(null, 200);
            }
            if ($img->width() > 200) {
                $img = $img->resize(200, null);
            }
            $hash = time() . md5($img->__toString());
            $target = public_path('uploads/restaurants/' . $hash . '.' . $info['extension']);
            $img->save($target);
            $restaurnat->tmp_image = url('/uploads/restaurnats/' . $hash . '.' . $info['extension']);
            // end thumbnail

        }
        return response()->json("Yes");
    }
}
