<?php

namespace App\Http\Controllers\admin;

use App\models\Order;
use App\models\Restaurant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Fee;
use App\models\Income;
use App\models\Outcome;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{
    public function salesReport(Request $request)
    {

        $filter_name = "today";
        $from_date = Carbon::today();
        $to_date = Carbon::now();
        $status = $request->has('status')?$request["status"]:'received';
        if ($request->has('from_date') && $request->has('to_date'))
        {
            $from_date = Carbon::parse($request->from_date);
            $to_date = Carbon::parse($request->to_date);
            $status = $request["status"];
            $filter_name= "custom";
        }

        if($request->has('filter') && $request->filter)
        {
            $filter_name = $request->filter;
            $report_dates = GetReportDates($filter_name);
            $from_date = $report_dates["from_date"];
            $to_date = $report_dates["to_date"];

        }

$income=Income::whereBetween('created_at',[$from_date,$to_date])->get();
$outcome=Outcome::whereBetween('created_at',[$from_date,$to_date])->get();
$fee=Fee::whereBetween('created_at',[$from_date,$to_date])->get();



     //  dd($orders);
        // if($request->has('export') && $request->export == 'yes')
        //     {

                return getCsvForOrders($income,$outcome,$fee);

        //     }

        // return redirect()->back();
    }


    public function itemsReport(Request $request)
    {


        $filter_name = "today";
        $items = [];

        $from_date = Carbon::today();
        $to_date = Carbon::now();

        if ($request->has('from_date') && $request->has('to_date'))
        {
            $from_date = Carbon::parse($request->from_date);
            $to_date = Carbon::parse($request->to_date);
            $filter_name= "custom";
        }

        if($request->has('filter') && $request->filter)
        {
            $filter_name = $request->filter;
            $report_dates = GetReportDates($filter_name);
            $from_date = $report_dates["from_date"];
            $to_date = $report_dates["to_date"];
        }


        $orders = Order::whereBetween('created_at',[$from_date,$to_date])->where('status','delivered')->get();

        foreach ($orders as $order)
        {
            $order_details = $order->details()->get();
            foreach ($order_details as $detail)
            {
                $detail_price = $detail->paid_price;
                $total_price = $detail->quantity * $detail_price;
                $data = [
                    'client' => $order->user->username ?? '',
                    'item_name' => $detail->product->name ?? '',
                    'paid_price' => $detail->paid_price ?? 0,
                    'quantity' => $detail->quantity ?? '',
                    'total_price' => $total_price,
                    'attributes' => $detail->attribute_details ?? ["color"=>"","size"=>""],
                    'order_date' => $order->created_at ?? '',
                    'delivered_at' => $order->delivered_at ?? ''
                ];

                array_push($items,$data);
            }
        }

        if($request->has('export') && $request->export == 'yes')
            return getCsv($items);

            // dd($items);
        return view('admin.reports.itemsReport',compact(['items','from_date','to_date','filter_name']));
    }
}
