<?php

namespace App\Http\Controllers\admin;
use App\models\Income;
use App\models\Outcome;
use App\models\Fee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function dashboard()
    {


        $daily_income= Income::where('created_at', '<',Carbon::now())->where('created_at', '>',Carbon::now()->submonth())->orderby('created_at','Asc')->get()->groupBy(function($d) {
            return Carbon::parse($d->created_at)->format('d-F-y');
            });
            $daily_income = $daily_income->map(function ($row) {
                return $row->sum('cost');
            });
            $daily_income= Arr::divide($daily_income->toArray());


            $daily_outcome= Outcome::where('created_at', '<',Carbon::now())->where('created_at', '>',Carbon::now()->submonth())->orderby('created_at','Asc')->get()->groupBy(function($d) {
                return Carbon::parse($d->created_at)->format('d-F-y');
                });
                $daily_outcome = $daily_outcome->map(function ($row) {
                    return $row->sum('cost');
                });
                $daily_outcome= Arr::divide($daily_outcome->toArray());


                $weekly_income[0]=Array(Carbon::now()->subDays(28)->format('d-F') .'/'. Carbon::now()->subDays(21)->format('d-F'),
                Carbon::now()->subDays(21)->format('d-F') .'/'. Carbon::now()->subDays(14)->format('d-F'),
                Carbon::now()->subDays(14)->format('d-F') .'/'. Carbon::now()->subDays(7)->format('d-F'),
                Carbon::now()->subDays(7)->format('d-F').'/'.Carbon::now()->format('d-F') );
                
                $weekly_income[1][0]= Income::where('created_at','<',Carbon::now()->subDays(21))->where('created_at','>=',Carbon::now()->subDays(28))->orderby('created_at','Asc')->sum('cost');
                $weekly_income[1][1]= Income::where('created_at','<',Carbon::now()->subDays(14))->where('created_at','>=',Carbon::now()->subDays(21))->orderby('created_at','Asc')->sum('cost');
                $weekly_income[1][2]= Income::where('created_at','<',Carbon::now()->subDays(7))->where('created_at','>=',Carbon::now()->subDays(14))->orderby('created_at','Asc')->sum('cost');
                $weekly_income[1][3]= Income::where('created_at','<',Carbon::now())->where('created_at','>=',Carbon::now()->subDays(7))->orderby('created_at','Asc')->sum('cost');
                

                
                $weekly_outcome[0]=Array(Carbon::now()->subDays(28)->format('d-F') .'/'. Carbon::now()->subDays(21)->format('d-F'),
                Carbon::now()->subDays(21)->format('d-F') .'/'. Carbon::now()->subDays(14)->format('d-F'),
                Carbon::now()->subDays(14)->format('d-F') .'/'. Carbon::now()->subDays(7)->format('d-F'),
                Carbon::now()->subDays(7)->format('d-F').'/'.Carbon::now()->format('d-F') );
                
                $weekly_outcome[1][0]= Outcome::where('created_at','<',Carbon::now()->subDays(21))->where('created_at','>=',Carbon::now()->subDays(28))->orderby('created_at','Asc')->sum('cost');
                $weekly_outcome[1][1]= Outcome::where('created_at','<',Carbon::now()->subDays(14))->where('created_at','>=',Carbon::now()->subDays(21))->orderby('created_at','Asc')->sum('cost');
                $weekly_outcome[1][2]= Outcome::where('created_at','<',Carbon::now()->subDays(7))->where('created_at','>=',Carbon::now()->subDays(14))->orderby('created_at','Asc')->sum('cost');
                $weekly_outcome[1][3]= Outcome::where('created_at','<',Carbon::now())->where('created_at','>=',Carbon::now()->subDays(7))->orderby('created_at','Asc')->sum('cost');
                
                $monthly_income= Income::where('created_at','<', Carbon::now())->where('created_at','>', Carbon::now()->subYear())->orderby('created_at','Asc')->get()->groupBy(function($d) {
                    return Carbon::parse($d->created_at)->format('F-y');
                    });
                    $monthly_income = $monthly_income->map(function ($row) {
                        return $row->sum('cost');
                    });
                    $monthly_income= Arr::divide($monthly_income->toArray());

                    $monthly_outcome= Outcome::where('created_at','<', Carbon::now())->where('created_at','>', Carbon::now()->subYear())->orderby('created_at','Asc')->get()->groupBy(function($d) {
                        return Carbon::parse($d->created_at)->format('F-y');
                        });
                        $monthly_outcome = $monthly_outcome->map(function ($row) {
                            return $row->sum('cost');
                        });
                        $monthly_outcome= Arr::divide($monthly_outcome->toArray());

$fees=Fee::orderby('id','desc')->get();

                return view('admin/dashboard',compact('fees','daily_outcome','daily_income','weekly_income','weekly_outcome','monthly_outcome','monthly_income'));
    

//         $check=false;
//         if(Auth::user()->role!='admin')
//         $check=true; //if restaurnat
//     if($check)
//     {
//         $restaurant=Restaurant::where('user_id',Auth::user()->id)->first();
//     }
//     $discounts=null;
//     $orders=  $check ?$restaurant->orders()->whereDate('created_at', Carbon::today()):Order::whereDate('created_at', Carbon::today());
//     $daily_earning=$orders->where('status','delivered')->sum('total_price');
//     $daily_orders=$orders->count();



//         $delivered_orders = $total_gain = $check ? $restaurant->orders()->where('status','delivered'): Order::where('status','delivered');
//         $total_orders = $check ?$restaurant->orders()->where('status','!=','canceled')->count():Order::where('status','!=','canceled')->count();
//         $total_gain = $delivered_orders->sum('total_price');
//         $completed_percentage = calcPercentage($total_orders,$delivered_orders->count());
//         $total_clients = User::where('role','client')->count();
//         $daily_clients=User::where('role','client')->whereDate('created_at', Carbon::today())->count();
//         $orders = $check ? $restaurant->orders()->orderBy('created_at','desc')->take(10)->get():Order::orderBy('created_at','desc')->take(10)->get();
//         $logs = UserActivity::orderBy('created_at','desc')->take(10)->get();
//      if($check==false)
//         {
//             $notifications= Notification::orderby('created_at','DESC')->get();
//         }
// else {
//     $user_id=Auth::user()->id;
//     $restaurant_id=Restaurant::where('user_id',$user_id)->pluck('id');
//     $restaurant_order=Order::where('restaurant_id',$restaurant_id)->pluck('id');
//     $discounts=Order::where('restaurant_id',$restaurant_id)->where('status','delivered')->sum('use_points');
//     $notifications=Notification::where('type','order')->whereIn('item_id',$restaurant_order)->orderby('created_at','DESC')->get();
// }
        // return view('admin/dashboard',compact(['total_orders','total_gain','completed_percentage','total_clients','orders','logs','notifications','daily_earning','daily_orders','daily_clients','discounts']));
    }
}
