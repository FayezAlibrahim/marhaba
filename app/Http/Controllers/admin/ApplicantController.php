<?php

namespace App\Http\Controllers\admin;

use App\models\Applicant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\JobApplication;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\models\Notification;
class ApplicantController extends Controller
{
    //
public function aplicantbyjob($id)
{
   $job=JobApplication::findorfail($id);
   return view('admin.JobApplications.applicant',compact('job'));
}
    public function cvupload(Request $request)
    {

        try {
            DB::beginTransaction();
            $file =  $request->file('cv');
            // Upload cv
            $destinationPath = 'uploads/attachments';

            $fileName = $file->getClientOriginalName();
            $uploadSuccess = $file->move($destinationPath, time() . $fileName);
            $Src = '/' . $destinationPath . '/' . time() . $fileName;
            $attachment = new Applicant;
            $attachment->job_application_id = $request['id'];
            $attachment->name = $fileName;
            $attachment->path = $Src;
            $attachment->save();

$notification_data = ['user_id'=>null,'title' => "New CV Uploaded." . ' #' . $request['id'], 'type' => 'order',  'is_new' => 'yes', 'notified' => 'no', 'notification_link' => "https://marhaba.ae.org/admin/jobApplications" ];
Notification::create($notification_data);




            DB::commit();

            return redirect()->back()->with('success', 'Staff was added successfully!!');
        } catch (\Exception $ex) {
            // dd($this->error_message);
            DB::rollBack();
            dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }
    }
}
