<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\JobApplication;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class JobApplicationController extends Controller
{
    private function StoreValidationRules()
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'image' => 'required|mimes:jpg,jpeg,png,webp',
        ];
    }
    private function UpdateValidationRules()
    {
        return [
            'id'=>'required|int|exists:job_applications,id',
            'title' => 'required|string',
            'description' => 'required|string',
            'image' => 'nullable|mimes:jpg,jpeg,png,webp',
        ];
    }


public function index()
{
    $JobApplications=JobApplication::all();
    return view('admin.JobApplications.index',compact('JobApplications'));
}
public function create()
{
    return view('admin.JobApplications.create');
}
public function store(Request $request)
{
    $validated_data = $request->validate($this->StoreValidationRules());
    try {
     DB::beginTransaction();
    $JobApplication=new JobApplication;
    $JobApplication->title=$validated_data['title'];
    $JobApplication->description=$validated_data['description'];

    if($request->has('image'))
    {
        $image = $validated_data['image'];
        $img_file_path = Storage::disk('public_images')->put('JobApplications', $image);
        $JobApplication->image = getMediaUrl($img_file_path);
    }
    $JobApplication->save();
    DB::commit();
    return redirect()->route('admin.JobApplications.index')->withSuccess('JobApplication was added successfully!!');
} catch (\Exception $ex) {
    DB::rollBack();
    dd($ex->getMessage());
    Log::error($ex->getMessage());
    return redirect()->back()->with('errors', $ex->getMessage());
}

}
public function edit($id)
{
    $JobApplication=JobApplication::find($id);
    return view('admin.JobApplications.edit',compact('JobApplication'));
}
public function update(Request $request)
{

        $validated_data = $request->validate($this->UpdateValidationRules());
        try {
            DB::beginTransaction();
           $JobApplication=JobApplication::find($validated_data['id']);
           $JobApplication->title=$validated_data['title'];
           $JobApplication->description=$validated_data['description'];
           if($request->has('image'))
           {
               $image = $validated_data['image'];
               $img_file_path = Storage::disk('public_images')->put('posts', $image);
               $JobApplication->image = getMediaUrl($img_file_path);
           }
           $JobApplication->save();
           DB::table('user_activities')->insert([
            'user_id' => Auth::user()->id,
            'activity_name' => 'Add Job Vacancy',
        ]);
           DB::commit();
           return redirect()->route('admin.JobApplications.index')->withSuccess('JobApplication was updated successfully!!');
       } catch (\Exception $ex) {
           DB::rollBack();
           dd($ex->getMessage());
           Log::error($ex->getMessage());
           return redirect()->back()->with('errors', $ex->getMessage());
       }

}

    public function destroy(Request $request)
    {

        $JobApplication=JobApplication::findorfail($request['id']);
        $delted=$JobApplication->delete();
        if($delted)
        {
            return redirect()->back()->withSuccess('JobApplication was deleted successfully!!');
        }
        else return redirect()->back()->with('An error occured');

    }
}
