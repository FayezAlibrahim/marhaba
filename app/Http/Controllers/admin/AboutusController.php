<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Aboutus;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
class AboutusController extends Controller
{
    private function StoreValidationRules()
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'image' => 'nullable|mimes:jpg,jpeg,png,webp',
        ];
    }
    private function UpdateValidationRules()
    {
        return [
            'id'=>'required|int|exists:aboutuses,id',
            'title' => 'required|string',
            'description' => 'nullable|string',
            'image' => 'nullable|mimes:jpg,jpeg,png,webp',
        ];
    }


public function index()
{
    $JobApplications=Aboutus::all();
    return view('admin.Aboutus.index',compact('JobApplications'));
}
public function create()
{
    return view('admin.Aboutus.create');
}
public function store(Request $request)
{
    $validated_data = $request->validate($this->StoreValidationRules());
    try {
     DB::beginTransaction();
    $JobApplication=new Aboutus;
    $JobApplication->title=$validated_data['title'];
    $JobApplication->description=$validated_data['description'];

    if($request->has('image'))
    {
        $image = $validated_data['image'];
        $img_file_path = Storage::disk('public_images')->put('aboutus', $image);
        $JobApplication->image = getMediaUrl($img_file_path);
    }
    $JobApplication->save();
    DB::commit();
    return redirect()->route('admin.Aboutus.index')->withSuccess('about was added successfully!!');
} catch (\Exception $ex) {
    DB::rollBack();
    dd($ex->getMessage());
    Log::error($ex->getMessage());
    return redirect()->back()->with('errors', $ex->getMessage());
}

}
public function edit($id)
{
    $JobApplication=Aboutus::find($id);
    return view('admin.Aboutus.edit',compact('JobApplication'));
}
public function update(Request $request)
{

        $validated_data = $request->validate($this->UpdateValidationRules());
        try {
            DB::beginTransaction();
           $JobApplication=Aboutus::find($validated_data['id']);
           $JobApplication->title=$validated_data['title'];
           $JobApplication->description=$validated_data['description'];
           if($request->has('image'))
           {
               $image = $validated_data['image'];
               $img_file_path = Storage::disk('public_images')->put('posts', $image);
               $JobApplication->image = getMediaUrl($img_file_path);
           }
           $JobApplication->save();

           DB::table('user_activities')->insert([
            'user_id' => Auth::user()->id,
            'activity_name' => 'Update About Us Details',
        ]);


           DB::commit();
           return redirect()->back()->withSuccess('Aboutus  was updated successfully!!');
       } catch (\Exception $ex) {
           DB::rollBack();
           dd($ex->getMessage());
           Log::error($ex->getMessage());
           return redirect()->back()->with('errors', $ex->getMessage());
       }

}

    public function destroy(Request $request)
    {

        $JobApplication=Aboutus::findorfail($request['id']);
        $delted=$JobApplication->delete();
        if($delted)
        {
            return redirect()->back()->withSuccess('JobApplication was deleted successfully!!');
        }
        else return redirect()->back()->with('An error occured');

    }
}
