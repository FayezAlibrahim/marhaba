<?php

namespace App\Http\Controllers\admin;

use App\models\Notification;
use App\models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Restaurant;
use Illuminate\Support\Facades\Auth;


class NotificationController extends Controller
{
    public function getNew()
    {

        if(Auth::check()&& Auth::user()->role=="admin")
       {
        $notifications = Notification::where('notified','no')->where('user_id',null);
    }
else{
    $notifications = Notification::where('notified','no')->where('user_id',Auth::user()->id);
}

        $data = $notifications->get()->toArray();
        return response()->json(['data' => $data,'count' => $notifications->count() == 0 ? '' : $notifications->count()], 200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
    }

public function change_isnew(Request $request)
{
   $notification=Notification::find($request['id']);
   $notification->is_new='no';
   $notification->save();
   return response()->json( ['status'=>'success'],200);


}
    public function count()
    {
        if(Auth::user()->role=="admin")
        {
         $notification_count=Notification::where('is_new','yes')->count();
        }
else {
    $user_id=Auth::user()->id;
    $restaurant_id=Restaurant::where('user_id',$user_id)->pluck('id');
    $restaurant_order=Order::where('restaurant_id',$restaurant_id)->pluck('id');
    $notification_count=Notification::where('type','order')->where('is_new','yes')->whereIn('item_id',$restaurant_order)->count();
}
return $notification_count;

    }

    public function clear()
    {

        if(Auth::check()&& Auth::user()->role=="admin")
        {
         $notifications = Notification::where('notified','no');
     }
 else{
    $orders=Restaurant::where('user_id',Auth::user()->id)->first()->orders()->pluck('id');
     $notifications = Notification::where('notified','no')->whereIn('item_id',$orders);
 }
    $notifications->update(['notified'=>'yes']);

    return response()->json( ['status'=>'success'],200);
    }

}
