<?php

namespace App\Http\Controllers\admin;

use App\models\City;
use App\models\Region;
use App\models\Role;
use App\models\User;
use App\models\Delivery;
use App\models\Restaurant;
use App\models\UserActivity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Appinfo;
use App\models\Fee;

use App\models\Order;
use App\models\UserNotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    private $index_view;
    private $create_view;
    private $edit_view;
    private $show_view;
    private $index_route;
    private $model_instance;
    private $success_message;
    private $error_message;
    private $update_success_message;
    private $update_error_message;

    public function __construct()
    {
        $this->index_view = 'admin.users.index';
        $this->create_view = 'admin.users.create';
        $this->show_view = 'admin.users.show';
        $this->edit_view = 'admin.users.edit';
        $this->index_route = 'admin.users.index';
        $this->success_message = trans('admin.created_successfully');
        $this->update_success_message = trans('admin.update_created_successfully');
        $this->error_message = trans('admin.fail_while_create');
        $this->update_error_message = trans('admin.fail_while_update');
        $this->model_instance = User::class;
    }

    private function StoreValidationRules($request)
    {
        return [
            'username' => 'required|string|min:3|max:200|unique:users,username',
            'email' => 'sometimes|nullable|email|unique:users',
            'address' => 'nullable|string',
            'phone' => Rule::requiredIf($request->role!="admin"),
            'phone'=>$request->role!="admin"?'unique:users,phone|min:10|max:10|regex:/[0-9]/':'nullable',
            'city_id' => 'required|exists:cities,id',
            'password' => 'required|min:4|max:20',
            'active' => 'required|in:yes,no',
            'role'   => 'required|in:client,admin,vendor,delivery',
        ];
    }
    private function updatePassword()
    {
        return [
            'password' => 'required|min:4|max:20'
        ];
    }
    private function UpdateValidationRules($user_id)
    {
        return [
            'username' => 'sometimes|string|min:3|max:200|exists:users,username',
            'phone' => 'sometimes|nullable|unique:users,phone,'.$user_id.'|min:10|max:10|regex:/[0-9]/',
            'city_id' => 'sometimes|exists:cities,id',
            'region_id' => 'nullable',
            'active' => 'sometimes|in:yes,no',
            'points' => 'required',
        ];
    }

    public function updateFee(Request $request)
    {
        $fee=Fee::findOrFail($request['id']);
        $fee->amount=$request['amount'];
        $fee->title=$request['title'];
        $fee->created_at=$request['feeDate'];
        $fee->save();
        return redirect()->back()->with('success', "Fee Added Successfully");

    }
    public function index()
    {

        has_access('users_show');

        $users = $this->model_instance::where('role','client')->get();
        return view($this->index_view, compact(['users']));
    }


    public function create()
    {

        has_access('users_create');
        $cities = City::all();
        $roles = Role::all();
        $regions=Region::all();
        $users=User::where('active','yes')->where('role','restaurant')->pluck('id');
        $restaurants=Restaurant::whereIn('user_id',$users)->get();
        return view($this->create_view,compact(['cities', 'roles','regions','restaurants']));
    }

    public function addFee(Request $request)
    {
        $fee=new Fee;
        $fee->amount=$request['amount'];
        $fee->title=$request['title'];
        $fee->created_at=$request['feeDate'];
        $fee->save();
        return redirect()->back()->with('success', "Fee Added Successfully");

    }

    public function contactus(Request $request)
    {
        DB::table('requests')->insert([
            'name' => isset($request['name'])?$request['name']:null,
            'phone' => isset($request['phone'])?$request['phone']:null,
            'email' => isset($request['email'])?$request['email']:null,
            'title' => isset($request['title'])?$request['title']:null,
            'message' => isset($request['message'])?$request['message']:null,
        ]);
        return redirect()->back()->with('success', "Added Successfully");

    }

    public function showRequests()
    {
       $requests= DB::table('requests')->get();
        return view('admin.requests.index', compact(['requests']));

    }




    public function deleteFee(Request $request)
    {
        $fee=Fee::findOrFail($request['id']);
   
        $deleted = $fee->delete();

        if ($deleted) {
            DB::table('user_activities')->insert([
                'user_id' => Auth::user()->id,
                'activity_name' => 'Delete General Fee',
            ]);
            return response()->json(['status' => 'success', 'message' => 'success']);
        }
    }
    public function update_password(Request $request)
    {
        $validated_data = $request->validate($this->updatePassword());

        Auth::user()->password=Hash::make($validated_data["password"]);
        Auth::user()->save();
        return redirect()->back()->with('success', "Updated Successfully");

    }
    public function store(Request $request)
    {
        has_access('users_create');
        $request['active']="yes";
        $validated_data = $request->validate($this->StoreValidationRules($request));
        $validated_data["password"] = Hash::make($validated_data["password"]);

        try
        {
            $object = $this->model_instance::create($validated_data);
            $object->status=1;
            $object->save();
            if($request->role=="delivery"){
            $request['rest_id']=$request['delivery_type']=='general'?null:$request['rest_id'];
            $Delivery=new Delivery;
            $Delivery->user_id=$object->id;
            $Delivery->type=$request['delivery_type'];
            $Delivery->restaurant_id=$request['rest_id'];
            $Delivery->save();
            }
            else if($request->role=="admin"){
                DB::table('role_user')->insert(['role_id' => 12, 'user_id' => $object->id]);
        }
            $log_message = trans('users.create_log') . '#' . $object->id;
            UserActivity::logActivity($log_message);
            return redirect()->route($this->index_route)->with('success', $this->success_message);
        }
        catch (\Exception $ex)
        {
            dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }


    }


    public function show($id)
    {

        has_access('users_show');

        $user = $this->model_instance::findOrFail($id);
        return view($this->show_view, compact('user'));
    }


    public function edit($id)
    {

        has_access('users_update');

        $user = $this->model_instance::findOrFail($id);
        $cities = City::all();
        $roles = Role::all();
        $regions=Region::all();
        $user_roles = $user->roles()->pluck('roles.id')->toArray();
        return view($this->edit_view, compact(['user','cities','roles', 'user_roles','regions']));
    }


    public function update(Request $request, $id)
    {

        has_access('users_update');

        $validated_data = $request->validate($this->UpdateValidationRules($id));

        try {
            DB::beginTransaction();
            $updated_instance = $this->model_instance::find($id);
            $updated_instance->roles()->sync(request()->roles);
            $updated_instance->update($validated_data);
            if($request->has('password') && !empty($request->password))
            {
                $user = User::find($id);
                $user->password = Hash::make($request->password);
                $user->update();

            }
            DB::commit();
            if ($updated_instance) {
                $log_message = trans('users.update_log') . '#' . $updated_instance->id;
                UserActivity::logActivity($log_message);
                return redirect()->route($this->index_route)->with('success', $this->update_success_message);
            } else {
                return redirect()->route($this->index_route)->with('error', $this->update_error_message);
            }
        } catch (\Exception $ex) {
            DB::rollBack();
            //dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }
    }




    public function changeStatus(Request $request) {
        $user = $this->model_instance::find($request->id);
        $status = $user->active;
        $new_status = $status == 'yes' ? 'no' : 'yes';
        $user->active = $new_status;
        $user->update();

        return response()->json(['status' => 'success'],200);

    }

    public function log_index()
    {
        $activity_logs = UserActivity::all();
        return view('admin.users.log_index',compact('activity_logs'));
    }

    public function user_log($id)
    {
        $activity_logs = UserActivity::where('user_id',$id)->get();

        return view('admin.users.log_index',compact('activity_logs'));
    }

    public function count()
    {
        $user_count=User::where('role','client')
                 ->whereDate('created_at', '>', Carbon::now()->subDays(5))
                 ->count();
                 return $user_count;
    }


public function spiner(Request $request)
{


    $orders=Order::where('status','received')->whereBetween('created_at', [$request['from'],$request['to']]);

    if(!in_array("all", $request['restaurants']))
    {
        $orders->whereIn('restaurant_id',$request['restaurants']);
    }
    $orders=$orders->get();

    return view('admin.restaurants.spiner',compact('orders'));
}
public function spinerwinner(Request $request)
{

if($request['money_value']!=null){
$user=User::find($request['user_id']);
$user->points+=$request['money_value']/Appinfo::first()->points_value;
$user->save();
$notification=new UserNotification();
$notification->title='مبروك';
$notification->type="competition";
$notification->body='تم إضافة '.$request['money_value']/Appinfo::first()->points_value.'إلى نقاطك.';
$notification->user_id=$user->id;
$notification->save();

$notification = [
    'title' => 'مبروك',
    'body' => 'تم إضافة '.$request['money_value']/Appinfo::first()->points_value.'إلى نقاطك.',
    'type'=>'competition',
    'resId'=>null,
    'product_id'=>null,
    'food_type_id'=>null,
    'priority' => 'high',
    'contentAvailable'=>true,
    'click_action'=> 'FLUTTER_NOTIFICATION_CLICK'
];
SendNotifications($notification,[$user->app_token]);

return response()->json(['status' => 'success'],200);
}
return response()->json(['status' => 'error'],403);
}


}
