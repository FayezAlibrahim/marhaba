<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Appinfo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\models\UserActivity;
use Illuminate\Support\Facades\Auth;

class AppinfoController extends Controller
{
    //

    private $index_view='admin.app_info.index';

    private function StoreValidationRules()
    {
        return [
        'about_us'=>'required|string',
        'call_center_number'=>'required',
        'phone'=>'required',
        'email_info'=>'required',
        'email_contact'=>'nullable',
        'id'=>'nullable',
        'facebook'=>'nullable',
        'instagram'=>'nullable',
        'twitter'=>'nullable',
'address'=>'nullable'
        ];

    }

    public function index()
    {
        $info=Appinfo::first();
      return view($this->index_view,compact('info'));
    }
    public function update(Request $request)
    {

        $validated_data = $request->validate($this->StoreValidationRules());

try{
    DB::beginTransaction();

        if($validated_data['id']!=null)
        {
           $info= Appinfo::find($validated_data['id']);
           $info->about_us=$validated_data['about_us'];
           $info->call_center_number=$validated_data['call_center_number'];
           $info->phone=$validated_data['phone'];
           $info->twitter=$validated_data['twitter'];
           $info->facebook=$validated_data['facebook'];
           $info->instagram=$validated_data['instagram'];
           $info->email_contact=$validated_data['email_contact'];
           $info->email_info=$validated_data['email_info'];
           $info->address=$validated_data['address'];

           $info->save();
        }
        else {
    $info=Appinfo::Create([
    'call_center_number'=>$validated_data['call_center_number'],
    'about_us'=>$validated_data['about_us'],
    'email_info'=>$validated_data['email_info'],

    ]);
 }

 DB::table('user_activities')->insert([
    'user_id' => Auth::user()->id,
    'activity_name' => 'Update App Info Details',
]);

 DB::commit();
 return redirect()->route('admin.appinfo.index')->withSuccess('success');

    }
    catch (\Exception $ex) {
        DB::rollBack();
       dd($ex->getMessage());
        Log::error($ex->getMessage());
        return redirect()->route($this->index_route)->with('error', $ex->getMessage());
    }

    }

    public function quickaccess()
    {
       $quick_access=Appinfo::all()->first()->quick_access;
       return $quick_access;
    }

}
