<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Category extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'name' => $this->name,
            'status' => $this->status,
            'image_url' => $this->image_url,
            'products' => new ProductCollection($this->products()->latest()->get()),
          //  'products' => $this->products()->latest()->get()->toArray(),

        ];
    }
}
