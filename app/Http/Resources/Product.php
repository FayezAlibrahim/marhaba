<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\models\ProductVariationAttribute;
use App\models\UserFavoriteProduct;
use App\models\Appinfo;
use App\models\User;

class Product extends JsonResource
{
    public function toArray($request)
    {
        $user_id=isset($request['user_id']) ? $request['user_id'] : 32;
        $objectArray = parent::toArray($request);
        $info = Appinfo::where('id', 31)->first();
        $env = $info->environment;
        if (isset($env) && $env == "test")
        {
            if (User::find($user_id)->environment == "test")
            {
                $currency=trans('orders.IQD');
            }
            else {
                $currency=trans('orders.SYP');
            }
        }
        else
        {
            $currency=trans('orders.SYP');
        }
        $objectArray["currency"] = $currency;
        $objectArray["unit_price"] = formatNumber($objectArray["unit_price"]);
        $additionalData = [ 
            'is_favorite'=>UserFavoriteProduct::where('user_id',$user_id)->where('product_id',$this->id)->count()>=1?'yes':'no',
            'product_images' => $this->get_images()->toArray(),
            'product_extras' => $this->extra()->get()->toArray(),
            'sizes'=> $this->sizes()->get()->toArray(),
        ];
        return array_merge($objectArray,$additionalData);
    }
}
