<?php

namespace App\Http\Resources;
use App\models\Restaurant;
use Illuminate\Http\Resources\Json\JsonResource;
use \Carbon\Carbon;

class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $chk=$request->chk?true:false;
        $short_details=$request->short_details?true:false;
        $p=parent::select('id','total_price','status')->get();
        $objectArray =  $p[0]->toArray($request);

        if($short_details ==true){
            return [
                'id'=>$this->id,
                '#id'=>"#".$this->id,
                'restaurant_name' => Restaurant::where('id',$this->restaurant_id)->first()->user->username,
                'delivery_address' => $this->delivery_address,
                'restaurant_address' => Restaurant::where('id',$this->restaurant_id)->first()->user->address,
                'restaurant_lat' => Restaurant::where('id',$this->restaurant_id)->first()->user->lat,
                'restaurant_lon' => Restaurant::where('id',$this->restaurant_id)->first()->user->lon,
                'customer_lat'=>$this->user->lat,
                'customer_lon'=>$this->user->lon,
                "total_price"=>intval(str_replace(",","",$this->total_price)),
                "delivery_cost"=>unserialize($this->expected_delivery_time)?unserialize($this->expected_delivery_time):["cost"=>null,"expected_delivery_time"=>null],
            ];
        }
       
        if(!$chk){
            return [
                'id'=>$this->id,
                '#id'=>"#".$this->id,
                'type'=>$this->type,
                'restaurant' => Restaurant::where('id',$this->restaurant_id)->first()->user->username,
                'restaurant_address' => Restaurant::where('id',$this->restaurant_id)->first()->user->address,
                'delivery_address' => $this->delivery_address,
                "total_price"=>intval(str_replace(",","",$this->total_price)),
                'translated_status' => trans('orders.'.$this->status),
                'date'=>$this->created_at?Carbon::parse($this->created_at)->format('Y-m-d'):"",
                'time'=>$this->created_at?$this->created_at->format('g:i A'):"",
                'allowed_cancel' => $this->is_cancel_allowed(),
                'has_rate'=>$this->hasMany('App\models\OrderRate')->count()>0?true:false,
                'rate'=>$this->hasMany('App\models\OrderRate')->count()>0?$this->hasMany('App\models\OrderRate')->first()->rate:null,
            ];
                    }


 
        return [
            'id'=>$this->id,
            '#id'=>"#".$this->id,
            'type'=>$this->type,
            'translated_status' => trans('orders.'.$this->status),
            'restaurant_name' => Restaurant::where('id',$this->restaurant_id)->first()->user->username,
            'delivery_address' => $this->delivery_address,
            'restaurant_address' => Restaurant::where('id',$this->restaurant_id)->first()->user->address,
            'restaurant_lat' => Restaurant::where('id',$this->restaurant_id)->first()->user->lat,
            'restaurant_lon' => Restaurant::where('id',$this->restaurant_id)->first()->user->lon,
            'customer_lat'=>$this->user->lat,
            'customer_lon'=>$this->user->lon,
            'customer_phone'=>$this->user->phone,
            'date'=>$this->created_at?Carbon::parse($this->created_at)->format('Y-m-d'):"",
            'time'=>$this->created_at?$this->created_at->format('g:i A'):"",
            "total_price"=>intval(str_replace(",","",$this->total_price)),
            "tax"=>$this->total_tax,
            "delivery_cost"=>unserialize($this->expected_delivery_time)?unserialize($this->expected_delivery_time):["cost"=>null,"expected_delivery_time"=>null],
            'products' =>$this->get_products_short_details(), 
        ];
    }
}
