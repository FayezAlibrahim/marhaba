<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SubCategory extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $per_page = $request->has('per_page') ? $request->per_page  : 10;
        return [
            'id' => $this->id,
            'name' => $this->name,
            'status' => $this->status,
            'image_url' => $this->image_url,
          //  'sub_categories' => new SubCategoryCollection($this->subCategories()->latest()->get()),
            //  'products' => $this->products()->latest()->get()->toArray(),

        ];
    }
}
