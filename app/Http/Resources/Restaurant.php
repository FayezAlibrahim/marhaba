<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class Restaurant extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $objectArray =  parent::toArray($request);
        $chk=$this->chk;
        $user_id=isset($request['user_id']) ? $request['user_id'] : $this->u_id;
        $user_id=isset($user_id) ? $user_id : 32;
            $additionalData = [
            'id' => $this->id,
            'is_favorite'=>in_array($this->id,$this->user->fav_ids($user_id)), //BUG
            'name'=>$this->user->username,
            'lat'=>$this->user->lat,
            'lon'=>$this->user->lon,
            'address'=>$this->user->address,
            'foodtypes'=>$chk?$this->foodtypes_products($user_id)->toArray():[],
            'categories' =>$this->categories()->get()->toArray(),
            'avg_rating'=>$this->avg_rate(),
            'is_open_now' =>$this->checkIfOpen(),
            'gallery' =>$chk?DB::table('restaurant_media')->select('id', 'media_url','media_url_16_9','media_url_4_3')->where('restaurant_id',$this->id)->get():[],
        ];

        return array_merge($objectArray,$additionalData);
    }

}
