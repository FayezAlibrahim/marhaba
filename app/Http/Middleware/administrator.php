<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class administrator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if((Auth::user()->role == "admin")||(Auth::user()->role == "restaurant") && Auth::user()->active == "yes")
            return $next($request);

        abort(403);
    }
}
