<?php

namespace App\Providers;

use App\models\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Blade::if('ifCanDo', function ($permissions) {
        //     if(Auth::check())
        //         return Auth::user()->canDo($permissions);
        //     return false;

        // });

       $notifications = Notification::where('is_new','yes')->orderBy('created_at','desc')->orderBy('notified','asc')->take(15)->get();
       $alerts = $notifications->filter(function($alert){
           return $alert->is_new == "yes";
       });

        view()->share(['notifications' => $notifications,'alerts' => $alerts->count()]);
    }
}
