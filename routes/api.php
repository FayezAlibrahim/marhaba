<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'api'], function () {
    Route::post('auth/login','UserController@login');
    Route::post('auth/register','UserController@register');
    Route::post('auth/code/verify','UserController@code_verify');
    Route::post('auth/code/resend','UserController@code_resend');
    Route::post('auth/password/forget','UserController@forget_password');
    Route::post('auth/password/new','UserController@new_password');
    Route::post('/restaurant/delete','RestaurantController@destroy');
    Route::get('cities','CityController@index');
    Route::get('cities/{id}/area','CityController@regions');
    Route::get('Allregions','CityController@Allregions');
    Route::post('users/{id}','UserController@uploadProfile');
    Route::post('user/inquiry','UserController@inquiry');
    Route::get('user/{id}/inquiries','UserController@myInquiries');
    Route::get('user/new/answered/{id}','UserController@newAnsweredInquiries');
    Route::get('users/{id}/orders','UserController@get_orders');
    Route::get('ads/{id}','AdsController@index');
    Route::get('/categories','CategoryController@index');
    Route::post('/attribute/remove', 'AttributeController@destroy');
    Route::get('/allCategories','CategoryController@allCategories');
    Route::get('/categories/{id}/restaurants','CategoryController@getCategoryrestaurants');
    Route::get('/categories/{id}','CategoryController@show');
    Route::get('/products','ProductController@index')->name('api.products.all');
    Route::get('/products/{id}','ProductController@show')->name('api.products.show');
    Route::get('/products/offers/all','ProductController@offers')->name('api.products.offers');
    Route::post('/products/variation','ProductController@get_product_variation')->name('api.products.variation');
    Route::post('/items/search','ProductController@search');
    Route::get('/items/search','ProductController@search');

    Route::post('/orders','OrderController@store');
    Route::post('/orders/cancel','OrderController@cancel');
    Route::post('/complaints','ComplaintController@store');
    Route::post('users/auth/token','UserController@setToken');
    Route::get('users/{id}/wallet','UserController@get_user_wallet');
    Route::post('user/basket/addProduct','UserController@addProductToBasket');
    Route::get('user/{id}/basket/empty','UserController@emptyBasket');
    Route::get('user/{id}/basket','UserController@getUserBasketProducts');
    Route::post('/address/store','AddressController@store');
    Route::post('/order/rateupdate','OrderController@updaterate');

    Route::get('/users/new','UserController@newusers');
    Route::get('/restaurant/new/{all}','RestaurantController@newest_restaurants');
    Route::get('/restaurant/rate/{all}','RestaurantController@best_restaurants');
    Route::get('/products_top/{all}','RestaurantController@most_ordered_products');
    Route::get('/restaurant/new','RestaurantController@newest_restaurants');
    Route::get('/restaurant/rate','RestaurantController@best_restaurants');
    Route::get('/products_top','RestaurantController@most_ordered_products');
    Route::post('/restaurants','RestaurantController@all_restaurants');
    Route::get('/restaurants/favourite/{id}','RestaurantController@favourite_restaurants');
    Route::post('/favourite/add/{user_id}/{rest_id}','RestaurantController@add_to_favourite');
    Route::get('/favourite/remove/{user_id}/{rest_id}','RestaurantController@remove_from_favourite');
    Route::get('/user/orders/history/{id}','UserController@users_orders');
    Route::get('/order/details/{id}','UserController@order_details');
    Route::get('/restaurants/products/{ft_id}','RestaurantController@restaurnat_products');
    Route::post('/order/rate','OrderRateController@store');
    Route::get('resizeAllImages','RestaurantController@resizeAllImages');
    Route::get('/restaurant/details/{rest_id}/{user_id}','RestaurantController@getRestInfo');
    Route::get('/user/notification/{id}','UserNotificationController@user_notification');
    Route::post('/user/favoraite/product/{user_id}/{product_id}','UserFavoriteProductController@store');
    Route::post('/user/favoraite/product/destroy/{user_id}/{product_id}','UserFavoriteProductController@delete');
    Route::get('/user/favoraite/products/{user_id}','UserFavoriteProductController@getusersfavorite');
    Route::get('calcCost/{user_id}/{rest_id}','UserController@test');
    Route::post('/users/delete/user','UserController@destroy');
    Route::get('app/info/','AppinfoController@index');
    Route::post('/restaurant/update_status/{status}/{rest_id}','RestaurantController@update_status');
    Route::post('/check_order_cost','OrderController@checkOrderCost');
    Route::post('/paypalpage','PaymentController@paypalpage');
    Route::post('/create-paypal-transaction-test', 'PaymentController@createPaymenttest');
Route::post('/confirm-paypal-transaction-test', 'PaymentController@confirmPaymenttest');
Route::get('/success','PaymentController@success');
Route::get('/user_info/{user_id}/','UserController@user_info');
Route::get('/delivery/orders/history/{id}','DeliveryController@orders_history');
Route::post('/delivery/orders/accept_reject','DeliveryController@accept_reject_order');

Route::get('/delivery/info/{id}','DeliveryController@delivery_info');
Route::get('/delivery/current/order/{id}','DeliveryController@current_order');

Route::get('/delivery/orders/{id}','DeliveryController@index');

Route::post('/delivery/status/change','DeliveryController@change_status');
Route::post('/delivery/status_change/order','DeliveryController@change_ordre_status');




});


