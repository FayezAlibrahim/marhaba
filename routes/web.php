<?php
use Intervention\Image\ImageManagerStatic as Image;
use App\models\Restaurant;
use App\models\Staff;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

Route::get('/dd',function (){
    $img = Image::make(public_path('/uploads/dd.jpg'))->resize(1200, 900)->insert(public_path('/uploads/q1.png'),'top-right',50,50);
    // $img = Image::make(public_path('/uploads/dd.jpg'))->resize(400, null);
    return $img->response('jpg');
});
Route::get('/login', 'Auth\LoginController@loginForm');
Route::get('/','ClientController@home')->name('home');
Route::get('/staff/all','ClientController@allstaff');
Route::get('/staff/profile/{id}','ClientController@profilestaff');
Route::get('/job/all','ClientController@alljob');
Route::post('/cv/upload','admin\ApplicantController@cvupload');
Route::post('/contact/us','admin\UserController@contactus');
Route::get('/setlocale/{locale}',function($locale){

    Session::put('locale',$locale);

return redirect()->back();
});
Auth::routes();
Route::group( [ 'prefix' => 'admin',  'middleware' => ['auth','administrator'] , 'namespace' => 'admin' , 'as' => 'admin.'], function()
{
    Route::post('fee/delete','UserController@deleteFee');
    Route::post('/update_fee','UserController@updateFee');
    Route::get('/services','ServiceController@index')->name('services.index');
    Route::get('/aboutus/show','UserController@showRequests');
    Route::get('/services/create','ServiceController@create');
    Route::post('/services/store','ServiceController@store')->name('services.store');;
    Route::get('/services/edit/{id}','ServiceController@edit');
    Route::post('/services/update','ServiceController@update')->name('services.update');
    Route::post('/services/destroy','ServiceController@destroy');

    Route::get('/team','TeamController@index')->name('team.index');;
    Route::get('/team/create','TeamController@create');
    Route::post('/team/store','TeamController@store')->name('team.store');;
    Route::get('/team/edit/{id}','TeamController@edit');
    Route::post('/team/update','TeamController@update')->name('teams.update');
    Route::post('/team/destroy','TeamController@destroy');

    Route::get('/jobApplications','JobApplicationController@index')->name('JobApplications.index');
    Route::get('/jobApplications/create','JobApplicationController@create');
    Route::post('/jobApplications/store','JobApplicationController@store')->name('JobApplications.store');
    Route::get('/jobApplications/edit/{id}','JobApplicationController@edit');
    Route::post('/aboutus/update','AboutusController@update')->name('aboutus.update');
    Route::get('/aboutus/edit/{id}','AboutusController@edit');

Route::get('/jobApplications/applicant/{id}','ApplicantController@aplicantbyjob');
    Route::post('/jobApplications/update','JobApplicationController@update')->name('JobApplications.update');
    Route::post('/jobApplications/destroy','JobApplicationController@destroy');

    Route::get('/staff','StaffController@index')->name('staff.index');
    Route::get('/staff/create','StaffController@create');
    Route::post('/staff/store','StaffController@store')->name('staff.store');
    Route::get('/staff/edit/{id}','StaffController@edit');
    Route::post('/staff/update/{id}','StaffController@update')->name('staff.update');
    Route::post('/staff/destroy','StaffController@destroy')->name('staff.destroy');
    Route::get('/staff/show/{id}','StaffController@show');


    Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
    Route::get('/restaurants', 'RestaurantController@index')->name('restaurants');
    Route::get('/restaurants/create', 'RestaurantController@create')->name('restaurants.create');
    Route::post('/restaurants/store', 'RestaurantController@store')->name('restaurants.store');
    Route::resource('/categories', 'CategoryController');
    Route::get('/category/search', 'CategoryController@search')->name('categories.search');
    Route::get('/delivery/list', 'DeliveryController@index')->name('delivery.index');
Route::get('/delivery/edit/{id}','DeliveryController@edit')->name('delivery.edit');
Route::post('/delivery/update/{id}','DeliveryController@update')->name('delivery.update');
    Route::resource('/orders', 'OrderController');
    Route::post('/order/status','OrderController@changeStatus')->name('orders.status');
    Route::resource('/users', 'UserController');
    Route::resource('/roles', 'RoleController');
    Route::resource('/products', 'ProductController');
    Route::get('/products/variation/{id}/edit', 'ProductController@editVariation')->name('products.variation.edit');
    Route::put('/products/variation/{id}/update', 'ProductController@updateVariation')->name('products.variation.update');
    Route::get('/product/search', 'ProductController@search')->name('products.search');
    Route::resource('/cities', 'CityController');
    Route::resource('/inquiries', 'InquiryController');
    Route::get('/inquiries/user/{id}', 'InquiryController@userInquiries')->name('userInquires');
    Route::get('/start_chat/{id}', 'InquiryController@start_chat');
    Route::resource('/productTypes', 'ProductTypeController');
    Route::resource('/attributes', 'AttributeController');
    Route::post('/attribute/save', 'AttributeController@save')->name('attributes.save');
    Route::get('/attribute/trans/{id}', 'AttributeController@attribute_trans')->name('attributes.trans');
    Route::post('/attribute/translate', 'AttributeController@attribute_trans_values')->name('attributes.value_trans');
    Route::resource('/ads', 'AdsController');
    Route::resource('/complaints', 'ComplaintController');
    Route::post('/complaint/status', 'ComplaintController@changeStatus')->name('complaints.status');
    Route::post('/users/status', 'UserController@changeStatus')->name('users.status');
    Route::get('/users/logs/index', 'UserController@log_index')->name('logs.index');
    Route::get('/users/{id}/logs', 'UserController@user_log')->name('users.logs.show');
    Route::get('/notifications','NotificationController@getNew')->name('notifications.new');
    Route::resource('/alerts','AlertController');
    Route::post('/alert/send','AlertController@send')->name('alerts.send');
    Route::get('/reports/sales','ReportController@salesReport')->name('reports.sales');
    Route::post('/reports/custom','ReportController@salesReport');
    Route::get('/reports/items','ReportController@itemsReport')->name('reports.items');
    Route::resource('/regions', 'RegionsController');
    Route::get('/city/{id}/regions', 'CityController@GetCityRegions');
    Route::get('/address/{id}','AddressController@GetUsersAddress')->name('users.address');
    Route::get('/restaurants/edit/{id}','RestaurantController@edit')->name('restaurants.edit');
    Route::post('/restaurants/update','RestaurantController@update')->name('restaurants.update');
    Route::get('/products/edit/{id}','ProductController@edit');
    Route::post('/products/update/{id}','ProductController@update');
    Route::post('/products/delete','ProductController@destroy');
    Route::post('/products/addfoodtypes','ProductController@add_foodtypes');
    Route::get('/appinfo','AppinfoController@index')->name('appinfo.index');
    Route::post('/appinfo/update','AppinfoController@update')->name('appinfo.update');
    Route::get('/foodtypes','FoodTypeController@index');
    Route::post('/foodtypes/destroy/{id}','FoodTypeController@destroy');
    Route::get('/categories/delete/{id}','CategoryController@destroy');
    Route::post('/address/store','AddressController@store');
    Route::get('/pennding/orders','OrderController@penndingorders');
    Route::post('/order/edit/{id}','OrderController@updateorder')->name('orderupdate');

    Route::get('/notifications/count','NotificationController@count');
    Route::get('/users_count/count','UserController@count');
    Route::post('/product_delete/delete','ProductController@destroy');
    Route::get('/qucickaccess','AppinfoController@quickaccess');
    Route::post('/spiner/winner','UserController@spinerwinner');
    Route::post('/spiner','UserController@spiner');
    Route::post('/update_password','UserController@update_password');

    Route::post('/foodtype/proiority','FoodTypeController@changepriority');

    Route::get('/notifications/clear','NotificationController@clear');
    Route::post('/notifications/change_isnew','NotificationController@change_isnew');
    Route::post('/add_fee','UserController@addFee');
    Route::post('/update_fee','UserController@updateFee');


});

Route::get('/privacy-policy',function (){return view('privacy-policy');});
Route::get('/test',function (){    return view('admin.reports.salesReport');});

Route::get('/resize',function (){
    $restaurnats=Restaurant::get();
       foreach($restaurnats as $restaurnat){
               $info=pathinfo($restaurnat->image_url);
               $sub=$info['basename'];
           $img=Image::make(public_path('uploads/restaurants/'.$sub));
           if($img->height() > 200){
               $img = $img->resize(null, 200);
           }
           if($img->width() > 200){
               $img = $img->resize(200, null);
           }
           $hash = time().md5($img->__toString());
           $target = public_path('uploads/restaurants/'.$hash.'.'.$info['extension']);
           $img->save($target);
           $restaurnat->tmp_image=url('/uploads/restaurants/'.$hash.'.'.$info['extension']);
           $restaurnat->save();
           // end thumbnail
       }
       return response()->json("Yes");
    });


