<?php


return [
    'Errors_En' => [
        'TYPE000' => 'This type already exist!',
        'TYPE001' => 'Type field is not filled!',
        'TYPE002' => 'Type does not exist!',

        'MODEL000' => 'This model already exist!',
        'MODEL001' => 'Model field is not filled!',

        'PRD000' => 'order cant be canceled!',
        'PRD001' => 'Invalid year!',


        'GENERAL000' => 'General error!',
        'GENERAL001' => 'Multiple errors!',

        'AUTH000' => 'Invalid key!',
        'AUTH001' => 'Not authenticated!',
        'AUTH002' => 'Not authorized!',
        'AUTH003' => 'Products limit exceeded!',
        'AUTH004' => 'Account is not Active!',

        'USER000' => 'User not found!',
        'USER001' => 'User Products count is larger than this limit!',
        'USER002' => 'Invalid username and/or password!',
        'USER003' => 'Phone already exists!',
        'USER004' => 'Email already exists!',
        'USER005' => 'Old password field does not exist!',
        'USER006' => 'New password field does not exist!',
        'USER007' => 'Old password is not correct!',
        'USER008' => 'Username/Password format is not Correct ',
        'USER009' => 'This Product is already in your favorites list!',
        'USER010' => 'This Product is not in your favorites list!',

    ]
];